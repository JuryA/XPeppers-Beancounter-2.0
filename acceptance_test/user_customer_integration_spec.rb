require 'rspec'
require 'logger'
require 'rest_client'
require 'json'
require 'mechanize'
require './lib/login_helper'

RSpec.configure do |c|
  c.include LoginHelper
end

describe "beancounter integration" do
  let(:any_customer) { "tca" }
  let(:any_api_key)  { "4994d6ee-9566-4190-8b38-232ff7709ba9" }
  let(:facebook_user_id)  { "100006475729715" }
  let(:twitter_user_id)   { "1670021802" }

  after :all do
    #TODO Now the user doesn't delete in customer set
    #RestClient.delete "http://beancounter.xpeppers.com/beancounter-platform/rest/user/100006475729715?apikey=4994d6ee-9566-4190-8b38-232ff7709ba9"
  end

  context "facebook small loop" do
    context "mobile login" do
      it "first step witout customer" do
        response = RestClient.get "http://#{LoginHelper::SERVER}/beancounter-platform/rest/user/register/facebook/mobile"
        response.code.should == 200
        response.headers[:set_cookie][3].should include("http%25253A%25252F%25252F#{LoginHelper::SERVER}%25252Frest%25252Fuser%25252Foauth%25252Fatomic%25252Fcallback%25252Ffacebook")
      end

      it "first step with customer" do
        response = RestClient.get "http://#{LoginHelper::SERVER}/beancounter-platform/rest/user/register/facebook/mobile?customer=#{any_customer}"
        response.code.should == 200
        response.headers[:set_cookie][3].should include("http%25253A%25252F%25252F#{LoginHelper::SERVER}%25252Frest%25252Fuser%25252Fcustomer%25252F#{any_customer}%25252Foauth%25252Fatomic%25252Fcallback%25252Ffacebook")
      end

      it "second step without customer" do
        response = login_flow_for_mobile_facebook
        response.content.should include('"status":"OK"')
      end

      it "second step with customer" do
        response = login_flow_for_mobile_facebook(any_customer)
        response.content.should include('"status":"OK"')
      end
    end

    context "web login" do
      it "first step witout customer" do
        response = RestClient.get "http://#{LoginHelper::SERVER}/beancounter-platform/rest/user/register/facebook/web?redirect=http://localhost"
        response.code.should == 200
        response.headers[:set_cookie][3].should include("http%25253A%25252F%25252F#{LoginHelper::SERVER}%25252Frest%25252Fuser%25252Foauth%25252Fatomic%25252Fcallback%25252Ffacebook%25252Fweb")
      end

      it "first step with customer" do
        response = RestClient.get "http://#{LoginHelper::SERVER}/beancounter-platform/rest/user/register/facebook/web?redirect=http://localhost&customer=#{any_customer}"
        response.code.should == 200
        response.headers[:set_cookie][3].should include("http%25253A%25252F%25252F#{LoginHelper::SERVER}%25252Frest%25252Fuser%25252Fcustomer%25252F#{any_customer}%25252Foauth%25252Fatomic%25252Fcallback%25252Ffacebook%25252Fweb")
      end

      it "second step without customer" do
        response = login_flow_for_web_facebook
        response.content.should include('Signed in as Patricia Carrierosky')
      end

      it "second step with customer" do
        response = login_flow_for_web_facebook(any_customer)
        response.content.should include('Signed in as Patricia Carrierosky')
      end
    end
  end

  context "twitter small loop" do
    context "mobile login" do
      it "first step witout customer" do
        response = RestClient.get "http://#{LoginHelper::SERVER}/beancounter-platform/rest/user/register/twitter/mobile"
        response.code.should == 200
        response.should include("Vuoi autorizzare l'accesso di Beancounter.io al tuo account")
      end

      it "first step with customer" do
        response = RestClient.get "http://#{LoginHelper::SERVER}/beancounter-platform/rest/user/register/twitter/mobile?customer=#{any_customer}"
        response.code.should == 200
        response.should include("Vuoi autorizzare l'accesso di Beancounter.io al tuo account")
      end

      it "second step without customer" do
        response = login_flow_for_mobile_twitter
        response.content.should include('"status":"OK"')
      end

      it "second step with customer" do
        response = login_flow_for_mobile_twitter(any_customer)
        response.content.should include('"status":"OK"')
      end
    end

    context "web login" do
      it "first step witout customer" do
        response = RestClient.get "http://#{LoginHelper::SERVER}/beancounter-platform/rest/user/register/twitter/web?redirect=http://localhost"
        response.code.should == 200
        response.should include("Vuoi autorizzare l'accesso di Beancounter.io al tuo account")
      end

      it "first step with customer" do
        response = RestClient.get "http://#{LoginHelper::SERVER}/beancounter-platform/rest/user/register/twitter/web?redirect=http://localhost&customer=#{any_customer}"
        response.code.should == 200
        response.should include("Vuoi autorizzare l'accesso di Beancounter.io al tuo account")
      end

      it "second step without customer" do
        response = login_flow_for_web_twitter
        response.content.should include('Signed in as Sara Cinesca')
      end

      it "second step with customer" do
        response = login_flow_for_web_twitter(any_customer)
        response.content.should include('Signed in as Sara Cinesca')
      end
    end
  end

  context "list for all customer users" do
    it "returns list of all users for TCA customer" do
      response = RestClient.get "http://#{LoginHelper::SERVER}/beancounter-platform/rest/user/all?apikey=#{any_api_key}"
      response.code.should == 200
      json_users_list = JSON.parse(response.body)["object"]

      usernames = json_users_list.map{ |json_user| json_user.fetch("username") }
      usernames.should include(facebook_user_id)
      usernames.should include(twitter_user_id)
    end
  end

end
