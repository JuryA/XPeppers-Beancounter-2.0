require 'rspec'
require 'logger'
require 'rest_client'
require 'json'
require 'mechanize'
require 'cgi'
require './lib/login_helper'

RSpec.configure do |c|
  c.include LoginHelper
end

describe "user email integration" do
  let(:any_email) { "any@email.com" }

  before :each do
    login_response = login_flow_for_web_twitter
    query_params = CGI::parse(login_response.uri.query)
    @username = query_params["username"].first
    @token = query_params["token"].first
  end

  it "put user email api should return 200" do
    put_email_response = RestClient.put "http://#{LoginHelper::SERVER}/beancounter-platform/rest/user/#{@username}/property?token=#{@token}", "email=#{any_email}"
    put_email_response.code.should == 200
  end

  it "should store user email property" do
    put_email_response = RestClient.put "http://#{LoginHelper::SERVER}/beancounter-platform/rest/user/#{@username}/property?token=#{@token}", "email=#{any_email}"
    user_response = RestClient.get "http://#{LoginHelper::SERVER}/beancounter-platform/rest/user/#{@username}/me?token=#{@token}"
    json_user = JSON.parse(user_response.body)["object"]
    json_user["email"].should eq(any_email)
  end

  it "without email should generate 500 response" do
    begin
      RestClient.put "http://#{LoginHelper::SERVER}/beancounter-platform/rest/user/#{@username}/property?token=#{@token}", ""
      fail_with("Expected 500 internal server error")
    rescue => e
      e.message.should eq("500 Internal Server Error")
    end
  end
end
