module LoginHelper
  SERVER = "beancounter.xpeppers.com"

  def login_flow_for_mobile_facebook(customer=nil)
    a = Mechanize.new { |agent|
      agent.follow_meta_refresh = true
    }

    customer_path = customer.nil? ? '' : "customer/#{customer}/"
    a.get("https://www.facebook.com/dialog/oauth?client_id=478821725538809&redirect_uri=http://#{SERVER}/rest/user/#{customer_path}oauth/atomic/callback/facebook/") do |page|
      new_page = page.form_with(:method => "POST", :id => 'login_form') do |f|
        f.email  = 'hizuphx_carrierosky_1377095310@tfbnw.net'
        f.pass   = 'xpeppersabc123'
      end.submit

      return new_page
    end
  end

  def login_flow_for_web_facebook(customer=nil)
    a = Mechanize.new { |agent|
      agent.follow_meta_refresh = true
    }

    customer_path = customer.nil? ? '' : "customer/#{customer}/"
    redirect_uri = "aHR0cDovL2JlYW5jb3VudGVyLnhwZXBwZXJzLmNvbTo4MDgwL2F1dGg"
    a.get("https://www.facebook.com/dialog/oauth?client_id=478821725538809&redirect_uri=http://#{SERVER}/rest/user/#{customer_path}oauth/atomic/callback/facebook/web/#{redirect_uri}") do |page|
      new_page = page.form_with(:method => "POST", :id => 'login_form') do |f|
        f.email  = 'hizuphx_carrierosky_1377095310@tfbnw.net'
        f.pass   = 'xpeppersabc123'
      end.submit

      return new_page
    end
  end

  def login_flow_for_mobile_twitter(customer=nil)
    a = Mechanize.new { |agent|
        agent.follow_meta_refresh = true
    }

    customer_query = customer.nil? ? '' : "?customer=#{customer}"
    a.get("http://#{SERVER}/beancounter-platform/rest/user/register/twitter/mobile#{customer_query}") do |page|
      new_page = page.form_with(:id => 'login_form') do |f|
        f.field_with(:name => "session[username_or_email]").value  = 'saracinesca2013'
        f.field_with(:name => "session[password]").value           = 'qweqwe123123'
      end.submit

      return new_page
    end
  end

  def login_flow_for_web_twitter(customer=nil)
    a = Mechanize.new { |agent|
        agent.follow_meta_refresh = true
    }

    customer_query = customer.nil? ? '' : "&customer=#{customer}"
    a.get("http://#{SERVER}/beancounter-platform/rest/user/register/twitter/web?redirect=http://beancounter.xpeppers.com:8080/auth#{customer_query}") do |page|
      new_page = page.form_with(:id => 'login_form') do |f|
        f.field_with(:name => "session[username_or_email]").value  = 'saracinesca2013'
        f.field_with(:name => "session[password]").value           = 'qweqwe123123'
      end.submit

      return new_page
    end
  end

end
