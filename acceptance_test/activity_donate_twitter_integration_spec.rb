require 'rspec'
require 'logger'
require 'rest_client'
require 'json'
require 'mechanize'
require 'cgi'
require './lib/login_helper'

RSpec.configure do |c|
  c.include LoginHelper
end

describe "activity donate integration" do
  let(:username) { "100006472513596" } #Ludovico Mistico
  let(:bc_api_key) { "7c0af1c4-f411-41a3-90b0-b7ea4097c7a3" } #Xpeppers
  let(:any_activity) { "{\"object\": {\"type\": \"DONATION-ITEM\",\"url\": \"http://www.any-url321321.com\",\"picture\":\"http://www.inter.it/html/splash/splash-cielo2.jpg\",\"name\": null,\"caption\":\"Leevia promuove l'iniziativa 'Il 5xMille ad Atlantide'\",\"description\":\"WeTipp donera' 0.20 euro ad Atlantide Onlus per ogni persona che condividera' l'iniziativa!\", \"message\": \"siete agili?\",\"link\": \"http://donate.leevia.com/\"},\"context\": {\"date\": null,\"service\": \"twitter\",\"mood\": null},\"verb\": \"DONATE\"}" }

  before :each do
    @token = "40d47df5-b7b4-42cb-9b3a-7826b356d6a3" #json_user["userToken"]
  end

  it "adding donate activity should post activity to twitter" do
    add_activity_response = RestClient.post "http://#{LoginHelper::SERVER}/beancounter-platform/rest/activities/add/#{username}?token=#{@token}", "activity=#{any_activity}"
  end
end
