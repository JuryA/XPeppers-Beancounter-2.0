require 'rspec'
require 'logger'
require 'rest_client'
require 'json'
require 'mechanize'
require 'cgi'
require './lib/login_helper'

RSpec.configure do |c|
  c.include LoginHelper
end

describe "activity upload photo integration" do
  let(:username) { "100006472513596" } #Ludovico Mistico
  let(:bc_api_key) { "7c0af1c4-f411-41a3-90b0-b7ea4097c7a3" } #Xpeppers
  let(:any_activity) { "{\"object\": {\"type\":\"UPLOAD-PHOTO-ITEM\",\"url\": \"http://www.any-url321321.com\",\"picture\":\"http://www.inter.it/html/splash/splash-cielo2.jpg\",\"name\":\"test-name\",\"message\": \"Ho realizzato questa barchetta!\"},\"context\": {\"date\": null,\"service\": \"facebook\",\"mood\": null},\"verb\": \"UPLOAD\"}" }

  before :each do
    response = RestClient.get "http://#{LoginHelper::SERVER}/beancounter-platform/rest/user/#{username}?apikey=#{bc_api_key}"

    json_user = JSON.parse(response)["object"]
    json_facebook = json_user["services"]["facebook"]
    @access_token = json_facebook["session"]
    @token = "40d47df5-b7b4-42cb-9b3a-7826b356d6a3" #json_user["userToken"]
  end

  it "adding update photo activity should post photo activity to facebook" do
    add_activity_response = RestClient.post "http://#{LoginHelper::SERVER}/beancounter-platform/rest/activities/add/#{username}?token=#{@token}", "activity=#{any_activity}"
    facebook_last_feed_response = RestClient.get "https://graph.facebook.com/#{username}/feed?limit=1&access_token=#{@access_token}"
    json_data = JSON.parse(facebook_last_feed_response.body)["data"]
    json_data[0]["link"].should eq("http://www.any-url321321.com/")
    json_data[0]["name"].should eq("Ho realizzato questa barchetta!")
  end

end
