package io.beancounter.activities;

import io.beancounter.commons.helper.jackson.ObjectMapperFactory;
import io.beancounter.commons.model.activity.Activity;
import io.beancounter.commons.model.activity.tca.Link;
import io.beancounter.commons.model.activity.uhopper.MallPlace;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * put class description here
 *
 * @author Davide Palmisano ( dpalmisano@gmail.com )
 */
public class MarshalAndUnmarshalTestCase {

    private static final String JSON = "{\"id\":\"891aed77-7f8b-4a28-991f-34683a281ead\",\"verb\":\"TWEET\",\"object\":{\"type\":\"TWEET\",\"url\":\"http://twitter.com/ElliottWilson/status/220164023340118017\",\"name\":\"ElliottWilson\",\"description\":null,\"text\":\"RT @RapRadar3: RAPRADAR: New Mixtape: Theophilus London Rose Island Vol. 1 http://t.co/BynRjPJm\",\"hashTags\":[],\"urls\":[\"http://bit.ly/P5Tzc1\"]},\"context\":{\"date\":1341326168000,\"service\":\"http://sally.beancounter.io\",\"mood\":null}}";

    @Test
    public void testUnmarshal() throws IOException {
        ObjectMapper objectMapper = ObjectMapperFactory.createMapper();
        Activity activity = objectMapper.readValue(JSON, Activity.class);
        Assert.assertNotNull(activity);
    }

    @Test
    public void testMarshalUnmarshalMallPlaceObject() throws IOException {
        MallPlace mallPlace = new MallPlace("mall-id", "sensor-id", 32343, 4321);
        ObjectMapper objectMapper = ObjectMapperFactory.createMapper();
        String mallPlaceJson = objectMapper.writeValueAsString(mallPlace);
        MallPlace expected = objectMapper.readValue(mallPlaceJson, MallPlace.class);
        Assert.assertEquals(mallPlace, expected);
    }
    
    @Test
    public void testMarshalUnmarshalLinkObject() throws IOException {
        Link link = new Link("www.any.link.com");
        ObjectMapper objectMapper = ObjectMapperFactory.createMapper();
        String linkJson = objectMapper.writeValueAsString(link);
        Link expected = objectMapper.readValue(linkJson, Link.class);
        Assert.assertEquals(link, expected);
    }

}
