package io.beancounter.publisher.facebook.adapters;

import io.beancounter.commons.model.activity.Verb;
import io.beancounter.commons.model.activity.leevia.PhotoUpload;

import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.types.FacebookType;

public class PhotoUploadPublisher  implements Publisher<PhotoUpload> {

	@Override
	public FacebookType publishActivity(String token, Verb verb, PhotoUpload photoUpload) {
    FacebookClient client = new DefaultFacebookClient(token);
    return client.publish(
            "me/feed",
            FacebookType.class,
            Parameter.with("name", photoUpload.getMessage()),
            Parameter.with("picture", photoUpload.getPicture().toString()),
            Parameter.with("link", photoUpload.getUrl().toString())
    );
	}

}
