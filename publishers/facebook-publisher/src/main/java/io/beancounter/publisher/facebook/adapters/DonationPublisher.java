package io.beancounter.publisher.facebook.adapters;

import io.beancounter.commons.model.activity.Verb;
import io.beancounter.commons.model.activity.leevia.Donation;

import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.types.FacebookType;

public class DonationPublisher implements Publisher<Donation> {

	public FacebookType publishActivity(String token, Verb verb, Donation donation) {
        FacebookClient client = new DefaultFacebookClient(token);
        return client.publish(
                "me/feed",
                FacebookType.class,
                Parameter.with("name", donation.getMessage()),
                Parameter.with("picture", donation.getPicture().toString()),
                Parameter.with("description", donation.getDescription()),
                Parameter.with("caption", donation.getCaption()),
                Parameter.with("link", donation.getLink().toString())
        );
    }
}
