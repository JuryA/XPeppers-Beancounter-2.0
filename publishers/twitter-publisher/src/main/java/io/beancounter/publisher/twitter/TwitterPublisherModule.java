package io.beancounter.publisher.twitter;

import io.beancounter.auth.DefaultAuthServiceManager;
import io.beancounter.commons.helper.PropertiesHelper;
import io.beancounter.commons.model.AuthServiceConfig;

import java.util.Properties;

import org.apache.camel.component.properties.PropertiesComponent;
import org.apache.camel.guice.CamelModuleWithMatchingRoutes;
import org.apache.camel.guice.jndi.JndiBind;

import twitter4j.Twitter;
import twitter4j.TwitterFactory;

import com.google.inject.Provides;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

/**
 *
 * @author Enrico Candino ( enrico.candino@gmail.com )
 */
public class TwitterPublisherModule extends CamelModuleWithMatchingRoutes {

    @Override
    protected void configure() {
        super.configure();

        Properties properties = PropertiesHelper.readFromClasspath("/beancounter.properties");
        AuthServiceConfig service = DefaultAuthServiceManager.buildService("twitter", properties);

        Configuration conf = new ConfigurationBuilder().setUseSSL(true).build();
        Twitter twitter = new TwitterFactory(conf).getInstance();
        twitter.setOAuthConsumer(service.getApikey(), service.getSecret());
        bind(Twitter.class).toInstance(twitter);

        bind(TwitterPublisher.class);
        bind(TwitterPublisherRoute.class);
    }

    @Provides
    @JndiBind("properties")
    PropertiesComponent propertiesComponent() {
        PropertiesComponent pc = new PropertiesComponent();
        pc.setLocation("classpath:beancounter.properties");
        return pc;
    }
}
