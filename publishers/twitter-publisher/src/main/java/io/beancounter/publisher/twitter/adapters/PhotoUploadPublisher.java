package io.beancounter.publisher.twitter.adapters;

import io.beancounter.commons.model.activity.Verb;
import io.beancounter.commons.model.activity.leevia.PhotoUpload;
import io.beancounter.publisher.twitter.TwitterPublisherException;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import twitter4j.Status;
import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;

public class PhotoUploadPublisher implements Publisher<PhotoUpload> {

    private static final Logger LOG = LoggerFactory.getLogger(PhotoUploadPublisher.class);

    @Override
    public Status publish(Twitter twitter, Verb verb, PhotoUpload photoUpload) throws TwitterPublisherException {
        Status status;
        StatusUpdate statusUpdate;
        String message = getMessage(photoUpload);

        try {
            statusUpdate = new StatusUpdate(message);
            InputStream body = getInputStream(photoUpload);
            statusUpdate.setMedia("barchetta", body);

            status = twitter.updateStatus(statusUpdate);
        } catch (TwitterException e) {
            final String errMessage = "Error while updating the status to [" + message + "]";
            LOG.error(errMessage);
            throw new TwitterPublisherException(errMessage, e);
        } catch (IOException e) {
            final String errMessage = "Error while updating the status to [" + message + "]" + "with picture [" + photoUpload.getPicture().toString() + "]";
            LOG.error(errMessage);
            throw new TwitterPublisherException(errMessage, e);
		}
        return status;
    }

    private InputStream getInputStream(PhotoUpload photoUpload) throws IOException {
        URL imageUrl = photoUpload.getPicture();

        return imageUrl.openStream();
	}

	private String getMessage(PhotoUpload photoUpload) {
        return "" + Trimmer.trim(photoUpload.getMessage(), photoUpload.getUrl(), 3) + " - " + photoUpload.getUrl().toString();
    }
}
