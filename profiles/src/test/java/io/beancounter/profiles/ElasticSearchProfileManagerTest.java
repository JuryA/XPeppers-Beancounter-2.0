package io.beancounter.profiles;

import io.beancounter.commons.helper.Clock;
import io.beancounter.commons.helper.PropertiesHelper;
import io.beancounter.commons.helper.es.ElasticSearchConfiguration;
import io.beancounter.commons.model.Category;
import io.beancounter.commons.model.Interest;
import io.beancounter.commons.model.User;
import io.beancounter.commons.model.UserProfile;
import io.beancounter.commons.model.auth.OAuthAuth;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.admin.cluster.health.ClusterHealthRequest;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequest;
import org.elasticsearch.action.admin.indices.refresh.RefreshRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.node.Node;
import org.elasticsearch.node.NodeBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.testng.annotations.*;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;

/**
 * Created by Trink0 on 07/04/14.
 */
public class ElasticSearchProfileManagerTest {
    private Node node;
    private Client client;
    private final String esDirectory = "es";
    private static String INDEX_NAME = "beancounter";
    private static final String ELASTICSEARCH_CONF = "/es.properties";
    private ElasticSearchProfileManager profileManager;
    private Clock clock;

    @BeforeSuite
    public void beforeSuite() throws Exception {
        node = NodeBuilder.nodeBuilder()
                .settings(ImmutableSettings.settingsBuilder()
                        .put("path.home", esDirectory)
                )
                .node();
        client = node.client();

        try {
            // The default index will be created with 5 shards with 1 replica
            // per shard.
            client.admin().indices().create(new CreateIndexRequest(INDEX_NAME)).actionGet();
        } catch (ElasticsearchException indexAlreadyExists) {
            clearIndex();
        }

        // Wait for shards to settle in the idle state before continuing.
        client.admin().cluster().health(new ClusterHealthRequest(INDEX_NAME)
                .waitForYellowStatus()).actionGet();
        clock = new Clock();
    }

    @AfterSuite
    public void afterSuite() throws Exception {
        node.close();
        delete(new File(esDirectory));
    }

    @BeforeTest
    public void setUp() throws Exception {
        Properties properties = PropertiesHelper.readFromClasspath(ELASTICSEARCH_CONF);
        ElasticSearchConfiguration configuration = ElasticSearchConfiguration.build(properties);
        profileManager = new ElasticSearchProfileManager(configuration);
    }

    @AfterTest
    public void tearDown() throws Exception {
        profileManager.shutDown();
        profileManager = null;
    }
    @BeforeMethod
    public void beforeMethod() throws Exception {
        clearIndex();
    }

    private void refreshIndex() {
        // Refresh so we're looking at the latest version of the index.
        client.admin().indices().refresh(new RefreshRequest(INDEX_NAME)).actionGet();
    }

    private void clearIndex() throws Exception {
        client.prepareDeleteByQuery(INDEX_NAME)
                .setQuery(QueryBuilders.matchAllQuery())
                .execute().actionGet();

        // Wait for shards to settle (return to idle state).
        refreshIndex();
        client.admin().cluster().health(new ClusterHealthRequest(INDEX_NAME).waitForYellowStatus()).actionGet();
    }

    private User getUser() {
        User user = new User();
        user.setId(UUID.randomUUID());
        user.setName("test-name");
        user.setSurname("test-surname");
        user.setPassword("test-pwd");
        user.setUsername("test-username");
        user.addService("test-service", new OAuthAuth("s", "c"));
        return user;
    }
    private void delete(File file) throws IOException {
        if (!file.exists()) {
            return;
        }

        if (file.isDirectory()) {
            for (File f : file.listFiles()) {
                delete(f);
            }
        }

        if (!file.delete()) {
            throw new IOException("Unable to delete file " + file + ".");
        }
    }
    private UserProfile defaultUserProfile(User user) throws URISyntaxException {
        if (user == null)
            user = getUser();

        UserProfile profile = new UserProfile(user.getId());
        profile.setUsername(user.getUsername());
        profile.setVisibility(UserProfile.Visibility.PUBLIC);
        profile.setLastUpdated(clock.utcnow());

        profile.setInterests(new HashSet<Interest>(Arrays.asList(
                new Interest(new URI("/int1"), "Int1", 0.5, true)
        )));
        profile.setCategories(new HashSet<Category>(Arrays.asList(
                new Category(new URI("/cat1"), "Cat1", 0.5, 1)
        )));

        return profile;
    }

    @Test
    public void storeProfileShouldBePersited() throws ProfileManagerException, URISyntaxException {
        User user = getUser();
        UserProfile profile = defaultUserProfile(user);

        profileManager.store(profile, user);

        refreshIndex();

        SearchResponse searchResponse = client.prepareSearch(INDEX_NAME)
                .setQuery(QueryBuilders.matchAllQuery())
                .execute().actionGet();

        SearchHits hits = searchResponse.getHits();
        assertEquals(hits.getTotalHits(), 1);
    }
    @Test
    public void storeProfileShouldBePersitedWithCorrectValues() throws ProfileManagerException, URISyntaxException {
        User user = getUser();
        UserProfile profile = defaultUserProfile(user);

        profileManager.store(profile, user);

        refreshIndex();

        SearchResponse searchResponse = client.prepareSearch(INDEX_NAME)
                .setQuery(QueryBuilders.matchAllQuery())
                .execute().actionGet();

        SearchHit hit = searchResponse.getHits().getAt(0);
        assertEquals(hit.getType(), "profile");

        Map<String, Object> source = hit.getSource();
        assertEquals(source.get("username"), "test-username");
        List interests = (ArrayList) source.get("interests");
        assertEquals(interests.size(), 1);
        Map<String, Object> interest = (Map<String, Object>) interests.get(0);
        assertEquals(interest.get("weight"), 0.5);
    }
    @Test
    public void storeUpdateProfileShouldNotCreateaNewObject() throws ProfileManagerException, URISyntaxException {
        User user = getUser();
        UserProfile profile = defaultUserProfile(user);

        profileManager.store(profile, user);
        refreshIndex();
        profile.setInterests(new HashSet<Interest>(Arrays.asList(
                new Interest(new URI("/int1"), "Int1", 0.75, true)
        )));
        profileManager.store(profile, user);
        refreshIndex();

        SearchResponse searchResponse = client.prepareSearch(INDEX_NAME)
                .setQuery(QueryBuilders.matchAllQuery())
                .execute().actionGet();


        SearchHits hits = searchResponse.getHits();
        assertEquals(hits.getTotalHits(), 1);
        assertEquals(hits.getAt(0).getId(), user.getId().toString());
        List interests = (ArrayList) hits.getAt(0).getSource().get("interests");
        assertEquals(interests.size(), 1);
        Map<String, Object> interest = (Map<String, Object>) interests.get(0);
        assertEquals(interest.get("weight"), 0.75);
    }
    @Test
    public void lookupStoredProfileShouldRetrieveElement() throws ProfileManagerException, URISyntaxException {
        User user = getUser();
        UserProfile profile = defaultUserProfile(user);

        profileManager.store(profile, user);
        refreshIndex();

        UserProfile storedProfile = profileManager.lookup(user.getId());
        assertEquals(storedProfile, profile);
    }
    @Test
    public void lookupNotExistingProfileShouldReturnNull() throws ProfileManagerException, URISyntaxException {
        User user = getUser();
        UserProfile profile = defaultUserProfile(user);

        profileManager.store(profile, user);
        refreshIndex();

        UserProfile missingProfile = profileManager.lookup(UUID.randomUUID());
        assertNull(missingProfile);
    }
}
