package io.beancounter.profiles;

import io.beancounter.commons.model.User;
import io.beancounter.commons.model.UserProfile;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.UUID;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

/**
 * Created by Trink0 on 07/04/14.
 */
public class UnifiedProfileManagerTest {
    private UnifiedProfileManager unifiedProfileManager;
    private ProfileManager es;
    private ProfileManager jedis;
    @BeforeTest
    public void setUp(){
        es = mock(ProfileManager.class);
        jedis = mock(ProfileManager.class);
        unifiedProfileManager = new UnifiedProfileManager(es, jedis);
    }
    @Test
    public void storeShouldCallBothManager() throws ProfileManagerException {
        UUID userId = UUID.randomUUID();
        UserProfile profile = new UserProfile(userId);
        User user = new User(userId);
        unifiedProfileManager.store(profile, user);
        verify(es).store(profile, user);
        verify(jedis).store(profile, user);
    }
    @Test
    public void lookupShouldCallOnlyJedisManager() throws ProfileManagerException {
        UUID userId = UUID.randomUUID();
        unifiedProfileManager.lookup(userId);
        verify(jedis).lookup(userId);
        verifyZeroInteractions(es);
    }
    @Test
    public void lookupShortProfileShouldCallOnlyJedisManager() throws ProfileManagerException {
        UUID userId = UUID.randomUUID();
        unifiedProfileManager.lookupShortProfile(userId);
        verify(jedis).lookupShortProfile(userId);
        verifyZeroInteractions(es);
    }
}
