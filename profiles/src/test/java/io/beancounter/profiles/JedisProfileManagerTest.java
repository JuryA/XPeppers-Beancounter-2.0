package io.beancounter.profiles;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import com.google.inject.Binder;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.google.inject.name.Names;
import io.beancounter.commons.helper.Clock;
import io.beancounter.commons.helper.IdGenerator;
import io.beancounter.commons.helper.jackson.ObjectMapperFactory;
import io.beancounter.commons.helper.jedis.JedisPoolFactory;
import io.beancounter.commons.model.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

import io.beancounter.commons.model.auth.Auth;
import io.beancounter.commons.model.auth.OAuthAuth;
import org.mockito.InOrder;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * @author Enrico Candino ( enrico.candino@gmail.com )
 */
public class JedisProfileManagerTest {

    private ObjectMapper mapper;
    private IdGenerator idGenerator;
    private Clock clock;

    private Jedis jedis;
    private JedisPool jedisPool;
    private ProfileManager profileManager;


    @BeforeClass
    public void setUp() {
        mapper = ObjectMapperFactory.createMapper();
        idGenerator = new IdGenerator();
        clock = new Clock();

        jedis = mock(Jedis.class);
        jedisPool = mock(JedisPool.class);
        when(jedisPool.getResource()).thenReturn(jedis);

        final JedisPoolFactory poolFactory = new JedisPoolFactory() {
            @Override
            public JedisPool build() {
                return jedisPool;
            }
        };

        Injector injector = Guice.createInjector(new Module() {
            @Override
            public void configure(Binder binder) {
                Properties properties = new Properties();
                properties.put("redis.db.profiles", "1");
                properties.put("redis.db.shortProfiles", "2");
                Names.bindProperties(binder, properties);

                binder.bind(JedisPoolFactory.class).toInstance(poolFactory);
                binder.bind(ProfileManager.class).to(JedisProfileManager.class);
            }
        });

        profileManager = injector.getInstance(ProfileManager.class);
    }

    @Test
    public void testStoreBothProfiles() throws Exception {
        User user = defaultUser();
        UserProfile profile = defaultUserProfile(user);
        ShortUserProfile shortProfile = new ShortUserProfile(profile, user);

        String userId = user.getId().toString();
        String jsonProfile = mapper.writeValueAsString(profile);
        String jsonShortProfile = mapper.writeValueAsString(shortProfile);

        profileManager.store(profile, user);

        InOrder ordered = inOrder(jedis, jedisPool);
        ordered.verify(jedis).select(1);
        ordered.verify(jedis).set(userId, jsonProfile);
        ordered.verify(jedis).select(2);
        ordered.verify(jedis).set(userId, jsonShortProfile);
        ordered.verify(jedisPool).returnResource(jedis);
    }

    @Test
    public void testLookupUserProfile() throws Exception {
        UserProfile profile = defaultUserProfile(null);
        UUID userId = profile.getUserId();
        String jsonProfile = mapper.writeValueAsString(profile);

        when(jedis.get(userId.toString())).thenReturn(jsonProfile);
        UserProfile actual = profileManager.lookup(userId);

        InOrder ordered = inOrder(jedis, jedisPool);
        ordered.verify(jedis).select(1);
        ordered.verify(jedis).get(userId.toString());
        ordered.verify(jedisPool).returnResource(jedis);

        assertEquals(actual.getUserId(), profile.getUserId());
        assertEquals(actual.getUsername(), profile.getUsername());
        assertEquals(actual.getVisibility(), profile.getVisibility());
        assertEquals(actual.getLastUpdated(), profile.getLastUpdated());
        assertEquals(actual.getInterests(), profile.getInterests());
        assertEquals(actual.getCategories(), profile.getCategories());
    }

    @Test
    public void testLookupShortProfile() throws Exception {
        User user = defaultUser();
        UserProfile profile = defaultUserProfile(user);
        ShortUserProfile shortProfile = new ShortUserProfile(profile, user);
        UUID userId = profile.getUserId();
        String jsonShortProfile = mapper.writeValueAsString(shortProfile);

        when(jedis.get(userId.toString())).thenReturn(jsonShortProfile);
        ShortUserProfile actual = profileManager.lookupShortProfile(userId);

        InOrder ordered = inOrder(jedis, jedisPool);
        ordered.verify(jedis).select(2);
        ordered.verify(jedis).get(userId.toString());
        ordered.verify(jedisPool).returnResource(jedis);

        assertEquals(actual.getUserId(), shortProfile.getUserId());
        assertEquals(actual.getVisibility(), shortProfile.getVisibility());
        assertEquals(actual.getCustomer(), shortProfile.getCustomer());
        assertEquals(actual.getUpdated(), shortProfile.getUpdated());
        assertEquals(actual.getAge(), shortProfile.getAge());
        assertEquals(actual.getGender(), shortProfile.getGender());
        assertEquals(actual.getLocation(), shortProfile.getLocation());
        assertEquals(actual.getTopics(), shortProfile.getTopics());
    }

    private User defaultUser() {
        Map<String, String> meta = new HashMap<String, String>();
        meta.put("facebook.user.gender", "Other");
        meta.put("facebook.user.locationname", "Trento, Italy");
        meta.put("facebook.user.birthday", "01/01/1980");

        Map<String, Auth> services = new HashMap<String, Auth>();
        services.put("facebook", new OAuthAuth("session", "secret"));

        User user = new User("John", "Doe", "dude", "secret");
        user.setId(idGenerator.randomUUID(User.class));
        user.setMetadata(meta);
        user.setCustomer("xpeppers");
        user.setEmail("dude@gmail.com");
        user.setUserToken(idGenerator.randomUUID("token"));
        user.setServices(services);

        return user;
    }

    private UserProfile defaultUserProfile(User user) throws URISyntaxException {
        if (user == null)
            user = defaultUser();

        UserProfile profile = new UserProfile(user.getId());
        profile.setUsername(user.getUsername());
        profile.setVisibility(UserProfile.Visibility.PROTECTED);
        profile.setLastUpdated(clock.utcnow());

        profile.setInterests(new HashSet<Interest>(Arrays.asList(
            new Interest(new URI("/int1"), "Int1", 0.5, true)
        )));
        profile.setCategories(new HashSet<Category>(Arrays.asList(
            new Category(new URI("/cat1"), "Cat1", 0.5, 1)
        )));

        return profile;
    }
}