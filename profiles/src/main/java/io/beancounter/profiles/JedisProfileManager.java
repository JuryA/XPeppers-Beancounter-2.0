package io.beancounter.profiles;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.beancounter.commons.helper.jackson.ObjectMapperFactory;
import io.beancounter.commons.helper.jedis.JedisPoolFactory;
import io.beancounter.commons.model.ShortUserProfile;
import io.beancounter.commons.model.User;
import io.beancounter.commons.model.UserProfile;

import java.io.IOException;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.exceptions.JedisConnectionException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * Redis-based implementation.
 *
 * @author Enrico Candino ( enrico.candino@gmail.com )
 * @author alex@cloudware.it
 */
public class JedisProfileManager implements ProfileManager {

    private static Logger LOGGER = LoggerFactory.getLogger(JedisProfileManager.class);

    private JedisPool pool;

    private ObjectMapper mapper;

    @Inject
    @Named("redis.db.profiles") private int profilesDb;

    @Inject
    @Named("redis.db.shortProfiles") private int shortProfilesDb;

    @Inject
    public JedisProfileManager(JedisPoolFactory factory) {
        pool = factory.build();
        mapper = ObjectMapperFactory.createMapper();
    }

    @Override
    public void store(UserProfile profile, User user) throws ProfileManagerException {
        String userId = user.getId().toString();
        LOGGER.debug("Storing profile for user: {}", userId);
        ShortUserProfile shortProfile = new ShortUserProfile(profile, user);
        String jsonProfile;
        String jsonShortProfile;
        try {
            jsonProfile = mapper.writeValueAsString(profile);
            jsonShortProfile = mapper.writeValueAsString(shortProfile);
        } catch (JsonProcessingException e) {
            LOGGER.error(e.getMessage(), e);
            throw new ProfileManagerException("Serialization error", e);
        }

        Jedis jedis = getJedisResource();
        if (jedis == null)
            throw new ProfileManagerException("Database error");

        boolean broken = false;
        try {
            // Fingers crossed: Redis does not have a cross-database
            // transactions. It might happen so that first set() succeeds
            // while the second one does not.
            jedis.select(profilesDb);
            jedis.set(userId, jsonProfile);
            jedis.select(shortProfilesDb);
            jedis.set(userId, jsonShortProfile);

        } catch (JedisConnectionException e) {
            LOGGER.error(e.getMessage(), e);
            broken = true;
            throw new ProfileManagerException("Database connection issue");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new ProfileManagerException("Unable to store user profile");
        } finally {
            if (broken)
                pool.returnBrokenResource(jedis);
            else
                pool.returnResource(jedis);
        }
    }

    @Override
    public UserProfile lookup(UUID userId) throws ProfileManagerException {
        return internalLookup(profilesDb, userId, UserProfile.class);
    }

    @Override
    public ShortUserProfile lookupShortProfile(UUID userId) throws ProfileManagerException {
        return internalLookup(shortProfilesDb, userId, ShortUserProfile.class);
    }

    protected <T> T internalLookup(int db, UUID key, Class<T> type)
            throws ProfileManagerException
    {
        String uid = key.toString();
        LOGGER.debug("Looking up {} for key: {}", type, key);

        Jedis jedis = getJedisResource(db);
        if (jedis == null)
            throw new ProfileManagerException("Database error");

        String json = null;
        boolean broken = false;
        try {
            json = jedis.get(uid);
        } catch (JedisConnectionException e) {
            LOGGER.error(e.getMessage(), e);
            broken = true;
            throw new ProfileManagerException("Database connection issue", e);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new ProfileManagerException("Unable to retrieve data", e);
        } finally {
            if (broken)
                pool.returnBrokenResource(jedis);
            else
                pool.returnResource(jedis);
        }

        if (json == null)
            return null;

        T obj;
        try {
            obj = mapper.readValue(json, type);
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
            throw new ProfileManagerException("Deserialization error", e);
        }

        return obj;
    }

    /**
     * Same as {@link #getJedisResource(int)} but does not pre-selects
     * database number.
     *
     * The caller is responsible to {@code jedis.select(db)}.
     */
    private Jedis getJedisResource() {
        return getJedisResource(-1);
    }

    /**
     * Get a redis connection from the pool.
     *
     * @param db Redis database number or -1 to disable db pre-selection.
     *
     * @return Null if no resource is available, connection errors or any other
     * issues.
     */
    private Jedis getJedisResource(int db) {
        Jedis jedis = null;

        try {
            jedis = pool.getResource();
            if (db >= 0)
                jedis.select(db);

        } catch (JedisConnectionException e) {
            LOGGER.error(e.getMessage(), e);
            if (jedis != null) {
                pool.returnBrokenResource(jedis);
                jedis = null;
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            if (jedis != null) {
                pool.returnResource(jedis);
                jedis = null;
            }
        }

        return jedis;
    }
}