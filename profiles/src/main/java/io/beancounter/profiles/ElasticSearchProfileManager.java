package io.beancounter.profiles;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import io.beancounter.activities.ActivityStoreException;
import io.beancounter.commons.helper.es.ElasticSearchConfiguration;
import io.beancounter.commons.helper.es.NodeInfo;
import io.beancounter.commons.helper.jackson.ObjectMapperFactory;
import io.beancounter.commons.model.ShortUserProfile;
import io.beancounter.commons.model.User;
import io.beancounter.commons.model.UserProfile;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.cluster.node.DiscoveryNode;
import org.elasticsearch.common.collect.ImmutableList;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.sort.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.UUID;

import static org.elasticsearch.index.query.FilterBuilders.andFilter;
import static org.elasticsearch.index.query.FilterBuilders.termFilter;
import static org.elasticsearch.index.query.QueryBuilders.filteredQuery;
import static org.elasticsearch.index.query.QueryBuilders.queryString;

/**
 * Created by Trink0 on 07/04/14.
 */
public class ElasticSearchProfileManager implements ProfileManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(ElasticSearchProfileManager.class);
    private ObjectMapper mapper;
    public static final String INDEX_NAME = "beancounter";

    public static final String INDEX_TYPE = "profile";

    private ElasticSearchConfiguration configuration;

    private Client client;
    private Client getClient() {
        Settings settings = ImmutableSettings.settingsBuilder()
                .put("client.transport.sniff", true)
                .build();
        TransportClient transportClient = new TransportClient(settings);
        for (NodeInfo node : configuration.getNodes()) {
            transportClient.addTransportAddress(
                    new InetSocketTransportAddress(node.getHost(), node.getPort())
            );
        }
        ImmutableList<DiscoveryNode> nodes = transportClient.connectedNodes();
        if (nodes.isEmpty()) {
            final String errMsg = "Could not connect to elasticsearch cluster. " +
                    "Please check the settings in the es.properties file.";
            transportClient.close();
            LOGGER.error(errMsg);
            throw new RuntimeException(errMsg);
        }
        return transportClient;
    }
    @Inject
    public ElasticSearchProfileManager(
            @Named("esConfiguration") ElasticSearchConfiguration configuration
    ) {
        this.configuration = configuration;
        client = getClient();
        mapper = ObjectMapperFactory.createMapper();
    }
    @Override
    public void store(UserProfile profile, User user) throws ProfileManagerException {
        byte[] jsonProfile = null;
        try {
            jsonProfile = mapper.writeValueAsBytes(profile);
        } catch (JsonProcessingException e) {
            final String errMsg = "Error while serializing to json [" + profile + "]";
            LOGGER.error(errMsg, e);
            throw new ProfileManagerException(errMsg, e);
        }
        try {
            client.prepareIndex(INDEX_NAME, INDEX_TYPE)
                    .setSource(jsonProfile)
                    .setId(profile.getUserId().toString())
                    .execute().actionGet();
        } catch (ElasticsearchException e) {
            final String errMsg = "Error while storing profile [" + jsonProfile + "]";
            LOGGER.error(errMsg, e);
            throw new ProfileManagerException(errMsg, e);
        }
    }

    @Override
    public UserProfile lookup(UUID userId) throws ProfileManagerException {
        GetResponse getResponse = client.prepareGet(INDEX_NAME, INDEX_TYPE, userId.toString())
                .execute()
                .actionGet();
        UserProfile profile = null;

        try {
            byte[] source = getResponse.getSourceAsBytes();
            if (source != null) {
                profile = mapper.readValue(
                        source,
                        UserProfile.class);
            }
        } catch (IOException e) {
            final String errMsg = "Error while deserializing from json [" + getResponse.getSource() + "]";
            LOGGER.error(errMsg, e);
            throw new ProfileManagerException(errMsg, e);
        }
        return profile;
    }

    @Override
    public ShortUserProfile lookupShortProfile(UUID userId) throws ProfileManagerException {
        return null;
    }
    public void shutDown() throws ProfileManagerException {
        try {
            client.close();
        } catch (Exception e) {
            final String errMsg = "Error while closing the client";
            LOGGER.error(errMsg, e);
            throw new ProfileManagerException(errMsg, e);
        }
    }
}
