package io.beancounter.profiles;

import io.beancounter.commons.model.ShortUserProfile;
import io.beancounter.commons.model.User;
import io.beancounter.commons.model.UserProfile;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * put class description here
 *
 * @author Davide Palmisano ( dpalmisano@gmail.com )
 */
public final class FakeProfiles implements ProfileManager {

    private final Set<UserProfile> profiles = new HashSet<UserProfile>();

    @Override
    public synchronized void store(UserProfile up, User user)
            throws ProfileManagerException
    {
        profiles.remove(up);
        profiles.add(up);
    }

    @Override
    public UserProfile lookup(UUID userId) throws ProfileManagerException {
        for(UserProfile up : profiles) {
            if(up.getUserId().equals(userId)) {
                return up;
            }
        }
        return null;
    }

    @Override
    public ShortUserProfile lookupShortProfile(UUID userId) throws ProfileManagerException {
        throw new RuntimeException("Not implemented");
    }
}
