package io.beancounter.profiles;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import io.beancounter.commons.model.ShortUserProfile;
import io.beancounter.commons.model.User;
import io.beancounter.commons.model.UserProfile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

/**
 * Created by Trink0 on 07/04/14.
 */
public class UnifiedProfileManager implements ProfileManager{
    private static final Logger LOGGER = LoggerFactory.getLogger(UnifiedProfileManager.class);

    private ProfileManager es;
    private ProfileManager jedis;
    @Inject
    public UnifiedProfileManager(@Named("ElasticsearchProfile") ProfileManager es,
                                 @Named("JedisProfile")ProfileManager jedis)
    {
        this.es = es;
        this.jedis = jedis;
    }

    @Override
    public void store(UserProfile profile, User user) throws ProfileManagerException {
        try {
            es.store(profile, user);
        } catch (ProfileManagerException e) {
            final String errMsg = "Error while storing profile [" + profile + "] on ElasticSearch";
            LOGGER.error(errMsg, e);
        }
        try {
            jedis.store(profile, user);
        } catch (ProfileManagerException e) {
            final String errMsg = "Error while storing profile [" + profile + "] on Redis";
            LOGGER.error(errMsg, e);
            throw e;
        }
    }

    @Override
    public UserProfile lookup(UUID userId) throws ProfileManagerException {
        return jedis.lookup(userId);
    }

    @Override
    public ShortUserProfile lookupShortProfile(UUID userId) throws ProfileManagerException {
        return jedis.lookupShortProfile(userId);
    }
}
