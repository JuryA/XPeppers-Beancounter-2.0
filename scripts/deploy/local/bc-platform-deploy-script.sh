#!/bin/bash
dest_folder="/var/lib/tomcat7/webapps"
src_folder="../../../"
app="beancounter-platform"

sudo service tomcat7 stop
sudo rm -rf $dest_folder/$app
sudo cp $src_folder/platform/target/beancounter-platform.war $dest_folder
sudo service tomcat7 start
