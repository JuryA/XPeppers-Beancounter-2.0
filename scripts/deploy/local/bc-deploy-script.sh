#!/bin/bash
dest_folder="/usr/local/beancounter/"
src_folder="../../../"
version="1.8.1-SNAPSHOT"

#cp $src_folder/<package> $dest_folder
cp $src_folder/indexer/target/indexer-$version-jar-with-dependencies.jar $dest_folder
cp $src_folder/dispatcher/target/dispatcher-$version-jar-with-dependencies.jar $dest_folder
cp $src_folder/resolver/resolver-process/target/resolver-$version-jar-with-dependencies.jar $dest_folder
cp $src_folder/filter/filter-process/target/filter-$version-jar-with-dependencies.jar $dest_folder
cp $src_folder/publishers/twitter-publisher/target/twitter-publisher-$version-jar-with-dependencies.jar $dest_folder
cp $src_folder/publishers/facebook-publisher/target/facebook-publisher-$version-jar-with-dependencies.jar $dest_folder
cp $src_folder/publishers/jms-publisher/target/jms-publisher-$version-jar-with-shades.jar $dest_folder
cp $src_folder/realtime-profiler/realtime-profiler-process/target/profiler-$version-jar-with-dependencies.jar $dest_folder
cp $src_folder/listeners/twitter/target/twitter-listener-$version-jar-with-dependencies.jar $dest_folder
cp $src_folder/listeners/facebook/facebook-process/target/facebook-listener-$version-jar-with-dependencies.jar $dest_folder
