#!/bin/bash
dest_folder="/usr/local/beancounter/"
src_folder="../../../../"
version="1.8.1-SNAPSHOT"

user="deploy"
server=$1
port=$2

echo "=== stopping all processes remotely ==="
PIDSCOMMAND="ps -ef | grep -E '*-jar-with-dependencies.jar' | awk '{print \$2}'"
PIDS=$(ssh -i ~/.ssh/deploy -p $port $user@$server $PIDSCOMMAND;)
echo $PIDS
for PID in $PIDS
do
    KILLCOMMAND="sudo kill -9 $PID"
    echo "=== killing $PID ==="
    ssh -i ~/.ssh/deploy -p $port $user@$server $KILLCOMMAND;
    echo "=== $PID killed ==="
done
