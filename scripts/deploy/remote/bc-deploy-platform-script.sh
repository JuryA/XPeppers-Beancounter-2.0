#!/bin/bash
dest_folder="/var/lib/tomcat7/webapps"
src_folder="../../../../"
app="beancounter-platform"

user="deploy"
server=$1
port=$2

ssh -i ~/.ssh/deploy -p $port $user@$server "sudo service tomcat7 stop;
sudo rm -rf $dest_folder/$app"
scp -i ~/.ssh/deploy -P $port $src_folder/platform/target/beancounter-platform.war $user@$server:$dest_folder
ssh -i ~/.ssh/deploy -p $port $user@$server "sudo service tomcat7 start"
