#!/bin/bash

server="beancounter.xpeppers.com"
port="2224"

../bc-deploy-jar-script.sh $server $port
../bc-deploy-init-script.sh $server $port
../bc-deploy-platform-script.sh $server $port
