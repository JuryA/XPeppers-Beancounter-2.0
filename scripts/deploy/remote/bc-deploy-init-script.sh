#!/bin/bash
dest_folder="/usr/local/beancounter"
src_folder="../../../init.d"
version="1.8.1-SNAPSHOT"

rc2_path="/etc/rc2.d/"

user="deploy"
server=$1
port=$2

echo "=== stopping and removing services ==="

STOPSERVICES=$(cat <<EOF
  cd $rc2_path;
  for SERVICE in \$(ls | grep beancounter | grep -v init); do
    sudo ./\$SERVICE stop;
    sudo rm \$SERVICE;
  done;
  sudo rm beancounter_init;
  rm -r $dest_folder/init.d;
EOF
)

ssh -i ~/.ssh/deploy -p $port $user@$server $STOPSERVICES

echo "=== uploading new init scripts ==="
scp -r -i ~/.ssh/deploy -P $port $src_folder $user@$server:$dest_folder

echo "=== activating init scripts ==="

ACTIVATESERVICES=$(cat <<EOF
  chmod a+x $dest_folder/init.d/*;
  cd $rc2_path;
  sudo ln -s $dest_folder/init.d/beancounter_init;

  for SERVICE in \$(ls $dest_folder/init.d | grep beancounter | grep -v init); do
    sudo ln -s $dest_folder/init.d/\$SERVICE S10\$SERVICE;
    sudo ./S10\$SERVICE start;
  done;
EOF
)

ssh -i ~/.ssh/deploy -p $port $user@$server $ACTIVATESERVICES
