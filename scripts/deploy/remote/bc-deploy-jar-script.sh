#!/bin/bash
dest_folder="/usr/local/beancounter/"
src_folder="../../../../"
version="1.8.1-SNAPSHOT"


server=$1
port=$2
user="deploy"
keyfile="deploy"

UNUSEDPROCESS="listeners/twitter"
PROCESSES="listeners/facebook/facebook-process resolver/resolver-process dispatcher realtime-profiler/realtime-profiler-process filter/filter-process indexer publishers/facebook-publisher publishers/twitter-publisher export/exporter-process"
for PROCESS in $PROCESSES
do
    scp -i ~/.ssh/$keyfile -P $port $src_folder$PROCESS/target/*-$version-jar-with-dependencies.jar $user@$server:$dest_folder
done

DEPLOYSCRIPT=$(cat <<EOF
  echo "removing old symbolic links ...";
  cd $dest_folder;
  for SYMLINK in \$(find . -maxdepth 1 -type l -print); do
    echo removing symlink \$SYMLINK;
    rm \$SYMLINK;
  done;

  echo "upgrading symbolic links ...";
  for PROCESS in \$(ls | grep $version | sed 's/-$version-jar-with-dependencies.jar//'); do
    echo new process is \$PROCESS;
    ln -s \$PROCESS-$version-jar-with-dependencies.jar \$PROCESS;
  done;

  echo "symlinks to processes successfully updated";
EOF
)

ssh -i ~/.ssh/$keyfile -p $port $user@$server $DEPLOYSCRIPT
