#!/bin/bash

dest_folder="/usr/local/beancounter/"

user="deploy"
server=$1
port=$2

COMMAND="cd $dest_folder; echo 'publish register \"appchanges\"' > publish.redisCmds; redis-cli < publish.redisCmds; rm -r publish.redisCmds"

ssh -i ~/.ssh/deploy -p $port $user@$server $COMMAND;
