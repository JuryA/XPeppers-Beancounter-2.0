#!/bin/bash
# Usage:
#   ./update_app.sh http://BEANCOUNTER_API_HOST/beancounter-platform/rest \
#                   app_name uuid-api-key appdata.json

HOST=$1

NAME=$2
KEY=$3
FILENAME=$4

curl -i -X PUT -H 'Content-Type: application/json' -d @$FILENAME "$HOST/application/$NAME?apikey=$KEY"
