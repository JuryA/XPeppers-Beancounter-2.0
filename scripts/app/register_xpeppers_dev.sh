#!/bin/bash
# No args. Requires register_app.sh to be in the same dir with this script.
# Usage: ./register_xpeppers.sh

APP_NAME=xpeppers
APP_EMAIL=beancounter@xpeppers.com
API_HOST=http://192.168.10.10:8080/beancounter-platform/rest
UPDATE_PAYLOAD=stage.json

BASEDIR=$(dirname $0)

APPJSON=$($BASEDIR/register_app.sh $API_HOST $APP_NAME $APP_EMAIL)
API_KEY=$(echo $APPJSON | grep -Eo '"apiKey"\s*:\s*"([^"]+)' | cut -d '"' -f 4)
$BASEDIR/update_app.sh $API_HOST $APP_NAME $API_KEY $BASEDIR/$UPDATE_PAYLOAD
