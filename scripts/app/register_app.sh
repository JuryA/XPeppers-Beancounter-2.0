#!/bin/bash
# Usage:
#   ./register_app.sh http://BEANCOUNTER_API_HOST/beancounter-platform/rest \
#                     app_name email@example.org 'optional description'

API_HOST=$1

NAME=$2
EMAIL=$3
DESC=$4

curl -s -d "name=$NAME" -d "email=$EMAIL" -d "description=$DESC" "$API_HOST/application/register"
