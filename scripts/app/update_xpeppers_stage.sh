#!/bin/bash
APP_NAME=xpeppers
APP_EMAIL=beancounter@xpeppers.com
API_HOST=http://stage.beancounter.xpeppers.com/beancounter-platform/rest
UPDATE_PAYLOAD=stage.json
#Should be updated if a new registration has been made
API_KEY=5d9e61e0-8dce-4b07-a355-5f7a313becea

BASEDIR=$(dirname $0)
$BASEDIR/update_app.sh $API_HOST $APP_NAME $API_KEY $BASEDIR/$UPDATE_PAYLOAD
