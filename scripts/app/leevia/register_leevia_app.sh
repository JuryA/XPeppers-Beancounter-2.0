#!/bin/bash

NAME=leevia
DESCRIPTION=leevia-application
EMAIL=diego@leevia.com
CALLBACK=http://www.yourcallback.com
PAYLOAD="name=$NAME&description=$DESCRIPTION&email=$EMAIL&oauthCallback=$CALLBACK"

curl -v -d "$PAYLOAD" http://leevia-bc.cloudapp.net/beancounter-platform/rest/application/register
