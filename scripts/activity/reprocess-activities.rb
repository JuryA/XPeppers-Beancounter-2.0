#!/usr/bin/env ruby
require 'elasticsearch'
require 'memcached'
require 'json'

VERB = ARGV[0] || ''
ELASTICSEARCH_HOST = ARGV[1] || '192.168.10.10:9200'
KESTREL_HOST = ARGV[2] || '192.168.10.10:22133'

@profiler_queue = 'profiler-activities'
@client = Elasticsearch::Client.new log: false, host: ELASTICSEARCH_HOST
@queue = Memcached.new KESTREL_HOST

def upper_case?(character)
  not /[[:upper:]]/.match(character).nil?
end

def reorder(categories = [])
  return categories if categories.size != 2
  categories.reverse! if upper_case? categories.last[0]
  categories
end

def normalize_name_of(categories = [])
  if categories.size > 1
    categories = reorder categories
    return [categories.join('/')]
  end
  return categories
end

def scroll_id_from(params = {})
  @client.search(params)['_scroll_id']
end

def elasticsearch_pop(scroll_id)
  results = @client.scroll scroll_id: scroll_id, scroll: '10m'
  return results['hits']['hits'], results['_scroll_id']
end

def kestrel_push(activities = [])
  activities.each do |activity|
    json_act = activity['_source'].to_json
    activity['_source']['activity']['object']['categories'] = normalize_name_of activity['_source']['activity']['object']['categories']
    puts activity
    @queue.set @profiler_queue, json_act, 604800, false
  end
end

query = VERB == '' ? { match_all: {} } : { match: { verb: VERB } }
scroll_id = scroll_id_from index: 'beancounter', body: { query: query }, search_type: 'scan', scroll: '10m', size: 50

begin
  activities, scroll_id = elasticsearch_pop scroll_id
  kestrel_push activities
end while activities.size > 0
