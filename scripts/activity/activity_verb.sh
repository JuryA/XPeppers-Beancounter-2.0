#!/bin/bash
curl -XGET http://192.168.10.10:9200/beancounter/_search -d '
{
    "aggregations": {
        "recent_action": {
            "filter" : { "range" : { "activity.context.date" : { "gt" : 1364292941000 } } },
            "aggregations": {
                "popular_action_overall": {
                    "terms": { "field": "activity.verb" }

                }
            }
        }
    },
    "size" : "0"
}'