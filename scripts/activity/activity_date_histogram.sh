#!/bin/bash
curl -XGET http://192.168.10.10:9200/beancounter/_search -d '
{
    "aggregations": {
        "activity_by_hour": {
            "histogram": {
                "field":    "activity.context.date",
                "interval": 360000
            }
        }
    },
    "size" : "0"
}'