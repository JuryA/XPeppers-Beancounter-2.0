#!/bin/bash
curl -XGET http://192.168.10.10:9200/beancounter/_search -d '
{
    "aggregations": {
        "popular_action_overall": {
            "terms": { "field": "activity.verb" }
        },
        "by_hour": {
             "histogram": {
                "field":    "activity.context.date",
                "interval": 100000
            },
            "aggregations": {
                "popular_verb": {
                    "terms": { "field": "activity.verb" }
                }
            }
        }
    }
}'