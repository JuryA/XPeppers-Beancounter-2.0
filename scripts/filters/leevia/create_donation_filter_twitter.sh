#!/bin/bash

SERVER=leevia-bc.cloudapp.net
NAME=donation-filter-twitter
DESCRIPTION=a-donation-filter
QUEUE=publish-to-twitter
VERB=DONATE
TYPE=io.beancounter.filter.model.pattern.ObjectPattern
TYPEPATTERN=io.beancounter.commons.model.activity.leevia.Donation
PAYLOAD="description=$DESCRIPTION&queue=$QUEUE&pattern={\"userId\":{\"uuid\":null},\"verb\":{\"verb\":\"$VERB\"},\"object\":{\"type\":\"$TYPE\", \"typePattern\":{\"string\":\"$TYPEPATTERN\"},\"url\":{\"url\":null}},\"context\":{\"date\":{\"date\":1,\"bool\":\"AFTER\"},\"service\":{\"string\":\"twitter\"},\"mood\":{\"string\":\"\"},\"username\":{\"string\":\"\"}}}"

APIKEY=df33e26a-d82b-4844-bfc8-2118d38428cf

../create_filter.sh $SERVER $NAME $APIKEY $PAYLOAD
