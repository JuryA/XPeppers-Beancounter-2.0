#!/bin/bash

SERVER=leevia-bc.cloudapp.net
NAME=upload-photo-filter-facebook
DESCRIPTION=an-upload-photo-filter-for-facebook
QUEUE=publish-to-facebook
VERB=UPLOAD
TYPE=io.beancounter.filter.model.pattern.ObjectPattern
TYPEPATTERN=io.beancounter.commons.model.activity.leevia.PhotoUpload
PAYLOAD="description=$DESCRIPTION&queue=$QUEUE&pattern={\"userId\":{\"uuid\":null},\"verb\":{\"verb\":\"$VERB\"},\"object\":{\"type\":\"$TYPE\", \"typePattern\":{\"string\":\"$TYPEPATTERN\"},\"url\":{\"url\":null}},\"context\":{\"date\":{\"date\":1,\"bool\":\"AFTER\"},\"service\":{\"string\":\"facebook\"},\"mood\":{\"string\":\"\"},\"username\":{\"string\":\"\"}}}"

APIKEY=df33e26a-d82b-4844-bfc8-2118d38428cf

../create_filter.sh $SERVER $NAME $APIKEY $PAYLOAD
