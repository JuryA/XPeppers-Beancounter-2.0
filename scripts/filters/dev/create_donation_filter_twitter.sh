#!/bin/bash

SERVER=beancounter.xpeppers.com
NAME=donation-filter-twitter
DESCRIPTION=a-donation-filter
QUEUE=publish-to-twitter
VERB=DONATE
TYPE=io.beancounter.filter.model.pattern.ObjectPattern
TYPEPATTERN=io.beancounter.commons.model.activity.leevia.Donation
PAYLOAD="description=$DESCRIPTION&queue=$QUEUE&pattern={\"userId\":{\"uuid\":null},\"verb\":{\"verb\":\"$VERB\"},\"object\":{\"type\":\"$TYPE\", \"typePattern\":{\"string\":\"$TYPEPATTERN\"},\"url\":{\"url\":null}},\"context\":{\"date\":{\"date\":1,\"bool\":\"AFTER\"},\"service\":{\"string\":\"twitter\"},\"mood\":{\"string\":\"\"},\"username\":{\"string\":\"\"}}}"

APIKEY=7c0af1c4-f411-41a3-90b0-b7ea4097c7a3

../create_filter.sh $SERVER $NAME $APIKEY $PAYLOAD
