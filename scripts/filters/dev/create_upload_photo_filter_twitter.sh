#!/bin/bash

SERVER=beancounter.xpeppers.com
NAME=upload-photo-filter-twitter
DESCRIPTION=an-upload-photo-filter-for-twitter
QUEUE=publish-to-twitter
VERB=UPLOAD
TYPE=io.beancounter.filter.model.pattern.ObjectPattern
TYPEPATTERN=io.beancounter.commons.model.activity.leevia.PhotoUpload
PAYLOAD="description=$DESCRIPTION&queue=$QUEUE&pattern={\"userId\":{\"uuid\":null},\"verb\":{\"verb\":\"$VERB\"},\"object\":{\"type\":\"$TYPE\", \"typePattern\":{\"string\":\"$TYPEPATTERN\"},\"url\":{\"url\":null}},\"context\":{\"date\":{\"date\":1,\"bool\":\"AFTER\"},\"service\":{\"string\":\"twitter\"},\"mood\":{\"string\":\"\"},\"username\":{\"string\":\"\"}}}"

APIKEY=7c0af1c4-f411-41a3-90b0-b7ea4097c7a3

../create_filter.sh $SERVER $NAME $APIKEY $PAYLOAD
