package io.beancounter.commons.tests;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ProtocolVersion;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHttpResponse;

import java.io.*;

/**
 * Test helpers
 */
public class TestUtils {

    public static String getResourceAsString(String path) throws IOException {
        StringBuilder content = new StringBuilder();
        InputStream in = TestUtils.class.getResourceAsStream(path);

        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        for (int c = reader.read(); c != -1; c = reader.read())
            content.append((char)c);

        return content.toString();
    }

    public static HttpResponse getResourceAsHttpResponse(String path, int code)
            throws IOException
    {
        String body = getResourceAsString(path);
        return getStringAsHttpResponse(body, code);
    }

    public static HttpResponse getStringAsHttpResponse(String body, int code)
            throws IOException
    {
        HttpEntity entity = new StringEntity(body, "UTF-8");
        ProtocolVersion proto = new ProtocolVersion("HTTP", 1, 1);
        BasicHttpResponse resp = new BasicHttpResponse(proto, code, null);
        resp.setEntity(entity);
        return resp;
    }
}
