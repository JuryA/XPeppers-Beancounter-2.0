package io.beancounter.tests.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.grizzly.http.embed.GrizzlyWebServer;

import io.beancounter.commons.helper.jackson.ObjectMapperFactory;
import org.apache.http.*;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpOptions;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.testng.annotations.*;
import static org.testng.Assert.*;


public abstract class ServletBasedTestCase {

    protected static final Logger logger = Logger.getLogger(ServletBasedTestCase.class);

    protected final URI base_uri;
    protected final int port;

    protected GrizzlyWebServer server;
    private HttpClient client;
    private ObjectMapper mapper;


    protected ServletBasedTestCase() {
        final String uriTemplate = "http://localhost:%d/rest/";
        port = findFreePort();
        try {
            base_uri = new URI(String.format(uriTemplate, port));
        } catch (URISyntaxException urise) {
            throw new RuntimeException(urise);
        }
    }

    @BeforeSuite
    public void setupOnce() throws Exception {
        startFrontendService();
    }

    @AfterSuite
    public void teardownOnce() {
        server.stop();
        if (client != null)
            client.getConnectionManager().shutdown();
    }

    protected void startFrontendService() throws IOException {
        server = new GrizzlyWebServer(port);
        setupFrontendService();
        server.start();
    }

    /**
     * Override this to start your custom frontend service.
     */
    abstract protected void setupFrontendService();

    protected String getServiceHost() {
        String base = base_uri.resolve("/").toString();
        return base.substring(0, base.length()-1);
    }

    protected String resolveUrl(String path) {
        return base_uri.resolve(path).toString();
    }

    /**
     * Makes a simple GET request to the internal Grizzly server instance.
     *
     * @param url If relative, will be prefixed with {@link #base_uri}.
     */
    protected HttpResponse get(String url) throws IOException {
        if (!url.startsWith("http"))
            url = base_uri + url;
        return makeRequest(new HttpGet(url));
    }

    /**
     * Makes a URL-encoded POST request to the internal Grizzly server instance.
     *
     * @param url If relative, will be prefixed with {@link #base_uri}.
     * @param params An even-length list, used as (name, value) pairs
     */
    protected HttpResponse post(String url, String... params) throws IOException {
        if ((params.length & 1) != 0)
            throw new IllegalArgumentException("Params len should be even!");

        if (!url.startsWith("http"))
            url = base_uri + url;
        HttpPost req = new HttpPost(url);

        List<NameValuePair> payload = new ArrayList<NameValuePair>();
        for (int i=0; i < params.length; i += 2) {
            payload.add(new BasicNameValuePair(params[i], params[i+1]));
        }

        req.setEntity(new UrlEncodedFormEntity(payload, Consts.UTF_8));
        return makeRequest(req);
    }

    protected HttpResponse makeRequest(HttpRequestBase req) throws IOException {
        HttpResponse resp = getClient().execute(req);
        HttpEntity entity = resp.getEntity();
        if (entity != null)
            resp.setEntity(new BufferedHttpEntity(entity));
        EntityUtils.consume(resp.getEntity());
        return resp;
    }

    protected HttpResponse makeCorsRequest(
            Class<? extends HttpRequestBase> method,
            String origin,
            String path
    ) throws Exception {
        HttpRequestBase req = method.newInstance();
        req.setURI(URI.create(base_uri + path));
        req.setHeader("Origin", origin);
        return makeRequest(req);
    }

    protected HttpResponse makeCorsRequest(
            Class<? extends HttpRequestBase> method,
            String path
    ) throws Exception {
        return makeCorsRequest(method, "http://cors.test", path);
    }

    protected HttpResponse makeCorsPreflightRequest(String origin, String path) throws Exception {
        return makeCorsRequest(HttpOptions.class, origin, path);
    }

    protected HttpResponse makeCorsPreflightRequest(String path) throws Exception {
        return makeCorsRequest(HttpOptions.class, path);
    }

    protected void verifyHeaderValue(HttpResponse resp, String name, String value) {
        Header[] header = resp.getHeaders(name);
        assertNotNull(header);
        assertEquals(header.length, 1, name + " IN " + Arrays.asList(resp.getAllHeaders()));
        assertEquals(header[0].getValue(), value);
    }

    protected void verifyNullHeader(HttpResponse resp, String... names) {
        for(String name: names) {
            Header[] header = resp.getHeaders(name);
            if (header != null && header.length > 0)
                fail("Did not expect header " + header[0]);
        }
    }

    protected void verifyNoCors(HttpResponse resp) throws Exception {
        verifyNullHeader(resp,
                "Access-Control-Allow-Origin",
                "Access-Control-Expose-Headers",
                "Access-Control-Allow-Methods",
                "Access-Control-Allow-Headers",
                "Access-Control-Allow-Credentials",
                "Access-Control-Max-Age");
    }

    protected <T> T fromJson(String content, Class<T> t) throws IOException {
        return getJsonMapper().readValue(content, t);
    }

    protected synchronized HttpClient getClient() {
        if (client == null)
            client = new DefaultHttpClient();
        return client;
    }

    protected synchronized ObjectMapper getJsonMapper() {
        if (mapper == null)
            mapper = ObjectMapperFactory.createMapper();
        return mapper;
    }

    protected int findFreePort() {
        int retries = 0;

        while (retries++ < 3) {
            ServerSocket sock = null;
            try {
                sock = new ServerSocket(0);
                return sock.getLocalPort();
            } catch (IOException e) {
                // port is busy
            } finally {
                if (sock != null)
                    try { sock.close(); } catch (IOException ignored) {}
            }
        }

        throw new RuntimeException("Unable to find free port");
    }
}
