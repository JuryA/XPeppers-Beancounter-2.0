package org.apache.camel.component.redis;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.component.direct.DirectConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.Topic;

public class RedisConsumer extends DirectConsumer implements MessageListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(RedisConsumer.class);
    private final RedisConfiguration redisConfiguration;

    public RedisConsumer(RedisEndpoint redisEndpoint, Processor processor,
                         RedisConfiguration redisConfiguration) {
        super(redisEndpoint, processor);
        this.redisConfiguration = redisConfiguration;
    }

    @Override
    protected void doStart() throws Exception {
        super.doStart();
        Collection<Topic> topics = toTopics(redisConfiguration.getChannels());
        RedisMessageListenerContainer listenerContainer = redisConfiguration.getListenerContainer();
        listenerContainer.addMessageListener(this, topics);
        LOGGER.info("Start Listener Container: ");
        listenerContainer.start();
    }

    private Collection<Topic> toTopics(String channels) {
        String[] channelsArrays = channels.split(",");
        String command = redisConfiguration.getCommand();
        LOGGER.info("Command: " + command);
        List<Topic> topics = new ArrayList<Topic>();
        for (String channel : channelsArrays) {
            LOGGER.info("Channel: " + channel);
            if (Command.PSUBSCRIBE.toString().equals(command)) {
                topics.add(new PatternTopic(channel));
            } else if (Command.SUBSCRIBE.toString().equals(command)) {
                LOGGER.info("Adding SUBSCRIBE to channel topic: " + channel);
                topics.add(new ChannelTopic(channel));
            } else {
                throw new RuntimeException("Unsupported Command: " + command);
            }
        }
        return topics;
    }

    @Override
    public void onMessage(Message message, byte[] pattern) {
        LOGGER.info("Message: " + message.getBody() + " on channel: " + message.getChannel());
        Object value = redisConfiguration.getSerializer().deserialize(message.getBody());

        Exchange exchange = getEndpoint().createExchange();
        exchange.getIn().setHeader(RedisConstants.CHANNEL, message.getChannel());
        exchange.getIn().setHeader(RedisConstants.PATTERN, pattern);
        exchange.getIn().setBody(value);
        try {
            LOGGER.info("Ready to process message with excahnge: " + exchange.toString());
            getProcessor().process(exchange);
        } catch (Exception e) {
            LOGGER.error("Error on process message: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }
}
