package io.beancounter.listener.facebook.core.converter.custom;

import com.google.inject.Inject;
import com.google.inject.Provider;
import io.beancounter.commons.helper.jackson.ObjectMapperFactory;
import io.beancounter.commons.model.activity.Context;
import io.beancounter.commons.model.activity.facebook.Like;
import io.beancounter.listener.facebook.core.model.FacebookData;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This {@link Converter} is responsible of converting <i>Facebook</i> responses
 * into {@link Like}s.
 *
 * @author Davide Palmisano ( dpalmisano@gmail.com )
 * @author Enrico Candino ( enrico.candino@gmail.com )
 * @author Francesco Bux (francesco.bux@xpeppers.com) altered category conversion
 */
public class FacebookLikeConverter implements Converter<FacebookData, Like> {

    private static final Logger LOGGER = LoggerFactory.getLogger(FacebookLikeConverter.class);

    private static final String SERVICE = "facebook";

    private ObjectMapper mapper;
    private Provider<HttpClient> httpClientProvider;

    @Inject
    public FacebookLikeConverter(Provider<HttpClient> httpClientProvider) {
        this.httpClientProvider = httpClientProvider;
        this.mapper = ObjectMapperFactory.createMapper();
    }

    @Override
    public Like convert(FacebookData facebookData, boolean isOpenGraph) throws ConverterException {
        if(!isOpenGraph) {
            return convert(facebookData);
        }
        Like like = convert(facebookData);
        Map<String, String> fields = new HashMap<String, String>();
        fields.put("description", "description");
        fromOpenGraph(facebookData.getId(), fields, like);
        return like;
    }

    @Override
    public Context getContext(FacebookData facebookData, String userId) throws ConverterException {
        Context context = new Context();
        context.setDate(new DateTime(facebookData.getCreatedTime()));
        context.setUsername(userId);
        context.setService(SERVICE);
        return context;
    }

    private void fromOpenGraph(String id, Map<String, String> fields, Like like) throws ConverterException {
        Map<String, String> jsonAsMap = makeHttpCall(id);
        for(String fieldOnJson : fields.keySet()) {
            String jsonValue = jsonAsMap.get(fieldOnJson);
            if(jsonValue != null) {
                set(like, fields.get(fieldOnJson), jsonValue);
            }
        }
    }

    @SuppressWarnings("unchecked")
    private Map<String, String> makeHttpCall(String graphObjectId)
            throws ConverterException
    {
        HttpGet req = new HttpGet("https://graph.facebook.com/" + graphObjectId);
        HttpClient client = httpClientProvider.get();
        String body;
        try {
            HttpResponse resp = client.execute(req);
            if (resp.getStatusLine().getStatusCode() != HttpStatus.SC_OK)
                throw new ConverterException("Response error: " + resp.getStatusLine());

            HttpEntity entity = resp.getEntity();
            if (entity == null)
                throw new ConverterException("Unable to consume from " + req);

            body = EntityUtils.toString(entity, "UTF-8");

        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
            throw new ConverterException(e.getMessage(), e);
        } finally {
            LOGGER.debug("Shutting down Connection Manager");
            client.getConnectionManager().shutdown();
        }

        Object map;
        try {
            map = mapper.readValue(body, Object.class);
        } catch (IOException e) {
            final String errMsg = "Error while parsing ["+body+"] from "+req;
            LOGGER.error(errMsg, e);
            throw new ConverterException(errMsg, e);
        }

        try {
            return (Map<String, String>) map;
        } catch (ClassCastException e) {
            LOGGER.error(e.getMessage(), e);
            throw new UnconvertableException("Unable to parse FB response", e);
        }
    }

    private void set(Like like, String fieldName, String jsonValue) throws ConverterException {
        Method setter;
        fieldName = fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1, fieldName.length());
        try {
            setter = like.getClass().getMethod("set" + fieldName, String.class);
        } catch (NoSuchMethodException e) {
            final String errMsg = "cannot find on class Like method [set"+ fieldName + "]";
            LOGGER.error(errMsg, e);
            throw new ConverterException(errMsg, e);
        }
        try {
            setter.invoke(like, jsonValue);
        } catch (IllegalAccessException e) {
            final String errMsg = "error while invoking [set"+ fieldName + "]";
            LOGGER.error(errMsg, e);
            throw new ConverterException(errMsg, e);
        } catch (InvocationTargetException e) {
            final String errMsg = "error while invoking [set"+ fieldName + "]";
            LOGGER.error(errMsg, e);
            throw new ConverterException(errMsg, e);
        }
    }

    private Like convert(FacebookData facebookData) throws ConverterException {
        Like like = new Like();
        like.setName(facebookData.getName());
        for(String category : getCategories(facebookData.getCategory())) {
            like.addCategory(category);
        }
        String candidateUrl = "http://www.facebook.com/" + facebookData.getId();
        URL url;
        try {
            url = new URL(candidateUrl);
        } catch (MalformedURLException e) {
            final String errMsg = "[" + candidateUrl + "] is ill-formed";
            LOGGER.error(errMsg, e);
            throw new ConverterException(errMsg, e);
        }
        like.setUrl(url);
        return like;
    }

    private String[] getCategories(String category) {
        return new String[]{category.trim().toLowerCase()};
    }
}
