package io.beancounter.listener.facebook.core.converter.custom;

import com.google.inject.Provider;
import io.beancounter.commons.model.activity.facebook.Like;
import io.beancounter.commons.tests.TestUtils;
import io.beancounter.listener.facebook.core.converter.FacebookActivityConverterException;
import io.beancounter.listener.facebook.core.model.FacebookData;

import java.net.URL;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.*;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ClientConnectionManager;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * put class description here
 *
 * @author Davide Palmisano ( dpalmisano@gmail.com )
 */
public class FacebookLikeConverterTestCase {

    private HttpClient mockHttpClient;
    private ClientConnectionManager mockConnMgr;

    private FacebookLikeConverter converter;

    @BeforeMethod
    public void setUp() throws FacebookActivityConverterException {
        mockHttpClient = mock(HttpClient.class);
        mockConnMgr = mock(ClientConnectionManager.class);
        when(mockHttpClient.getConnectionManager()).thenReturn(mockConnMgr);

        converter = new FacebookLikeConverter(new Provider<HttpClient>() {
            @Override
            public HttpClient get() {
                return mockHttpClient;
            }
        });
    }

    @Test
    public void test() throws Exception {
        HttpResponse resp = TestUtils.getResourceAsHttpResponse(
                "/facebook/radio_globo.json", 200);
        when(mockHttpClient.execute(any(HttpGet.class))).thenReturn(resp);

        Like like = converter.convert(facebookData(), true);

        assertNotNull(like);
        assertEquals(like.getCategories().iterator().next(), "radio station");
        assertEquals(like.getName(), "Radio Globo");
        assertEquals(like.getUrl(), new URL("http://www.facebook.com/53420726443"));
        verify(mockConnMgr).shutdown();
    }

    private FacebookData facebookData() {
        FacebookDataTest fbd = new FacebookDataTest();
        fbd.setName("Radio Globo");
        fbd.setCategory("Radio station");
        fbd.setCreatedTime("2011-12-23T13:14:41+0000");
        fbd.setId("53420726443");
        return fbd;
    }

}
