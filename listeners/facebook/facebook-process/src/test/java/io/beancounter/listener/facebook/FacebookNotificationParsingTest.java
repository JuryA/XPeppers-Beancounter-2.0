package io.beancounter.listener.facebook;

import io.beancounter.commons.helper.jackson.ObjectMapperFactory;
import io.beancounter.listener.facebook.core.model.FacebookNotification;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Enrico Candino ( enrico.candino@gmail.com )
 */
public class FacebookNotificationParsingTest {

    @Test
    public void testParsing() throws IOException {
        String json = "{\"object\":\"user\",\"entry\":[{\"uid\":1335845740,\"changed_fields\":[\"name\",\"picture\"],\"time\":232323},{\"uid\":1234,\"changed_fields\":[\"friends\"],\"time\":232325}]}";
        ObjectMapper mapper = ObjectMapperFactory.createMapper();
        FacebookNotification notification = mapper.readValue(json, FacebookNotification.class);
        Assert.assertNotNull(notification);
    }

}