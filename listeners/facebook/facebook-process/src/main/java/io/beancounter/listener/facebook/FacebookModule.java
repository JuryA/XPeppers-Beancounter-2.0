package io.beancounter.listener.facebook;

import io.beancounter.applications.ApplicationsManager;
import io.beancounter.applications.JedisApplicationsManagerImpl;
import io.beancounter.auth.*;
import io.beancounter.commons.helper.PropertiesHelper;
import io.beancounter.commons.helper.jedis.DefaultJedisPoolFactory;
import io.beancounter.commons.helper.jedis.JedisPoolFactory;
import io.beancounter.commons.http.HttpClientProvider;
import io.beancounter.commons.model.AuthServiceConfig;
import io.beancounter.commons.model.activity.Verb;
import io.beancounter.listener.commons.ActivityConverter;
import io.beancounter.listener.facebook.core.converter.FacebookActivityConverter;
import io.beancounter.listener.facebook.core.converter.FacebookActivityConverterException;
import io.beancounter.listener.facebook.core.converter.custom.FacebookLikeConverter;
import io.beancounter.listener.facebook.core.converter.custom.FacebookShareConverter;
import io.beancounter.listener.facebook.core.model.FacebookData;
import io.beancounter.resolver.JedisResolver;
import io.beancounter.resolver.Resolver;
import io.beancounter.usermanager.JedisUserManager;
import io.beancounter.usermanager.JedisUserTokenManager;
import io.beancounter.usermanager.UserManager;
import io.beancounter.usermanager.UserTokenManager;
import io.beancounter.auth.DefaultAuthServiceManager;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.camel.component.properties.PropertiesComponent;
import org.apache.camel.guice.CamelModuleWithMatchingRoutes;
import org.apache.camel.guice.jndi.JndiBind;

import com.google.inject.Provides;
import com.google.inject.multibindings.MapBinder;
import com.google.inject.name.Names;
import com.restfb.types.Post;
import org.apache.camel.spi.LifecycleStrategy;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.SerializationException;

/**
 * @author Enrico Candino ( enrico.candino@gmail.com )
 */
public class FacebookModule extends CamelModuleWithMatchingRoutes {

    public void configure() {
        super.configure();
        Properties redisProperties = PropertiesHelper.readFromClasspath("/redis.properties");
        Names.bindProperties(binder(), redisProperties);
        bindInstance("redisProperties", redisProperties);
        bind(JedisPoolFactory.class).to(DefaultJedisPoolFactory.class).asEagerSingleton();

        Properties properties = PropertiesHelper.readFromClasspath("/beancounter.properties");
        Names.bindProperties(binder(), properties);
        bind(Resolver.class).to(JedisResolver.class);

        AuthServiceConfig twitterService = DefaultAuthServiceManager.buildService("twitter", properties);
        AuthServiceConfig facebookService = DefaultAuthServiceManager.buildService("facebook", properties);
        bind(AuthServiceConfig.class)
                .annotatedWith(Names.named("service.twitter"))
                .toInstance(twitterService);
        bind(AuthServiceConfig.class)
                .annotatedWith(Names.named("service.facebook"))
                .toInstance(facebookService);

        MapBinder<AuthServiceConfig, AuthHandler> authHandlerBinder
                = MapBinder.newMapBinder(binder(), AuthServiceConfig.class, AuthHandler.class);
        authHandlerBinder.addBinding(facebookService).to(FacebookAuthHandler.class);

        bind(AuthServiceManager.class).to(DefaultAuthServiceManager.class);
        bind(UserTokenManager.class).to(JedisUserTokenManager.class);
        bind(UserManager.class).to(JedisUserManager.class);
        bind(RequestStateManager.class).to(JedisRequestStateManager.class);
        bind(ApplicationsManager.class).to(JedisApplicationsManagerImpl.class);
        FacebookActivityConverter fac = new FacebookActivityConverter();
        try {
            fac.registerConverter(
                    Post.class,
                    Verb.SHARE,
                    new FacebookShareConverter());
            fac.registerConverter(
                    FacebookData.class,
                    Verb.LIKE,
                    new FacebookLikeConverter(new HttpClientProvider()));
        } catch (FacebookActivityConverterException e) {
            throw new RuntimeException("Error while instantiating Facebook converters", e);
        }
        bind(FacebookActivityConverter.class).toInstance(fac);
        bind(FacebookFactory.class).to(FacebookProvider.class);
        bind(ActivityConverter.class).to(FacebookNotificationConverter.class);
        bind(FacebookRoute.class);
    }

    @Provides
    @JndiBind("properties")
    PropertiesComponent propertiesComponent() {
        PropertiesComponent pc = new PropertiesComponent();
        pc.setLocation("classpath:beancounter.properties");
        return pc;
    }
    @Provides
    List<LifecycleStrategy> lifecycleStrategy(JedisPoolFactory factory) {
        ArrayList<LifecycleStrategy> list = new ArrayList<LifecycleStrategy>();
        list.add(new ResourceCleanupStrategy(factory));
        return list;
    }
    @Provides
    @JndiBind("serializer")
    RedisSerializer redisSerializer() {
        return new RedisSerializer<String>() {

            private static final String CHARSET = "UTF-8";

            @Override
            public byte[] serialize(String s) throws SerializationException {
                try {
                    return s.getBytes(CHARSET);
                } catch (UnsupportedEncodingException e) {
                    throw new RuntimeException(e);
                }
            }

            @Override
            public String deserialize(byte[] bytes) throws SerializationException {
                try {
                    return new String(bytes, CHARSET);
                } catch (UnsupportedEncodingException e) {
                    throw new RuntimeException(e);
                }
            }
        };
    }

}