package io.beancounter.listener.facebook;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.name.Named;
import io.beancounter.applications.ApplicationsManager;
import io.beancounter.commons.helper.jackson.ObjectMapperFactory;
import io.beancounter.commons.model.Application;
import io.beancounter.commons.model.activity.Activity;
import io.beancounter.listener.commons.ActivityConverter;
import io.beancounter.listener.facebook.core.model.FacebookNotification;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.management.RuntimeErrorException;
import javax.servlet.http.HttpServletRequest;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;

import com.google.inject.Inject;
import org.apache.commons.codec.binary.Hex;
import org.apache.http.client.utils.URLEncodedUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

public class FacebookRoute extends RouteBuilder {

    public static final String FIELDS = "feed,likes";
    @Inject
    private ActivityConverter activityConverter;

    @Inject
    private ApplicationsManager appManager;

    @Inject
    @Named("service.host")
    private String serviceHost;

    private static int SERVICE_PORT = 34567;

    ObjectMapper mapper = ObjectMapperFactory.createMapper();

    public static final String X_HUB_SIGNATURE = "X-Hub-Signature";

    @Override
    public void configure() throws Exception {
        errorHandler(deadLetterChannel(errorEndpoint()));

        from(fromRegisterChannel()).process(
                new Processor() {
                    public void process(Exchange exchange) throws Exception {
                        log.info("started update subscription");
                        String appName = exchange.getIn().getBody(String.class);
                        Application application = appManager.get(appName);
                        String facebookAppId = application.getFacebookAppId();
                        String facebookAppSecret = application.getFacebookAppSecret();

                        String callbackUrl = serviceHost + ":" + SERVICE_PORT +"/facebook/" + appName;
                        String encodedCallbackUrl = URLEncoder.encode(callbackUrl, "UTF-8");
                        String fieldsEncoded = URLEncoder.encode(FIELDS, "UTF-8");
                        String encodedAccesToken = URLEncoder.encode(facebookAppId + "|" + facebookAppSecret, "UTF-8");
                        String endodedVerifyToken =  URLEncoder.encode("TEST-BEANCOUNTER-FACEBOOK", "UTF-8");
                        String bodyMessage ="object=user&callback_url=" + encodedCallbackUrl
                                + "&fields=" + fieldsEncoded + "&verify_token=" + endodedVerifyToken +"&access_token=" + encodedAccesToken;

                        String endpointUri = "https://graph.facebook.com/" + facebookAppId
                                + "/subscriptions";
                        log.info("ready to update application: " + appName + " with url: " + endpointUri + " with body: " + bodyMessage);
                        exchange.getIn().setHeader(Exchange.HTTP_URI, endpointUri);
                        exchange.getIn().setBody(bodyMessage);
                    }
                }).setHeader(Exchange.HTTP_METHOD, constant("POST")).to("https://dummy_facebook_url");

        from(fromFacebookEndpoint())
                .choice()
                .when(header(Exchange.HTTP_METHOD).isEqualTo("GET"))
                .to("direct:verification")
                .when(header(Exchange.HTTP_METHOD).isEqualTo("POST"))
                .to("direct:streaming");

        from("direct:verification")
                .process(new Processor() {
                    @Override
                    public void process(Exchange exchange) throws Exception {
                        log.debug("started verification");
                        HttpServletRequest request = exchange.getIn()
                                .getHeader(Exchange.HTTP_SERVLET_REQUEST, HttpServletRequest.class);
                        if ("subscribe".equals(request.getParameter("hub.mode"))
                                && "TEST-BEANCOUNTER-FACEBOOK"
                                .equals(request.getParameter("hub.verify_token"))) {
                            exchange.getOut().setBody(request.getParameter("hub.challenge"));
                        }
                        log.debug("hub.mode [" + request.getParameter("hub.mode") + "] - hub.verify_token ["
                                + request.getParameter("hub.verify_token") + "]");
                    }
                });

        from("direct:streaming")
                .process(new Processor() {
                    @Override
                    public void process(Exchange exchange) throws Exception {
                        HttpServletRequest request = exchange.getIn()
                                .getHeader(Exchange.HTTP_SERVLET_REQUEST, HttpServletRequest.class);

                        String[] parts = request.getRequestURL().toString().split("/");
                        String appName = parts[parts.length - 1];
                        log.debug("Notification for app: {}", appName);

                        Application app = appManager.get(appName);
                        if (app == null) {
                            throw new RuntimeException("App " + appName + " not found");
                        }

                        String payload = exchange.getIn().getBody(String.class);
                        log.debug("Received payload: {}", payload);
                        verifySignature(request.getHeader(X_HUB_SIGNATURE), payload, app.getFacebookAppSecret());
                        FacebookNotification notification = mapper.readValue(payload, FacebookNotification.class);

                        List<Activity> activities = activityConverter.getActivities(appName, notification);
                        log.debug("Have " + activities.size() + " activities for app: " + appName);
                        for (Activity a : activities) {
                            a.setApplicationName(appName);
                        }

                        exchange.getIn().setBody(activities);
                    }
                })
                .split(body())
                .marshal().json(JsonLibrary.Jackson)
                .log(body().toString())
                .to(toKestrelQueue());


        //for monitoring the process
        from("jetty:http://0.0.0.0:34591/facebook/ping")
                .transform(constant("PONG\n"));

    }

    protected String fromFacebookEndpoint() {
        return "jetty:http://0.0.0.0:" + SERVICE_PORT +"/facebook/?matchOnUriPrefix=true";
    }

    protected String toKestrelQueue() {
        return "kestrel://{{kestrel.queue.social.url}}";
    }

    protected String errorEndpoint() {
        return "log:" + getClass().getSimpleName() + "?{{camel.log.options.error}}";
    }

    protected String fromRegisterChannel() {
        // TODO (med) this should be configurable and not embedded
        log.info("Registering Subscribe to appchanges");
        return "redis://localhost:6379?command=SUBSCRIBE&channels=appchanges&serializer=#serializer";
    }

    protected void verifySignature(String xHubSign, String payload, String secret) {
        Mac hmac;
        try {
            hmac = Mac.getInstance("HmacSHA1");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e.getMessage(), e);
        }

        String signature = null;
        try {
            hmac.init(new SecretKeySpec(secret.getBytes("UTF-8"), "HmacSHA1"));
            signature = Hex.encodeHexString(hmac.doFinal(payload.getBytes("UTF-8")));
            signature = "sha1=" + signature;
        } catch (InvalidKeyException|UnsupportedEncodingException e) {
            log.error(e.getMessage(), e);
        }

        if (signature == null || !signature.equals(xHubSign)) {
            String msg = "Invalid signature: " + xHubSign + " expected: " + signature;
            log.error(msg);
            throw new RuntimeException(msg);
        }
    }
}
