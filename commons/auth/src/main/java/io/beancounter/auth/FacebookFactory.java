package io.beancounter.auth;

import com.restfb.FacebookClient;

/**
 * Facebook client factory interface
 */
public interface FacebookFactory {

    /**
     * Create a new Facebook client with provided access token.
     */
    public FacebookClient create(String accessToken);
}
