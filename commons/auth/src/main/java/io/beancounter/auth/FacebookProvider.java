package io.beancounter.auth;

import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;

/**
 * Default implementation of {@link FacebookFactory}.
 */
public class FacebookProvider implements FacebookFactory {
    @Override
    public FacebookClient create(String accessToken) {
        return new DefaultFacebookClient(accessToken);
    }
}
