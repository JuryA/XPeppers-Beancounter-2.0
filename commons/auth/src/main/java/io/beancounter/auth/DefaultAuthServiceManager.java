package io.beancounter.auth;

import io.beancounter.commons.model.AuthServiceConfig;
import io.beancounter.commons.model.User;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.Properties;

import com.google.inject.Inject;

/**
 * @author Davide Palmisano ( dpalmisano@gmail.com )
 */
public final class DefaultAuthServiceManager extends AbstractAuthServiceManager {

    /** prefix in a properties config file */
    public static final String CFG_PREFIX = "service";

    @Inject
    public DefaultAuthServiceManager(Map<AuthServiceConfig, AuthHandler> authHandlers) {
        for (Map.Entry<AuthServiceConfig, AuthHandler> handler : authHandlers.entrySet()) {
            handlers.put(handler.getKey(), handler.getValue());
        }
    }

    public User register(User user, String service, String token)
            throws AuthServiceManagerException
    {
        AuthHandler handler = getHandler(service);
        try {
            return handler.auth(user, token);
        } catch (AuthHandlerException e) {
            throw new AuthServiceManagerException(
                    "Error while authorizing user [" + user.getId() +
                    "] from [" + service + "]", e);
        }
    }

    public static AuthServiceConfig buildService(String service, Properties props) {
        AuthServiceConfig cfg = new AuthServiceConfig(service);
//        cfg.setDescription(property(props, service, "description"));
//        cfg.setApikey(property(props, service, "apikey"));
//        cfg.setSecret(property(props, service, "secret"));
        cfg.setOAuth2Callback(property(props, service, "oauth2callback"));
//
//        try {
//            cfg.setEndpoint(new URL(property(props, service, "endpoint")));
//        } catch (MalformedURLException e) {
//            throw new RuntimeException("Invalid endpoint URL for " + service, e);
//        }
//
//        String sessUrl = property(props, true, service, "session");
//        if (sessUrl != null) {
//            try {
//                cfg.setSessionEndpoint(new URL(sessUrl));
//            } catch (MalformedURLException e) {
//                throw new RuntimeException("Invalid session endpoint URL: " + sessUrl, e);
//            }
//        }

        return cfg;
    }

    private static String property(Properties properties, String... names) {
        return property(properties, false, names);
    }

    private static String property(Properties props, boolean optional, String... names) {
        String key = CFG_PREFIX;
        for(String name : names) {
            key += "." + name;
        }
        String result = props.getProperty(key);
        if(!optional && result == null) {
            throw new RuntimeException("["+ key + "] is null");
        }
        return result;
    }
}
