package io.beancounter.auth;

import io.beancounter.commons.model.AuthServiceConfig;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Davide Palmisano ( dpalmisano@gmail.com )
 */
public abstract class AbstractAuthServiceManager implements AuthServiceManager {

    protected Map<AuthServiceConfig, AuthHandler> handlers = new HashMap<AuthServiceConfig, AuthHandler>();

    public void addHandler(AuthServiceConfig service, AuthHandler handler)
            throws AuthServiceManagerException
    {
        handlers.put(service, handler);
    }

    public AuthHandler getHandler(String service)
            throws AuthServiceManagerException
    {
        for(AuthServiceConfig cfg: handlers.keySet()) {
            if(cfg.getName().equals(service)) {
                return handlers.get(cfg);
            }
        }
        throw new AuthServiceManagerException("service config for '" + service + "' not found");
    }

    public List<AuthServiceConfig> getServices()
            throws AuthServiceManagerException
    {
        return new ArrayList<>(handlers.keySet());
    }

    public AuthServiceConfig getService(String serviceName)
            throws AuthServiceManagerException
    {
        for(AuthServiceConfig config: handlers.keySet()) {
            if(config.getName().equals(serviceName)) {
                return config;
            }
        }
        return null;
    }
}
