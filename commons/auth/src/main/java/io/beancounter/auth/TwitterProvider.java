package io.beancounter.auth;

import com.google.inject.Provider;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Twitter client provider
 */
public class TwitterProvider implements Provider<Twitter> {

    private TwitterFactory factory;

    public TwitterProvider() {
        Configuration conf = new ConfigurationBuilder().setUseSSL(true).build();
        factory = new TwitterFactory(conf);
    }

    @Override
    public Twitter get() {
        return factory.getInstance();
    }
}
