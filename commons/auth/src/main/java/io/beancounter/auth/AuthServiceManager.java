package io.beancounter.auth;

import io.beancounter.commons.model.AuthServiceConfig;
import io.beancounter.commons.model.User;

import java.util.List;

/**
 * @author Davide Palmisano ( dpalmisano@gmail.com )
 */
public interface AuthServiceManager {

    /**
     * Return an authorized user.
     *
     * @param user
     * @param service
     * @param token
     * @return
     * @throws AuthServiceManagerException
     *
     */
    public User register(User user, String service, String token)
            throws AuthServiceManagerException;

    /**
     *
     * @param service
     * @param handler
     * @throws AuthServiceManagerException
     */
    public void addHandler(AuthServiceConfig service, AuthHandler handler)
            throws AuthServiceManagerException;

    /**
     *
     * @param service
     * @return
     * @throws AuthServiceManagerException
     */
    public AuthHandler getHandler(String service)
            throws AuthServiceManagerException;

    /**
     *
     * @return
     * @throws AuthServiceManagerException
     */
    public List<AuthServiceConfig> getServices()
            throws AuthServiceManagerException;

    /**
     *
     * @param serviceName
     * @return
     * @throws AuthServiceManagerException
     */
    public AuthServiceConfig getService(String serviceName)
            throws AuthServiceManagerException;


}
