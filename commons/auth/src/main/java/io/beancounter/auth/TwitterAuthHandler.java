package io.beancounter.auth;

import com.google.inject.Provider;
import io.beancounter.applications.ApplicationsManager;
import io.beancounter.applications.ApplicationsManagerException;
import io.beancounter.commons.helper.UriUtils;
import io.beancounter.commons.model.Application;
import io.beancounter.commons.model.auth.OAuthToken;
import io.beancounter.commons.model.AuthServiceConfig;
import io.beancounter.commons.model.User;
import io.beancounter.commons.model.activity.Activity;
import io.beancounter.commons.model.activity.Context;
import io.beancounter.commons.model.activity.Tweet;
import io.beancounter.commons.model.activity.Verb;
import io.beancounter.commons.model.auth.ServiceUser;
import io.beancounter.commons.model.auth.OAuthAuth;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.EncoderException;
import org.joda.time.DateTime;
import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.TwitterApi;
import org.scribe.model.Token;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import twitter4j.HashtagEntity;
import twitter4j.Paging;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.URLEntity;
import twitter4j.auth.AccessToken;

import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * put class description here
 *
 * @author Davide Palmisano ( dpalmisano@gmail.com )
 */
public class TwitterAuthHandler extends BaseAuthHandler {

    protected static final Logger LOGGER =
            LoggerFactory.getLogger(TwitterAuthHandler.class);

    public static final String SERVICE = "twitter";
    public static final String CHANNEL = "appchanges";
    public static final String TWITTER_BASE_URL = "http://twitter.com/";

    @Inject
    private RequestStateManager reqStateManager;

    @Inject
    private Provider<Twitter> twitterProvider;

    @Inject
    public TwitterAuthHandler(
            @Named("service.twitter") AuthServiceConfig config,
            Provider<ServiceBuilder> serviceBuilderProvider,
            ApplicationsManager appManager
    ) {
        super(config, serviceBuilderProvider, appManager);
    }

    @Override
    public User auth(User user, String token) throws AuthHandlerException {
        throw new AuthHandlerException("Twitter OAuth MUST have a token and a verifier");
    }

    @Override
    public ServiceUser auth(String verifier) throws AuthHandlerException {
        throw new AuthHandlerException("Twitter OAuth MUST have a token and a verifier");
    }

    @Override
    public ServiceUser authWithCustomer(String verifier, String customer)
            throws AuthHandlerException {
        throw new AuthHandlerException("Twitter OAuth MUST have a token and a verifier");
    }
    
    @Override
    public ServiceUser authWithRedirect(String verifier, String finalRedirect)
            throws AuthHandlerException {
        throw new AuthHandlerException("Twitter OAuth MUST have a token and a verifier");
    }

    @Override
    @Deprecated
    public ServiceUser auth(User user, String token, String verifier)
            throws AuthHandlerException
    {
        Token accessToken = getAccessToken(token, verifier, null, null);
        twitter4j.User twitterUser = getTwitterUser(user.getCustomer(), accessToken);
        if (twitterUser == null)
            throw new AuthHandlerException("Unable to fetch twitter user");

        setAuthMetadata(user, twitterUser);
        String twitterId = String.valueOf(twitterUser.getId());
        OAuthAuth auth = new OAuthAuth(accessToken.getToken(), accessToken.getSecret());
        user.addService(config.getName(), auth);

        notifySignup(twitterId);

        return new ServiceUser(twitterId, user);
    }

    @Override
    @Deprecated
    public ServiceUser auth(String token, String verifier) throws AuthHandlerException {
        Token accessToken = getAccessToken(token, verifier, null, null);
        twitter4j.User twitterUser = getTwitterUser((Application) null, accessToken);
        if (twitterUser == null)
            throw new AuthHandlerException("Unable to fetch twitter user");

        // Users created in this way will have beancounter username equals
        // to their Twitter id.
        // TODO (high) implement a retry policy to be sure it's unique
        String twitterId = String.valueOf(twitterUser.getId());
        User user = new User();
        user.setUsername(twitterId);
        setAuthMetadata(user, twitterUser);

        OAuthAuth auth = new OAuthAuth(accessToken.getToken(), accessToken.getSecret());
        user.addService(config.getName(), auth);
        notifySignup(twitterId);

        return new ServiceUser(twitterId, user);
    }

    @Override
    public ServiceUser authWithCustomer(String token,
                                        String verifier,
                                        String customer)
            throws AuthHandlerException
    {
        return auth(token, verifier);
    }

    /**
     * Counterpart of {@link #getTokenWithState} used during the callback
     * phase.
     *
     * @param token OAuth 1.0a token param
     * @param verifier OAuth 1.0a verifier param.
     * @param application A merchant name, e.g. "xpeppers".
     * @return An authenticated user object on success.
     *
     * @throws AuthHandlerException Mostly when third party (e.g. twitter)
     * responded with an error during token/code verification.
     */
    @Override
    public ServiceUser auth2WithCustomer(
            String token,
            String verifier,
            String application
    ) throws AuthHandlerException {
        Application app;
        try {
            app = appManager.get(application);
        } catch (ApplicationsManagerException e) {
            throw new AuthHandlerException(e.getMessage(), e);
        }

        if (app == null)
            throw new AuthHandlerException("Invalid application: " + application);

        Token accessToken = getAccessToken(
                token,
                verifier,
                app.getTwitterConsumerKey(),
                app.getTwitterConsumerSecret());

        twitter4j.User twitterUser = getTwitterUser(app, accessToken);
        if (twitterUser == null)
            throw new AuthHandlerException("Unable to fetch twitter user");

        // Users created in this way will have beancounter username equals
        // to their Twitter id.
        // TODO (high) implement a retry policy to be sure it's unique
        String twitterId = String.valueOf(twitterUser.getId());
        User user = new User();
        user.setUsername(twitterId);
        setAuthMetadata(user, twitterUser);

        OAuthAuth auth = new OAuthAuth(accessToken.getToken(), accessToken.getSecret());
        user.addService(config.getName(), auth);
        notifySignup(application);

        ServiceUser srvUser = new ServiceUser(twitterId, user);
        srvUser.setUserCustomer(application);
        return srvUser;
    }
    @Override
    public void notifySignup(String application) {
        reqStateManager.notify(CHANNEL, application);
    }

    @Override
    public OAuthToken getToken() throws AuthHandlerException {
        return createOAuthServiceAndGetToken(config.getAtomicOAuthCallback().toString(), null, null);
    }

    @Override
    public OAuthToken getTokenWithCustomer(String customer)
            throws AuthHandlerException {
        return createOAuthServiceAndGetToken(config.getAtomicOAuthCallbackWithCustomer(customer).toString(), null, null);
    }
    
    @Override
    public OAuthToken getToken(URL finalRedirectUrl) throws AuthHandlerException {
        return getTokenWithCustomer(finalRedirectUrl, null);
    }
    

    @Override
    public OAuthToken getTokenWithCustomer(URL finalRedirectUrl, String customer)
            throws AuthHandlerException {
        String encodedFinalRedirect;
        try {
            encodedFinalRedirect = UriUtils.encodeBase64(finalRedirectUrl.toString());
        } catch (UnsupportedEncodingException uee) {
            throw new AuthHandlerException("UTF-8 encoding is not supported on this system.", uee);
        } catch (EncoderException eex) {
            throw new AuthHandlerException(
                    "Error while encoding final redirect URL [" + finalRedirectUrl + "].",
                    eex
            );
        }

        String callback = config.getAtomicOAuthCallbackWithCustomer(customer) + "web/" + encodedFinalRedirect;

        return createOAuthServiceAndGetToken(callback, null, null);
    }

    /**
     * Same as {@link #getTokenWithCustomer} but also requires a state ID, which
     * would normally used to mitigate CSRFs.
     *
     * @param stateId A state ID. For OAuth providers that do not natively
     *                support {@code state} param, this will be embedded
     *                into the redirect URL.
     * @param application A merchant name, e.g. "xpeppers".
     * @return A token with a non-null state ID.
     * @throws AuthHandlerException
     */
    @Override
    public OAuthToken getTokenWithState(String stateId, String application)
    throws AuthHandlerException {
        String callback = config.getOAuth2Callback(application, stateId);
        return createOAuthServiceAndGetToken(callback, stateId, application);
    }

    @Override
    public OAuthToken getToken(String username) throws AuthHandlerException {
        return createOAuthServiceAndGetToken(config.getOAuthCallback() + username, null, null);
    }

    @Override
    public OAuthToken getToken(String username, URL callback) throws AuthHandlerException {
        return createOAuthServiceAndGetToken(callback + username, null, null);
    }

    @Override
    public String getService() {
        return SERVICE;
    }

    @Override
    public List<Activity> grabActivities(String application, OAuthAuth auth, String username, int limit)
            throws AuthHandlerException
    {
        Twitter client = getTwitterClient(application, auth);
        if (client == null)
            return new ArrayList<>();

        Paging paging = new Paging(1, limit);
        ResponseList<Status> statuses;
        try {
            statuses = client.getUserTimeline("", paging);
        } catch (TwitterException e) {
            final String errMsg = "error while getting tweets for user [" + username + "]";
            LOGGER.error(errMsg, e);
            throw new AuthHandlerException(errMsg, e);
        }
        return toActivities(statuses);
    }

    private List<Activity> toActivities(ResponseList<Status> statuses) {
        List<Activity> activities = new ArrayList<>();
        for(Status status : statuses) {
            Activity activity;
            try {
                activity = toActivity(status);
            } catch (AuthHandlerException e) {
                // just log and skip
                LOGGER.error("error while converting this tweet {} to a beancounter activity", status, e);
                continue;
            }
            activities.add(activity);
        }
        return activities;
    }

    private Activity toActivity(Status status) throws AuthHandlerException {
        URL tweetUrl;
        String tweetUrlCandidate = TWITTER_BASE_URL + status.getUser().getName() + "/status/" + status.getId();
        try {
            tweetUrl = new URL(tweetUrlCandidate);
        } catch (MalformedURLException e) {
            throw new AuthHandlerException("error [" + tweetUrlCandidate + "] is ill-formed", e);
        }
        io.beancounter.commons.model.activity.Tweet tweet = new Tweet();
        tweet.setUrl(tweetUrl);
        tweet.setText(status.getText());
        for(HashtagEntity ht : status.getHashtagEntities()) {
            tweet.addHashTag(ht.getText());
        }
        for(URLEntity urlEntity : status.getURLEntities()) {
            tweet.addUrl(urlEntity.getExpandedURL());
        }
        Context context = new Context();
        context.setService("twitter");
        context.setUsername(status.getUser().getName());
        context.setDate(new DateTime(status.getCreatedAt().getTime()));
        Activity a = new Activity();
        a.setVerb(Verb.TWEET);
        a.setObject(tweet);
        a.setContext(context);
        return a;
    }

    /**
     * Requests auth request token, stores it in internal database with default
     * expiration time.
     *
     * @param callback Where to redirect the user after authorization phase.
     * @param stateId Original auth request state ID. Can be null.
     * @param application Merchant name, e.g. "xpeppers".
     *
     * @return Auth token object with both redirect and state ID (if not null).
     * @throws AuthHandlerException
     */
    private OAuthToken createOAuthServiceAndGetToken(
            String callback,
            String stateId,
            String application
    ) throws AuthHandlerException {
        Application app;
        try {
            app = appManager.get(application);
        } catch (ApplicationsManagerException e) {
            throw new AuthHandlerException(e.getMessage());
        }

        if (app == null)
            throw new AuthHandlerException("Invalid application: " + application);

        OAuthService twitterOAuth = serviceBuilderProvider.get()
                .provider(TwitterApi.Authenticate.class)
                .apiKey(app.getTwitterConsumerKey())
                .apiSecret(app.getTwitterConsumerSecret())
                .callback(callback)
                .build();

        Token token = twitterOAuth.getRequestToken();

        try {
            reqStateManager.put(token.getToken(), token.getSecret());
        } catch (RequestStateException e) {
            throw new AuthHandlerException(e.getMessage(), e);
        }

        String redirectUrl = twitterOAuth.getAuthorizationUrl(token);

        try {
            return new OAuthToken(new URL(redirectUrl), stateId);
        } catch (MalformedURLException e) {
            throw new AuthHandlerException("Invalid redirect URL: " + redirectUrl, e);
        }
    }

    private Token getAccessToken(
            String token,
            String verifier,
            String consumerKey,
            String consumerSecret
    ) throws AuthHandlerException {
        String secret = reqStateManager.popSecret(token);
        if (secret == null)
            throw new AuthHandlerException("Invalid request token: " + token);

        OAuthService twitterOAuth = serviceBuilderProvider.get()
                .provider(TwitterApi.Authenticate.class)
                .apiKey(consumerKey)
                .apiSecret(consumerSecret)
                .build();

        Token requestToken = new Token(token, secret);
        return twitterOAuth.getAccessToken(requestToken, new Verifier(verifier));
    }

    @Override
    public ServiceUser authWithRedirectAndCustomer(String verifier,
            String decodedFinalRedirect, String customer) throws AuthHandlerException {
        throw new AuthHandlerException("Twitter OAuth MUST have a token and a verifier");
    }

    private void setAuthMetadata(User user, twitter4j.User twitterUser) {
        user.addMetadata("twitter.user.id", String.valueOf(twitterUser.getId()));
        user.addMetadata("twitter.user.name", twitterUser.getName());
        user.addMetadata("twitter.user.screenName", twitterUser.getScreenName());
        user.addMetadata("twitter.user.description", twitterUser.getDescription());
        user.addMetadata("twitter.user.imageUrl", twitterUser.getProfileImageURL());
        user.addMetadata("twitter.user.location", twitterUser.getLocation());
        user.addMetadata("twitter.user.followers", Integer.toString(twitterUser.getFollowersCount()));
        user.addMetadata("twitter.user.following", Integer.toString(twitterUser.getFriendsCount()));
        user.addMetadata("twitter.user.statusesCount", Integer.toString(twitterUser.getStatusesCount()));
        user.addMetadata("twitter.user.listedByCount", Integer.toString(twitterUser.getListedCount()));
        user.addMetadata("twitter.user.creationDate", twitterUser.getCreatedAt().toString());
    }

    private twitter4j.User getTwitterUser(String application, Token accessToken)
            throws AuthHandlerException
    {
        Application app = null;
        try {
            app = appManager.get(application);
        } catch (ApplicationsManagerException e) {
            LOGGER.error("Invalid app: " + application, e);
        }
        return app != null ? getTwitterUser(app, accessToken) : null;
    }

    private twitter4j.User getTwitterUser(Application app, Token accessToken)
            throws AuthHandlerException
    {
        twitter4j.User twitterUser;

        try {
            Twitter client = getTwitterClient(app, accessToken);
            if (client == null)
                return null;
            twitterUser = client.verifyCredentials();
        } catch (TwitterException twe) {
            // When Twitter service or network is unavailable,
            // or if supplied credentials are wrong.
            String message = (twe.getStatusCode() == 401)
                    ? "OAuth credentials are invalid."
                    : "Error connecting to Twitter. StatusCode: " + twe.getStatusCode();
            throw new AuthHandlerException(message, twe);
        }

        return twitterUser;
    }

    Twitter getTwitterClient(String application, Token token) {
        Application app = null;
        try {
            app = appManager.get(application);
        } catch (ApplicationsManagerException e) {
            LOGGER.error("Error creating Twitter client: invalid app " + application, e);
        }
        return app != null ? getTwitterClient(app, token) : null;
    }

    Twitter getTwitterClient(Application app, Token token) {
        Twitter client = twitterProvider.get();
        client.setOAuthConsumer(app.getTwitterConsumerKey(), app.getTwitterConsumerSecret());
        if (token != null)
            client.setOAuthAccessToken(new AccessToken(token.getToken(), token.getSecret()));

        return client;
    }

    Twitter getTwitterClient(String application, OAuthAuth auth) {
        return getTwitterClient(application, new Token(auth.getSession(), auth.getSecret()));
    }
}