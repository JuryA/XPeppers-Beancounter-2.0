package io.beancounter.auth;

import com.google.inject.Provider;
import io.beancounter.applications.ApplicationsManager;
import io.beancounter.applications.ApplicationsManagerException;
import io.beancounter.commons.helper.UriUtils;
import io.beancounter.commons.helper.reflection.ReflectionHelper;
import io.beancounter.commons.helper.reflection.ReflectionHelperException;
import io.beancounter.commons.http.HttpClientProvider;
import io.beancounter.commons.model.Application;
import io.beancounter.commons.model.AuthServiceConfig;
import io.beancounter.commons.model.auth.OAuthToken;
import io.beancounter.commons.model.User;
import io.beancounter.commons.model.activity.Activity;
import io.beancounter.commons.model.activity.Context;
import io.beancounter.commons.model.activity.Object;
import io.beancounter.commons.model.activity.Verb;
import io.beancounter.commons.model.auth.ServiceUser;
import io.beancounter.commons.model.auth.OAuthAuth;
import io.beancounter.listener.facebook.core.FacebookUtils;
import io.beancounter.listener.facebook.core.converter.custom.ConverterException;
import io.beancounter.listener.facebook.core.converter.custom.FacebookLikeConverter;
import io.beancounter.listener.facebook.core.converter.custom.FacebookShareConverter;
import io.beancounter.listener.facebook.core.model.FacebookData;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.EncoderException;
import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.FacebookApi;
import org.scribe.model.Token;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.types.Page;
import com.restfb.types.Post;

/**
 * put class description here
 *
 * @author Davide Palmisano ( dpalmisano@gmail.com )
 */
public class FacebookAuthHandler extends BaseAuthHandler {
    private static final Logger LOGGER =
            LoggerFactory.getLogger(FacebookAuthHandler.class);

    public final static String SERVICE_NAME = "facebook";

    public static final String SCOPE =
            "email,user_likes,user_location,user_interests,user_activities," +
            "read_stream,user_birthday,user_location,publish_actions";

    public static final String FETCH_ME_FIELDS =
            "id, name, first_name, middle_name, last_name, age_range, " +
            "picture, gender, email, link, birthday, location";

    public static final String FETCH_LOCATION_FIELDS = "location, name";

    @Inject
    private FacebookFactory clientFactory;

    @Inject
    public FacebookAuthHandler(
            @Named("service.facebook") AuthServiceConfig config,
            Provider<ServiceBuilder> serviceBuilderProvider,
            ApplicationsManager appManager
    ) {
        super(config, serviceBuilderProvider, appManager);
    }

    public User auth(User user, String token) throws AuthHandlerException {
        throw new AuthHandlerException("Facebook OAuth MUST have a verifier");
    }

    public ServiceUser auth(User user, String token, String verifier)
            throws AuthHandlerException {
        if (verifier == null) {
            auth(user, token);
        }
        Verifier v = new Verifier(verifier);
        OAuthService facebookOAuth = serviceBuilderProvider.get()
                .provider(FacebookApi.class)
                .apiKey(config.getApikey())
                .apiSecret(config.getSecret())
                .scope(SCOPE)
                .callback(config.getOAuthCallback() + user.getUsername())
                .build();
        Token requestToken = null;
        Token accessToken = facebookOAuth.getAccessToken(requestToken, v);
        user.addService(config.getName(), new OAuthAuth(
                accessToken.getToken(), accessToken.getSecret()));
        Map<String, String> data = getUserData(accessToken.getToken());
        for (String k : data.keySet()) {
            user.addMetadata(k, data.get(k));
        }
        String facebookUserId = getUserId(accessToken.getToken());
        return new ServiceUser(facebookUserId, user);
    }

    private String getUserId(String token) {
        FacebookClient client = clientFactory.create(token);
        com.restfb.types.User user = client.fetchObject("me",
                com.restfb.types.User.class);
        return user.getId();
    }

    public OAuthToken getToken(String username) throws AuthHandlerException {
        OAuthService facebookOAuth = serviceBuilderProvider.get()
                .provider(FacebookApi.class)
                .apiKey(config.getApikey())
                .apiSecret(config.getSecret())
                .scope(SCOPE)
                .callback(config.getOAuthCallback() + username).build();
        Token token = null;
        String redirectUrl = facebookOAuth.getAuthorizationUrl(token);
        try {
            return new OAuthToken(new URL(redirectUrl));
        } catch (MalformedURLException e) {
            throw new AuthHandlerException(
                    "The redirect url is not well formed", e);
        }
    }

    @Override
    public OAuthToken getToken() throws AuthHandlerException {
        return getTokenWithCustomer(null);
    }

    @Override
    public OAuthToken getTokenWithCustomer(String customer) throws AuthHandlerException {
        OAuthService facebookOAuth = serviceBuilderProvider.get()
                .provider(FacebookApi.class)
                .apiKey(config.getApikey())
                .apiSecret(config.getSecret())
                .scope(SCOPE)
                .callback(config.getAtomicOAuthCallbackWithCustomer(customer).toString()).build();
        Token token = null;
        String redirectUrl = facebookOAuth.getAuthorizationUrl(token);
        try {
            return new OAuthToken(new URL(redirectUrl));
        } catch (MalformedURLException e) {
            throw new AuthHandlerException(
                    "The redirect url is not well formed", e);
        }
    }

    public OAuthToken getToken(URL finalRedirectUrl)
            throws AuthHandlerException {
        return getTokenWithCustomer(finalRedirectUrl, null);
    }

    @Override
    public OAuthToken getTokenWithCustomer(URL finalRedirectUrl, String customer) throws AuthHandlerException {
        String encodedRedirect;
        try {
            encodedRedirect = UriUtils
                    .encodeBase64(finalRedirectUrl.toString());
        } catch (UnsupportedEncodingException uee) {
            throw new AuthHandlerException(
                    "UTF-8 encoding is not supported on this system.", uee);
        } catch (EncoderException eex) {
            throw new AuthHandlerException(
                    "Error while encoding final redirect URL ["
                            + finalRedirectUrl + "].", eex);
        }

        OAuthService facebookOAuth = serviceBuilderProvider.get()
                .provider(FacebookApi.class)
                .apiKey(config.getApikey())
                .apiSecret(config.getSecret())
                .scope(SCOPE)
                .callback(config.getAtomicOAuthCallbackWithCustomer(customer).toString() + "web/" + encodedRedirect).build();
        Token token = null;
        String redirectUrl = facebookOAuth.getAuthorizationUrl(token);
        try {
            return new OAuthToken(new URL(redirectUrl));
        } catch (MalformedURLException e) {
            throw new AuthHandlerException(
                    "The redirect url is not well formed", e);
        }
    }

    /**
     * Same as {@link #getTokenWithCustomer} but also requires a state ID, which
     * would normally used to mitigate CSRFs.
     *
     * @param stateId A state ID. For OAuth providers that do not natively
     *                support {@code state} param, this will be embedded
     *                into the redirect URL.
     * @param application A merchant name, e.g. "xpeppers".
     * @return A token with a non-null state ID.
     * @throws AuthHandlerException
     */
    @Override
    public OAuthToken getTokenWithState(String stateId, String application)
            throws AuthHandlerException
    {
        Application app;
        try {
            app = appManager.get(application);
        } catch (ApplicationsManagerException e) {
            throw new AuthHandlerException(e.getMessage());
        }

        if (app == null)
            throw new AuthHandlerException("Invalid application: " + application);

        OAuthService auth = serviceBuilderProvider.get()
            .provider(FacebookApi.class)
            .apiKey(app.getFacebookAppId())
            .apiSecret(app.getFacebookAppSecret())
            .scope(SCOPE)
            .callback(config.getOAuth2Callback(application)).build();
        String redirectUrl = auth.getAuthorizationUrl(null);
        try {
            // Unfortunately Scribe does not support OAuth 2.0 state parameter.
            // See this for more details:
            // https://github.com/fernandezpablo85/scribe-java/pull/128
            redirectUrl += "&state=" + URLEncoder.encode(stateId, "UTF-8");
            return new OAuthToken(new URL(redirectUrl), stateId);
        } catch (MalformedURLException e) {
            throw new AuthHandlerException(
                "Invalid redirect URL: " + redirectUrl, e);
        } catch (UnsupportedEncodingException e) {
            throw new AuthHandlerException(
                "Could not encode state parameter", e);
        }
    }

    @Override
    public OAuthToken getToken(String username, URL callback)
            throws AuthHandlerException {
        OAuthService facebookOAuth;
        facebookOAuth = serviceBuilderProvider.get()
                .provider(FacebookApi.class)
                .apiKey(config.getApikey())
                .apiSecret(config.getSecret())
                .scope(SCOPE)
                .callback(callback + username).build();
        Token token = null;
        String redirectUrl = facebookOAuth.getAuthorizationUrl(token);
        try {
            return new OAuthToken(new URL(redirectUrl));
        } catch (MalformedURLException e) {
            throw new AuthHandlerException(
                    "The redirect url is not well formed", e);
        }
    }

    @Override
    public ServiceUser auth(String verifier) throws AuthHandlerException {
        return authWithCustomer(verifier, null);
    }
    @Override
    public ServiceUser authWithCustomer(String verifier, String customer) throws AuthHandlerException {
        Verifier v = new Verifier(verifier);
        String callback = config.getAtomicOAuthCallbackWithCustomer(customer).toString();
        OAuthService facebookOAuth = serviceBuilderProvider.get()
                .provider(FacebookApi.class)
                .apiKey(config.getApikey())
                .apiSecret(config.getSecret())
                .scope(SCOPE)
                .callback(callback).build();
        Token requestToken = null;
        Token accessToken = facebookOAuth.getAccessToken(requestToken, v);
        Map<String, String> data;
        try {
            data = getUserData(accessToken.getToken());
        } catch (Exception e) {
            final String errMsg = "Error while getting data for anonymous user";
            throw new AuthHandlerException(errMsg, e);
        }
        User user = createNewUser(data);
        user.addService(config.getName(), new OAuthAuth(
                accessToken.getToken(), accessToken.getSecret()));
        return new ServiceUser(data.get("facebook.user.id"), user);
    }


    @Override
    public ServiceUser authWithRedirect(String verifier,
            String finalRedirect) throws AuthHandlerException {
        return authWithRedirectAndCustomer(verifier, finalRedirect, null);
    }

    @Override
    public ServiceUser authWithRedirectAndCustomer(String verifier,
            String finalRedirect, String customer) throws AuthHandlerException {
        if (verifier == null) {
            auth((User) null, null);
        }

        String encodedRedirect;
        try {
            encodedRedirect = UriUtils.encodeBase64(finalRedirect);
        } catch (UnsupportedEncodingException uee) {
            throw new AuthHandlerException(
                    "UTF-8 encoding is not supported on this system.", uee);
        } catch (EncoderException eex) {
            throw new AuthHandlerException(
                    "Error while encoding final redirect URL [" + finalRedirect
                            + "].", eex);
        }

        Verifier v = new Verifier(verifier);
        OAuthService facebookOAuth = serviceBuilderProvider.get()
                .provider(FacebookApi.class)
                .apiKey(config.getApikey())
                .apiSecret(config.getSecret())
                .scope(SCOPE)
                .callback(
                        config.getAtomicOAuthCallbackWithCustomer(customer).toString() + "web/"
                                + encodedRedirect).build();
        Token requestToken = null;
        Token accessToken = facebookOAuth.getAccessToken(requestToken, v);
        Map<String, String> data;
        try {
            data = getUserData(accessToken.getToken());
        } catch (Exception e) {
        	LOGGER.error("Error getting user data: " + e.getMessage());
            final String errMsg = "Error while getting data for anonymous user";
            throw new AuthHandlerException(errMsg, e);
        }
        User user = createNewUser(data);
        user.addService(config.getName(), new OAuthAuth(
                accessToken.getToken(), accessToken.getSecret()));
        return new ServiceUser(data.get("facebook.user.id"), user);
    }

    @Override
    public void notifySignup(String application) {
        LOGGER.debug("Unused Signup Notifcation for application: {} ", application);
    }

    @Override
    public ServiceUser auth(String token, String verifier)
            throws AuthHandlerException {
        return auth(verifier);
    }

    @Override
    public ServiceUser authWithCustomer(String token, String verifier,
            String customer) throws AuthHandlerException {
        return authWithCustomer(verifier, customer);
    }

    /**
     * Counterpart of {@link #getTokenWithState} used during the callback
     * phase.
     *
     * Uses oauth2 callback patterns.
     *
     * @param ignored Not used in OAuth 2.0
     * @param verifier OAuth 2.0 code parameter.
     * @param application A merchant name, e.g. "xpeppers".
     * @return An authenticated user object with customer set on success.
     *
     * @throws AuthHandlerException Mostly when third party (e.g. facebook)
     * responded with an error during token/code verification.
     */
    @Override
    public ServiceUser auth2WithCustomer(
            String ignored,
            String verifier,
            String application
    ) throws AuthHandlerException {
        Application app;
        try {
            app = appManager.get(application);
        } catch (ApplicationsManagerException e) {
            throw new AuthHandlerException(e.getMessage());
        }

        if (app == null)
            throw new AuthHandlerException("Invalid application: " + application);

        OAuthService auth = serviceBuilderProvider.get()
                .provider(FacebookApi.class)
                .apiKey(app.getFacebookAppId())
                .apiSecret(app.getFacebookAppSecret())
                .scope(SCOPE)
                .callback(config.getOAuth2Callback(application))
                .build();

        Verifier v = new Verifier(verifier);
        Token atok = auth.getAccessToken(null, v);

        Map<String, String> data;
        try {
            data = getUserData(atok.getToken());
        } catch (Exception e) {
            final String errMsg = "Error while getting data for anonymous user";
            throw new AuthHandlerException(errMsg, e);
        }

        User user = createNewUser(data);
        OAuthAuth accessToken = new OAuthAuth(atok.getToken(), atok.getSecret());
        user.addService(config.getName(), accessToken);

        ServiceUser srvUser = new ServiceUser(data.get("facebook.user.id"), user);
        srvUser.setUserCustomer(application);
        return srvUser;
    }

    @Override
    public List<Activity> grabActivities(String application, OAuthAuth auth, String identifier,
            int limit) throws AuthHandlerException {
        FacebookClient client = clientFactory.create(auth.getSession());
        // grab shares
        Collection<Post> posts = FacebookUtils.fetch(Post.class, client,
                "feed", limit);
        FacebookShareConverter shareConverter = new FacebookShareConverter();
        List<Activity> result = new ArrayList<Activity>();
        for (Post post : posts) {
            io.beancounter.commons.model.activity.Object object;
            Context context;
            try {
                object = shareConverter.convert(post, true);
                context = shareConverter.getContext(post, identifier);
            } catch (ConverterException e) {
                // just log and skip
                LOGGER.warn(
                        "error while converting Facebook POST from user {}",
                        identifier, e);
                continue;
            }
            Activity activity = toActivity(object, Verb.SHARE);
            activity.setContext(context);
            result.add(activity);
        }
        // grab likes
        Collection<FacebookData> likes = FacebookUtils.fetch(
                FacebookData.class, client, "likes", limit);
        FacebookLikeConverter likeConverter = new FacebookLikeConverter(new HttpClientProvider());
        for (FacebookData like : likes) {
            io.beancounter.commons.model.activity.Object object;
            Context context;
            try {
                object = likeConverter.convert(like, true);
                context = likeConverter.getContext(like, identifier);
            } catch (ConverterException e) {
                // just log and skip
                LOGGER.warn(
                        "error while converting Facebook LIKE from user {}",
                        identifier, e);
                continue;
            }
            Activity activity = toActivity(object, Verb.LIKE);
            activity.setContext(context);
            result.add(activity);
        }
        return result;
    }

    private Activity toActivity(Object object, Verb verb) {
        Activity activity = new Activity();
        activity.setVerb(verb);
        activity.setObject(object);
        return activity;
    }

    private Map<String, String> getUserData(String token)
            throws AuthHandlerException
    {
        FacebookClient client = clientFactory.create(token);
        
        LOGGER.info("Fetching Facebook user fields with token: " + token);    
        CustomFacebookUser user = client.fetchObject(
                "me",
                CustomFacebookUser.class,
                Parameter.with("fields", FETCH_ME_FIELDS));

        setUserLocationData(user);

        LOGGER.debug("Facebook User: " + user.toString());
        ReflectionHelper.Access[] fields;
        try {
            // let's grab only primitives and String
            fields = ReflectionHelper.getGetters(CustomFacebookUser.class,
                    true, String.class);
        } catch (ReflectionHelperException e) {
            final String errMsg = "Error while getting getters from ["
                    + CustomFacebookUser.class.getName() + "]";
            throw new AuthHandlerException(errMsg, e);
        }
        Map<String, String> data = new HashMap<String, String>();
        for (ReflectionHelper.Access field : fields) {
            String key = "facebook.user." + field.getField();
            String value;
            try {
                value = ReflectionHelper.invokeAndCast(field.getMethod(), user,
                        String.class);
            } catch (ReflectionHelperException e) {
                final String errMsg = "Error while invoking ["
                        + field.getMethod() + "] from ["
                        + CustomFacebookUser.class.getName() + "]";
                throw new AuthHandlerException(errMsg, e);
            }
            data.put(key, value);
        }
        return data;
    }

    private void setUserLocationData(CustomFacebookUser user) {
        String locationId = user.getLocationId(); 
        if (locationId != null) {
            FacebookClient client = clientFactory.create(null);
        	Page locationPage = client.fetchObject(
                    locationId,
                    Page.class,
                    Parameter.with("fields", FETCH_LOCATION_FIELDS));
        	user.setLocation(locationPage);
        }
    }

    private User createNewUser(Map<String, String> data) {
        // users created in this way will have beancounter username equals
        // to the facebook one.
        // TODO (high) implement a retry policy to be sure it's unique
        String candidateBCUsername = String.valueOf(data
                .get("facebook.user.id"));
        User user = new User();
        user.setUsername(candidateBCUsername);
        for (String k : data.keySet()) {
            user.addMetadata(k, data.get(k));
        }
        return user;
    }

    @Override
    public String getService() {
        return SERVICE_NAME;
    }

}
