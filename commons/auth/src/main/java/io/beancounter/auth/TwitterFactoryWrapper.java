package io.beancounter.auth;

import twitter4j.Twitter;
import twitter4j.TwitterFactory;

import com.google.inject.Inject;

/**
 * @deprecated Use {@link TwitterProvider}
 */
@Deprecated
public class TwitterFactoryWrapper {

    private TwitterFactory twitterFactory;

    @Inject
    public TwitterFactoryWrapper(TwitterFactory twitterFactory) {
        this.twitterFactory = twitterFactory;
    }

    public Twitter getInstance() {
        return twitterFactory.getInstance();
    }
}
