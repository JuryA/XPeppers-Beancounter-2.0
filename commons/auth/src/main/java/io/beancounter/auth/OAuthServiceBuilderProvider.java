package io.beancounter.auth;

import com.google.inject.Provider;
import org.scribe.builder.ServiceBuilder;

/**
 * Provider for an OAuth service builder.
 * Currently uses Scribe library.
 */
public class OAuthServiceBuilderProvider implements Provider<ServiceBuilder> {
    @Override
    public ServiceBuilder get() {
        return new ServiceBuilder();
    }
}
