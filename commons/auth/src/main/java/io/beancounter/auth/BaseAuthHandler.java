package io.beancounter.auth;

import com.google.inject.Provider;
import io.beancounter.applications.ApplicationsManager;
import io.beancounter.commons.model.AuthServiceConfig;
import org.scribe.builder.ServiceBuilder;

/**
 * Base class for real Auth handler implementations like facebook and twitter.
 */
public abstract class BaseAuthHandler implements AuthHandler {

    /** AuthServiceConfig holds settings of a single social network */
    protected AuthServiceConfig config;

    /** Auth service builder provider */
    protected Provider<ServiceBuilder> serviceBuilderProvider;

    /** Merchant apps manager */
    protected ApplicationsManager appManager;

    public BaseAuthHandler(
            AuthServiceConfig config,
            Provider<ServiceBuilder> serviceBuilderProvider,
            ApplicationsManager appManager)
    {
        this.config = config;
        this.serviceBuilderProvider = serviceBuilderProvider;
        this.appManager = appManager;
    }

}
