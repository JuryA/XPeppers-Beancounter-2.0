package io.beancounter.auth;

import java.io.IOException;

import com.restfb.Facebook;
import com.restfb.json.JsonObject;
import com.restfb.json.JsonTokener;
import com.restfb.types.Location;
import com.restfb.types.Page;

/**
 * This class is intended to wrap user data from <i>Facebook Response</i>.
 *
 * @see <a href="https://developers.facebook.com/docs/reference/api/user/">Facebook field list</a>.
 *
 * @author Davide Palmisano ( dpalmisano@gmail.com )
 */
public class CustomFacebookUser {

    @Facebook
    private String id;

    @Facebook("name")
    private String name;
    
    @Facebook("first_name")
    private String firstName;
    
    @Facebook("middle_name")
    private String middleName;

    @Facebook
    private String picture;

    @Facebook("last_name")
    private String lastName;

    @Facebook("email")
    private String email;

    @Facebook("age_range")
    private String ageRange;
    
    @Facebook("link")
    private String link;
    
    @Facebook
    private String gender;
    
    @Facebook("birthday")
    private String birthday;

    @Facebook("location")
    private JsonObject location;

    private String locationName;

    private String locationLatitude;

    private String locationLongitude;

    public JsonObject getLocation() {
        return location;
    }

    public void setLocation(JsonObject location) {
        this.location = location;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getAgeRange() {
        return ageRange;
    }

    public void setAgeRange(String ageRange) {
        this.ageRange = ageRange;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPicture() {
        JsonObject jsonObject = new JsonObject(new JsonTokener(picture));
        return jsonObject.getJsonObject("data").getString("url");
    }

    public void setPicture(String picture) throws IOException {
        this.picture = picture;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getLocationLatitude() {
        return locationLatitude;
    }

    public void setLocationLatitude(Double locationLatitude) {
        this.locationLatitude = locationLatitude.toString();
    }

    public String getLocationLongitude() {
        return locationLongitude;
    }

    public void setLocationLongitude(Double locationLongitude) {
        this.locationLongitude = locationLongitude.toString();
    }

    @Override
    public String toString() {
        return "CustomFacebookUser{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", firstName='" + firstName + '\'' +
                ", picture='" + picture + '\'' +
                ", lastName='" + lastName + '\'' +
                ", gender='" + gender + '\'' +
                ", email='" + email + '\'' +
                ", link='" + link + '\'' +
                ", ageRange='" + ageRange + '\'' +
                ", birthday='" + birthday + '\'' +
                ", locationName='" + locationName + '\'' +
                ", locationLongitude='" + locationLongitude + '\'' +
                ", locationLatitude='" + locationLatitude + '\'' +
                '}';
    }

	public String getLocationId() {
		if (this.location != null)
			return location.getString("id");
		return null;
	}

	public void setLocation(Page locationPage) {
		Location location = locationPage.getLocation();
		if (this.location != null) {
			this.locationName = this.location.getString("name");
		}
        this.locationLatitude = location.getLatitude().toString();
        this.locationLongitude = location.getLongitude().toString();		
	}
}