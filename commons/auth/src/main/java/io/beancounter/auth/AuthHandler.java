package io.beancounter.auth;

import io.beancounter.commons.model.auth.OAuthToken;
import io.beancounter.commons.model.User;
import io.beancounter.commons.model.activity.Activity;
import io.beancounter.commons.model.auth.OAuthAuth;
import io.beancounter.commons.model.auth.ServiceUser;

import java.net.URL;
import java.util.List;

/**
 * @author Davide Palmisano ( dpalmisano@gmail.com )
 */
public interface AuthHandler {

    public User auth(User user, String token) throws AuthHandlerException;

    /**
     * To handle <i>OAuth</i> callbacks.
     *
     * @param user
     * @param token
     * @param verifier
     * @return
     * @throws AuthHandlerException
     */
    public ServiceUser auth(
            User user,
            String token,
            String verifier
    ) throws AuthHandlerException;

    public ServiceUser auth(
            String token,
            String verifier
    ) throws AuthHandlerException;

    public ServiceUser authWithCustomer(String token, String verifier,
            String customer) throws AuthHandlerException;
    /**
     * To handle anonymous <i>OAuth</i> callbacks.
     *
     * @param verifier
     * @return
     */
    public ServiceUser auth(String verifier) throws AuthHandlerException;

    public ServiceUser authWithCustomer(String verifier, String customer) throws AuthHandlerException;

    
    /**
     * Totally equivalent to auth(String verifier) but it allows custom redirect url.
     *
     * @param verifier
     * @param finalRedirect
     * @return
     * @throws AuthHandlerException
     */
    public ServiceUser authWithRedirect(String verifier, String finalRedirect)
            throws AuthHandlerException;

    /**
     * Counterpart of {@link #getTokenWithState} used during the callback
     * phase.
     *
     * Uses oauth2 callback patterns.
     *
     * @param token OAuth 1.0a token. Specify null for OAuth 2.0
     * @param verifier Either OAuth 1.0a verifier or OAuth 2.0 code.
     * @param application A merchant name, e.g. "xpeppers".
     * @return An authenticated user object on success.
     *
     * @throws AuthHandlerException Mostly when third party (e.g. facebook)
     * responded with an error during token/code verification.
     */
    public ServiceUser auth2WithCustomer(String token, String verifier, String application)
            throws AuthHandlerException;

    /**
     * @return
     * @throws AuthHandlerException
     */
    public OAuthToken getToken() throws AuthHandlerException;

    /**
     * @param customer
     * @return
     * @throws AuthHandlerException
     */
    public OAuthToken getTokenWithCustomer(String customer) throws AuthHandlerException;

    /**
     * Same as {@link #getTokenWithCustomer} but also requires a state ID, which
     * would normally used to mitigate CSRFs.
     *
     * @param stateId A state ID. For OAuth providers that do not natively
     *                support {@code state} param, this will be embedded
     *                into the redirect URL.
     * @param customer A merchant name, e.g. "xpeppers".
     * @return A token with a non-null state ID.
     * @throws AuthHandlerException
     */
    public OAuthToken getTokenWithState(String stateId, String customer)
            throws AuthHandlerException;
    
    /**
     * @param username
     * @return
     * @throws AuthHandlerException
     */
    public OAuthToken getToken(String username) throws AuthHandlerException;

    /**
     * Totally equivalent to OAuthToken getToken() but it allows a final redirect url.
     *
     * @param finalRedirectUrl
     * @return
     * @throws AuthHandlerException
     */
    public OAuthToken getToken(URL finalRedirectUrl) throws AuthHandlerException;

    /**
     * @param username
     * @param callback a custom callback, which overrides the {@link io
     *                 .beancounter.commons.model.AuthServiceConfig} one.
     * @return
     * @throws AuthHandlerException
     */
    public OAuthToken getToken(String username, URL callback) throws AuthHandlerException;

    /**
     * Returns the service url which this authWithRedirect serves.
     *
     * @return
     */
    public String getService();

    /**
     * To grab an initial bunch of user activities.
     *
     * @param auth
     * @param identifier
     * @param limit
     * @return
     * @throws AuthHandlerException
     */
    public List<Activity> grabActivities(String application, OAuthAuth auth, String identifier, int limit)
            throws AuthHandlerException;

    public OAuthToken getTokenWithCustomer(URL finalRedirectUrl, String customer)
            throws AuthHandlerException;

    public ServiceUser authWithRedirectAndCustomer(String verifier,
                                                   String decodedFinalRedirect,
                                                   String customer)
            throws AuthHandlerException;

    void notifySignup(String application);
}
