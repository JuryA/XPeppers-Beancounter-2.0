package io.beancounter.auth;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import io.beancounter.commons.helper.jackson.ObjectMapperFactory;
import io.beancounter.commons.helper.jedis.JedisPoolFactory;

import io.beancounter.commons.model.auth.RequestState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.Response;
import redis.clients.jedis.Transaction;
import redis.clients.jedis.exceptions.JedisConnectionException;

import java.util.UUID;

/**
 * An implementation that uses Redis as the backend storage.
 *
 * @author alex@cloudware.it
 */
public class JedisRequestStateManager implements RequestStateManager {
    /** This class default logger. */
    private static Logger LOGGER =
        LoggerFactory.getLogger(JedisRequestStateManager.class);

    /** Redis server connection pool. */
    private static JedisPool pool;

    /** (De-)serializer for storing data in Redis db. */
    private ObjectMapper mapper;

    /** Redis database number for RequestState data. */
    @Inject
    @Named("redis.db.requestTokens") private int reqStateDb;

    @Inject
    public JedisRequestStateManager(JedisPoolFactory jedisPoolFactory) {
        pool = jedisPoolFactory.build();
        mapper = ObjectMapperFactory.createMapper();
    }

    /**
     * Pull existing state from the backend db.
     *
     * @param stateId State ID. Cannot be null.
     * @return Null if there's no state by that ID.
     */
    @Override
    public RequestState pop(String stateId) {
        Jedis jedis = getJedis();
        if (jedis == null) return null;

        String json = null;
        boolean isConnectionIssue = false;
        try {
            Transaction txn = jedis.multi();
            Response<String> resp = txn.get(stateId);
            txn.del(stateId);
            txn.exec();
            json = resp.get();
        } catch (JedisConnectionException jce) {
            LOGGER.error("Redis connection error", jce);
            isConnectionIssue = true;
            pool.returnBrokenResource(jedis);
        } catch (Exception e) {
            LOGGER.error("Error fetching state: " + stateId, e);
        } finally {
            if (!isConnectionIssue)
                pool.returnResource(jedis);
        }

        RequestState state = null;
        if (json != null)
            try {
                state = mapper.readValue(json, RequestState.class);
            } catch (Exception e) {
                LOGGER.error("State deserializing error", e);
            }
        return state;
    }

    /**
     * Store the state in the backend db.
     * @param state If you need to update a state field,
     *              remove existing state and created a new one.
     * @return State ID or null if the state could not be created.
     */
    @Override
    public String put(RequestState state) {
        return put(state, DEFAULT_EXPIRE);
    }

    /**
     * Same as {@link #put(RequestState)} but with custom expire date.
     *
     * @param expire Exiration time in seconds, e.g. {@code 1800}.
     * @return State ID or null if the state could not be created.
     */
    @Override
    public String put(RequestState state, int expire) {
        String json;
        try {
            json = mapper.writeValueAsString(state);
        } catch (JsonProcessingException e) {
            LOGGER.error("JSON marshaling error of " + state, e);
            return null;
        }

        Jedis jedis = getJedis();
        if (jedis == null) return null;

        String stateId = null;
        boolean isConnectionIssue = false;
        try {
            int retries = 0;
            while (retries++ < 3) {
                stateId = UUID.randomUUID().toString();
                if (jedis.setnx(stateId, json) == 1L) {
                    jedis.expire(stateId, expire);
                    break;
                }
                LOGGER.warn("Request state put retry: " + retries);
                stateId = null;
            }
        } catch (JedisConnectionException jce) {
            LOGGER.error("Redis connection error", jce);
            isConnectionIssue = true;
            stateId = null;
            pool.returnBrokenResource(jedis);
        } catch (Exception e) {
            LOGGER.error("Error storing state: " + stateId, e);
            stateId = null;
        } finally {
            if (!isConnectionIssue)
                pool.returnResource(jedis);
        }

        return stateId;
    }

    /**
     * Store a request secret under the token key.
     *
     * @param token A request token.
     * @param secret Request token secret.
     * @param expire token expiration time, in seconds.
     */
    @Override
    public void put(String token, String secret, int expire)
            throws RequestStateException
    {
        Jedis jedis = getJedis();
        if (jedis == null)
            throw new RequestStateException("Unnable to connect to storage backend");

        boolean isConnectionIssue = false;

        try {

            if (jedis.setnx(token, secret) != 1L)
                throw new RequestStateException("Token already exists");
            jedis.expire(token, expire);

        } catch (JedisConnectionException jce) {
            isConnectionIssue = true;
            LOGGER.error("Redis connection error", jce);
            pool.returnBrokenResource(jedis);
        } catch (Exception e) {
            LOGGER.error("Error storing token: " + token, e);
        } finally {
            if (!isConnectionIssue)
                pool.returnResource(jedis);
        }
    }

    /**
     * Same as {@link #put(String, String, int)} but using default
     * expiration time.
     *
     * @param token A request token.
     * @param secret Request token secret.
     */
    @Override
    public void put(String token, String secret)
            throws RequestStateException
    {
        put(token, secret, DEFAULT_EXPIRE);
    }

    /**
     * Pops existing request token from a storage backend.
     *
     * @param token A request token.
     * @return {@code null} if token not found or there was an error in storage
     * backend.
     */
    @Override
    public String popSecret(String token) {
        Jedis jedis = getJedis();
        if (jedis == null)
            return null;

        String secret = null;
        boolean isConnectionIssue = false;
        try {

            Transaction txn = jedis.multi();
            Response<String> resp = txn.get(token);
            txn.del(token);
            txn.exec();
            if (resp != null)
                secret = resp.get();

        } catch (JedisConnectionException jce) {
            isConnectionIssue = true;
            LOGGER.error("Redis connection error", jce);
            pool.returnBrokenResource(jedis);
        } catch (Exception e) {
            LOGGER.error("Error fetching token secret: " + token, e);
        } finally {
            if (!isConnectionIssue)
                pool.returnResource(jedis);
        }

        return secret;
    }

    /**
     * Publish login action of a user identified by {@code value}.
     *
     * @param channel A channel name/ID
     * @param value Third party value to be notified.
     */
    @Override
    public void notify(String channel, String value) {
        final String errMsg = "Cannot notify [" + value + "] on [" + channel + "]: ";

        Jedis jedis = getJedis();
        if (jedis == null) {
            LOGGER.error(errMsg + "connection error");
            return;
        }

        boolean isConnectionIssue = false;
        try {
            jedis.publish(channel, value);
        } catch (JedisConnectionException e) {
            isConnectionIssue = true;
            LOGGER.error(errMsg + e.getMessage(), e);
            pool.returnBrokenResource(jedis);
        } catch (Exception e) {
            LOGGER.error(errMsg + e.getMessage(), e);
        } finally {
            if (!isConnectionIssue)
                pool.returnResource(jedis);
        }
    }

    /**
     * Get a redis connection from the pool.
     *
     * @return Null if no resource is available, connection errors or any other
     * issues.
     */
    private Jedis getJedis() {
        Jedis jedis = null;

        try {
            jedis = pool.getResource();
            jedis.select(reqStateDb);
            return jedis;

        } catch (JedisConnectionException e) {
            LOGGER.error("Jedis connection error [database="+reqStateDb+"]", e);
            if (jedis != null)
                pool.returnBrokenResource(jedis);
        } catch (Exception e) {
            if (jedis != null)
                pool.returnResource(jedis);
            LOGGER.error("Error while getting a Jedis resource", e);
        }

        return null;
    }
}
