package io.beancounter.auth;

/**
 * @author Davide Palmisano ( dpalmisano@gmail.com )
 */
public class AuthServiceManagerException extends Exception {

    public AuthServiceManagerException(String message, Exception e) {
        super(message, e);
    }

    public AuthServiceManagerException(String message) {
        super(message);
    }
}
