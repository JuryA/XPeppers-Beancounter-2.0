package io.beancounter.auth;

import io.beancounter.commons.model.AuthServiceConfig;
import io.beancounter.commons.model.User;

import java.util.HashMap;
import java.util.List;

/**
 * @author Enrico Candino ( enrico.candino@gmail.com )
 */
public class MockAuthServiceManager implements AuthServiceManager {

    @Override
    public User register(User user, String service, String token) throws AuthServiceManagerException {
        throw new UnsupportedOperationException("NIY");
    }

    @Override
    public void addHandler(AuthServiceConfig service, AuthHandler handler) throws AuthServiceManagerException {
        throw new UnsupportedOperationException("NIY");
    }

    @Override
    public AuthHandler getHandler(String service) throws AuthServiceManagerException {
        throw new UnsupportedOperationException("NIY");
    }

    @Override
    public List<AuthServiceConfig> getServices() throws AuthServiceManagerException {
        throw new UnsupportedOperationException("NIY");
    }

    @Override
    public synchronized AuthServiceConfig getService(String serviceName) throws AuthServiceManagerException {
        HashMap<String, AuthServiceConfig> services = new HashMap<String, AuthServiceConfig>();
        AuthServiceConfig s1 = new AuthServiceConfig("fake-oauth-service");
        AuthServiceConfig s2 = new AuthServiceConfig("fake-simple-service");
        services.put(s1.getName(), s1);
        services.put(s2.getName(), s2);
        return services.get(serviceName);
    }
}