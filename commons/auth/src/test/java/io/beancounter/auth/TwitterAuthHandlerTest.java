package io.beancounter.auth;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

import com.google.inject.*;
import com.google.inject.name.Names;
import io.beancounter.applications.ApplicationsManager;
import io.beancounter.commons.model.Application;
import io.beancounter.commons.model.AuthServiceConfig;

import java.net.URL;

import io.beancounter.commons.model.auth.OAuthToken;
import io.beancounter.commons.model.auth.ServiceUser;
import org.mockito.ArgumentCaptor;
import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.TwitterApi;
import org.scribe.model.Token;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import twitter4j.Twitter;
import twitter4j.auth.AccessToken;

public class TwitterAuthHandlerTest {
    public final static String APP_NAME = "chumhum";
    public final static String CONSUMER_KEY = "twitter-key";
    public final static String CONSUMER_SECRET = "twitter-secret";

    private ApplicationsManager appManager;
    private RequestStateManager reqStateManager;

    private Application app;
    private OAuthService auth;
    private ServiceBuilder builder;
    private AuthServiceConfig config;
    private Twitter client;

    private TwitterAuthHandler handler;

    @BeforeMethod
    public void setUp() throws Exception {
        config = new AuthServiceConfig("twitter");
        config.setEndpoint(new URL("https://api.twitter.com/1/statuses/user_timeline.json"));
        config.setSessionEndpoint(new URL("https://api.twitter.com/oauth/request_token"));
        config.setOAuth2Callback("http://beancounter.io/auth/twitter/callback");

        auth = mock(OAuthService.class);
        builder = mock(ServiceBuilder.class);
        when(builder.provider(TwitterApi.Authenticate.class)).thenReturn(builder);
        when(builder.apiKey(any(String.class))).thenReturn(builder);
        when(builder.apiSecret(any(String.class))).thenReturn(builder);
        when(builder.callback(any(String.class))).thenReturn(builder);
        when(builder.build()).thenReturn(auth);

        client = mock(Twitter.class);
        reqStateManager = mock(RequestStateManager.class);

        app = new Application(APP_NAME, "Test app", "chumhum@example.org");
        app.setTwitterConsumerKey(CONSUMER_KEY);
        app.setTwitterConsumerSecret(CONSUMER_SECRET);

        appManager = mock(ApplicationsManager.class);
        when(appManager.get(any(String.class))).thenReturn(app);

        handler = Guice.createInjector(new Module() {
            @Override
            public void configure(Binder binder) {
                binder.bind(AuthServiceConfig.class).annotatedWith(Names.named("service.twitter")).toInstance(config);
                binder.bind(ApplicationsManager.class).toInstance(appManager);
                binder.bind(RequestStateManager.class).toInstance(reqStateManager);
                binder.bind(Twitter.class).toProvider(new Provider<Twitter>() {
                    @Override public Twitter get() { return client; }
                });
                binder.bind(ServiceBuilder.class).toProvider(new Provider<ServiceBuilder>() {
                    @Override public ServiceBuilder get() { return builder; }
                });
            }
        }).getInstance(TwitterAuthHandler.class);
    }

    @Test
    public void testGetTokenWithState() throws Exception {
        final Token reqToken = new Token("tok", "secret");
        final String redirectUrl = "http://twitter.com/auth";
        final String stateId = "state-123";
        final String callback = config.getOAuth2Callback(APP_NAME, stateId);
        when(auth.getRequestToken()).thenReturn(reqToken);
        when(auth.getAuthorizationUrl(any(Token.class))).thenReturn(redirectUrl);

        OAuthToken authToken = handler.getTokenWithState(stateId, APP_NAME);

        assertEquals(authToken.getStateId(), stateId);
        assertEquals(authToken.getRedirectPage().toString(), redirectUrl);
        verify(builder).apiKey(CONSUMER_KEY);
        verify(builder).apiSecret(CONSUMER_SECRET);
        verify(builder).callback(callback);
        verify(reqStateManager).put(reqToken.getToken(), reqToken.getSecret());
        verify(appManager).get(APP_NAME);
    }

    @Test
    public void testAuth2WithCustomer() throws Exception {
        final String reqToken = "req-token-123";
        final String reqSecret = "req-secret";
        final String reqVerifier = "code-12345";
        final String twUserId = "11223344";
        final String accTokenId = twUserId + "-access-tok";
        final String accTokenSecret = "access-secret";
        final Token accessToken = new Token(accTokenId, accTokenSecret);
        when(reqStateManager.popSecret(anyString())).thenReturn(reqSecret);

        twitter4j.User twUser = mock(twitter4j.User.class);
        when(twUser.getId()).thenReturn(new Long(twUserId));
        when(twUser.getCreatedAt()).thenReturn(new java.util.Date());
        when(client.verifyCredentials()).thenReturn(twUser);

        ArgumentCaptor<Token> tokenArg = ArgumentCaptor.forClass(Token.class);
        ArgumentCaptor<Verifier> verifierArg = ArgumentCaptor.forClass(Verifier.class);
        doReturn(accessToken).when(auth).getAccessToken(tokenArg.capture(), verifierArg.capture());

        ServiceUser srvUser = handler.auth2WithCustomer(reqToken, reqVerifier, APP_NAME);

        assertNotNull(srvUser);
        assertEquals(srvUser.getId(), twUserId);
        assertEquals(verifierArg.getValue().getValue(), reqVerifier);
        assertEquals(tokenArg.getValue().getToken(), reqToken);
        assertEquals(tokenArg.getValue().getSecret(), reqSecret);
        verify(client).setOAuthConsumer(CONSUMER_KEY, CONSUMER_SECRET);
        verify(client).setOAuthAccessToken(new AccessToken(accTokenId, "access-secret"));
        verify(reqStateManager).popSecret(reqToken);
        verify(reqStateManager).notify(TwitterAuthHandler.CHANNEL, APP_NAME);
        verify(appManager).get(APP_NAME);
    }
}
