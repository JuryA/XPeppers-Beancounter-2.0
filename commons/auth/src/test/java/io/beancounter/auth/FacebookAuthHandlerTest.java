package io.beancounter.auth;

import com.google.inject.Binder;
import com.google.inject.Guice;
import com.google.inject.Module;
import com.google.inject.Provider;
import com.google.inject.name.Names;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import io.beancounter.applications.ApplicationsManager;
import io.beancounter.commons.model.Application;
import io.beancounter.commons.model.AuthServiceConfig;
import io.beancounter.commons.model.auth.OAuthToken;
import io.beancounter.commons.model.auth.ServiceUser;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.mockito.ArgumentCaptor;
import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.Api;
import org.scribe.builder.api.FacebookApi;
import org.scribe.model.Token;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.net.URL;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

/**
 * Tests for {@link FacebookAuthHandlerTest}.
 *
 * @author alex@cloudware.it
 */
public class FacebookAuthHandlerTest {
    public static final String APP_NAME = "chumhum";
    public static final String FB_CLIENT_ID = "fb-client-123";
    public static final String FB_CLIENT_SECRET = "fb-client-secret";


    private ApplicationsManager appManager;
    private Application app;

    private AuthServiceConfig config;
    private OAuthService auth;
    private ServiceBuilder builder;
    private FacebookClient client;

    private FacebookAuthHandler handler;

    @BeforeMethod
    public void setUp() throws Exception {
        config = new AuthServiceConfig("facebook");
        config.setOAuth2Callback("http://beancounter.io/facebook/callback/{customer}");

        auth = mock(OAuthService.class);
        builder = mock(ServiceBuilder.class);
        when(builder.provider(FacebookApi.class)).thenReturn(builder);
        when(builder.apiKey(anyString())).thenReturn(builder);
        when(builder.apiSecret(anyString())).thenReturn(builder);
        when(builder.scope(anyString())).thenReturn(builder);
        when(builder.callback(anyString())).thenReturn(builder);
        when(builder.build()).thenReturn(auth);

        app = new Application("chumhum", "Test app", "chumhum@example.org");
        app.setFacebookAppId(FB_CLIENT_ID);
        app.setFacebookAppSecret(FB_CLIENT_SECRET);

        appManager = mock(ApplicationsManager.class);
        when(appManager.get(any(String.class))).thenReturn(app);

        client = mock(FacebookClient.class);

        handler = Guice.createInjector(new Module() {
            @Override
            public void configure(Binder binder) {
                binder.bind(AuthServiceConfig.class).annotatedWith(Names.named("service.facebook")).toInstance(config);
                binder.bind(ApplicationsManager.class).toInstance(appManager);
                binder.bind(ServiceBuilder.class).toProvider(new Provider<ServiceBuilder>() {
                    @Override public ServiceBuilder get() { return builder; }
                });
                binder.bind(FacebookFactory.class).toInstance(new FacebookFactory() {
                    @Override
                    public FacebookClient create(String accessToken) {
                        return client;
                    }
                });
            }
        }).getInstance(FacebookAuthHandler.class);
    }

    @Test
    public void testGetTokenWithState() throws Exception {
        final String stateId = "state-12345";
        final String callback = config.getOAuth2Callback(APP_NAME);
        when(auth.getAuthorizationUrl(any(Token.class))).thenReturn("http://facebook.com/auth");

        OAuthToken token = handler.getTokenWithState(stateId, APP_NAME);

        assertNotNull(token);
        assertEquals(token.getStateId(), stateId);

        String redirectUrl = token.getRedirectPage().toString();
        assertTrue(redirectUrl.contains("state=" + stateId), redirectUrl);
        verify(builder).apiKey(FB_CLIENT_ID);
        verify(builder).apiSecret(FB_CLIENT_SECRET);
        verify(builder).callback(callback);
        verify(builder).scope(FacebookAuthHandler.SCOPE);
        verify(appManager).get(APP_NAME);
    }

    @Test
    public void testAuth2WithCustomer() throws Exception {
        final String code = "code-12345";
        final String callback = config.getOAuth2Callback(APP_NAME);
        final Token accToken = new Token(null, "access-token-secret");

        final String fbUserId = "fb-id-12345";
        final CustomFacebookUser fbUser = new CustomFacebookUser();
        fbUser.setId(fbUserId);
        fbUser.setPicture("{\"data\": {\"url\": \"http://facebook/photo.jpg\" }}");

        ArgumentCaptor<Token> tokenArg = ArgumentCaptor.forClass(Token.class);
        ArgumentCaptor<Verifier> verifierArg = ArgumentCaptor.forClass(Verifier.class);
        doReturn(accToken).when(auth).getAccessToken(tokenArg.capture(), verifierArg.capture());
        when(client.fetchObject(
                "me",
                CustomFacebookUser.class,
                Parameter.with("fields", FacebookAuthHandler.FETCH_ME_FIELDS))
        ).thenReturn(fbUser);

        ServiceUser srvUser = handler.auth2WithCustomer(null, code, APP_NAME);

        assertNotNull(srvUser);
        assertEquals(srvUser.getId(), fbUserId);
        assertNull(tokenArg.getValue());
        assertEquals(verifierArg.getValue().getValue(), code);
        verify(builder).apiKey(FB_CLIENT_ID);
        verify(builder).apiSecret(FB_CLIENT_SECRET);
        verify(builder).callback(callback);
        verify(builder).scope(FacebookAuthHandler.SCOPE);
        verify(appManager).get(APP_NAME);
    }
}
