package io.beancounter.commons.helper;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

/**
 * Util class that always knows the current time.
 * All models and services that need a timestamp should use this class
 * instead of inline DateTime instantiation.
 *
 * Useful for testing.
 *
 * @author alex@cloudware.it
 */
public class Clock {
    /**
     * Get current date/time in specified timezone.
     */
    public DateTime now(DateTimeZone zone) {
        return new DateTime(zone);
    }

    /**
     * Get current date/time in UTC.
     */
    public DateTime utcnow() {
        return now(DateTimeZone.UTC);
    }
}
