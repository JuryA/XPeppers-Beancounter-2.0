package io.beancounter.commons.helper;

import java.util.UUID;

/**
 * Util class that can generate new unique IDs.
 * All models that require a unique ID should use this class
 * instead of inline randomUUID invocation.
 *
 * Useful for testing.
 *
 * @author alex@cloudware.it
 */
public class IdGenerator {
    /**
     * Generates a pseudo random UUID.
     * Same as {@link java.util.UUID#randomUUID()}.
     *
     * @param scope Optional. Identifies whom the ID is being generated
     *              for. Models could use their class names to be
     *              distinguishable.
     */
    public UUID randomUUID(String scope) {
        return UUID.randomUUID();
    }

    /**
     * Same as {@link #randomUUID(String)} but uses {@code klass} name as
     * the {@code scope} param.
     *
     * @param klass Optional class whom the ID is being generated for.
     */
    public UUID randomUUID(Class klass) {
        String scope = null;
        if (klass != null)
            scope = klass.getName();
        return randomUUID(scope);
    }
}
