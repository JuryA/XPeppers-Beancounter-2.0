package io.beancounter.commons.http;

import com.google.inject.Provider;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;

public class HttpClientProvider implements Provider<HttpClient> {

    @Override
    public HttpClient get() {
        return new DefaultHttpClient();
    }
}
