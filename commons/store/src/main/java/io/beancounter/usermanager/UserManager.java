package io.beancounter.usermanager;

import io.beancounter.commons.model.auth.OAuthToken;
import io.beancounter.commons.model.User;
import io.beancounter.commons.model.activity.Activity;
import io.beancounter.commons.model.auth.AtomicSignUp;
import io.beancounter.commons.model.auth.ServiceUser;
import io.beancounter.commons.model.auth.RequestState;

import java.net.URL;
import java.util.Collection;
import java.util.List;

/**
 * Defines main responsabilities of a class handling users in
 * the <i>beancounter.io</i> ecosystem.
 *
 * @author Davide Palmisano ( dpalmisano@gmail.com )
 */
public interface UserManager {

    /**
     * Store a {@link User} on the Beancounter.
     */
    public void storeUser(User user)
            throws UserManagerException;

    /**
     * Retrieve a {@link User} from the Beancounter.
     */
    public User getUser(String username)
            throws UserManagerException;

    /**
     * Retrieve user by customer from the Beancounter.
     */
    public Collection<User> getUsersByCustomer(String customerName)
            throws UserManagerException;

    /**
     * Completely flushes out all the {@link User} data.
     */
    public void deleteUser(User user)
            throws UserManagerException;

    /**
     * Get the user <a href="http://oauth.net">OAuth</> token.
     */
    public OAuthToken getOAuthToken(String service, String username)
            throws UserManagerException;

    /**
     * @param url the custom callback. it overwrites the one in
     * {@link io.beancounter.commons.model.AuthServiceConfig}
     */
    public OAuthToken getOAuthToken(String serviceName, String username, URL url)
            throws UserManagerException;


    /**
     * It handles all the <i>OAuth-like</i> protocols handshaking.
     */
    public void registerService(String service, User user, String token)
            throws UserManagerException;

    /**
     * It handles all the <i>OAuth</i> protocol handshaking.
     */
    public ServiceUser registerOAuthService(
            String service,
            User user,
            String token,
            String verifier
    ) throws UserManagerException;

    /**
     * Removes from the provided {@link User} a service with the name
     * provided as input.
     */
    void deregisterService(String service, User userObj)
            throws UserManagerException;

    /**
     * This voids the current user OAuth token for the given service.
     */
    public void voidOAuthToken(User user, String service)
            throws UserManagerException;

    /**
     * This method asks for an {@link OAuthToken} without the needs of a
     * {@link User} username.
     */
    public OAuthToken getOAuthToken(String service)
            throws UserManagerException;

    /**
     * @throws UserManagerException
     */
    public OAuthToken getOAuthTokenWithCustomer(String srvName, String customer)
            throws UserManagerException;
    
    /**
     * Totally equivalent to #getOAuthToken(String service) but it allows a final
     * redirect user.
     */
    public OAuthToken getOAuthToken(String service, URL finalRedirectUrl)
            throws UserManagerException;

    /**
     * Totally equivalent to #getOAuthToken(String service) but it allows a final
     * redirect user.
     */
    public OAuthToken getOAuthTokenWithCustomer(
            String service,
            URL finalRedirectURL,
            String customer
    ) throws UserManagerException;


    /**
     * Alternative version of {@link #getOAuthTokenWithCustomer} with additional
     * info about the original user request.
     *
     * @param stateId A state that refers to the original user request.
     * @param service Third party identity provider, e.g. "facebook'.
     * @param customer A merchant name, e.g. "xpeppers".
     * @return A token with the originally provided {@code stateId}.
     * @throws UserManagerException
     */
    public OAuthToken getOAuthTokenWithState(
            String stateId,
            String service,
            String customer
    ) throws UserManagerException;
    
    /**
     * Counterpart of {@link #getOAuthTokenWithState}:
     *
     * verifies user credentials and creates a new user or fetches the
     * existing one.
     *
     * Supports both OAuth 1.0a and OAuth 2.0.
     *
     * @param service Third party service name, e.g. "facebook".
     * @param token OAuth 1.0a token param. Should be null for OAuth 2.0.
     * @param verifier Either verifier (1.0a) or code (2.0) param.
     * @param state The original auth request state.
     *
     * @return Already persisted user info on success.
     * @throws UserManagerException Mostly when third party errors off.
     */
    AtomicSignUp storeUserFromOAuthWithState(
            String service,
            String token,
            String verifier,
            RequestState state
    ) throws UserManagerException;

    /**
     * This method grabs the latest activities a user performed on a service.
     *
     * Deprecated since 1.7.0. The preferable method is to use the
     * ActivityGrabberManager.
     */
    @Deprecated
    public List<Activity> grabUserActivities(
            User user,
            String identifier,
            String service,
            int limit
    ) throws UserManagerException;

    public User updateUserWithOAuthCredentials(
            User existingUser,
            String service,
            ServiceUser srvUser
    ) throws UserManagerException;


    public void setUserEmail(User user, String email)
            throws UserManagerException;
}
