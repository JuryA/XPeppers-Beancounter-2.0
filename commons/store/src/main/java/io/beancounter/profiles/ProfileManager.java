package io.beancounter.profiles;

import io.beancounter.commons.model.ShortUserProfile;
import io.beancounter.commons.model.User;
import io.beancounter.commons.model.UserProfile;

import java.util.UUID;

/**
 * put class description here
 *
 * @author Davide Palmisano ( dpalmisano@gmail.com )
 */
public interface ProfileManager {

    /**
     * It always update regardless what's already there
     */
    public void store(UserProfile profile, User user)
            throws ProfileManagerException;

    /**
     * Fetch profile by the given user ID.
     */
    public UserProfile lookup(UUID userId)
            throws ProfileManagerException;

    /**
     * Fetch a shorter version of user profile by the given user ID.
     */
    public ShortUserProfile lookupShortProfile(UUID userId)
            throws ProfileManagerException;
}
