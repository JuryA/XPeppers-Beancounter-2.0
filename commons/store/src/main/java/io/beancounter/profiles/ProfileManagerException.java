package io.beancounter.profiles;

/**
 * put class description here
 *
 * @author Davide Palmisano ( dpalmisano@gmail.com )
 */
public class ProfileManagerException extends Exception {

    public ProfileManagerException(String message) {
        super(message);
    }

    public ProfileManagerException(String message, Exception e) {
        super(message, e);
    }

}
