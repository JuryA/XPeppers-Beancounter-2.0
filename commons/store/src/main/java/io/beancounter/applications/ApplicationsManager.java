package io.beancounter.applications;

import io.beancounter.commons.model.Application;

import java.util.UUID;

/**
 * This interface models the minimum behavior of a class
 * responsible to manage all the permissions of an application that
 * would be able to interact with the <i>beancounter.io</i> APIs.
 *
 * @author Davide Palmisano ( dpalmisano@gmail.com )
 */
public interface ApplicationsManager {

    /**
     * Creates a new application.
     */
    public Application registerApplication(String name, String description, String email)
            throws ApplicationsManagerException;

    /**
     * Removes existing application.
     */
    public boolean deregisterApplication(String name)
            throws ApplicationsManagerException;

    /**
     * Returns an {@link Application} by its name or {@code null} if not found.
     */
    public Application get(String name)
            throws ApplicationsManagerException;

    /**
     * Updates existing application.
     */
    public void update(Application app)
            throws ApplicationsManagerException;

    /**
     * Returns <code>true</code> if an application identified with the key
     * parameter, could perform the input action on that object type.
     */
    public boolean isAuthorized(String name, UUID key)
            throws ApplicationsManagerException;
}
