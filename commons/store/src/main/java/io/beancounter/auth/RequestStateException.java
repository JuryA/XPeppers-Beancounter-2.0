package io.beancounter.auth;

public class RequestStateException extends Exception {
    public RequestStateException(String message) {
        super(message);
    }

    public RequestStateException(String message, Throwable cause) {
        super(message, cause);
    }
}
