package io.beancounter.auth;

import io.beancounter.commons.model.auth.RequestState;

/**
 * Manages RequestState objects and OAuth 1.0 request tokens.
 *
 * @author alex@cloudware.it
 */
public interface RequestStateManager {
    /**
     * Default expiration time of request states and request token/secret,
     * in seconds.
     * TODO(alex): Make this configuration via beancounter.properties?
     */
    public final static int DEFAULT_EXPIRE = 3600;

    /**
     * Pops existing request state from a storage backend.
     * @param stateId State ID. Cannot be null.
     * @return Null if no state found by the provided state ID.
     */
    public RequestState pop(String stateId);

    /**
     * Store a RequestState in a storage backend with defaul expiration time
     * {@link #DEFAULT_EXPIRE}.
     *
     * @param state ID of the provided state is always ignored,
     *              even if not null. If you need to update a state field,
     *              remove existing state and created a new one.
     * @return State ID
     */
    public String put(RequestState state);

    /**
     * Same as {@link #put(RequestState)} but uses custom expiration time.
     * @param expire Exiration time in seconds, e.g. {@code 1800}.
     * @return State ID
     */
    public String put(RequestState state, int expire);

    /**
     * Store a request secret under the token key.
     *
     * @param token A request token.
     * @param secret Request token secret.
     * @param expire token expiration time, in seconds.
     */
    public void put(String token, String secret, int expire) throws RequestStateException;

    /**
     * Same as {@link #put(String, String, int)} but using default
     * expiration time.
     *
     * @param token A request token.
     * @param secret Request token secret.
     */
    public void put(String token, String secret) throws RequestStateException;

    /**
     * Pops existing request token from a storage backend.
     *
     * @param token A request token.
     * @return {@code null} if token not found or there was an error in storage
     * backend.
     */
    public String popSecret(String token);

    /**
     * Publish login action of a user identified by {@code serviceUserId}.
     *
     * @param channel A channel name/ID
     * @param serviceUserId Third party social network user ID.
     */
    public void notify(String channel, String serviceUserId);
}
