package io.beancounter.activities;

public class ActivityModifyException extends Exception {

    public ActivityModifyException(String message) {
        super(message);
    }

    public ActivityModifyException(String message, Exception e) {
        super(message, e);
    }

}