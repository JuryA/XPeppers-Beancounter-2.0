package io.beancounter.activities;

public class ActivityNullException extends Exception {
    
	public ActivityNullException(String message) {
        super(message);
    }

    public ActivityNullException(String message, Exception e) {
        super(message, e);
    }

}
