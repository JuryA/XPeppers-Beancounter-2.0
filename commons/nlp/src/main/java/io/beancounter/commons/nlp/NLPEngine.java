package io.beancounter.commons.nlp;

import java.net.URL;

/**
 * put class description here
 *
 * @author Davide Palmisano ( dpalmisano@gmail.com )
 */
public interface NLPEngine {

    /**
     * Maximum content size (in bytes) that we can handle.
     * Processing larger content may result in {@link NLPEngineException}.
     */
    public static final long MAX_BYTES_CONTENT = 1024*1024*5;

    public NLPEngineResult enrich(String text) throws NLPEngineException;

    public NLPEngineResult enrich(URL url) throws NLPEngineException;

}
