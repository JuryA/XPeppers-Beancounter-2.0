package io.beancounter.commons.model;

import static org.testng.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.Test;

public class UserTest {

    @Test
    public void setMetadataWhenPreviouslyEmpty() {
        User user = new User();
        Map<String, String> metadata = new HashMap<String, String>(){{
            put("key", "value");
        }};

        user.setMetadata(metadata);

        assertEquals(user.getMetadata(), metadata);
    }

    @Test
    public void setMetadataDoesnotOverridePreviousMetadata() {
        User user = new User();
        Map<String, String> previousMetadata = new HashMap<String, String>(){{
            put("key", "value");
        }};
        Map<String, String> newMetadata = new HashMap<String, String>(){{
            put("newkey", "newvalue");
        }};
        Map<String, String> expectedMetadata = new HashMap<String, String>(){{
            put("key", "value");
            put("newkey", "newvalue");
        }};

        user.setMetadata(previousMetadata);
        user.setMetadata(newMetadata);

        assertEquals(user.getMetadata(), expectedMetadata);
    }
    
    @Test
    public void setMetadataShouldOverrideValuesIfPreviouslySet() {
        User user = new User();
        Map<String, String> originalMetadata = new HashMap<String, String>(){{
            put("key", "value");
        }};
        Map<String, String> newMetadata = new HashMap<String, String>(){{
            put("key", "newvalue");
        }};
        Map<String, String> expectedMetadata = new HashMap<String, String>(){{
            put("key", "newvalue");
        }};

        user.setMetadata(originalMetadata);
        user.setMetadata(newMetadata);

        assertEquals(user.getMetadata(), expectedMetadata);
    }
}
