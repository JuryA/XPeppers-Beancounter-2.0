package io.beancounter.commons.model;

import org.joda.time.DateTime;
import org.joda.time.Years;
import org.testng.annotations.Test;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

import static org.testng.Assert.*;


public class ShortUserProfileTest {

    @Test
    public void clearTopicsInConstructor() {
        ShortUserProfile profile = new ShortUserProfile();
        assertNotNull(profile.getTopics());
        assertEquals(profile.getTopics().size(), 0);

        profile = new ShortUserProfile(UUID.randomUUID());
        assertNotNull(profile.getTopics());
        assertEquals(profile.getTopics().size(), 0);
    }

    @Test
    public void constructFromProfileAndUser() throws URISyntaxException {
        Map<String, String> meta = new HashMap<String, String>();
        meta.put("facebook.user.gender", "Other");
        meta.put("facebook.user.locationname", "Trento, Italy");
        meta.put("facebook.user.birthday", "02/10/1980");  // MM/dd/yyyy
        DateTime dob = new DateTime(1980, 2, 10, 0, 0);
        int age = Years.yearsBetween(dob, DateTime.now()).getYears();

        User user = new User("John", "Doe", "dude", "secret");
        user.setId(UUID.randomUUID());
        user.setMetadata(meta);
        user.setCustomer("xpeppers");

        UserProfile profile = new UserProfile(user.getId());
        profile.setVisibility(UserProfile.Visibility.PROTECTED);
        profile.setLastUpdated(DateTime.now());
        profile.setInterests(new HashSet<Interest>(Arrays.asList(
                new Interest(new URI("/int1"), "Int1", 0.5, true)
        )));
        profile.setCategories(new HashSet<Category>(Arrays.asList(
                new Category(new URI("/cat1"), "Cat1", 0.5, 1)
        )));

        ShortUserProfile shortProfile = new ShortUserProfile(profile, user);

        assertEquals(shortProfile.getUserId(), user.getId());
        assertEquals(shortProfile.getCustomer(), user.getCustomer());
        assertEquals(shortProfile.getUpdated(), profile.getLastUpdated());
        assertEquals(shortProfile.getVisibility(), profile.getVisibility());
        assertEquals(shortProfile.getGender(), "Other");
        assertEquals(shortProfile.getLocation(), "Trento, Italy");
        assertEquals(shortProfile.getAge(), Integer.valueOf(age));
    }
}
