package io.beancounter.commons.model;

import static org.testng.Assert.assertEquals;

import java.net.MalformedURLException;
import java.net.URL;

import org.testng.annotations.Test;

public class ServiceTest {

    @Test
    public void getAtomicOAuthCallbackWithCustomerReturnsCallbackWithCustomerInPath()
            throws MalformedURLException {
        AuthServiceConfig service = new AuthServiceConfig("service-name");
        service.setAtomicOAuthCallback(new URL(
                "http://api.beancounter.io/rest/user/oauth/callback/twitter/"));

        assertEquals(service.getAtomicOAuthCallbackWithCustomer("any_customer").toString(),
                "http://api.beancounter.io/rest/user/customer/any_customer/oauth/callback/twitter/");
    }

    @Test
    public void getAtomicOAuthCallbackWithCustomerWithReturnsUnmodifiedCallbackWhenCustomerIsEmpty()
            throws MalformedURLException {
        AuthServiceConfig service = new AuthServiceConfig("service-name");
        service.setAtomicOAuthCallback(new URL(
                "http://api.beancounter.io/rest/user/oauth/callback/twitter/"));

        assertEquals(service.getAtomicOAuthCallbackWithCustomer("").toString(),
                "http://api.beancounter.io/rest/user/oauth/callback/twitter/");
    }

    @Test
    public void getAtomicOAuthCallbackWithCustomerWithReturnsUnmodifiedCallbackWhenCustomerIsNull()
            throws MalformedURLException {
        AuthServiceConfig service = new AuthServiceConfig("service-name");
        service.setAtomicOAuthCallback(new URL(
                "http://api.beancounter.io/rest/user/oauth/callback/twitter/"));

        assertEquals(service.getAtomicOAuthCallbackWithCustomer(null).toString(),
                "http://api.beancounter.io/rest/user/oauth/callback/twitter/");
    }
}
