package io.beancounter.commons.model;

import io.beancounter.commons.model.activity.ResolvedActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Extended profile encapsulates UserProfile and ResolvedActivities list that
 * caused profile update.
 *
 * The class was meant as a message exchange format only and would not be
 * persisted in a storage backend.
 *
 * @author alex@cloudware.it
 */
public class ExtendedUserProfile {

    private UserProfile profile;
    private List<ResolvedActivity> activities =
            new ArrayList<ResolvedActivity>();

    /** empty constructor for serializers */
    public ExtendedUserProfile() {
    }

    public ExtendedUserProfile(UserProfile profile) {
        this.profile = profile;
    }

    public void addActivity(ResolvedActivity activity) {
        activities.add(activity);
    }

    public UserProfile getProfile() {
        return profile;
    }

    public List<ResolvedActivity> getActivities() {
        return activities;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ExtendedUserProfile that = (ExtendedUserProfile) o;

        if (activities != null ? !activities.equals(that.activities) : that.activities != null)
            return false;
        if (profile != null ? !profile.equals(that.profile) : that.profile != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = profile != null ? profile.hashCode() : 0;
        result = 31 * result + (activities != null ? activities.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ExtendedUserProfile{" +
                "profile=" + profile +
                ", activities=" + activities +
                '}';
    }
}
