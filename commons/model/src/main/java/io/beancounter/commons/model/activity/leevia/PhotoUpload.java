package io.beancounter.commons.model.activity.leevia;

import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PhotoUpload extends io.beancounter.commons.model.activity.Object {
    private static final Logger LOGGER = LoggerFactory.getLogger(Donation.class);

    private String message;
    private URL picture;

    public PhotoUpload() {
        super();
    }

	@Override
	public String toString() {
		return "PhotoUpload{" +
                "message= " + message +
                " picture= " + picture +
                '}';
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public URL getPicture() {
		return picture;
	}

	public void setPicture(URL picture) {
		this.picture = picture;
	}

}
