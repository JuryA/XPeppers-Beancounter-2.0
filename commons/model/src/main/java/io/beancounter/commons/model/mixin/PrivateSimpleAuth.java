package io.beancounter.commons.model.mixin;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Enrico Candino ( enrico.candino@gmail.com )
 */
public abstract class PrivateSimpleAuth {
    @JsonIgnore
    abstract String getSession();
}
