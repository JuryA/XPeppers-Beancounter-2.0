package io.beancounter.commons.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.joda.ser.DateTimeSerializer;
import org.joda.time.DateTime;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * Incapsulates essential properties from User and UserProfile models.
 * Used in Platform API to make things easier for consumers.
 *
 * Equality and hash are based only on {@code id} property.
 *
 * @author alex@cloudware.it
 */
public class ShortUserProfile implements Serializable {
    private UUID userId;
    private String customer;
    private UserProfile.Visibility visibility;
    private String gender;
    private String location;
    private Integer age;
    private Set<Topic> topics;
    private DateTime updated;

    public ShortUserProfile() {
        clearTopics();
    }

    public ShortUserProfile(UUID id) {
        this.userId = id;
        clearTopics();
    }

    public ShortUserProfile(final UserProfile profile, final User user) {
        this(user.getId());
        this.customer = user.getCustomer();
        this.gender = user.getGender();
        this.location = user.getLocationName();
        this.age = user.getAge();
        this.visibility = profile.getVisibility();
        this.updated = profile.getLastUpdated();
        addTopics(profile.getCategories());
        addTopics(profile.getInterests());
    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public UserProfile.Visibility getVisibility() {
        return visibility;
    }

    public void setVisibility(UserProfile.Visibility visibility) {
        this.visibility = visibility;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @JsonSerialize(using=DateTimeSerializer.class)
    public DateTime getUpdated() {
        return updated;
    }

    public void setUpdated(DateTime updated) {
        this.updated = updated;
    }

    @JsonTypeInfo(
        use=JsonTypeInfo.Id.NAME,
        include=JsonTypeInfo.As.PROPERTY,
        property="kind")
    @JsonSubTypes({
        @JsonSubTypes.Type(value = Interest.class, name = "interest"),
        @JsonSubTypes.Type(value = Category.class, name = "category")})
    public Set<Topic> getTopics() {
        return topics;
    }

    public void setTopics(Set<Topic> topics) {
        clearTopics();
        addTopics(topics);
    }

    public void addTopics(Collection<? extends Topic> topics) {
        this.topics.addAll(topics);
    }

    private void clearTopics() {
        if (topics == null)
            topics = new HashSet<Topic>();
        topics.clear();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ShortUserProfile that = (ShortUserProfile) o;
        return !(userId != null ? !userId.equals(that.userId) : that.userId != null);
    }

    @Override
    public int hashCode() {
        return userId != null ? userId.hashCode() : 0;
    }
}
