package io.beancounter.commons.model.mixin;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.net.URI;
import java.util.Collection;
import java.util.UUID;

@JsonAutoDetect(
    getterVisibility = JsonAutoDetect.Visibility.NONE,
    isGetterVisibility = JsonAutoDetect.Visibility.NONE)
public interface ShortInterest {

    @JsonProperty
    public URI getResource();

    @JsonProperty
    public String getLabel();

    @JsonProperty
    public double getWeight();

    @JsonProperty
    public boolean isVisible();
}
