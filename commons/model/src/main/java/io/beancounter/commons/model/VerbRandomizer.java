package io.beancounter.commons.model;

import io.beancounter.commons.model.activity.Verb;
import io.beancounter.commons.tests.Randomiser;

import java.util.Random;

// TODO: move this class into test-helpers/....
public class VerbRandomizer implements Randomiser<Verb> {
    private Random random = new Random();
    private String name;

    public VerbRandomizer(String name) {
        this.name = name;
    }

    @Override
    public Class<Verb> type() {
        return Verb.class;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public Verb getRandom() {
        return Verb.values()[random.nextInt(Verb.values().length)];
    }
}
