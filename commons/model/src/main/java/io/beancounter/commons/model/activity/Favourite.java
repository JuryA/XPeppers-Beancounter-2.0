package io.beancounter.commons.model.activity;

import io.beancounter.commons.model.activity.tca.Link;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * @author Nicola Moretto ( nicola.moretto@xpeppers.com )
 */
@JsonTypeInfo(use=JsonTypeInfo.Id.NAME, include=JsonTypeInfo.As.PROPERTY, property="type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = Favourite.class, name = "FAVOURITE"),
        @JsonSubTypes.Type(value = Link.class, name = "LINK")
})
public class Favourite extends io.beancounter.commons.model.activity.Object {

    private static final Logger LOGGER = LoggerFactory.getLogger(Favourite.class);
    
    public Favourite() {
        super();
    }

    
	@Override
    public String toString() {
        return "Favourite{}";
    }
}
