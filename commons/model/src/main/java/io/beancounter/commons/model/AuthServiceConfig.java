package io.beancounter.commons.model;

import io.beancounter.commons.tests.annotations.Random;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * This class models all the data of an external service, <i>beancounter.io</i>
 * could connect to.
 *
 * @author Davide Palmisano ( dpalmisano@gmail.com )
 */
public class AuthServiceConfig implements Serializable {

    private static final long serialVersionUID = 4514345235L;

    private String name;

    private String description;

    private URL endpoint;

    private String apikey;

    private String secret;

    private String authRequest;

    private URL sessionEndpoint;

    private URL OAuthCallback;

    private URL atomicOAuthCallback;

    private String oAuth2Callback;

    @Random(names = {"name", "description", "secret", "authRequest"})
    public AuthServiceConfig(String name, String description, String secret, String authRequest) {
        this.name = name;
        this.description = description;
        this.secret = secret;
        this.authRequest = authRequest;
    }

    public AuthServiceConfig(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public URL getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(URL endpoint) {
        this.endpoint = endpoint;
    }

    public String getApikey() {
        return apikey;
    }

    public void setApikey(String apikey) {
        this.apikey = apikey;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public URL getAtomicOAuthCallback() {
        return atomicOAuthCallback;
    }
    
    public URL getAtomicOAuthCallbackWithCustomer(String customer) {
        if(customer == null || customer.isEmpty())
            return getAtomicOAuthCallback();

        String[] splitUrl = atomicOAuthCallback.toString().split("user");
        String callbackWithCustomer = splitUrl[0] + "user/customer/" + customer + splitUrl[1];
        try {
            return new URL(callbackWithCustomer);
        } catch (MalformedURLException e) {
            throw new RuntimeException("Malformed authorization URL '" + callbackWithCustomer + "'");
        }
    }

    public String getOAuth2Callback() {
        return getOAuth2Callback(null, null);
    }

    public String getOAuth2Callback(String customer) {
        return getOAuth2Callback(customer, null);
    }

    public String getOAuth2Callback(String customer, String stateId) {
        if (oAuth2Callback == null)
            return null;
        String url = oAuth2Callback.replaceAll("\\{service}", name);
        if (customer != null)
            url = url.replaceAll("\\{customer}", customer);
        if (stateId != null)
            url = url.replaceAll("\\{state}", stateId);
        return url;
    }

    public URL getAuthRequest() {
        try {
            return new URL(String.format(authRequest, apikey));
        } catch (MalformedURLException e) {
            throw new RuntimeException("Malformed authorization URL '" + authRequest + "'");
        }
    }

    public void setAuthRequest(String authRequest) {
        this.authRequest = authRequest;
    }

    public void setSessionEndpoint(URL sessionEndpoint) {
        this.sessionEndpoint = sessionEndpoint;
    }

    public URL getSessionEndpoint() {
        return sessionEndpoint;
    }

    public URL getOAuthCallback() {
        return OAuthCallback;
    }

    public void setOAuthCallback(URL OAuthCallback) {
        this.OAuthCallback = OAuthCallback;
    }

    public void setAtomicOAuthCallback(URL atomicOAuthCallback) {
        this.atomicOAuthCallback = atomicOAuthCallback;
    }

    public void setOAuth2Callback(String oAuth2Callback) {
        this.oAuth2Callback = oAuth2Callback;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AuthServiceConfig)) return false;

        AuthServiceConfig service = (AuthServiceConfig) o;

        if (name != null ? !name.equals(service.name) : service.name != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "AuthServiceConfig{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", endpoint=" + endpoint +
                ", apikey='" + apikey + '\'' +
                ", secret='" + secret + '\'' +
                ", authRequest='" + authRequest + '\'' +
                ", sessionEndpoint=" + sessionEndpoint +
                ", OAuthCallback=" + OAuthCallback +
                ", atomicOAuthCallback=" + atomicOAuthCallback +
                ", oAuth2Callback=" + oAuth2Callback +
                '}';
    }

}
