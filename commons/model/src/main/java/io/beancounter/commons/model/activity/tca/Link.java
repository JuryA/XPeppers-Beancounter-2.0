package io.beancounter.commons.model.activity.tca;

import io.beancounter.commons.model.activity.Favourite;

public class Link extends Favourite {

	private String link;
	
	public Link() {
        super();
    }
	
	public Link(String link) {
		super();
		this.link = link;
    }

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}
	
    @Override
    public String toString() {
        return "Link{" +
                "link=" + link +
                '}';
    }
}
