package io.beancounter.commons.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.beancounter.commons.model.auth.Auth;
import io.beancounter.commons.tests.annotations.Random;

import org.joda.time.DateTime;
import org.joda.time.Years;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Models the main <a href="http://beancounter.io">User</a>
 * characteristics.
 *
 * @author Davide Palmisano ( dpalmisano@gmail.com )
 */
public class User implements Serializable {

    private static final long serialVersionUID = 324345235L;

    private UUID id;

    private String name;

    private String surname;

    private Map<String, Auth> services = new HashMap<String, Auth>();

    private String password;

    private String username;

    private Map<String, String> metadata = new HashMap<String, String>();

    private UUID userToken;

    private String customer;

    private String email;

    public User() {
        id = UUID.randomUUID();
    }

    public User(UUID userId) {
        this.id = userId;
    }

    @Random(names = {"name", "surname", "username", "password"})
    public User(String name, String surname, String username, String password) {
        this();
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.password = password;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Map<String, Auth> getServices() {
        return services;
    }

    public void setServices(Map<String, Auth> services) {
        this.services = services;
    }

    public Auth getAuth(String service) {
        return services.get(service);
    }

    public void addService(String service, Auth auth) {
        services.put(service, auth);
    }

    public void removeService(String service) {
        services.remove(service);
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void addMetadata(String key, String value) {
        metadata.put(key, value);
    }

    public String getMetadata(String key) {
        return metadata.get(key);
    }

    public Map<String, String> getMetadata() {
        return metadata;
    }

    public void setMetadata(Map<String, String> metadata) {
        for (Map.Entry<String, String> entry : metadata.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            addMetadata(key, value);
        }
    }

    public UUID getUserToken() {
        return userToken;
    }

    public void setUserToken(UUID userToken) {
        this.userToken = userToken;
    }
    
    public String getCustomer(){
        return customer;
    }
    
    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gender using available metadata from social networks.
     * @return Gender string ("male" or "female") or null.
     */
    @JsonIgnore
    public String getGender() {
        // Twitter does not support gender field
        return getMetadata("facebook.user.gender");
    }

    /**
     * Geographical location using available metadata
     * from social networks.
     * @return Location string, e.g. "Trento, Italy", or null.
     */
    @JsonIgnore
    public String getLocationName() {
        String location = getMetadata("facebook.user.locationname");
        if (location == null || location.length() == 0)
            location = getMetadata("twitter.user.location");
        return location;
    }

    /**
     * Age using available metadata from social networks.
     * @return User age as integer or null if not available.
     */
    @JsonIgnore
    public Integer getAge() {
        Integer age = null;

        String birthday = getMetadata("facebook.user.birthday");
        if (birthday != null) {
            try {
                DateTimeFormatter fmt = DateTimeFormat.forPattern("MM/dd/yyyy");
                DateTime dob = DateTime.parse(birthday, fmt);
                age = Years.yearsBetween(dob, DateTime.now()).getYears();
            } catch (Exception e) { /* pass */ }
        }

        // Twitter does not support age field
        return age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (username != null ? !username.equals(user.username) : user.username != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return username != null ? username.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", services=" + services +
                ", password='" + password + '\'' +
                ", username='" + username + '\'' +
                ", metadata=" + metadata + '\'' +
                ", customer='" + customer + '\'' +
                ", email='" + email +
                '}';
    }
}
