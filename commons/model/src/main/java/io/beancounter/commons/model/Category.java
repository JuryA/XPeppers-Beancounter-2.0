package io.beancounter.commons.model;

import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * It represent a {@link User} category of interest.
 *
 * @author Davide Palmisano ( dpalmisano@gmail.com )
 */
public class Category extends Topic<Category> {

    private Collection<URL> urls = new ArrayList<URL>();

    public Category() {
    }

    public Category(String label, URI resource) {
        super(resource, label);
    }

    public Category(URI resource, String label, double weight, Integer taxId) {
        super(resource, label, weight);
        this.taxonomyNodeId = taxId;
    }

    public Category(Category category) {
        super(category.getResource(), category.getLabel());
        setUrls(category.getUrls());
        setTaxonomyNodeId(category.getTaxonomyNodeId());
        setWeight(category.getWeight());
    }

    public Collection<URL> getUrls() {
        return urls;
    }

    public void setUrls(Collection<URL> urls) {
        this.urls = urls;
    }

    public void addUrl(URL url) {
        urls.add(url);
    }

    @Override
    public String toString() {
        return "Category{" +
                "urls=" + urls +
                "taxNodeId=" + taxonomyNodeId +
                "} " + super.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result
                + ((taxonomyNodeId == null) ? 0 : taxonomyNodeId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Topic<Category> other = (Topic<Category>) obj;
        if (taxonomyNodeId == null) {
            if (other.taxonomyNodeId != null) {
                return false;
            }
        } else if (!taxonomyNodeId.equals(other.taxonomyNodeId)) {
            return false;
        }
        return true;
    }

    @Override
    public Category merge(Category nu, Category old, int threshold) {
        // if the activities of the old one are above the threshold, drop the exceeding ones
        if (old.getUrls().size() > threshold) {
            List<URL> oldActivities = new ArrayList<URL>(old.getUrls());
            for (int i = 0; i < oldActivities.size() - threshold; i++) {
                oldActivities.remove(i);
            }
            old.setUrls(oldActivities);
        }
        for (URL url : nu.getUrls()) {
            old.addUrl(url);
        }
        old.setWeight(old.getWeight() + nu.getWeight());
        return old;
    }

}
