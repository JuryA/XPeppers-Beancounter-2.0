package io.beancounter.commons.model.mixin;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Enrico Candino ( enrico.candino@gmail.com )
 */
public abstract class PrivateUser {
    @JsonIgnore abstract String getPassword();
    @JsonIgnore abstract String getUserToken();
}
