package io.beancounter.commons.model.auth;

import java.net.URL;

/**
 * put class description here
 *
 * @author Davide Palmisano ( dpalmisano@gmail.com )
 */
public class OAuthToken {

    private URL redirectPage;
    private String stateId;

    public OAuthToken(URL redirectPage) {
        this.redirectPage = redirectPage;
        this.stateId = null;
    }

    public OAuthToken(URL redirectPage, String stateId) {
        this.redirectPage = redirectPage;
        this.stateId = stateId;
    }

    public URL getRedirectPage() {
        return redirectPage;
    }

    public String getStateId() {
        return stateId;
    }

    @Override
    public String toString() {
        return "OAuthToken{" +
                "redirectPage=" + redirectPage +
                ", stateId='" + stateId + '\'' +
                '}';
    }
}
