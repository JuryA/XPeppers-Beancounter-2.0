package io.beancounter.commons.model;

import java.io.Serializable;
import java.net.URL;
import java.util.UUID;

/**
 * This class models an application able to consume data from the
 * beancounter.io platform.
 *
 * @author Davide Palmisano ( dpalmisano@gmail.com )
 */
public class Application implements Serializable {
    private String name;
    private String description;
    private String email;

    private UUID apiKey;

    private URL callbackSuccess;
    private URL callbackFailure;

    private String facebookAppId;
    private String facebookAppSecret;

    private String twitterConsumerKey;
    private String twitterConsumerSecret;
    private String twitterAccessToken;
    private String twitterTokenSecret;

    private String gaTrackingId;
    private String gaAge;
    private String gaGender;
    private String gaLocation;
    private String gaCat1;
    private String gaCat2;
    private String gaCat3;

    /**
     * Empty constructor mainly for deserialization.
     */
    public Application() {
    }

    public Application(String name, String description, String email) {
        this.apiKey = UUID.randomUUID();
        this.name = name;
        this.description = description;
        this.email = email;
    }

    /**
     * Update app settings excluding name and API key.
     */
    public void update(Application otherApp) {
        setDescription(otherApp.getDescription());
        setEmail(otherApp.getEmail());

        setCallbackSuccess(otherApp.getCallbackSuccess());
        setCallbackFailure(otherApp.getCallbackFailure());

        setFacebookAppId(otherApp.getFacebookAppId());
        setFacebookAppSecret(otherApp.getFacebookAppSecret());

        setTwitterConsumerKey(otherApp.getTwitterConsumerKey());
        setTwitterConsumerSecret(otherApp.getTwitterConsumerSecret());
        setTwitterAccessToken(otherApp.getTwitterAccessToken());
        setTwitterTokenSecret(otherApp.getTwitterTokenSecret());

        setGaTrackingId(otherApp.getGaTrackingId());
        setGaAge(otherApp.getGaAge());
        setGaGender(otherApp.getGaGender());
        setGaLocation(otherApp.getGaLocation());
        setGaCat1(otherApp.getGaCat1());
        setGaCat2(otherApp.getGaCat2());
        setGaCat3(otherApp.getGaCat3());
    }

    public String getName() {
        return name;
    }

    public UUID getApiKey() {
        return apiKey;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public URL getCallbackSuccess() {
        return callbackSuccess;
    }

    public void setCallbackSuccess(URL callbackSuccess) {
        this.callbackSuccess = callbackSuccess;
    }

    public URL getCallbackFailure() {
        return callbackFailure;
    }

    public void setCallbackFailure(URL callbackFailure) {
        this.callbackFailure = callbackFailure;
    }

    public String getFacebookAppId() {
        return facebookAppId;
    }

    public void setFacebookAppId(String facebookAppId) {
        this.facebookAppId = facebookAppId;
    }

    public String getFacebookAppSecret() {
        return facebookAppSecret;
    }

    public void setFacebookAppSecret(String facebookAppSecret) {
        this.facebookAppSecret = facebookAppSecret;
    }

    public String getTwitterConsumerKey() {
        return twitterConsumerKey;
    }

    public void setTwitterConsumerKey(String twitterConsumerKey) {
        this.twitterConsumerKey = twitterConsumerKey;
    }

    public String getTwitterConsumerSecret() {
        return twitterConsumerSecret;
    }

    public void setTwitterConsumerSecret(String twitterConsumerSecret) {
        this.twitterConsumerSecret = twitterConsumerSecret;
    }

    public String getTwitterAccessToken() {
        return twitterAccessToken;
    }

    public void setTwitterAccessToken(String twitterAccessToken) {
        this.twitterAccessToken = twitterAccessToken;
    }

    public String getTwitterTokenSecret() {
        return twitterTokenSecret;
    }

    public void setTwitterTokenSecret(String twitterTokenSecret) {
        this.twitterTokenSecret = twitterTokenSecret;
    }

    public String getGaTrackingId() {
        return gaTrackingId;
    }

    public void setGaTrackingId(String gaTrackingId) {
        this.gaTrackingId = gaTrackingId;
    }

    public String getGaAge() {
        return gaAge;
    }

    public void setGaAge(String gaAge) {
        this.gaAge = gaAge;
    }

    public String getGaGender() {
        return gaGender;
    }

    public void setGaGender(String gaGender) {
        this.gaGender = gaGender;
    }

    public String getGaLocation() {
        return gaLocation;
    }

    public void setGaLocation(String gaLocation) {
        this.gaLocation = gaLocation;
    }

    public String getGaCat1() {
        return gaCat1;
    }

    public void setGaCat1(String gaCat1) {
        this.gaCat1 = gaCat1;
    }

    public String getGaCat2() {
        return gaCat2;
    }

    public void setGaCat2(String gaCat2) {
        this.gaCat2 = gaCat2;
    }

    public String getGaCat3() {
        return gaCat3;
    }

    public void setGaCat3(String gaCat3) {
        this.gaCat3 = gaCat3;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Application that = (Application) o;

        if (!apiKey.equals(that.apiKey)) return false;
        if (!name.equals(that.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + apiKey.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Application{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", email='" + email + '\'' +
                ", apiKey=" + apiKey +
                ", callbackSuccess=" + callbackSuccess +
                ", callbackFailure=" + callbackFailure +
                ", facebookAppId='" + facebookAppId + '\'' +
                ", facebookAppSecret='" + facebookAppSecret + '\'' +
                ", twitterConsumerKey='" + twitterConsumerKey + '\'' +
                ", twitterConsumerSecret='" + twitterConsumerSecret + '\'' +
                ", twitterAccessToken='" + twitterAccessToken + '\'' +
                ", twitterTokenSecret='" + twitterTokenSecret + '\'' +
                ", gaTrackingId='" + gaTrackingId + '\'' +
                ", gaAge='" + gaAge + '\'' +
                ", gaGender='" + gaGender + '\'' +
                ", gaLocation='" + gaLocation + '\'' +
                ", gaCat1='" + gaCat1 + '\'' +
                ", gaCat2='" + gaCat2 + '\'' +
                ", gaCat3='" + gaCat3 + '\'' +
                '}';
    }
}
