package io.beancounter.commons.model.activity.adapters;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.joda.time.DateTime;

/**
 * @author Enrico Candino ( enrico.candino@gmail.com )
 */
public class DateTimeAdapterJAXB extends XmlAdapter<Long, DateTime> {

    @Override
    public DateTime unmarshal(Long l) throws Exception {
        return new DateTime(l);
    }

    @Override
    public Long marshal(DateTime dateTime) throws Exception {
        return dateTime.getMillis();
    }
}