package io.beancounter.commons.model.activity.leevia;

import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Donation extends io.beancounter.commons.model.activity.Object {
    private static final Logger LOGGER = LoggerFactory.getLogger(Donation.class);
    
    private String message;
    private String caption;
    private URL picture;
    private URL link;
    
    public Donation() {
        super();
    }

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "Donation{" +
                "message= " + message +
                "caption= " + caption +
                " link= " + link +
                " picture= " + picture +
                '}';
	}

	public URL getLink() {
		return link;
	}

	public void setLink(URL link) {
		this.link = link;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public URL getPicture() {
		return picture;
	}

	public void setPicture(URL picture) {
		this.picture = picture;
	}
}
