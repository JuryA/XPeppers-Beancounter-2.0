package io.beancounter.commons.model.activity;

import io.beancounter.commons.tests.annotations.Random;

import java.io.Serializable;

/**
 * @author Davide Palmisano ( dpalmisano@gmail.com )
 */
public enum Verb implements Serializable {
    FAVOURITE,
    DONATE,
    UPLOAD,
	LOCATED,
    @Deprecated
    FOLLOWING,
    SHARE,
    @Deprecated
    MAKEFRIEND,
    RSVP,
    FAVORITED,
    LIKE,
    LISTEN,
    SONG,
    TWEET,
    WATCHED,
    CHECKIN,
    LOGIN_WEB,
    LOGIN_MOBILE,
    SIGNUP_WEB,
    SIGNUP_MOBILE,
    COMMENT;
    @Random(names = {})
    Verb() {}
}
