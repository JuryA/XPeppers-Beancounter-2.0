package io.beancounter.commons.model.auth;

import io.beancounter.commons.model.User;

/**
 * This class is used to bind third party (social) user, as it is returned from
 * the {@code AuthHandler} interface, with the corresponding service
 * username or general identifier.
 *
 * @author Davide Palmisano ( dpalmisano@gmail.com )
 */
public class ServiceUser {

    private String id;

    private User user;

    public ServiceUser(String id, User user) {
        this.id = id;
        this.user = user;
    }

    public String getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public String getUsername() {
        return user.getUsername();
    }
    
    public void setUserCustomer(String customer){
        user.setCustomer(customer);
    }

    @Override
    public String toString() {
        return "ServiceUser{" +
                "id='" + id + '\'' +
                ", user=" + user +
                '}';
    }
}
