package io.beancounter.commons.freebase;

import io.beancounter.commons.utils.HttpCommand;
import io.beancounter.commons.utils.JsonParser;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class FreeBaseResolver {

    private String apiKey;

    private final HttpCommand command;

    private final String END_POINT = "https://www.googleapis.com/freebase/v1sandbox/search";

    public FreeBaseResolver(HttpCommand command, String apiKey) {

        this.command = command;

        this.setApiKey(apiKey);

    }

    public List<FreeBaseResult> lookup(String searchTerm) throws Exception{
        List<FreeBaseResult> results = new ArrayList<FreeBaseResult>();

        if (searchTerm == null || "".equals(searchTerm)) {
            return results;
        }

        command.setURL(buildQuery(searchTerm));

        try {
            results = parseResults(command.execute());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return results;
    }

    // TODO lookupByURI

    private List<FreeBaseResult> parseResults(String jsonResponse)
            throws Exception {

        FreeBaseResponse response = new JsonParser().deserialize(jsonResponse,
                FreeBaseResponse.class);

        return response.getResult();
    }

    private String buildQuery(String searchTerm)
            throws UnsupportedEncodingException {
        String encodedQueryTerm = URLEncoder.encode(searchTerm, "UTF-8");
        return END_POINT + "?key=" + getApiKey() + "&query=" + encodedQueryTerm;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

}
