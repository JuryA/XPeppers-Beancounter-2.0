package io.beancounter.commons.freebase;

public class FreeBaseResult {

    // TODO notable/type/name
    //      TOPIC/TOPIC/FACT

    private String mid;
    private String name;
    private float score;

    private FreeBaseNotable notable;

    public FreeBaseResult(){

    }

    public FreeBaseResult(String mid, String name, float score, FreeBaseNotable notable) {
        this.mid = mid;
        this.name = name;
        this.score = score;
        this.notable = notable;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public FreeBaseNotable getNotable() {
        return notable;
    }

    public void setNotable(FreeBaseNotable notable) {
        this.notable = notable;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((mid == null) ? 0 : mid.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((notable == null) ? 0 : notable.hashCode());
        result = prime * result + Float.floatToIntBits(score);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        FreeBaseResult other = (FreeBaseResult) obj;
        if (mid == null) {
            if (other.mid != null) {
                return false;
            }
        } else if (!mid.equals(other.mid)) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        if (notable == null) {
            if (other.notable != null) {
                return false;
            }
        } else if (!notable.equals(other.notable)) {
            return false;
        }
        if (Float.floatToIntBits(score) != Float.floatToIntBits(other.score)) {
            return false;
        }
        return true;
    }


}
