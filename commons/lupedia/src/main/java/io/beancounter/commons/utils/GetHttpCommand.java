package io.beancounter.commons.utils;


import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

public class GetHttpCommand implements HttpCommand {

	private String url;
	
	@Override
	public String execute() throws Exception {
		
		HttpGet request = new HttpGet(getURL());
		
		DefaultHttpClient client = new DefaultHttpClient();
		
		HttpResponse response = client.execute(request);
		
		return EntityUtils.toString(response.getEntity());
	}


	public String getURL() {
		return url;
	}


	@Override
	public void setURL(String url) {
		this.url = url;
	}


	

}
