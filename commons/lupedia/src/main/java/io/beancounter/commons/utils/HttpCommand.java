package io.beancounter.commons.utils;

public interface HttpCommand {
	
	String execute() throws Exception;
	
	void setURL(String url);
}
