package io.beancounter.commons.freebase;

import java.util.List;

public class FreeBaseError {
	
	private int code;
	
	private String message;

	private List<FreeBaseErrorMessage> errors;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public List<FreeBaseErrorMessage> getErrors() {
		return errors;
	}

	public void setErrors(List<FreeBaseErrorMessage> errors) {
		this.errors = errors;
	}
	
	
}
