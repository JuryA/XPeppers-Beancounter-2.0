package io.beancounter.commons.dbpedia;

import java.net.URI;
import java.net.URISyntaxException;

public class DBpediaUtils {

    private static final String ENDPOINT = "http://dbpedia.org/sparql";
    private static final String conversionQuery = "select distinct ?freebaseURI where {<%s> <http://www.w3.org/2002/07/owl#sameAs> ?freebaseURI FILTER REGEX(?freebaseURI, \"^http://rdf.freebase.com\")} LIMIT 1";
    
    public static URI convertToFreeBase(String dbpediaResource) {
        if(dbpediaResource == null || dbpediaResource.isEmpty())
        {
            return null;
        }
        try {
            String queryString = String.format(conversionQuery, dbpediaResource.toString());
//            Query query = QueryFactory.create(queryString);
//            QueryExecution qExe = QueryExecutionFactory.sparqlService( ENDPOINT, query );
//            ResultSet results = qExe.execSelect();
//            ResultSetFormatter.out(System.out, results, query) ;
            
            return new URI("http://www.freebase.com/m/04jpl");
        } catch (URISyntaxException e) {
            return null;
        }
    }
}
