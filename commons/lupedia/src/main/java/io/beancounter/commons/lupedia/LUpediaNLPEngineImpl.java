package io.beancounter.commons.lupedia;

import com.fasterxml.jackson.databind.type.CollectionType;
import com.google.inject.Inject;
import com.google.inject.Provider;
import io.beancounter.commons.helper.jackson.ObjectMapperFactory;
import io.beancounter.commons.nlp.Entity;
import io.beancounter.commons.nlp.NLPEngine;
import io.beancounter.commons.nlp.NLPEngineException;
import io.beancounter.commons.nlp.NLPEngineResult;
import io.beancounter.commons.redirects.RedirectException;
import io.beancounter.commons.redirects.RedirectResolver;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;

import de.l3s.boilerpipe.BoilerpipeProcessingException;
import de.l3s.boilerpipe.extractors.ArticleExtractor;
import org.apache.http.*;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * put class description here
 *
 * @author Davide Palmisano ( dpalmisano@gmail.com )
 */
public class LUpediaNLPEngineImpl implements NLPEngine {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(LUpediaNLPEngineImpl.class);

    public static final String ENDPOINT =
            "http://lupedia.ontotext.com/lookup/text2json";

    public static final String LU_THRESHOLD = "threshold";
    public static final String LU_LOOKUP_TEXT = "lookupText";

    Provider<HttpClient> httpClientProvider;

    @Inject
    public LUpediaNLPEngineImpl(Provider<HttpClient> httpClientProvider) {
        this.httpClientProvider = httpClientProvider;
    }

    @Override
    public NLPEngineResult enrich(String text) throws NLPEngineException {
        List<LUpediaEntity> entities;
        try {
            entities = extract(text);
        } catch (IOException e) {
            throw new NLPEngineException(e.getMessage(), e);
        }
        NLPEngineResult result = new NLPEngineResult();
        for(LUpediaEntity entity : entities) {
            String candidateLabel = getURILastPart(entity.getInstanceUri());
            Entity e = Entity.build(
                    entity.getInstanceUri(),
                    candidateLabel);
            e.setType(getURILastPart(entity.getInstanceClass()));
            result.addEntity(e);
        }
        return result;
    }

    @Override
    public NLPEngineResult enrich(URL url) throws NLPEngineException {
        String content;
        try {
            content = fetch(url);
        } catch (IOException e) {
            throw new NLPEngineException("Error fetching from: " + url, e);
        }

        String text;
        try {
            text = ArticleExtractor.INSTANCE.getText(content);
        } catch (BoilerpipeProcessingException e) {
            throw new NLPEngineException("Error extracting text from: "+url, e);
        }
        return enrich(text);
    }

    protected List<LUpediaEntity> extract(String text) throws IOException {
        List<LUpediaEntity> entities;

        ObjectMapper mapper = ObjectMapperFactory.createMapper();
        CollectionType types = TypeFactory.
                defaultInstance().
                constructCollectionType(
                        List.class,
                        LUpediaEntity.class);

        List<NameValuePair> payload = new ArrayList<NameValuePair>();
        payload.add(new BasicNameValuePair(LU_THRESHOLD, "0.85"));
        payload.add(new BasicNameValuePair(LU_LOOKUP_TEXT, text));

        HttpPost request = new HttpPost(ENDPOINT);
        request.setEntity(new UrlEncodedFormEntity(payload, Consts.UTF_8));

        HttpClient client = httpClientProvider.get();
        InputStream instream = null;
        try {
            HttpResponse resp = client.execute(request);
            LOGGER.debug("Lupedia responded with: {}", resp.getStatusLine());

            if (resp.getStatusLine().getStatusCode() != HttpStatus.SC_OK)
                throw new IOException("Bad response from Lupedia");

            HttpEntity httpEntity = resp.getEntity();
            if (httpEntity == null)
                throw new IOException("Unable to consume Lupedia response");

            instream = httpEntity.getContent();
            entities = mapper.readValue(instream, types);
        } finally {
            if (instream != null)
                try {
                    instream.close();
                } catch (IOException ignored) {
                    LOGGER.error("Couldn't close input stream", ignored);
                }
            LOGGER.debug("Shutting down Connection Manager");
            client.getConnectionManager().shutdown();
        }

        return entities;
    }

    protected String fetch(URL url) throws IOException {
        LOGGER.debug("Fetching from: {}", url);
        HttpGet request = new HttpGet(url.toString());

        HttpClient client = httpClientProvider.get();
        String content;
        try {
            HttpResponse resp = client.execute(request);
            LOGGER.debug("Status: {}", resp.getStatusLine());

            if (resp.getStatusLine().getStatusCode() != HttpStatus.SC_OK)
                throw new IOException("Bad response status code");

            HttpEntity entity = resp.getEntity();
            if (entity == null)
                throw new IOException("Unable to fetch from: " + url);

            long len = entity.getContentLength();
            if (len > MAX_BYTES_CONTENT)
                throw new IOException("Content is too big: " + len);

            content = EntityUtils.toString(entity, "UTF-8");
        } finally {
            LOGGER.debug("Shutting down Connection Manager");
            client.getConnectionManager().shutdown();
        }

        return content;
    }

    protected String getURILastPart(String uri) {
        URI uriObj;
        try {
            uriObj = new URI(uri);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
        String path = uriObj.getPath();
        String[] pathParts = path.split("/");
        return pathParts[pathParts.length - 1];
    }
}