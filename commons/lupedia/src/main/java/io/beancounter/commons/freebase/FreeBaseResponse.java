package io.beancounter.commons.freebase;

import java.util.ArrayList;
import java.util.List;

public class FreeBaseResponse {

	private String status;

	private List<FreeBaseResult> result = new ArrayList<FreeBaseResult>();

	private float cost;

	private FreeBaseError error;

	private float hits;
	
	public List<FreeBaseResult> getResult() {
		return result;
	}

	public void setResult(List<FreeBaseResult> result) {
		this.result = result;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public float getCost() {
		return cost;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}

	public float getHits() {
		return hits;
	}

	public void setHits(float hits) {
		this.hits = hits;
	}

	public FreeBaseError getError() {
		return error;
	}

	public void setError(FreeBaseError error) {
		this.error = error;
	}

}
