package io.beancounter.commons.lupedia;

import com.google.inject.Provider;
import io.beancounter.commons.nlp.NLPEngineException;
import io.beancounter.commons.nlp.NLPEngineResult;
import org.apache.http.client.HttpClient;

import java.net.URL;

public class ExtendedLUpediaNLPEngineImpl extends LUpediaNLPEngineImpl {

    public ExtendedLUpediaNLPEngineImpl(Provider<HttpClient> httpclProvider) {
        super(httpclProvider);
    }

    @Override
    public NLPEngineResult enrich(String text) throws NLPEngineException {
        /*
         * get results from parent method
         * for each result -> map to freebase = use DBpediaUtils.convertToFreeBase( convert dbpedia result to freebase uri and query freebase uri to obtain json)
         * once obtained freebase result, extract notable type and enrich result as follows:
         * 
         * for category:
         *  set path to notable_type
         * 
         * for entity:
         *  set path to notable_type/name
         */
        return null;
    }

    @Override
    public NLPEngineResult enrich(URL url) throws NLPEngineException {
        // TODO Auto-generated method stub
        return null;
    }

}
