package io.beancounter.commons.utils;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonParser {
	
	public <T> T deserialize(String json, Class<T> type) throws Exception{
		return mapperFactory().readValue(json, type);
	}

	private ObjectMapper mapperFactory() {
		return new ObjectMapper(); 
	}
}
