package io.beancounter.commons.freebase;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import io.beancounter.commons.freebase.FreeBaseNotable;
import io.beancounter.commons.freebase.FreeBaseResolver;
import io.beancounter.commons.freebase.FreeBaseResult;
import io.beancounter.commons.utils.HttpCommand;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class FreeBaseResolverTest {

    private FreeBaseResolver resolver;

    private HttpCommand httpCommand;

    private static final String API_KEY = "AIzaSyBkJnCWxO_8fHhKIzmoWnXrhvVvIrAdfy8";

    @BeforeMethod
    public void setUp() {
        httpCommand = mock(HttpCommand.class);
        resolver = new FreeBaseResolver(httpCommand, API_KEY);
    }

    @Test
    public void processWithUnreachableEndpointShouldReturnEmptyListOfResults()
            throws Exception {

        when(httpCommand.execute()).thenThrow(new Exception());

        List<FreeBaseResult> results = resolver.lookup("expiredApiKeyCase");

        verify(httpCommand).execute();

        Assert.assertEquals(0, results.size());

    }

    @Test
    public void processWithWrongApiKeyShouldReturnEmptyListOfResults()
            throws Exception {

        when(httpCommand.execute()).thenReturn(freebaseApiKeyExpiredJson());

        List<FreeBaseResult> results = resolver.lookup("expiredApiKeyCase");

        verify(httpCommand).execute();

        Assert.assertEquals(0, results.size());

    }

    @Test
    public void searchingForAStringWithFakeNotEmptyResultShouldReturnPopulatedMap()
            throws Exception {

        when(httpCommand.execute()).thenReturn(fakeResults());

        List<FreeBaseResult> results = resolver.lookup("fakeNotEmpty");

        verify(httpCommand).execute();

        Assert.assertEquals(1, results.size());
        Assert.assertEquals(listOfResults(), results);
    }

    @Test
    public void searchingForAStringWithFakeEmptyResultsShouldReturnEmptyMap()
            throws Exception {

        when(httpCommand.execute()).thenReturn(fakeEmptyResults());

        List<FreeBaseResult> results = resolver.lookup("fakeEmpty");

        verify(httpCommand).execute();

        Assert.assertEquals(0, results.size());
    }

    @Test
    public void searchingForEmptyStringShouldReturnEmptyList() throws Exception {

        verify(httpCommand, never()).execute();

        Assert.assertEquals(0, resolver.lookup("").size());
    }

    @Test
    public void searchingForNullStringShouldReturnEmptyList() throws Exception {

        verify(httpCommand, never()).execute();

        Assert.assertEquals(0, resolver.lookup(null).size());
    }

    private String fakeResults() {
        return "{"
                + "\"status\":\"200 OK\","
                + "\"result\":[{"
                + "\"mid\":\"/m/0b1zz\","
                + "\"name\":\"Nirvana\","
                + "\"notable\":{\"name\":\"Record Producer\",\"id\":\"/music/producer\"},"
                + "\"score\":55.227268}]," + "\"cost\":10," + "\"hits\":0"
                + "}";
    }

    private ArrayList<FreeBaseResult> listOfResults() {
        return new ArrayList<FreeBaseResult>() {
            private static final long serialVersionUID = 1L;
            {
                add(new FreeBaseResult("/m/0b1zz", "Nirvana",
                        Float.parseFloat("55.227268"), new FreeBaseNotable(
                                "Record Producer", "/music/producer")));
            }
        };
    }

    private String fakeEmptyResults() {
        return "{" + "\"status\":\"200 OK\"," + "\"result\":[],"
                + "\"cost\":10," + "\"hits\":0" + "}";
    }

    private String freebaseApiKeyExpiredJson() {
        return "{\"error\": {\"code\": 400, \"message\": \"Bad Request\", \"errors\": [{\"domain\": \"usageLimits\", \"message\": \"Bad Request\", \"reason\": \"keyInvalid\"}]}}";
    }

}