package io.beancounter.commons.lupedia;

import io.beancounter.commons.nlp.NLPEngine;
import io.beancounter.commons.nlp.NLPEngineException;

import java.net.URI;
import java.net.URISyntaxException;

import junit.framework.Assert;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class DBpediaResolverTest {
  
    private DBpediaResolver dbpedia;
    private URI londonDbPediaURI;
    
    @BeforeTest
    public void setUp() throws URISyntaxException {
        dbpedia = new DBpediaResolver();
        londonDbPediaURI = new URI("http://dbpedia.org/resource/London");
    }
    
    @Test
    public void dbpediaResourceURIGetsResolvedCorrectly() throws NLPEngineException{
        DBpediaResult dbPediaResult = dbpedia.resolve(londonDbPediaURI);
        Assert.assertNotNull(dbPediaResult);
    }
}
