package io.beancounter.commons.lupedia;

import com.google.inject.Provider;
import io.beancounter.commons.nlp.NLPEngine;

import org.apache.http.client.HttpClient;
import org.testng.annotations.BeforeTest;

import static org.mockito.Mockito.mock;

public class ExtendedLUpediaNLPEngineImplTest {

    private NLPEngine engine;
    private HttpClient mockHttpClient;
    
    @BeforeTest
    private void setUp(){
        mockHttpClient = mock(HttpClient.class);
        engine = new ExtendedLUpediaNLPEngineImpl(new Provider<HttpClient>() {
            @Override
            public HttpClient get() {
                return mockHttpClient;
            }
        });
    }
    
}
