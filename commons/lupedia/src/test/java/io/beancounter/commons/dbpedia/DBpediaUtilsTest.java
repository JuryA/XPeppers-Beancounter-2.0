package io.beancounter.commons.dbpedia;

import org.testng.annotations.Test;

import java.net.URI;

import junit.framework.Assert;

public class DBpediaUtilsTest {
  
    
    private static final String LONDON_URI = "http://www.dbpedia.org/resource/London";
    private static final String LONDON_FB_URI = "http://www.freebase.com/m/04jpl";
    
    @Test
    public void convertingEmptyStringReturnsNull(){
        URI nullURI = DBpediaUtils.convertToFreeBase("");
        Assert.assertNull(nullURI);
    }
    
    @Test
    public void convertingURIPresentOnFreeBaseReturnsURI(){
        URI freebaseUri = DBpediaUtils.convertToFreeBase(LONDON_URI);
        Assert.assertEquals(LONDON_FB_URI, freebaseUri.toString());
    }
    
}
