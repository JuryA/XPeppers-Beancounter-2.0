package io.beancounter.commons.lupedia;

import java.net.URI;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import com.google.inject.Provider;
import io.beancounter.commons.nlp.Entity;
import io.beancounter.commons.nlp.NLPEngine;
import io.beancounter.commons.nlp.NLPEngineException;
import io.beancounter.commons.nlp.NLPEngineResult;

import io.beancounter.commons.tests.TestUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.ProtocolVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.message.BasicHttpResponse;

import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Matchers.any;

/**
 * @author Francesco "Shin" Bux francesco.bux@xpeppers.com
 *
 */
public class LUpediaNLPEngineImplTest {
  
    private NLPEngine nlpEngine;
    private HttpClient mockHttpClient;
    private ClientConnectionManager mockConnMgr;

    @BeforeMethod
    public void setUp() {
        mockConnMgr = mock(ClientConnectionManager.class);
        mockHttpClient = mock(HttpClient.class);
        when(mockHttpClient.getConnectionManager()).thenReturn(mockConnMgr);

        nlpEngine = new LUpediaNLPEngineImpl(new Provider<HttpClient>() {
            @Override
            public HttpClient get() {
                return mockHttpClient;
            }
        });
    }
    
    @Test
    public void testSimpleText() throws Exception {
        final String text = "London is a great city, but Rome is amazing too.";

        HttpResponse resp = TestUtils.getResourceAsHttpResponse(
                "/lupedia/london_rome.json", HttpStatus.SC_OK);
        when(mockHttpClient.execute(any(HttpPost.class))).thenReturn(resp);

        NLPEngineResult result = nlpEngine.enrich(text);

        Set<URI> actual = toURISet(result.getEntities());
        assertTrue(actual.contains(new URI("http://dbpedia.org/resource/London")));
        assertTrue(actual.contains(new URI("http://dbpedia.org/resource/Rome")));
        assertEquals(actual.size(), 2);
        verify(mockConnMgr).shutdown();
    }

    @Test
    public void testUrl() throws Exception {
        final URL url = new URL("http://www.bbc.co.uk/news/uk-18494541");
        HttpResponse bbcResp = TestUtils.getResourceAsHttpResponse(
                "/lupedia/uk-18494541.html", HttpStatus.SC_OK);
        HttpResponse lupediaResp = TestUtils.getResourceAsHttpResponse(
                "/lupedia/uk-18494541.json", HttpStatus.SC_OK);

        when(mockHttpClient.execute(any(HttpGet.class))).thenReturn(bbcResp);
        when(mockHttpClient.execute(any(HttpPost.class))).thenReturn(lupediaResp);

        NLPEngineResult result = nlpEngine.enrich(url);

        Set<URI> actual = toURISet(result.getEntities());
        assertTrue(actual.contains(new URI("http://dbpedia.org/resource/BBC")));
        assertTrue(actual.contains(new URI("http://dbpedia.org/resource/Barnsley")));
        assertTrue(actual.contains(new URI("http://dbpedia.org/resource/London")));
        assertTrue(actual.contains(new URI("http://dbpedia.org/resource/Cardiff")));
        verify(mockConnMgr, atLeastOnce()).shutdown();
    }

    @Test(expectedExceptions = NLPEngineException.class)
    public void testBadUrl() throws Exception {
        ProtocolVersion proto = new ProtocolVersion("HTTP", 1, 1);
        HttpResponse resp = new BasicHttpResponse(proto, 400, "Bad Request");
        when(mockHttpClient.execute(any(HttpGet.class))).thenReturn(resp);
        nlpEngine.enrich(new URL("http://bad.url"));
        verify(mockConnMgr).shutdown();
    }
    
    private Set<URI> toURISet(Set<Entity> entities) {
        Set<URI> uris = new HashSet<URI>();
        for (Entity entity : entities) {
            uris.add(entity.getResource());
        }
        return uris;
    }
}
