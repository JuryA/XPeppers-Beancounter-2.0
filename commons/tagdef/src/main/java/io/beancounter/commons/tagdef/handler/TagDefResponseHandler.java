package io.beancounter.commons.tagdef.handler;

import io.beancounter.commons.helper.jackson.ObjectMapperFactory;
import io.beancounter.commons.tagdef.TagDefResponse;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ResponseHandler;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * put class description here
 *
 * @author Davide Palmisano ( dpalmisano@gmail.com )
 */
public class TagDefResponseHandler implements ResponseHandler<TagDefResponse> {

    public TagDefResponse handleResponse(HttpResponse httpResponse)
            throws IOException {
        int status = httpResponse.getStatusLine().getStatusCode();
        if (status != HttpStatus.SC_OK) {
            return new TagDefResponse(TagDefResponse.Status.ERROR);
        }
        InputStream inputStream = httpResponse.getEntity().getContent();
        BufferedInputStream bis = new BufferedInputStream(inputStream);
        InputStreamReader reader = new InputStreamReader(bis);
        ObjectMapper mapper = ObjectMapperFactory.createMapper();
        try {
            return mapper.readValue(reader, TagDefResponse.class);
        } finally {
            reader.close();
            bis.close();
            inputStream.close();
        }
    }
}
