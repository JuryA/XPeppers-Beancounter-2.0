package io.beancounter.commons.tagdef.handler;

import io.beancounter.commons.tagdef.Def;
import io.beancounter.commons.tagdef.TagDefResponse;

import java.io.IOException;
import java.net.URL;
import java.util.Iterator;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

/**
 * @author Enrico Candino ( enrico.candino@gmail.com )
 */
public class TagDefResponseDeserializer extends JsonDeserializer<TagDefResponse> {

    @Override
    public TagDefResponse deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        ObjectCodec oc = jsonParser.getCodec();
        JsonNode node = oc.readTree(jsonParser);
        TagDefResponse response = new TagDefResponse(TagDefResponse.Status.OK);
        response.setAmount(node.get("num_defs").asInt());
        Iterator<JsonNode> i = node.get("defs").elements();
        while(i.hasNext()) {
            JsonNode n = i.next();
            Def def = new Def(
                    n.get("def").get("text").textValue(),
                    new URL(n.get("def").get("uri").textValue())
            );
            response.addDef(def);
        }
        return response;
    }
}