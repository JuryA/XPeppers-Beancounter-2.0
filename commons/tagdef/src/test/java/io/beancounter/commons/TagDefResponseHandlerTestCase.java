package io.beancounter.commons;

import io.beancounter.commons.tagdef.TagDefResponse;
import io.beancounter.commons.tagdef.handler.TagDefResponseHandler;

import java.io.IOException;
import java.net.URISyntaxException;

import io.beancounter.commons.tests.TestUtils;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import static org.testng.Assert.*;
import org.testng.annotations.Test;

/**
 * @author Enrico Candino ( enrico.candino@gmail.com )
 */
public class TagDefResponseHandlerTestCase {

    @Test
    public void testHandleResponse() throws IOException, URISyntaxException {
        // http://api.tagdef.com/ff.json
        HttpResponse resp = TestUtils.getResourceAsHttpResponse(
                "/tagdef/ff.json", HttpStatus.SC_OK);

        TagDefResponseHandler handler = new TagDefResponseHandler();
        TagDefResponse tagDefResponse = handler.handleResponse(resp);

        assertNotNull(tagDefResponse);
        assertTrue(tagDefResponse.getDefs().size() > 0);
        assertNotNull(tagDefResponse.getDefs().get(0).getText());
        assertNotNull(tagDefResponse.getDefs().get(0).getUrl());
        assertEquals(tagDefResponse.getStatus(), TagDefResponse.Status.OK);
    }

}