package io.beancounter.profiler.process;

import io.beancounter.commons.helper.PropertiesHelper;
import io.beancounter.commons.helper.es.ElasticSearchConfiguration;
import io.beancounter.commons.helper.jedis.DefaultJedisPoolFactory;
import io.beancounter.commons.helper.jedis.JedisPoolFactory;
import io.beancounter.commons.http.HttpClientProvider;
import io.beancounter.commons.linking.FacebookCogitoLinkingEngine;
import io.beancounter.commons.linking.LinkingEngine;
import io.beancounter.commons.lupedia.LUpediaNLPEngineImpl;
import io.beancounter.commons.nlp.NLPEngine;
import io.beancounter.profiler.DefaultProfilerImpl;
import io.beancounter.profiler.Profiler;
import io.beancounter.profiler.taxonomy.JedisTaxonomyNodeRepository;
import io.beancounter.profiler.taxonomy.TaxonomyIndex;
import io.beancounter.profiler.taxonomy.TaxonomyIndexImpl;
import io.beancounter.profiler.taxonomy.TaxonomyNodeRepository;
import io.beancounter.profiles.ElasticSearchProfileManager;
import io.beancounter.profiles.JedisProfileManager;
import io.beancounter.profiles.ProfileManager;

import java.util.Properties;

import io.beancounter.profiles.UnifiedProfileManager;
import org.apache.camel.component.properties.PropertiesComponent;
import org.apache.camel.guice.CamelModuleWithMatchingRoutes;
import org.apache.camel.guice.jndi.JndiBind;

import com.google.inject.Provides;
import com.google.inject.name.Names;
import org.apache.http.client.HttpClient;

public class ProfilerModule extends CamelModuleWithMatchingRoutes {

    @Override
    protected void configure() {
        super.configure();
        Properties redisProperties = PropertiesHelper.readFromClasspath("/redis.properties");
        Names.bindProperties(binder(), redisProperties);
        bindInstance("redisProperties", redisProperties);
        Properties profilerProperties = PropertiesHelper.readFromClasspath("/profiler.properties");
        bindInstance("profilerProperties", profilerProperties);
        bind(JedisPoolFactory.class).to(DefaultJedisPoolFactory.class).asEagerSingleton();
        //bind(ProfileManager.class).to(JedisProfileManager.class);
        bind(ProfileManager.class).to(UnifiedProfileManager.class);
        bind(ProfileManager.class).annotatedWith(Names.named("ElasticsearchProfile")).to(ElasticSearchProfileManager.class);
        bind(ProfileManager.class).annotatedWith(Names.named("JedisProfile")).to(JedisProfileManager.class);
        Properties esProperties = PropertiesHelper.readFromClasspath("/es.properties");
        bindInstance("esConfiguration", ElasticSearchConfiguration.build(esProperties));
        bind(HttpClient.class).toProvider(HttpClientProvider.class);

        // bind NLP and linking engine
        bind(NLPEngine.class).to(LUpediaNLPEngineImpl.class);
        bind(LinkingEngine.class).toInstance(new FacebookCogitoLinkingEngine());
        bind(TaxonomyNodeRepository.class).to(JedisTaxonomyNodeRepository.class);
        bind(TaxonomyIndex.class).to(TaxonomyIndexImpl.class);

        // bind profiler
        bind(Profiler.class).to(DefaultProfilerImpl.class);
        bind(ProfilerRoute.class);
    }

    @Provides
    @JndiBind("properties")
    PropertiesComponent propertiesComponent() {
        PropertiesComponent pc = new PropertiesComponent();
        pc.setLocation("classpath:beancounter.properties");
        return pc;
    }

}
