package io.beancounter.profiler.process;

import io.beancounter.commons.model.ExtendedUserProfile;
import io.beancounter.commons.model.UserProfile;
import io.beancounter.commons.model.activity.ResolvedActivity;
import io.beancounter.profiler.Profiler;

import io.beancounter.profiler.ProfilerException;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

public class ProfilerRoute extends RouteBuilder {

    public final static String DIRECT_PROFILE = "direct:convertToProfile";
    public final static String DIRECT_EXPORT = "direct:forExporter";

    private static final Logger LOGGER =
            LoggerFactory.getLogger(ProfilerRoute.class);

    @Inject
    private Profiler profiler;

    public void configure() {
        errorHandler(deadLetterChannel(errorEndpoint()));

        from(fromKestrel())
                .unmarshal().json(JsonLibrary.Jackson, ResolvedActivity.class)
                .process(new Processor() {
                    @Override
                    public void process(Exchange exchange) throws Exception {
                        ResolvedActivity resolvedActivity =
                                exchange.getIn().getBody(ResolvedActivity.class);
                        LOGGER.debug("profiling activity: {}.", resolvedActivity);

                        UserProfile profile = null;
                        try {
                            profile = profiler.profile(resolvedActivity);
                        } catch (ProfilerException e) {
                            final String msg = "Error while profiling user: " +
                                               resolvedActivity.getUserId();
                            LOGGER.error(msg, e);
                        }

                        ExtendedUserProfile extProfile = null;
                        if (profile != null) {
                            extProfile = new ExtendedUserProfile(profile);
                            extProfile.addActivity(resolvedActivity);
                        }

                        exchange.getIn().setBody(extProfile);
                    }
                })
                .filter(body().isNotNull())
                .multicast().to(DIRECT_PROFILE, DIRECT_EXPORT);

        from(DIRECT_PROFILE)
                .process(new Processor(){
                    @Override
                    public void process(Exchange exchange) throws Exception {
                        Message msg = exchange.getIn();
                        ExtendedUserProfile extProfile =
                                msg.getBody(ExtendedUserProfile.class);
                        msg.setBody(extProfile.getProfile());
                    }
                })
                .marshal().json(JsonLibrary.Jackson)
                .convertBodyTo(String.class)
                .multicast().to(toWriter(), toAnalyser());

        from(DIRECT_EXPORT)
                .marshal().json(JsonLibrary.Jackson)
                .convertBodyTo(String.class)
                .to(toExporter());
    }

    protected String toAnalyser() {
        return "kestrel://{{kestrel.queue.analyser.profiles.url}}";
    }

    protected String toWriter() {
        return "kestrel://{{kestrel.queue.profiles.url}}";
    }

    protected String toExporter() {
        return "kestrel://{{kestrel.queue.exporter.url}}";
    }

    protected String fromKestrel() {
        return "kestrel://{{kestrel.queue.profiler.url}}?concurrentConsumers=10&waitTimeMs=500";
    }

    protected String errorEndpoint() {
        return "log:" + getClass().getSimpleName() + "?{{camel.log.options.error}}";
    }


}
