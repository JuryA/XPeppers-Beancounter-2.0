package io.beancounter.profiler.process;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import io.beancounter.commons.helper.jackson.ObjectMapperFactory;
import io.beancounter.commons.model.ExtendedUserProfile;
import io.beancounter.commons.model.User;
import io.beancounter.commons.model.UserProfile;
import io.beancounter.commons.model.VerbRandomizer;
import io.beancounter.commons.model.activity.Activity;
import io.beancounter.commons.model.activity.ResolvedActivity;
import io.beancounter.commons.model.auth.OAuthAuth;
import io.beancounter.commons.tests.TestsBuilder;
import io.beancounter.commons.tests.TestsException;
import io.beancounter.profiler.Profiler;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.testng.CamelTestSupport;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Binder;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;

public class ProfilerRouteTest extends CamelTestSupport {
    private Injector injector;
    private Profiler profiler;

    private ObjectMapper mapper = ObjectMapperFactory.createMapper();

    @Override
    protected RouteBuilder createRouteBuilder() throws Exception {
        return injector.getInstance(ProfilerRoute.class);
    }

    @Override
    public String isMockEndpoints() {
        // override this method and return the pattern for which endpoints to mock.
        // use * to indicate all
        return "*";
    }

    @BeforeMethod
    public void setUp() throws Exception {

        injector = Guice.createInjector(new Module() {

            @Override
            public void configure(Binder binder) {
                profiler = mock(Profiler.class);
                binder.bind(Profiler.class).toInstance(profiler);

                binder.bind(ProfilerRoute.class).toInstance(new ProfilerRoute() {
                    @Override
                    public String fromKestrel() {
                        return "direct:start";
                    }

                    @Override
                    public String errorEndpoint() {
                        return "mock:error";
                    }

                    @Override
                    protected String toWriter() {
                        return "direct:writer";
                    }

                    @Override
                    protected String toAnalyser() {
                        return "direct:analyser";
                    }

                    @Override
                    protected String toExporter() {
                        return "direct:exporter";
                    }
                });
            }
        });
        super.setUp();
    }


    @Test
    public void activityReachesProfiler() throws Exception {
        ResolvedActivity resolvAct = createRandomResolvedActivity(null);

        MockEndpoint error = getMockEndpoint("mock:error");
        error.expectedMessageCount(0);

        template.sendBody("direct:start", asJSON(resolvAct));

        verify(profiler).profile(any(ResolvedActivity.class));
        error.assertIsSatisfied();
    }

    @Test
    public void profileReachesWriter() throws Exception {
        ResolvedActivity resolvAct = createRandomResolvedActivity(null);
        UserProfile profile = new UserProfile(resolvAct.getUserId());

        when(profiler.profile(any(ResolvedActivity.class))).
                thenReturn(profile);

        MockEndpoint writer = getMockEndpoint("mock:direct:writer");
        writer.expectedBodiesReceived(asJSON(profile));

        template.sendBody("direct:start", asJSON(resolvAct));

        writer.assertIsSatisfied();
    }

    @Test
    public void profileReachesAnalyser() throws Exception {
        ResolvedActivity resolvAct = createRandomResolvedActivity(null);
        UserProfile profile = new UserProfile(resolvAct.getUserId());

        when(profiler.profile(any(ResolvedActivity.class))).
                thenReturn(profile);

        MockEndpoint writer = getMockEndpoint("mock:direct:analyser");
        writer.expectedBodiesReceived(asJSON(profile));

        template.sendBody("direct:start", asJSON(resolvAct));

        writer.assertIsSatisfied();
    }

    @Test
    public void extendedProfileReachesExporter() throws Exception {
        ResolvedActivity resolvAct = createRandomResolvedActivity(null);
        UserProfile profile = new UserProfile(resolvAct.getUserId());
        ExtendedUserProfile extProfile = new ExtendedUserProfile(profile);
        extProfile.addActivity(resolvAct);

        when(profiler.profile(any(ResolvedActivity.class))).
                thenReturn(profile);

        MockEndpoint exporter = getMockEndpoint("mock:direct:exporter");
        exporter.expectedBodiesReceived(asJSON(extProfile));

        template.sendBody("direct:start", asJSON(resolvAct));

        exporter.assertIsSatisfied();
    }

    private ResolvedActivity createRandomResolvedActivity(Activity act) {
        if (act == null) {
            act = createRandomActivity();
        }
        User user = new User("John", "Doe", "dude", "secret");
        user.addService("chumhum", new OAuthAuth("session", "secret"));

        return new ResolvedActivity(user.getId(), act, user);
    }

    private Activity createRandomActivity() {
        try {
            TestsBuilder testsBuilder = TestsBuilder.getInstance();
            testsBuilder.register(new VerbRandomizer("verb-randomizer"));
            return testsBuilder.build().build(Activity.class).getObject();
        } catch (TestsException e) {
            throw new RuntimeException(e);
        }
    }

    private String asJSON(Object obj) throws Exception {
        return mapper.writeValueAsString(obj);
    }
}

