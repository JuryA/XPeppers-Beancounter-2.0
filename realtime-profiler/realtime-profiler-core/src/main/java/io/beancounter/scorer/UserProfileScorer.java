package io.beancounter.scorer;

import io.beancounter.commons.model.Category;
import io.beancounter.commons.model.Topic;
import io.beancounter.commons.model.UserProfile;
import io.beancounter.profiler.taxonomy.TaxonomyIndex;
import io.beancounter.profiler.taxonomy.TaxonomyIndexImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserProfileScorer {

	private UserProfile userProfile;

	private List<Category> categoriesScore = new ArrayList<Category>();
	
	private TaxonomyIndex taxonomyIndex = new TaxonomyIndexImpl();
	
	public UserProfileScorer(UserProfile userProfile) {
		this.userProfile = userProfile;
	}

	public double sumScores() {
		
		double sumScore = 0.0;
		for(Topic<Category> category : categoriesScore){
			sumScore += category.getWeight();
		}
		return sumScore;
	}

	public void compute(Integer taxonomyLevel) {
		
		double totalWeight = computeTotalWeight();
		
		Map<Integer, Category> localScores = new HashMap<Integer, Category>();
	
		for(Category category : userProfile.getCategories()){
			Category drilledCategory = null;
			if(taxonomyLevel != null){
				drilledCategory = taxonomyIndex.getAncestorCategory(category.getTaxonomyNodeId(), taxonomyLevel);
			}else{
				drilledCategory = new Category(category);
			}
			
			if(localScores.containsKey(drilledCategory.getTaxonomyNodeId())){
				localScores.get(drilledCategory.getTaxonomyNodeId()).setWeight(localScores.get(drilledCategory.getTaxonomyNodeId()).getWeight() + category.getWeight());
			}else{
				drilledCategory.setWeight(category.getWeight());
				localScores.put(drilledCategory.getTaxonomyNodeId(), drilledCategory);
			}
		}
		
		for(Category category : localScores.values()){
			category.setWeight(100 * (category.getWeight() / totalWeight));
			categoriesScore.add(category);
		}
	}

	private double computeTotalWeight() {
		double totalWeight = 0.0;
		for(Topic<Category> category : userProfile.getCategories()){
			totalWeight += category.getWeight();
		}
		return totalWeight;
	}
	
	public void compute() {
		compute(null);
	}

	public List<Category> getScores() {
		return categoriesScore;
	}


}
