package io.beancounter.profiler.rules.custom;

import io.beancounter.commons.linking.LinkingEngine;
import io.beancounter.commons.nlp.NLPEngine;
import io.beancounter.profiler.rules.ObjectProfilingRule;
import io.beancounter.profiler.taxonomy.TaxonomyIndex;

import java.util.Properties;

public class GenericObjectProfilingRule
extends ObjectProfilingRule<io.beancounter.commons.model.activity.Object> {

    public GenericObjectProfilingRule(
            io.beancounter.commons.model.activity.Object object,
            NLPEngine nlpEngine,
            LinkingEngine linkingEngine
            ) {
        super(object, nlpEngine, linkingEngine);
    }

    public GenericObjectProfilingRule(
            io.beancounter.commons.model.activity.Object object,
            NLPEngine nlpEngine,
            LinkingEngine linkingEngine,
            TaxonomyIndex taxonomyIndex
            ) {
        super(object, nlpEngine, linkingEngine, taxonomyIndex);
    }

    @Override
    public void run(Properties properties) {
        getLogger().debug("rule started");

        process(nlpEngineResultFor(getObject().getUrl()));

        getLogger().debug("rule ended with {} interests found", interests.size());
    }

}