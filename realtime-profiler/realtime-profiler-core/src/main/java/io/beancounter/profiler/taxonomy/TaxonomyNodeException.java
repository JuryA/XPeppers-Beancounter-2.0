package io.beancounter.profiler.taxonomy;

public class TaxonomyNodeException extends Exception {
    public TaxonomyNodeException(String message, Exception e) {
        super(message, e);
    }
}
