package io.beancounter.profiler.rules;

import io.beancounter.commons.linking.LinkingEngine;
import io.beancounter.commons.model.Category;
import io.beancounter.commons.model.Interest;
import io.beancounter.commons.model.Topic;
import io.beancounter.commons.nlp.Entity;
import io.beancounter.commons.nlp.NLPEngine;
import io.beancounter.commons.nlp.NLPEngineException;
import io.beancounter.commons.nlp.NLPEngineResult;
import io.beancounter.profiler.DuplicateTaxonomyIdException;
import io.beancounter.profiler.taxonomy.TaxonomyIndex;
import io.beancounter.profiler.taxonomy.TaxonomyNode;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class ObjectProfilingRule<T extends io.beancounter.commons.model.activity.Object>
implements ProfilingRule {


    protected final Set<Interest> interests = new HashSet<Interest>();

    protected final Set<Category> categories = new HashSet<Category>();

    private T object;

    private NLPEngine nlpEngine;

    private LinkingEngine linkingEngine;

    private TaxonomyIndex taxonomyIndex;

    private Logger LOGGER = null;

    public ObjectProfilingRule(NLPEngine nlpEngine, LinkingEngine linkingEngine) {
        this.nlpEngine = nlpEngine;
        this.linkingEngine = linkingEngine;
    }

    public ObjectProfilingRule(T object, NLPEngine nlpEngine, LinkingEngine linkingEngine) {
        this(nlpEngine, linkingEngine);
        this.object = object;
    }

    public ObjectProfilingRule(T object, NLPEngine nlpEngine, LinkingEngine linkingEngine, TaxonomyIndex index) {
        this(object, nlpEngine, linkingEngine);
        this.taxonomyIndex = index;
    }

    @Override
    public List<Interest> getInterests() {
        ArrayList<Interest> interests = new ArrayList<Interest>();
        for (Interest interest : this.interests) {
            interests.add(new Interest(interest));
        }
        return interests;
    }

    @Override
    public List<Category> getCategories() {
        ArrayList<Category> categories = new ArrayList<Category>();
        for (Category category : this.categories) {
            categories.add(new Category(category));
        }
        return categories;
    }

    @Override
    public NLPEngine getNLPEngine() {
        return nlpEngine;
    }

    @Override
    public LinkingEngine getLinkingEngine() {
        return linkingEngine;
    }

    public TaxonomyIndex getTaxonomyIndex() {
        return taxonomyIndex;
    }

    protected T getObject() {
        return object;
    }

    protected void wire(Topic topic, io.beancounter.commons.nlp.Feature nlpFeature) {
        TaxonomyNode indexedNode = getTaxonomyIndex().lookupAndCreateByPath(nlpFeature.getPath());
        topic.setTaxonomyNodeId(indexedNode.getId());
    }

    protected void grabCategoriesFrom(Set<io.beancounter.commons.nlp.Category> nlpCategories) {
        for (io.beancounter.commons.nlp.Category nlpCategory : nlpCategories) {
            Category category = new Category(nlpCategory.getLabel(), nlpCategory.getResource());

            wire(category, nlpCategory);

            category.setWeight(1.0);
            categories.add(category);
        }
    }

    protected void grabInterestsFrom(Set<Entity> nlpEntities) {
        for (io.beancounter.commons.nlp.Entity nlpEntity : nlpEntities) {
            Interest interest = new Interest(nlpEntity.getLabel(), nlpEntity.getResource());

            wire(interest, nlpEntity);

            interest.setWeight(1.0);
            interests.add(interest);
        }
    }

    protected void process(NLPEngineResult nlpEngineResult) {
        try {
            getTaxonomyIndex().updateFrom(nlpEngineResult);
        } catch (DuplicateTaxonomyIdException e) {
            final String errMsg = "Unable to update taxonomy index from NLPEngineResult";
            LOGGER.error(errMsg, e);
        }

        grabCategoriesFrom(nlpEngineResult.getCategories());
        grabInterestsFrom(nlpEngineResult.getEntities());
    }

    protected NLPEngineResult nlpEngineResultFor(URL url) {
        NLPEngineResult result = new NLPEngineResult();
        try {
            result = getNLPEngine().enrich(url);
        } catch (NLPEngineException e) {
            final String errMsg = "Error while extracting knowledge from text [" + url + "]";
            LOGGER.error(errMsg, e);
        }
        return result;
    }

    protected NLPEngineResult nlpEngineResultFor(String text) {
        NLPEngineResult result = new NLPEngineResult();
        try {
            result = getNLPEngine().enrich(text);
        } catch (NLPEngineException e) {
            final String errMsg = "Error while extracting knowledge from text [" + text + "]";
            LOGGER.error(errMsg, e);
        }
        return result;
    }

    protected Logger getLogger() {
        if (LOGGER == null) {
            LOGGER = LoggerFactory.getLogger(getClass());
        }
        return LOGGER;
    }

}
