package io.beancounter.profiler.taxonomy;

import java.util.List;

public interface TaxonomyNodeRepository {

	boolean store(TaxonomyNode node);
	
	TaxonomyNode lookup(String nodeId) throws Exception;

    List<String> getAllKeys() throws Exception;

}
