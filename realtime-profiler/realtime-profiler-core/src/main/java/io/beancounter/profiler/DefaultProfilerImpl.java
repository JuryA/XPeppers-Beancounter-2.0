package io.beancounter.profiler;

import io.beancounter.commons.linking.LinkingEngine;
import io.beancounter.commons.model.Category;
import io.beancounter.commons.model.Interest;
import io.beancounter.commons.model.Topic;
import io.beancounter.commons.model.UserProfile;
import io.beancounter.commons.model.activity.*;
import io.beancounter.commons.model.activity.Object;
import io.beancounter.commons.model.activity.facebook.Like;
import io.beancounter.commons.nlp.NLPEngine;
import io.beancounter.profiler.rules.ObjectProfilingRule;
import io.beancounter.profiler.rules.ProfilingRule;
import io.beancounter.profiler.rules.custom.DevNullProfilingRule;
import io.beancounter.profiler.rules.custom.FacebookLikeProfilingRule;
import io.beancounter.profiler.rules.custom.GenericObjectProfilingRule;
import io.beancounter.profiler.rules.custom.TweetProfilingRule;
import io.beancounter.profiler.taxonomy.TaxonomyIndex;
import io.beancounter.profiler.utils.Utils;
import io.beancounter.profiles.ProfileManager;
import io.beancounter.profiles.ProfileManagerException;

import java.lang.reflect.Constructor;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * In-memory, default implementation of {@link Profiler}.
 *
 * @author Davide Palmisano ( dpalmisano@gmail.com )
 * 
 */
public final class DefaultProfilerImpl implements Profiler {

    private static Logger LOGGER = LoggerFactory.getLogger(DefaultProfilerImpl.class);

    private final Map<Class<? extends io.beancounter.commons.model.activity.Object>, Class<? extends ObjectProfilingRule>>
    objectRules = new HashMap<Class<? extends Object>, Class<? extends ObjectProfilingRule>>();

    private final Map<Class<? extends io.beancounter.commons.model.activity.Object>, Class<? extends TaxonomyIndex>>
    objectTaxonomies = new HashMap<Class<? extends Object>, Class<? extends TaxonomyIndex>>();

    private final ProfileManager profileManager;

    private final NLPEngine nlpEng;

    private final LinkingEngine linkEng;

    private final Properties properties;

    private TaxonomyIndex taxonomyIndex;

    @Inject
    public DefaultProfilerImpl(ProfileManager profileManager,
            NLPEngine nlpEngine,
            LinkingEngine linkingEngine,
            TaxonomyIndex taxonomyIndex,
            @Named("profilerProperties") Properties properties)
                    throws ProfilerException {
        this.profileManager = profileManager;
        this.nlpEng = nlpEngine;
        this.taxonomyIndex = taxonomyIndex;
        this.linkEng = linkingEngine;
        this.properties = properties;

        registerRule(Tweet.class, TweetProfilingRule.class);
        registerRule(Like.class, FacebookLikeProfilingRule.class);
        registerRule(io.beancounter.commons.model.activity.Object.class, GenericObjectProfilingRule.class);

        taxonomyIndex.loadFromRepository();
    }

    @Override
    public void registerRule(Class<? extends io.beancounter.commons.model.activity.Object> type, Class<? extends ObjectProfilingRule> rule) throws ProfilerException {
        objectRules.put(type, rule);
    }

    @Override
    public void registerRule(Verb verb, ProfilingRule rule) throws ProfilerException {
        throw new UnsupportedOperationException("NIY");
    }

    private ObjectProfilingRule getRule(Activity activity) throws ProfilerException {

        Class<? extends Object> activityType = activity.getObject().getClass();

        Class<? extends ObjectProfilingRule> ruleClass = objectRules.get(activityType);

        if(ruleClass == null) {
            // if I can't handle it, then send to >> /dev/null
            LOGGER.warn("it seems I can't handle this activity [" + activity + "], setting to /dev/null rule");
            ruleClass = DevNullProfilingRule.class;
        }

        ObjectProfilingRule objectProfilingProfile = build(ruleClass, activityType, activity.getObject(), this.getNLPEngine(), this.getLinkingEngine(), this.taxonomyIndex);
        return objectProfilingProfile;
    }

    @Override
    public UserProfile profile(ResolvedActivity resolvedAct)
            throws ProfilerException
    {
        UUID userId = resolvedAct.getUserId();
        Activity activity = resolvedAct.getActivity();
        LOGGER.info("profiling started for user [" + userId + "]");

        ObjectProfilingRule profRule = getRule(activity);
        runRule(profRule, properties, activity);

        double multiplier = getMultiplier(activity.getVerb());
        UserProfile userProfile = grabOldProfile(userId);
        if (userProfile != null) {
            userProfile = update(userProfile, profRule, activity, multiplier);
        } else {
            userProfile = create(profRule, userId, activity, multiplier);
        }

        try {
            profileManager.store(userProfile, resolvedAct.getUser());
        } catch (ProfileManagerException e) {
            final String errMsg = "Error storing profile of user: " + userId;
            LOGGER.error(errMsg, e);
            throw new ProfilerException(errMsg, e);
        }

        LOGGER.info("profiling ended nicely for user [" + userId + "]");
        return userProfile;
    }

    /*
     * In the creation of the user we should store
     * 	category id :point (+1)
     */
    private UserProfile create(ObjectProfilingRule opr, UUID userId, Activity activity, Double multiplier) {
        /*
         * Currently Ignored
         */
        List<Interest> newInterests;

        newInterests = opr.getInterests();

        List<Interest> interests = setIdAndWeight(
                activity.getId(),
                newInterests,
                multiplier
                );
        interests = limit(interests);
        interests = normalize(interests);

        // Categories
        Collection<Category> newCategories;

        newCategories = opr.getCategories();

        List<Category> categories = setWeight(newCategories, multiplier);
        /*
        //categories = limit(categories);	// not used -> in creation we just have cat id : +1
        //categories = normalize(categories);	// same
         * 
         */
        return toProfile(userId, interests, categories);
    }

    /*
     * Update profile, we should update the counters
     * of the user:
     * 	category id : prev +1
     */
    private UserProfile update(UserProfile old, ObjectProfilingRule rule, Activity activity, Double multiplier) {
        Collection<Interest> oldInterests = old.getInterests();
        Collection<Category> oldCategories = old.getCategories();

        // Interests
        List<Interest> newInterests;

        newInterests = rule.getInterests();

        newInterests = setIdAndWeight(
                activity.getId(),
                newInterests,
                multiplier
                );
        Collection<Interest> updatedInterests;
        updatedInterests = update(oldInterests, newInterests);

        // Categories
        List<Category> newCategories;

        newCategories = rule.getCategories();

        newCategories = setWeight(
                newCategories,
                multiplier
                );

        Collection<Category> updatedCategories;

        updatedCategories = update(oldCategories, newCategories);

        old.setInterests(new HashSet<Interest>(updatedInterests));
        old.setCategories(new HashSet<Category>(updatedCategories));
        old.setLastUpdated(DateTime.now());

        return old;
    }

    private List<Category> setWeight(Collection<Category> newCategories, double multiplier) {
        List<Category> returningCategories = new ArrayList<Category>(newCategories);
        normalize(returningCategories);
        for (Topic<Category> c : returningCategories) {
            c.setWeight((c.getWeight() * multiplier));
        }
        return returningCategories;
    }

    private <T extends Topic> List<T> normalize(Collection<T> topics) {
        double sum = 0.0d;
        for(Topic t : topics) {
            sum += t.getWeight();
        }
        for(Topic t : topics) {
            t.setWeight( t.getWeight() / sum );
        }
        return new ArrayList<T>(topics);
    }

    private <T extends Topic> List<T> limit(List<T> topics) {
        int limit = getLimit();
        if (topics.size() > limit) {
            // sort them again
            Collections.sort(topics);
            Set<T> limited = new HashSet<T>();
            for (int i = 0; i < limit; i++) {
                limited.add(topics.get(i));
            }
            return new ArrayList<T>(limited);
        }
        return topics;
    }

    private <T extends Topic> Collection<T> update(Collection<T> oldTopics, List<T> topicToMergeIn) {
        Collection<T> updated;
        updated = merge(topicToMergeIn, new HashSet<T>(oldTopics));
        // return the new profile
        //updated = limit(new ArrayList<T>(updated));
        //updated = normalize(updated);
        return updated;
    }

    private void runRule(ObjectProfilingRule opr, Properties properties, Activity activity) {
        opr.run(properties);
    }

    private UserProfile grabOldProfile(UUID userId) throws ProfilerException {
        UserProfile old;
        try {
            old = getProfile(userId);
        } catch (ProfileManagerException e) {
            final String errMsg = "Error while looking up the old profile for user with id [" + userId + "]";
            LOGGER.error(errMsg, e);
            throw new ProfilerException(errMsg, e);
        }
        return old;
    }

    /*
     * sum points coming from the topics of the new activity
     */
    private <T extends Topic> Collection<T> merge(Collection<T> newTopics, Set<T> oldTopics) {
        Collection<T> merged = new HashSet<T>();
        Collection<T> newInterests = new HashSet<T>();

        // put in merged only the new ones
        for(T receivedTopic : newTopics) {
            if(Utils.contains(receivedTopic, oldTopics)) {

                URI resource = receivedTopic.getResource();

                T oldTopic = Utils.retrieve(resource, oldTopics);

                int threshold = Integer.parseInt(properties.getProperty("interest.activities.limit"), 10);

                merged.add(
                        (T) oldTopic.merge(receivedTopic, oldTopic, threshold)
                        );

                // remove it from the old ones
                oldTopics.remove(oldTopic);

            } else {
                newInterests.add(receivedTopic);
            }
        }

        // now perform the union between newOnes and oldOnes
        List<T> union = Utils.union(newInterests, oldTopics);
        // if |union| + |merged| > limit, than free some space.

        if(union.size() + merged.size() > getLimit()) {
            Collections.sort(union);
            union = Utils.cut(union, union.size() - merged.size());
        }
        // then add the merged ones
        for(T m : merged) {
            union.add(m);
        }
        return union;
    }

    private int getLimit() {
        return Integer.parseInt(properties.getProperty("interest.limit"));
    }

    private UserProfile toProfile(UUID userId, Collection<Interest> interests, Collection<Category> categories) {
        UserProfile up = new UserProfile(userId);
        up.setInterests(new HashSet<Interest>(interests));
        up.setCategories(new HashSet<Category>(categories));
        up.setVisibility(UserProfile.Visibility.PUBLIC);
        up.setLastUpdated(DateTime.now());
        return up;
    }

    private List<Interest> setIdAndWeight(UUID activityId, Collection<Interest> newInterests, double multiplier) {
        normalize(newInterests);
        for (Interest i : newInterests) {
            i.addActivity(activityId);
            i.setVisible(true);
            i.setWeight((i.getWeight() * multiplier));
        }
        return new ArrayList<Interest>(newInterests);
    }

    private double getMultiplier(Verb verb) {
        String verbProperty = properties.getProperty("verb.multiplier." + verb);
        if (verbProperty == null) {
            LOGGER.info("Missing property for verb " + verb);
            return 1;
        }
        return Double.parseDouble(verbProperty);
    }

    private UserProfile getProfile(UUID userId) throws ProfileManagerException {
        return profileManager.lookup(userId);
    }

    @Override
    public ProfileManager getProfileStore() {
        return profileManager;
    }

    @Override
    public NLPEngine getNLPEngine() {
        return nlpEng;
    }

    @Override
    public LinkingEngine getLinkingEngine() {
        return linkEng;
    }

    private ObjectProfilingRule build(
            Class<? extends ObjectProfilingRule> ruleClass,
            Class<? extends Object> type,
            Object object,
            NLPEngine nlpEngine,
            LinkingEngine linkEng,
            TaxonomyIndex index
            ) throws ProfilerException {
        if(ruleClass.equals(DevNullProfilingRule.class)) {
            return new DevNullProfilingRule();
        }
        Constructor<? extends ObjectProfilingRule> constructor;
        try {
            constructor = ruleClass.getConstructor(
                    type,
                    NLPEngine.class,
                    LinkingEngine.class,
                    TaxonomyIndex.class
                    );
        } catch (NoSuchMethodException e) {
            final String errMsg = "cannot find a constructor with type [" + type.getName() + "]";
            LOGGER.error(errMsg, e);
            throw new ProfilerException(errMsg, e);
        }
        try {
            return constructor.newInstance(object, nlpEngine, linkEng, index);
        } catch (Exception e) {
            final String errMsg = "error while instantiating a [" + ruleClass.getName() + "] with type [" + type.getName() + "]";
            LOGGER.error(errMsg, e);
            throw new ProfilerException(errMsg, e);
        }
    }

    @Override
    public void setTaxonomyIndex(TaxonomyIndex index) {
        this.taxonomyIndex = index;
    }

    @Override
    public TaxonomyIndex getTaxonomyIndex() {
        return this.taxonomyIndex;
    }

}
