package io.beancounter.profiler.taxonomy;

import io.beancounter.commons.helper.jackson.ObjectMapperFactory;
import io.beancounter.commons.helper.jedis.JedisPoolFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.exceptions.JedisConnectionException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.name.Named;

public class JedisTaxonomyNodeRepository implements TaxonomyNodeRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(JedisTaxonomyNodeRepository.class);

    private JedisPool jedisPool;
    private final ObjectMapper mapper;

    @Inject
    @Named("redis.db.taxonomy")
    private int database;

    @Inject
    public JedisTaxonomyNodeRepository(JedisPoolFactory factory) {
        this.setJedisPool(factory.build());
        mapper = ObjectMapperFactory.createMapper();
    }

    @Override
    public synchronized boolean store(TaxonomyNode node) {
        String nodeJson;
        try {
            nodeJson = mapper.writeValueAsString(node);
        } catch (IOException e) {
            final String errMsg = "Error while getting json for node [" + node.getId() + "]";
            LOGGER.error(errMsg, e);
            return false;
        }
        Jedis jedis = null;
        boolean isConnectionIssue = false;
        try {
            jedis = getJedisResource();
            jedis.set(node.getId().toString(), nodeJson);
            return true;
        } catch (JedisConnectionException e) {
            isConnectionIssue = true;
            final String errMsg = "Jedis Connection error while storing node [" + node.getId() + "]";
            LOGGER.error(errMsg, e);
        } catch (Exception e) {
            final String errMsg = "Error while storing node [" + node.getId() + "]";
            LOGGER.error(errMsg, e);
        } finally {
            if (isConnectionIssue) {
                getJedisPool().returnBrokenResource(jedis);
            } else {
                getJedisPool().returnResource(jedis);
            }
        }
        return false;
    }

    @Override
    public List<String> getAllKeys() throws Exception {
        LOGGER.debug("looking up keys of taxonomy");
        Jedis jedis = getJedisResource();
        List<String> keys = new ArrayList<String>();;
        boolean isConnectionIssue = false;
        try {
            Set<String> jedisKeys = jedis.keys("*");
            Iterator<String> it = jedisKeys.iterator();
            while (it.hasNext()) {
                keys.add(it.next());
            }
            return keys;
        } catch (JedisConnectionException e) {
            isConnectionIssue = true;
            final String errMsg = "Error while retrieving all keys";
            LOGGER.error(errMsg, e);
        } catch (Exception e) {
            final String errMsg = "Error while retrieving all keys";
            LOGGER.error(errMsg, e);
        } finally {
            if(isConnectionIssue) {
                getJedisPool().returnBrokenResource(jedis);
            } else {
                getJedisPool().returnResource(jedis);
            }
        }

        return keys;
    }

    @Override
    public TaxonomyNode lookup(String nodeId) throws Exception {
        LOGGER.debug("looking up profile for taxonomy node [" + nodeId + "]");
        TaxonomyNode taxonomyNode;
        Jedis jedis = getJedisResource();
        String stringNode;
        boolean isConnectionIssue = false;
        try {
            stringNode = jedis.get(nodeId);
        } catch (JedisConnectionException e) {
            isConnectionIssue = true;
            final String errMsg = "Error while retrieving taxonomy node [" + nodeId + "]";
            LOGGER.error(errMsg, e);
            throw new TaxonomyNodeException(errMsg, e);
        } catch (Exception e) {
            final String errMsg = "Error while retrieving taxonomy node [" + nodeId + "]";
            LOGGER.error(errMsg, e);
            throw new TaxonomyNodeException(errMsg, e);
        } finally {
            if(isConnectionIssue) {
                getJedisPool().returnBrokenResource(jedis);
            } else {
                getJedisPool().returnResource(jedis);
            }
        }
        if(stringNode == null) {
            return null;
        }
        try {
            taxonomyNode = mapper.readValue(stringNode, TaxonomyNode.class);
        } catch (IOException e) {
            final String errMsg = "Error while getting json for taxonomy node [" + nodeId + "]";
            LOGGER.error(errMsg, e);
            throw new TaxonomyNodeException(errMsg, e);
        }
        LOGGER.debug("Taxonomy node for [" + nodeId + "] looked up properly");
        return taxonomyNode;
    }

    public JedisPool getJedisPool() {
        return jedisPool;
    }

    public void setJedisPool(JedisPool jedisPool) {
        this.jedisPool = jedisPool;
    }

    private Jedis getJedisResource() throws Exception {
        Jedis jedis;
        try {
            jedis = getJedisPool().getResource();
        } catch (Exception e) {
            final String errMsg = "Error while getting a Jedis resource";
            LOGGER.error(errMsg, e);
            throw new Exception(errMsg, e);
        }
        boolean isConnectionIssue = false;
        try {
            jedis.select(database);
        } catch (JedisConnectionException e) {
            isConnectionIssue = true;
            final String errMsg = "Jedis Connection error while selecting database [" + database + "]";
            LOGGER.error(errMsg, e);
            throw new Exception(errMsg, e);
        } catch (Exception e) {
            getJedisPool().returnResource(jedis);
            final String errMsg = "Error while selecting database [" + database + "]";
            LOGGER.error(errMsg, e);
            throw new Exception(errMsg, e);
        } finally {
            if (isConnectionIssue) {
                getJedisPool().returnBrokenResource(jedis);
            }
        }
        return jedis;
    }


}
