package io.beancounter.profiler.taxonomy;

import io.beancounter.commons.model.Category;
import io.beancounter.commons.nlp.Entity;
import io.beancounter.commons.nlp.NLPEngineResult;
import io.beancounter.profiler.DuplicateTaxonomyIdException;
import io.beancounter.profiler.taxonomy.TaxonomyNode.Type;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

public class TaxonomyIndexImpl implements TaxonomyIndex {

    private static Logger LOGGER = LoggerFactory.getLogger(TaxonomyIndexImpl.class);

    private static final String ROOT_LABEL = "ROOT";
    private int nextInsertId = 0;

    protected Map<String, TaxonomyNode> index;
    protected Map<Integer, TaxonomyNode> indexById;
    protected TaxonomyNodeRepository taxonomyNodeRepository;

    public TaxonomyIndexImpl() {
        index = new HashMap<String, TaxonomyNode>();
        indexById = new HashMap<Integer, TaxonomyNode>();

        initRootNode();

        initFacebookPagesTaxonomy();

    }

    @Inject
    public TaxonomyIndexImpl(TaxonomyNodeRepository repository) {
        this();
        this.taxonomyNodeRepository = repository;
    }

    public TaxonomyNode createNode(Integer id, String label, Integer idParent) {
        return new TaxonomyNode(label, id, idParent);
    }

    private void populateIndex(TaxonomyNode node) {
        index.put(node.getLabel().toLowerCase(), node);
        indexById.put(node.getId(), node);
    }

    protected void initRootNode() {
        populateIndex(createNode(this.nextInsertId, ROOT_LABEL, null));
    }

    @Override
    public TaxonomyNode lookupById(Integer nodeId) {
        if (indexById.containsKey(nodeId)) {
            return indexById.get(nodeId);
        }
        return TaxonomyNode.nullNode();
    }

    @Override
    public TaxonomyNode addNode(TaxonomyNode node)
            throws DuplicateTaxonomyIdException {
        if (node.isNull()) {
            return node;
        }

        if (index.containsKey(node.getLabel())) {
            return index.get(node.getLabel());
        }

        if (indexById.containsKey(node.getId())) {
            throw new DuplicateTaxonomyIdException("Duplicated Id "
                    + node.getId());
        }

        populateIndex(node);
        return node;
    }

    @Override
    public boolean checkConsistency() {
        for (TaxonomyNode entry : index.values()) {
            Integer parentId = entry.getParentId();
            if (parentId != null && !indexById.containsKey(parentId)) {
                return false;
            }
        }
        return true;
    }

    private int level(TaxonomyNode node) {
        return (node.getParentId() == null) ? 0 : 1 + level(lookupById(node
                .getParentId()));
    }

    @Override
    public TaxonomyNode drillUp(TaxonomyNode node, Integer upToTaxonomyLevel) {

        if (upToTaxonomyLevel == null) {
            return node;
        }

        if (upToTaxonomyLevel >= level(node) || node.getParentId() == null) {
            return node;
        } else {
            return drillUp(lookupById(node.getParentId()), upToTaxonomyLevel);
        }
    }

    @Override
    public Category getAncestorCategory(Integer taxonomyNodeId,
                                        int taxonomyLevel) {

        TaxonomyNode node = this.drillUp(lookupById(taxonomyNodeId),
                taxonomyLevel);

        Category ancestorCategory = new Category();

        ancestorCategory.setLabel(node.getLabel());

        ancestorCategory.setTaxonomyNodeId(node.getId());

        return ancestorCategory;
    }

    @Override
    public TaxonomyNode lookupByPath(String path) {
        if (path == null || path.equals("/")) {
            return TaxonomyNode.nullNode();
        }

        TaxonomyNode taxonomyNode = index.get(normalizePath(path));
        return (null == taxonomyNode) ? TaxonomyNode.nullNode() : taxonomyNode;
    }

    @Override
    public TaxonomyNode lookupAndCreateByPath(String path) {
        TaxonomyNode taxonomyNode = lookupByPath(path);
        if (!taxonomyNode.isNull()) {
            return taxonomyNode;
        }

        try {
            taxonomyNode = createNodeFor(path);
        } catch (DuplicateTaxonomyIdException e) {
            taxonomyNode = TaxonomyNode.nullNode();
            LOGGER.debug("Duplicate Taxonomy Node for: " + path);
        }

        return taxonomyNode;
    }

    private TaxonomyNode createNodeFor(String path) throws DuplicateTaxonomyIdException {
        if (null == path) {
            return TaxonomyNode.nullNode();
        }
        return createNodeFor(Arrays.asList(path.split("/")));
    }

    private TaxonomyNode createNodeFor(List<String> path) throws DuplicateTaxonomyIdException {

        StringBuilder currentPathBuilder = new StringBuilder();
        TaxonomyNode node = TaxonomyNode.nullNode();
        Iterator<String> pathIterator = path.iterator();

        while (pathIterator.hasNext()) {

            String pathElement = pathIterator.next();

            if(pathElement.isEmpty()) {
                continue;
            }

            currentPathBuilder.append(pathElement);

            if(pathIterator.hasNext()){
                currentPathBuilder.append("/");
            }

            String nodePath = currentPathBuilder.toString().toLowerCase();

            node = lookupByPath(nodePath);

            if (node.isNull()) {
                node = createNode(hash(nodePath), nodePath, getParentNode(nodePath).getId());
                addNode(node);
                saveNode(node);
            }
        }
        return node;
    }

    private TaxonomyNode getParentNode(String nodePath) {
        if(nodePath.indexOf("/") == -1){
            return lookupByPath(ROOT_LABEL);
        }
        String parentPath = nodePath.substring(0, nodePath.lastIndexOf("/"));
        return lookupByPath(parentPath);
    }

    private Integer hash(String nodePath) {

        while(indexById.containsKey(nextInsertId)){
            nextInsertId++;
        }

        return nextInsertId;
    }

    @Override
    public boolean saveNode(TaxonomyNode node) {
        return taxonomyNodeRepository.store(node);
    }

    @Override
    public int size() {
        return this.indexById.size();
    }

    @Override
    public void updateFrom(NLPEngineResult result)
            throws DuplicateTaxonomyIdException {
        buildTreeFrom(result.getCategories());
        addLeavesFrom(result.getEntities());
    }

    private void addLeavesFrom(Set<Entity> entities)
            throws DuplicateTaxonomyIdException {
        for (Iterator<io.beancounter.commons.nlp.Entity> entityIterator = entities
                .iterator(); entityIterator.hasNext();) {
            updateFrom(entityIterator.next());
        }
    }

    private void buildTreeFrom(
            Set<io.beancounter.commons.nlp.Category> categories)
                    throws DuplicateTaxonomyIdException {
        for (Iterator<io.beancounter.commons.nlp.Category> categoryIterator = categories
                .iterator(); categoryIterator.hasNext();) {
            updateFrom(categoryIterator.next());
        }
    }

    private void updateFrom(Entity entity) throws DuplicateTaxonomyIdException {
        TaxonomyNode node = this.lookupAndCreateByPath(entity.getPath());
        node.setType(Type.INTEREST);
    }

    private void updateFrom(io.beancounter.commons.nlp.Category category)
            throws DuplicateTaxonomyIdException {
        this.lookupAndCreateByPath(category.getPath());
    }

    @Override
    public int getLastInsertId() {
        return this.nextInsertId;
    }

    private String normalizePath(String path) {
        if (path.startsWith("/")) {
            path = path.substring(1);
        }
        return path.toLowerCase();
    }

    protected boolean initFacebookPagesTaxonomy() {
        try{
            addNode(this.createNode(1, "Local business or place", 0));
            addNode(this.createNode(2506, "Airport", 1));
            addNode(this.createNode(2508, "Arts/Entertainment/Nightlife", 1));
            addNode(this.createNode(2523, "Attractions/Things to Do", 1));
            addNode(this.createNode(2509, "Automotive", 1));
            addNode(this.createNode(2512, "Bank/Financial Services", 1));
            addNode(this.createNode(1305, "Book Shop", 1));
            addNode(this.createNode(2518, "Business Services", 1));
            addNode(this.createNode(1111, "Cinema", 1));
            addNode(this.createNode(2101, "Club", 1));
            addNode(this.createNode(2519, "Community/Government", 1));
            addNode(this.createNode(1209, "Concert Venue", 1));
            addNode(this.createNode(1608, "Doctor", 1));
            addNode(this.createNode(2250, "Education", 1));
            addNode(this.createNode(2511, "Event Planning/Event Services", 1));
            addNode(this.createNode(2513, "Food/Grocery", 1));
            addNode(this.createNode(2514, "Health/Medical/Pharmacy", 1));
            addNode(this.createNode(2515, "Home Improvement", 1));
            addNode(this.createNode(2527, "Hospital/Clinic", 1));
            addNode(this.createNode(2501, "Hotel", 1));
            addNode(this.createNode(2503, "Landmark", 1));
            addNode(this.createNode(1607, "Lawyer", 1));
            addNode(this.createNode(1306, "Library", 1));
            addNode(this.createNode(2500, "Local Business", 1));
            addNode(this.createNode(2528, "Museum/Art Gallery", 1));
            addNode(this.createNode(2231, "Outdoor Gear/Sporting Goods", 1));
            addNode(this.createNode(2516, "Pet Services", 1));
            addNode(this.createNode(2517, "Professional Services", 1));
            addNode(this.createNode(2520, "Property", 1));
            addNode(this.createNode(2100, "Pub/bar", 1));
            addNode(this.createNode(2522, "Public Places", 1));
            addNode(this.createNode(1900, "Restaurant/Café", 1));
            addNode(this.createNode(2601, "School", 1));
            addNode(this.createNode(2521, "Shopping/Retail", 1));
            addNode(this.createNode(2510, "Spas/Beauty/Personal Care", 1));
            addNode(this.createNode(2507, "Sports Venue", 1));
            addNode(this.createNode(2524, "Sports/Recreation/Activities", 1));
            addNode(this.createNode(2525, "Tours/Sightseeing", 1));
            addNode(this.createNode(2526, "Transport", 1));
            addNode(this.createNode(2602, "University", 1));
            addNode(this.createNode(1013, "Company, organisation or institution", 0));
            addNode(this.createNode(2244, "Aerospace/Defence", 1013));
            addNode(this.createNode(2240, "Automobiles and Parts", 1013));
            addNode(this.createNode(2234, "Bank/Financial Institution", 1013));
            addNode(this.createNode(2254, "Biotechnology", 1013));
            addNode(this.createNode(2606, "Cause", 1013));
            addNode(this.createNode(2247, "Chemicals", 1013));
            addNode(this.createNode(2264, "Church/Religious Organisation", 1013));
            addNode(this.createNode(2260, "Community organisation", 1013));
            addNode(this.createNode(2200, "Company", 1013));
            addNode(this.createNode(2255, "Computers/Technology", 1013));
            addNode(this.createNode(2248, "Consulting/Business Services", 1013));
            addNode(this.createNode(2250, "Education", 1013));
            addNode(this.createNode(2238, "Energy/Utility", 1013));
            addNode(this.createNode(2251, "Engineering/Construction", 1013));
            addNode(this.createNode(2246, "Farming/Agriculture", 1013));
            addNode(this.createNode(2252, "Food/Beverages", 1013));
            addNode(this.createNode(2604, "Government Organisation", 1013));
            addNode(this.createNode(2214, "Health/Beauty", 1013));
            addNode(this.createNode(2243, "Health/Medical/Pharmaceuticals", 1013));
            addNode(this.createNode(2241, "Industrials", 1013));
            addNode(this.createNode(2236, "Insurance Company", 1013));
            addNode(this.createNode(2256, "Internet/Software", 1013));
            addNode(this.createNode(2249, "Legal/Law", 1013));
            addNode(this.createNode(2233, "Media/News/Publishing", 1013));
            addNode(this.createNode(2245, "Mining/Materials", 1013));
            addNode(this.createNode(2235, "Non-Governmental Organisation (NGO)", 1013));
            addNode(this.createNode(2603, "Non-Profit Organisation", 1013));
            addNode(this.createNode(2600, "Organisation", 1013));
            addNode(this.createNode(2261, "Political Organisation", 1013));
            addNode(this.createNode(2618, "Political Party", 1013));
            addNode(this.createNode(2239, "Retail and Consumer Merchandise", 1013));
            addNode(this.createNode(2601, "School", 1013));
            addNode(this.createNode(2237, "Small Business", 1013));
            addNode(this.createNode(2253, "Telecommunication", 1013));
            addNode(this.createNode(2242, "Transport/Freight", 1013));
            addNode(this.createNode(2258, "Travel/Leisure", 1013));
            addNode(this.createNode(2602, "University", 1013));
            addNode(this.createNode(3, "Brand or Product", 0));
            addNode(this.createNode(2301, "App Page", 3));
            addNode(this.createNode(2215, "Appliances", 3));
            addNode(this.createNode(2232, "Baby Goods/Kids Goods", 3));
            addNode(this.createNode(2206, "Bags/Luggage", 3));
            addNode(this.createNode(2303, "Board Game", 3));
            addNode(this.createNode(2216, "Building Materials", 3));
            addNode(this.createNode(2208, "Camera/Photo", 3));
            addNode(this.createNode(2205, "Cars", 3));
            addNode(this.createNode(2209, "Clothing", 3));
            addNode(this.createNode(2217, "Commercial Equipment", 3));
            addNode(this.createNode(2210, "Computers", 3));
            addNode(this.createNode(2263, "Drugs", 3));
            addNode(this.createNode(2213, "Electronics", 3));
            addNode(this.createNode(2252, "Food/Beverages", 3));
            addNode(this.createNode(2219, "Furniture", 3));
            addNode(this.createNode(2300, "Games/Toys", 3));
            addNode(this.createNode(2214, "Health/Beauty", 3));
            addNode(this.createNode(2218, "Home Decor", 3));
            addNode(this.createNode(2220, "Household Supplies", 3));
            addNode(this.createNode(2226, "Jewellery/Watches", 3));
            addNode(this.createNode(2221, "Kitchen/Cooking", 3));
            addNode(this.createNode(2212, "Office Supplies", 3));
            addNode(this.createNode(2231, "Outdoor Gear/Sporting Goods", 3));
            addNode(this.createNode(2222, "Patio/Garden", 3));
            addNode(this.createNode(2230, "Pet Supplies", 3));
            addNode(this.createNode(2265, "Phone/Tablet", 3));
            addNode(this.createNode(2201, "Product/Service", 3));
            addNode(this.createNode(2211, "Software", 3));
            addNode(this.createNode(2223, "Tools/Equipment", 3));
            addNode(this.createNode(2302, "Video Game", 3));
            addNode(this.createNode(2262, "Vitamins/Supplements", 3));
            addNode(this.createNode(2202, "Website", 3));
            addNode(this.createNode(2224, "Wine/Spirits", 3));
            addNode(this.createNode(4, "Artist, Band or Public Figure", 0));
            addNode(this.createNode(1103, "Actor/Director", 4));
            addNode(this.createNode(1601, "Artist", 4));
            addNode(this.createNode(1600, "Athlete", 1005));
            addNode(this.createNode(1301, "Author", 4));
            addNode(this.createNode(1609, "Business Person", 4));
            addNode(this.createNode(1606, "Chef", 4));
            addNode(this.createNode(1802, "Coach", 1005));
            addNode(this.createNode(1610, "Comedian", 4));
            addNode(this.createNode(1614, "Dancer", 4));
            addNode(this.createNode(1611, "Entertainer", 4));
            addNode(this.createNode(1113, "Fictional Character", 4));
            addNode(this.createNode(1114, "Film Character", 4));
            addNode(this.createNode(1701, "Government Official", 4));
            addNode(this.createNode(1604, "Journalist", 4));
            addNode(this.createNode(1612, "Monarch", 4));
            addNode(this.createNode(1202, "Musician/Band", 4));
            addNode(this.createNode(1605, "News Personality", 4));
            addNode(this.createNode(2632, "Pet", 4));
            addNode(this.createNode(1700, "Politician", 4));
            addNode(this.createNode(1108, "Producer", 4));
            addNode(this.createNode(1602, "Public Figure", 4));
            addNode(this.createNode(1613, "Teacher", 4));
            addNode(this.createNode(1109, "Writer", 4));
            addNode(this.createNode(5, "Entertainment", 0));
            addNode(this.createNode(1200, "Album", 5));
            addNode(this.createNode(1803, "Amateur Sports Team", 1005));
            addNode(this.createNode(1300, "Book", 5));
            addNode(this.createNode(1304, "Publisher", 1300));
            addNode(this.createNode(1305, "Book Shop", 1300));
            addNode(this.createNode(1306, "Library", 1300));
            addNode(this.createNode(1307, "Magazine", 1300));
            addNode(this.createNode(1308, "Literary Editor", 1300));
            addNode(this.createNode(1309, "Book Series", 1300));
            addNode(this.createNode(1111, "Cinema", 5));
            addNode(this.createNode(1208, "Concert Tour", 5));
            addNode(this.createNode(1209, "Concert Venue", 5));
            addNode(this.createNode(1113, "Fictional Character", 5));
            addNode(this.createNode(1114, "Film Character", 5));
            addNode(this.createNode(1306, "Library", 5));
            addNode(this.createNode(1307, "Magazine", 5));
            addNode(this.createNode(1105, "Movie", 5));
            addNode(this.createNode(1212, "Music Award", 5));
            addNode(this.createNode(1213, "Music Chart", 5));
            addNode(this.createNode(1207, "Music Video", 5));
            addNode(this.createNode(1801, "Professional Sports Team", 1005));
            addNode(this.createNode(1210, "Radio Station", 5));
            addNode(this.createNode(1211, "Record Label", 5));
            addNode(this.createNode(1804, "School Sports Team", 1005));
            addNode(this.createNode(1201, "Song", 5));
            addNode(this.createNode(1800, "Sports League", 1005));
            addNode(this.createNode(2507, "Sports Venue", 1005));
            addNode(this.createNode(1110, "Studio", 5));
            addNode(this.createNode(1404, "TV Channel", 1015));
            addNode(this.createNode(1402, "TV Channel", 1015));
            addNode(this.createNode(1400, "TV Show", 1015));
            addNode(this.createNode(1112, "TV/Film award", 1015));
            addNode(this.createNode(2701, "Arts/Humanities Website", 2202));
            addNode(this.createNode(2702, "Business/Economy Website", 2202));
            addNode(this.createNode(2703, "Computers/Internet Website", 2202));
            addNode(this.createNode(2704, "Education Website", 2202));
            addNode(this.createNode(2705, "Entertainment Website", 2202));
            addNode(this.createNode(2706, "Government Website", 2202));
            addNode(this.createNode(2707, "Health/Wellness Website", 2202));
            addNode(this.createNode(2708, "Home/Garden Website", 2202));
            addNode(this.createNode(2715, "Local/Travel Website", 2202));
            addNode(this.createNode(2709, "News/Media Website", 2202));
            addNode(this.createNode(2700, "Personal Blog", 2202));
            addNode(this.createNode(2717, "Personal Website", 2202));
            addNode(this.createNode(2710, "Recreation/Sports Website", 2202));
            addNode(this.createNode(2711, "Reference Website", 2202));
            addNode(this.createNode(2712, "Regional Website", 2202));
            addNode(this.createNode(2713, "Science Website", 2202));
            addNode(this.createNode(2714, "Society/Culture Website", 2202));
            addNode(this.createNode(2716, "Teens/Kids Website", 2202));
            addNode(this.createNode(1001, "Films", 5));
            addNode(this.createNode(1103, "Actor/Director", 1001));
            addNode(this.createNode(1105, "Movie", 1001));
            addNode(this.createNode(1108, "Producer", 1001));
            addNode(this.createNode(1109, "Writer", 1001));
            addNode(this.createNode(1110, "Studio", 1001));
            addNode(this.createNode(1111, "Cinema", 1001));
            addNode(this.createNode(1112, "TV/Film award", 1001));
            addNode(this.createNode(1113, "Fictional Character", 1001));
            addNode(this.createNode(1114, "Film Character", 1001));
            addNode(this.createNode(1015, "Television", 5));
            addNode(this.createNode(1405, "Episode", 1015));
            addNode(this.createNode(1005, "Sport", 5));
            addNode(this.createNode(1000, "Music", 5));

            return true;

        }catch(DuplicateTaxonomyIdException e)
        {
            return false;
        }
    }

    @Override
    public boolean loadFromRepository(){
        try {
            for (String nodeId : taxonomyNodeRepository.getAllKeys()){
                addNode(taxonomyNodeRepository.lookup(nodeId));
            }
            return true;
        } catch (DuplicateTaxonomyIdException e) {
            LOGGER.error("DuplicateExceptionId for " + e.getMessage());
        } catch (Exception e) {
            LOGGER.error("Error while loading from repository: " + e.getMessage());
        }
        return true;
    }
}