package io.beancounter.profiler.taxonomy;

public class TaxonomyNode {

    private final Integer id;
    private final Integer parentId;
    private final String label;
    private String uniqueName;

    public enum Type {
        CATEGORY, INTEREST, NULL
    };

    private Type type;

    public TaxonomyNode(String label, Integer id, Integer parentId) {
        this.label = label.toLowerCase();
        this.id = id;
        this.parentId = parentId;
        this.type = Type.CATEGORY;
    }

    public String getLabel() {
        return label;
    }

    public Integer getId() {
        return id;
    }

    public Integer getParentId() {
        return parentId;
    }

    public String getUniqueName() {
        return uniqueName;
    }

    public void setUniqueName(String uniqueName) {
        this.uniqueName = uniqueName;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public boolean isNull() {
        return getId() < 0 || getLabel().isEmpty();
    }

    public static TaxonomyNode nullNode() {
        TaxonomyNode nullNode = new TaxonomyNode("", -1, 0);
        nullNode.setType(Type.NULL);
        return nullNode;
    }
}
