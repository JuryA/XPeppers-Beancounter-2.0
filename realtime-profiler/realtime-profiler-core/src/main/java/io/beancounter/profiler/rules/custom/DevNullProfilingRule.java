package io.beancounter.profiler.rules.custom;

import io.beancounter.commons.linking.LinkingEngine;
import io.beancounter.commons.model.activity.Object;
import io.beancounter.commons.nlp.NLPEngine;
import io.beancounter.profiler.rules.ObjectProfilingRule;
import io.beancounter.profiler.taxonomy.TaxonomyIndex;

import java.util.Properties;

/**
 * It simulates a <i>/dev/null</i>
 *
 * @author Davide Palmisano ( dpalmisano@gmail.com )
 */
public class DevNullProfilingRule extends ObjectProfilingRule<Object> {

    public DevNullProfilingRule() {
        this(null, null, null, null);
    }

    private DevNullProfilingRule(Object object, NLPEngine nlpEngine, LinkingEngine linkingEngine) {
        super(object, nlpEngine, linkingEngine);
    }

    private DevNullProfilingRule(Object object, NLPEngine nlpEngine, LinkingEngine linkingEngine, TaxonomyIndex taxonomyIndex) {
        super(object, nlpEngine, linkingEngine, taxonomyIndex);
    }

    @Override
    public void run(Properties properties) {}
}
