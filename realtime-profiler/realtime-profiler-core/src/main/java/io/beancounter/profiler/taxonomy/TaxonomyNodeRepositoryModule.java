package io.beancounter.profiler.taxonomy;

import io.beancounter.commons.helper.PropertiesHelper;
import io.beancounter.commons.helper.jedis.DefaultJedisPoolFactory;
import io.beancounter.commons.helper.jedis.JedisPoolFactory;

import java.util.Properties;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;

public class TaxonomyNodeRepositoryModule extends AbstractModule {

	@Override
	protected void configure() {
		Properties properties = PropertiesHelper.readFromClasspath("/redis.properties");
		Names.bindProperties(binder(), properties);
        bind(JedisPoolFactory.class).to(DefaultJedisPoolFactory.class).asEagerSingleton();
        bind(TaxonomyNodeRepository.class).to(JedisTaxonomyNodeRepository.class);
	}
}
