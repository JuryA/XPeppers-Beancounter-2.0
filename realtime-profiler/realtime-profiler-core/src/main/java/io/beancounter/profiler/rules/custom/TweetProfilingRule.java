package io.beancounter.profiler.rules.custom;

import io.beancounter.commons.linking.LinkingEngine;
import io.beancounter.commons.model.activity.Tweet;
import io.beancounter.commons.nlp.NLPEngine;
import io.beancounter.profiler.rules.ObjectProfilingRule;
import io.beancounter.profiler.taxonomy.TaxonomyIndex;

import java.net.URL;
import java.util.Properties;

public class TweetProfilingRule extends ObjectProfilingRule<Tweet> {

    public TweetProfilingRule(Tweet tweet, NLPEngine nlpEngine, LinkingEngine linkingEngine) {
        super(tweet, nlpEngine, linkingEngine);
    }

    public TweetProfilingRule(Tweet tweet, NLPEngine nlpEngine, LinkingEngine linkingEngine, TaxonomyIndex index) {
        super(tweet, nlpEngine, linkingEngine, index);
    }

    @Override
    public void run(Properties properties) {
        getLogger().debug("rule started");
        Tweet tweet = getObject();

        process(nlpEngineResultFor(tweet.getText()));

        for (URL url : tweet.getUrls()) {
            process(nlpEngineResultFor(url));
        }

        getLogger().debug("rule ended with {} interests found", interests.size());
    }
}
