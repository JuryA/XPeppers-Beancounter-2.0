package io.beancounter.profiler.taxonomy;

import io.beancounter.commons.model.Category;
import io.beancounter.commons.nlp.NLPEngineResult;
import io.beancounter.profiler.DuplicateTaxonomyIdException;

/**
 * Created by Trink0 on 06/02/14.
 */
public interface TaxonomyIndex {
    TaxonomyNode lookupById(Integer nodeId);

    TaxonomyNode addNode(TaxonomyNode node)
            throws DuplicateTaxonomyIdException;

    boolean checkConsistency();

    TaxonomyNode drillUp(TaxonomyNode node, Integer upToTaxonomyLevel);

    Category getAncestorCategory(Integer taxonomyNodeId,
                                 int taxonomyLevel);

    TaxonomyNode lookupByPath(String path);

    TaxonomyNode lookupAndCreateByPath(String path);

    boolean saveNode(TaxonomyNode node);

    int size();

    void updateFrom(NLPEngineResult result)
                            throws DuplicateTaxonomyIdException;

    int getLastInsertId();

    boolean loadFromRepository();
}
