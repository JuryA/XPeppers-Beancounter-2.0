package io.beancounter.profiler.rules.custom;

import io.beancounter.commons.linking.LinkingEngine;
import io.beancounter.commons.model.Category;
import io.beancounter.commons.model.activity.facebook.Like;
import io.beancounter.commons.nlp.NLPEngine;
import io.beancounter.profiler.rules.ObjectProfilingRule;
import io.beancounter.profiler.taxonomy.TaxonomyIndex;
import io.beancounter.profiler.taxonomy.TaxonomyNode;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.Properties;

/**
 * This {@link io.beancounter.profiler.rules.ProfilingRule} implementation is able
 * to extract categories from a single <i>Facebook Like</i>.
 *
 * For each categories in the like, we map it to the taxonomy
 * if present (node returned) we return it.
 *	In particular the set of local categories is filled with the
 *	data coming from the taxonomy. As a standard and initial
 *	representation, the categories set will contain at most one
 *	element.
 *
 *	In this rule lies the point in which we should run the mapping procedures
 *
 *	Here we might also map some entity present in the object to a
 *	known entity in the taxonomy, namely a knowledge instance
 *
 * @author Francesco Bux ( francesco.bux@xpeppers.com )
 */
public class FacebookLikeProfilingRule extends ObjectProfilingRule<Like> {

    private final static String FREE_BASE_URI = "http://www.freebase.com/search?query=";

    public FacebookLikeProfilingRule(Like like, NLPEngine nlpEngine, LinkingEngine linkingEngine) {
        super(like, nlpEngine, linkingEngine);
    }

    public FacebookLikeProfilingRule(Like like, NLPEngine nlpEngine, LinkingEngine linkingEngine, TaxonomyIndex index) {
        super(like, nlpEngine, linkingEngine, index);
    }

    @Override
    public void run(Properties properties) {
        getLogger().debug("rule started");
        Like like = getObject();
        for(String category : like.getCategories()) {
            TaxonomyNode categoryNode = null;
            categoryNode = this.getTaxonomyIndex().lookupByPath(category);
            if(!categoryNode.isNull()){
                try {
                    Category indexedCategory = new Category(category, new URI(FREE_BASE_URI + URLEncoder.encode(categoryNode.getLabel(), "UTF-8")));
                    indexedCategory.setTaxonomyNodeId(categoryNode.getId());
                    indexedCategory.setWeight(1.0);
                    categories.add(indexedCategory);
                } catch (URISyntaxException e) {
                    final String errMsg = "error while trying to link [" + category + "]";
                    getLogger().error(errMsg, e);
                    continue;
                } catch (UnsupportedEncodingException e) {
                    final String errMsg = "error while encoding link for[" + category + "]";
                    getLogger().error(errMsg, e);
                    continue;
                }
            }else{
                continue;
            }
        }
        getLogger().debug("rule ended with {} interests found and {} categories found", interests.size(), categories.size());
    }
}