package io.beancounter.profiler;

public class DuplicateTaxonomyIdException extends Exception {

	public DuplicateTaxonomyIdException(String string) {
		super(string);
	}

}
