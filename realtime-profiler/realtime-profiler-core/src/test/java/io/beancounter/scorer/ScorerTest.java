package io.beancounter.scorer;

import io.beancounter.commons.cogito.CogitoNLPEngineImpl;
import io.beancounter.commons.model.Category;
import io.beancounter.commons.model.User;
import io.beancounter.commons.model.UserProfile;
import io.beancounter.commons.model.activity.*;
import io.beancounter.commons.model.activity.facebook.Like;
import io.beancounter.profiler.DefaultProfilerImpl;
import io.beancounter.profiler.Profiler;
import io.beancounter.profiler.ProfilerException;
import io.beancounter.profiler.rules.ObjectProfilingRule;
import io.beancounter.profiler.rules.custom.DevNullProfilingRule;
import io.beancounter.profiler.rules.custom.FacebookLikeProfilingRule;
import io.beancounter.profiler.rules.custom.TweetProfilingRule;
import io.beancounter.profiler.taxonomy.TaxonomyIndexImpl;
import io.beancounter.profiles.FakeProfiles;
import io.beancounter.profiles.ProfileManager;

import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class ScorerTest {

    private static final int FIRST_TAXONOMY_LEVEL = 1;

    private static final int SECOND_TAXONOMY_LEVEL = 2;

    private static final String endpoint = "http://test.expertsystem.it/IPTC_ITA/EssexWS.asmx/ESSEXIndexdata";

    private Profiler profiler;

    private ProfileManager profileManager;

    private Properties properties;

    private static final int LIMIT = 10;

    private static final int ACTIVITIES_PER_INTEREST = 5;

    @BeforeTest
    public void setUp() throws ProfilerException {
        properties = new Properties();
        // tweets are more important than other
        properties.setProperty("verb.multiplier.TWEET", "1.1");
        // likes are nothing atm
        properties.setProperty("verb.multiplier.LIKE", "1");
        // profiles are made only of top 5 interests
        properties.setProperty("interest.limit", String.valueOf(LIMIT));
        // activities per interest limit
        properties.setProperty("interest.activities.limit", String.valueOf(ACTIVITIES_PER_INTEREST));
        profileManager = new FakeProfiles();
        profiler = new DefaultProfilerImpl(
                profileManager,
                null,
                null,
                new TaxonomyIndexImpl(),
                properties
                );

        profiler.registerRule(Tweet.class, TweetProfilingRule.class);
        profiler.registerRule(io.beancounter.commons.model.activity.Object.class, DevNullProfilingRule.class);
    }

    @Test
    public void newUserHasSumScoresEqualsToZero() {
        UserProfile userProfile = new UserProfile();
        UserProfileScorer scorer = new UserProfileScorer(userProfile);

        Assert.assertEquals(scorer.sumScores(), 0.0);
    }

    @Test
    public void userWithOneLikeHasSumScoresEqualsToHundredPercent() throws Exception {
        UserProfile userProfile = profiler.profile(getFirstActivity());
        UserProfileScorer scorer = new UserProfileScorer(userProfile);

        scorer.compute();
        Assert.assertEquals(scorer.sumScores(), 100.0);
    }

    @Test
    public void userWithOneLikeHasOneCategoryWithScoreToHundredPercent() throws Exception {
        UserProfile userProfile = profiler.profile(getFirstActivity());
        UserProfileScorer scorer = new UserProfileScorer(userProfile);

        scorer.compute();
        Assert.assertEquals(scorer.getScores().iterator().next().getWeight(), 100.0);
    }


    @Test
    public void userWithTwoLikesWithSameWeightHasScoresAtFiftyPercent() throws Exception {
        ResolvedActivity act1 = getFirstActivity();
        ResolvedActivity act2 = getSecondActivity();

        FacebookLikeProfilingRule secondRunRule = profRule(getSecondLikeWithCategory());
        secondRunRule.run(properties);

        UserProfile userProfile = profiler.profile(act1);
        userProfile = (UserProfile) profilerUpdateMethod().invoke(
                profiler,
                userProfile,
                secondRunRule,
                act2.getActivity(),
                1.0);
        UserProfileScorer scorer = new UserProfileScorer(userProfile);
        scorer.compute();

        List<Category> scores = scorer.getScores();
        Iterator<Category> iterator = scores.iterator();
        Assert.assertEquals(iterator.next().getWeight(), 50.0);
        Assert.assertEquals(iterator.next().getWeight(), 50.0);
    }

    @Test
    public void userWithMultipleSiblingsLikesHasScoreOfParentAtHundredPercent() throws Exception {
        FacebookLikeProfilingRule secondRunRule = profRule(getLikeOnAutomotive());
        secondRunRule.run(properties);

        UserProfile userProfile = profiler.profile(getLikeActivityOnAirport());
        userProfile = (UserProfile) profilerUpdateMethod().invoke(
                profiler,
                userProfile,
                secondRunRule,
                getLikeActivityOnAutomotive().getActivity(),
                1.0);
        UserProfileScorer scorer = new UserProfileScorer(userProfile);
        scorer.compute(FIRST_TAXONOMY_LEVEL);

        List<Category> scores = scorer.getScores();
        Assert.assertEquals(scores.iterator().next().getWeight(), 100.0);
    }

    @Test
    public void processScoreAtSecondLevelForUserWithMultiplesLikesShouldReturnFiftyPercentPerLike() throws Exception {
        FacebookLikeProfilingRule secondRunRule = profRule(getLikeOnAutomotive());
        secondRunRule.run(properties);

        UserProfile userProfile = profiler.profile(getLikeActivityOnAirport());
        userProfile = (UserProfile) profilerUpdateMethod().invoke(
                profiler,
                userProfile,
                secondRunRule,
                getLikeActivityOnAutomotive().getActivity(),
                1.0);
        UserProfileScorer scorer = new UserProfileScorer(userProfile);
        scorer.compute(SECOND_TAXONOMY_LEVEL);

        List<Category> scores = scorer.getScores();
        Assert.assertEquals(scores.iterator().next().getWeight(), 50.0);
    }

    @Test
    public void processScoreAtTenthLevelForUserWithMultiplesLikesShouldReturnFiftyPercentPerLike() throws Exception {
        FacebookLikeProfilingRule secondRunRule = profRule(getLikeOnAutomotive());
        secondRunRule.run(properties);

        UserProfile userProfile = profiler.profile(getLikeActivityOnAirport());
        userProfile = (UserProfile) profilerUpdateMethod().invoke(
                profiler,
                userProfile,
                secondRunRule,
                getLikeActivityOnAutomotive().getActivity(),
                1.0);
        UserProfileScorer scorer = new UserProfileScorer(userProfile);
        scorer.compute(10);

        List<Category> scores = scorer.getScores();
        Assert.assertEquals(scores.iterator().next().getWeight(), 50.0);
    }

    private Like getSimpleLikeWithCategory() throws MalformedURLException {
        Like o = new Like();
        o.setCategories(Arrays.asList("Food/Grocery"));
        o.setUrl(new URL("http://www.facebook.com/39332170515"));
        o.setName("Carbonara");
        return o;
    }


    private Like getSecondLikeWithCategory() throws MalformedURLException {
        Like o = new Like();
        o.setCategories(Arrays.asList("Museum/Art Gallery"));
        o.setUrl(new URL("http://www.facebook.com/39332170515"));
        o.setName("Carbonara");
        return o;
    }

    private Like getLikeOnAutomotive() throws MalformedURLException {
        Like o = new Like();
        o.setCategories(Arrays.asList("Automotive"));
        o.setUrl(new URL("http://www.facebook.com/39332170515"));
        o.setName("Ducati");
        return o;
    }

    private Like getLikeOnAirport() throws MalformedURLException {
        Like o = new Like();
        o.setCategories(Arrays.asList("Airport"));
        o.setUrl(new URL("http://www.facebook.com/39332170515"));
        o.setName("OrioSulSerio");
        return o;
    }

    private ResolvedActivity getFirstActivity() throws MalformedURLException {
        Activity activity = new Activity();
        activity.setVerb(Verb.LIKE);

        activity.setObject(getSimpleLikeWithCategory());

        activity.setContext(new Context());
        return asResolvedActivity(activity);
    }

    private ResolvedActivity getSecondActivity() throws MalformedURLException {
        Activity activity = new Activity();
        activity.setVerb(Verb.LIKE);

        activity.setObject(getSecondLikeWithCategory());

        activity.setContext(new Context());
        return asResolvedActivity(activity);
    }

    private ResolvedActivity getLikeActivityOnAirport() throws MalformedURLException {
        Activity activity = new Activity();
        activity.setVerb(Verb.LIKE);

        activity.setObject(getLikeOnAirport());

        activity.setContext(new Context());
        return asResolvedActivity(activity);
    }

    private ResolvedActivity getLikeActivityOnAutomotive() throws MalformedURLException {
        Activity activity = new Activity();
        activity.setVerb(Verb.LIKE);

        activity.setObject(getLikeOnAutomotive());

        activity.setContext(new Context());
        return asResolvedActivity(activity);
    }

    private ResolvedActivity asResolvedActivity(Activity activity) {
        User user = new User();
        return new ResolvedActivity(user.getId(), activity, user);
    }

    private FacebookLikeProfilingRule profRule(Like like) {
        return new FacebookLikeProfilingRule(
                like,
                new CogitoNLPEngineImpl(endpoint),
                null,
                profiler.getTaxonomyIndex());
    }

    private Method profilerUpdateMethod() throws NoSuchMethodException {
        Method updateMethod = profiler.getClass().getDeclaredMethod(
                "update",
                UserProfile.class,
                ObjectProfilingRule.class,
                Activity.class,
                Double.class);
        updateMethod.setAccessible(true);
        return updateMethod;
    }
}