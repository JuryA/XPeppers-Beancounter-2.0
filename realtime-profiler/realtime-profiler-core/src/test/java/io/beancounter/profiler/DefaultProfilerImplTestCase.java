package io.beancounter.profiler;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import io.beancounter.commons.model.Category;
import io.beancounter.commons.model.Interest;
import io.beancounter.commons.model.User;
import io.beancounter.commons.model.UserProfile;
import io.beancounter.commons.model.activity.*;
import io.beancounter.commons.model.activity.Object;
import io.beancounter.commons.nlp.Entity;
import io.beancounter.commons.nlp.NLPEngine;
import io.beancounter.commons.nlp.NLPEngineResult;
import io.beancounter.profiler.taxonomy.TaxonomyIndex;
import io.beancounter.profiler.taxonomy.TaxonomyIndexImpl;
import io.beancounter.profiler.taxonomy.TaxonomyNode;
import io.beancounter.profiler.taxonomy.TaxonomyNodeRepository;
import io.beancounter.profiles.FakeProfiles;
import io.beancounter.profiles.ProfileManager;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Properties;
import java.util.Set;

import org.joda.time.DateTime;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class DefaultProfilerImplTestCase {

    private Profiler profiler;

    private ProfileManager profileManager;

    private Properties properties;

    private static final int MAX_INTERESTS = 10;

    private static final int ACTIVITIES_PER_INTEREST = 5;

    static final String FAKENLP_CATEGORY_PATH = "/location/london";
    static final String FAKENLP_CATEGORY_LABEL = "London";
    static final String FAKENLP_CATEGORY_URL = "http://fakenlp.fk/category/123";

    static final String FAKENLP_ENTITY_PATH = "/film/series";
    static final String FAKENLP_ENTITY_LABEL = "Doctor_who";
    static final String FAKENLP_ENTITY_URL = "http://fakenlp.fk/entity/123";

    private NLPEngine nlpEngine;

    @BeforeMethod
    public void setUp() throws ProfilerException {
        properties = new Properties();
        properties.setProperty("verb.multiplier.TWEET", "1.1");
        properties.setProperty("verb.multiplier.LIKE", "1");
        properties.setProperty("interest.limit", String.valueOf(MAX_INTERESTS));
        properties.setProperty("interest.activities.limit", String.valueOf(ACTIVITIES_PER_INTEREST));
        profileManager = new FakeProfiles();

        nlpEngine = mock(NLPEngine.class);

        TaxonomyNodeRepository repository = mock(TaxonomyNodeRepository.class);
        TaxonomyIndex index = new TaxonomyIndexImpl(repository);

        profiler = new DefaultProfilerImpl(profileManager, nlpEngine, null, index, properties);

        profiler.setTaxonomyIndex(index);
    }

    @Test
    public void grabInterestsFromAnActivity() throws Exception {
        when(nlpEngine.enrich(any(String.class))).thenReturn(nlpResultWithData());

        UserProfile userProfile = profiler.profile(tweetActivity());

        Assert.assertEquals(userProfile.getInterests().size(), 1);
    }

    @Test
    public void grabInterestsFromAnActivityWithUrl() throws Exception {
        when(nlpEngine.enrich(any(String.class))).thenReturn(nlpResultWithNoData());
        when(nlpEngine.enrich(any(URL.class))).thenReturn(nlpResultWithData());

        UserProfile userProfile = profiler.profile(tweetActivityWithUrl());

        Assert.assertEquals(userProfile.getInterests().size(), 1);
    }

    @Test
    public void grabInterestsFromAGenericActivityWithUrl() throws Exception {
        when(nlpEngine.enrich(any(String.class))).thenReturn(nlpResultWithData());
        when(nlpEngine.enrich(any(URL.class))).thenReturn(nlpResultWithData());

        UserProfile userProfile = profiler.profile(genericActivityWithUrl());

        Assert.assertEquals(userProfile.getInterests().size(), 1);
    }

    @Test
    public void grabNoInterestFromAnActivity() throws Exception {
        when(nlpEngine.enrich(any(String.class))).thenReturn(new NLPEngineResult());

        UserProfile userProfile = profiler.profile(emptyActivity());

        Assert.assertTrue(userProfile.getInterests().size() == 0);
    }

    @Test
    public void loginActivities() throws Exception {
        when(nlpEngine.enrich(any(URL.class))).thenReturn(nlpResultWithNoData());
        ActivityBuilder builder = new DefaultActivityBuilder();

        for (Verb v: new Verb[]{Verb.LOGIN_WEB, Verb.LOGIN_MOBILE, Verb.SIGNUP_WEB, Verb.SIGNUP_MOBILE}) {
            builder.push();
            builder.setVerb(v);
            builder.setContext(new DateTime(), "facebook", "dude");
            builder.setObject(Object.class, new URL("http://www.object.null"), null, new HashMap<String, java.lang.Object>());

            Activity activity = builder.pop();
            UserProfile profile = profiler.profile(asResolvedActivity(activity));
            Assert.assertNotNull(profile, "Null for verb: " + v);
            Assert.assertEquals(profile.getInterests().size(), 0, "Did not expect interests for verb: " + v);
        }
    }

    @Test
    public void afterProfilingRuleTaxonomyIndexGetsUpdated() throws Exception {
        when(nlpEngine.enrich(any(String.class))).thenReturn(nlpResultWithData());

        TaxonomyIndex index = profiler.getTaxonomyIndex();
        int beforeIndexSize = index.size();

        profiler.profile(tweetActivity());
        Assert.assertTrue(index.size() > beforeIndexSize);
    }

    @Test
    public void afterProfilingRuleAInterestTaxonomyNodeShouldExist() throws Exception{
        when(nlpEngine.enrich(any(String.class))).thenReturn(nlpResultWithData());

        profiler.profile(tweetActivity());

        TaxonomyNode node = profiler.getTaxonomyIndex().lookupByPath(FAKENLP_ENTITY_PATH);

        Assert.assertEquals(node.getType(), TaxonomyNode.Type.INTEREST);
    }

    @Test
    public void afterProfilingRuleACategoryShouldExist() throws Exception{
        when(nlpEngine.enrich(any(String.class))).thenReturn(nlpResultWithData());

        profiler.profile(tweetActivity());

        TaxonomyNode node = profiler.getTaxonomyIndex().lookupByPath(FAKENLP_CATEGORY_PATH);

        Assert.assertEquals(node.getType(), TaxonomyNode.Type.CATEGORY);
    }

    @Test
    public void categoryGrabbedFromAnActivityShouldBeAssociatedToATaxonomyIndexNode() throws Exception {
        when(nlpEngine.enrich(any(String.class))).thenReturn(nlpResultWithData());

        UserProfile userProfile = profiler.profile(tweetActivity());

        taxonomiesExistForCategories(userProfile.getCategories());
    }

    @Test
    public void interestGrabbedFromAnActivityShouldBeAssociatedToATaxonomyIndexNode() throws Exception {
        when(nlpEngine.enrich(any(String.class))).thenReturn(nlpResultWithData());

        UserProfile userProfile = profiler.profile(tweetActivity());

        taxonomiesExistForInterests(userProfile.getInterests());
    }

    private void taxonomiesExistForCategories(Set<Category> categories) {
        for (Category category : categories) {
            Integer taxonomyNodeId = category.getTaxonomyNodeId();
            Assert.assertNotNull(taxonomyNodeId);
            Assert.assertNotNull(profiler.getTaxonomyIndex().lookupById(taxonomyNodeId));
        }
    }

    private void taxonomiesExistForInterests(Set<Interest> interests) {
        for (Interest interest : interests) {
            Integer taxonomyNodeId = interest.getTaxonomyNodeId();
            Assert.assertNotNull(taxonomyNodeId);
            Assert.assertNotNull(profiler.getTaxonomyIndex().lookupById(taxonomyNodeId));
        }
    }

    private ResolvedActivity tweetActivity() throws MalformedURLException {
        Activity activity = new Activity();
        activity.setVerb(Verb.TWEET);

        Tweet tweet = new Tweet();
        tweet.setText("Just arrived at the BBC!");
        tweet.setUrl(new URL("http://twitter.com/dpalmisano/statuses/11232"));
        activity.setObject(tweet);

        activity.setContext(new Context());
        return asResolvedActivity(activity);
    }

    private ResolvedActivity tweetActivityWithUrl() throws MalformedURLException {
        Activity activity = new Activity();
        activity.setVerb(Verb.TWEET);

        Tweet tweet = new Tweet();
        tweet.setText("Just arrived at the BBC!");
        tweet.setUrl(new URL("http://twitter.com/dpalmisano/statuses/11232"));
        tweet.addUrl(new URL("http://www.foo.bar/test.html"));
        activity.setObject(tweet);

        activity.setContext(new Context());
        return asResolvedActivity(activity);
    }

    private ResolvedActivity genericActivityWithUrl() throws MalformedURLException {
        Activity activity = new Activity();
        activity.setVerb(Verb.SHARE);

        io.beancounter.commons.model.activity.Object share = new io.beancounter.commons.model.activity.Object();
        share.setUrl(new URL("http://www.foo.bar/test.html"));
        activity.setObject(share);

        activity.setContext(new Context());
        return asResolvedActivity(activity);
    }

    private ResolvedActivity emptyActivity() {
        Activity activity = new Activity();
        activity.setVerb(Verb.TWEET);

        Tweet tweet = new Tweet();
        tweet.setText("");
        activity.setObject(tweet);

        activity.setContext(new Context());
        return asResolvedActivity(activity);
    }

    private ResolvedActivity asResolvedActivity(Activity activity) {
        User user = new User();
        return new ResolvedActivity(user.getId(), activity, user);
    }

    private NLPEngineResult nlpResultWithNoData() {
        return new NLPEngineResult();
    }

    private NLPEngineResult nlpResultWithData() {
        NLPEngineResult result = new NLPEngineResult();
        result.addCategory(io.beancounter.commons.nlp.Category.build(FAKENLP_CATEGORY_URL, FAKENLP_CATEGORY_LABEL, FAKENLP_CATEGORY_PATH));
        result.addEntity(Entity.build(FAKENLP_ENTITY_URL, FAKENLP_ENTITY_LABEL, FAKENLP_ENTITY_PATH));
        return result;
    }
}
