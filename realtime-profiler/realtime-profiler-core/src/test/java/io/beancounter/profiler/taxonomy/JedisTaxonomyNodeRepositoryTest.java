package io.beancounter.profiler.taxonomy;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import io.beancounter.commons.helper.jackson.ObjectMapperFactory;
import io.beancounter.commons.helper.jedis.JedisPoolFactory;

import java.util.HashSet;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JedisTaxonomyNodeRepositoryTest {

    private TaxonomyNodeRepository repository;
    private Jedis jedis;
    private JedisPool jedisPool;
    private ObjectMapper mapper;

    @BeforeClass
    public void setUp() throws Exception {
        jedis = mock(Jedis.class);
        jedisPool = mock(JedisPool.class);
        JedisPoolFactory jedisPoolFactory = mock(JedisPoolFactory.class);

        when(jedisPoolFactory.build()).thenReturn(jedisPool);
        when(jedisPool.getResource()).thenReturn(jedis);

        repository = new JedisTaxonomyNodeRepository(jedisPoolFactory);

        mapper = ObjectMapperFactory.createMapper();
    }

    @Test
    public void testStoreTaxonomyNode() throws JsonProcessingException {

        TaxonomyNode node = getTaxonomyNode();
        String nodeJson = mapper.writeValueAsString(node);
        String nodeHash = node.getId().toString();
        when(jedis.set(nodeHash, nodeJson)).thenReturn(nodeHash);

        repository.store(node);

        verify(jedis).set(nodeHash, nodeJson);
    }

    @Test
    public void testLookupTaxonomyNode() throws Exception {
        when(jedis.set(any(String.class), any(String.class))).thenReturn(any(String.class));

        TaxonomyNode node = getTaxonomyNode();
        repository.store(node);

        repository.lookup(node.getId().toString());

        verify(jedis).get(node.getId().toString());
    }

    @Test
    public void testGetKeysForTaxonomy() throws Exception {

        HashSet<String> keys = new HashSet<String>();
        keys.add("1");

        when(jedis.keys("*")).thenReturn(keys);

        repository.getAllKeys();

        verify(jedis).keys(any(String.class));
    }

    private TaxonomyNode getTaxonomyNode() {
        TaxonomyNode node = new TaxonomyNode("UNIQUE_NAME", 1, null);
        return node;
    }

}
