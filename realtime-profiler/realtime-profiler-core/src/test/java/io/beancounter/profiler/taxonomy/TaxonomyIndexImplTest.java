package io.beancounter.profiler.taxonomy;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import io.beancounter.profiler.DuplicateTaxonomyIdException;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import junit.framework.Assert;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TaxonomyIndexImplTest {

    private static final String NOT_EXISTENT_NAME = "Not existent Name";
    private static final String EXISTING_NAME = "Name";
    private static final String ROOT_NAME = "ROOT";
    private static final String AIRPORT_LABEL = "airport";
    private static final String NODE_WITHOUT_PARENT_LABEL = "without_parent";
    private static final int NODE_WITHOUT_PARENT_ID = 21000;

    protected TaxonomyIndexImpl index;

    private TaxonomyNodeRepository taxonomyNodeRepository;

    @BeforeClass
    public void setup(){
        taxonomyNodeRepository = mock(TaxonomyNodeRepository.class);
    }

    @BeforeMethod
    public void beforeTest() {
        index = new TaxonomyIndexImpl(taxonomyNodeRepository);
        try {
            index.addNode(index.createNode(10, EXISTING_NAME.toLowerCase(), 0));
            index.addNode(index.createNode(1, "Local business or place", 0));
            index.addNode(index.createNode(2506, AIRPORT_LABEL, 1));
        } catch (DuplicateTaxonomyIdException e) {
            e.printStackTrace();
        }

    }

    @AfterTest
    public void afterTest() {
    }

    private TaxonomyNode lookupTaxonomy(String label) {
        return index.lookupByPath(label);
    }

    @Test
    public void lookupByExistingNameReturnsNode() {
        TaxonomyNode node = lookupTaxonomy(EXISTING_NAME);
        Assert.assertNotNull(node);
    }

    @Test
    public void lookupByNotExistingNameReturnsNull() {
        TaxonomyNode node = lookupTaxonomy(NOT_EXISTENT_NAME);
        Assert.assertTrue(node.isNull());
    }

    @Test
    public void lookupByExistingNameReturnsNodeWithSameName() {
        TaxonomyNode node = lookupTaxonomy(EXISTING_NAME.toLowerCase());
        Assert.assertFalse(node.isNull());
        Assert.assertEquals(EXISTING_NAME.toLowerCase(), node.getLabel());
    }

    @Test
    public void lookupByExistingNameReturnsNodeWithDefinedId() {
        TaxonomyNode node = lookupTaxonomy(EXISTING_NAME.toLowerCase());
        Assert.assertNotNull(node.getId());
    }

    @Test
    public void lookupByExistingNameReturnsNodeWithDefinedParentId() {
        TaxonomyNode node = lookupTaxonomy(EXISTING_NAME.toLowerCase());
        Assert.assertNotNull(node.getParentId());
    }

    @Test
    public void lookupByRootReturnsNodeWithUnDefinedParentId() {
        TaxonomyNode node = lookupTaxonomy("ROOT");
        Assert.assertNull(node.getParentId());
    }

    @Test
    public void addNewNodeReturnsNode() throws Exception {
        int id = 200;
        String label = "New Node";
        TaxonomyNode node = index.addNode(index.createNode(id, label, 0));
        Assert.assertNotNull(node);
    }

    @Test
    public void addExistingNodeReturnsExistingNode() throws Exception{
        int id = 100;
        TaxonomyNode node = index.addNode(index.createNode(id, EXISTING_NAME, 0));
        Assert.assertTrue(node.getId() != id);
    }

    @Test(expectedExceptions = DuplicateTaxonomyIdException.class)
    public void addExistingIdNodeRaiseException() throws Exception{
        int id = 100;
        index.addNode(index.createNode(id, "First Node", 0));
        index.addNode(index.createNode(id, "Raising Exception Node", 0));
    }

    @Test
    public void addNullNodeShouldNotAffectTaxonomyIndex() throws Exception{
        int originalSize = index.size();

        index.addNode(TaxonomyNode.nullNode());

        Assert.assertEquals(originalSize, index.size());
    }

    @Test
    public void checkConsistencyReturnsTrueWhenAllNodesHaveParent() throws Exception{
        index.addNode(index.createNode(100, "NEW", 0));
        Assert.assertTrue(index.checkConsistency());
    }

    @Test
    public void checkConsistencyReturnsFalseWhenOneNodeHasNotParent() throws Exception{
        int notExistingParent = 150;
        index.addNode(index.createNode(100, "NEW", 0));
        index.addNode(index.createNode(120, "OTHER NAME", notExistingParent));
        Assert.assertFalse(index.checkConsistency());
    }

    @Test
    public void checkTaxonomyReturnsTrue(){
        Assert.assertTrue(index.checkConsistency());
    }

    @Test
    public void drillUpFromRootReturnsNotNull(){
        TaxonomyNode node = index.drillUp(index.lookupByPath(ROOT_NAME.toLowerCase()), null);
        Assert.assertNotNull(node);
    }

    @Test
    public void drillUpFromRootReturnsRoot(){
        TaxonomyNode node = index.drillUp(index.lookupByPath(ROOT_NAME.toLowerCase()), null);
        Assert.assertEquals(node.getLabel(), ROOT_NAME.toLowerCase());
    }

    @Test
    public void drillingUpToZeroReturnsRoot(){
        TaxonomyNode node = index.drillUp(index.lookupByPath(AIRPORT_LABEL), 0);
        Assert.assertEquals(node.getLabel(), ROOT_NAME.toLowerCase());
    }

    @Test
    public void drillingUpToMinusOneReturnsRoot(){
        TaxonomyNode node = index.drillUp(index.lookupByPath(AIRPORT_LABEL), -1);
        Assert.assertEquals(node.getLabel(), ROOT_NAME.toLowerCase());
    }

    @Test
    public void drillingUpToMinusTenReturnsRoot(){

        TaxonomyNode node = index.drillUp(index.lookupByPath(AIRPORT_LABEL), -10);

        Assert.assertEquals(node.getLabel(), ROOT_NAME.toLowerCase());

    }

    @Test
    public void drillingUpFromNodeWithoutParentReturnsNode(){

        try {

            index.addNode(index.createNode(NODE_WITHOUT_PARENT_ID, NODE_WITHOUT_PARENT_LABEL, null));

        } catch (DuplicateTaxonomyIdException e) {

            e.printStackTrace();

        }

        TaxonomyNode node = index.drillUp(index.lookupByPath(NODE_WITHOUT_PARENT_LABEL), -10);

        Assert.assertEquals(node.getLabel(), NODE_WITHOUT_PARENT_LABEL.toLowerCase());

        Assert.assertEquals(node.getId().intValue(), NODE_WITHOUT_PARENT_ID);

    }

    @Test
    public void levelOfRootNodeShouldBeZero() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException{
        Method levelMethod = index.getClass().getDeclaredMethod("level", TaxonomyNode.class);

        levelMethod.setAccessible(true);

        Integer rootLevel = (Integer) levelMethod.invoke(index, index.lookupByPath(ROOT_NAME));

        Assert.assertEquals(rootLevel.intValue(), 0);
    }

    @Test
    public void drillingUpToNodeLevelReturnsSameNode() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException{

        Method levelMethod = index.getClass().getDeclaredMethod("level", TaxonomyNode.class);

        levelMethod.setAccessible(true);

        TaxonomyNode node = index.drillUp(index.lookupByPath(AIRPORT_LABEL), (Integer) levelMethod.invoke(index, index.lookupByPath(AIRPORT_LABEL)));

        Assert.assertEquals(node.getLabel(), AIRPORT_LABEL);
    }

    @Test
    public void lookupByNullPathShouldReturnNull() throws DuplicateTaxonomyIdException{
        Assert.assertTrue(index.lookupAndCreateByPath(null).isNull());
    }

    @Test
    public void lookupBySlashPathShouldReturnNull() throws DuplicateTaxonomyIdException{
        Assert.assertTrue(index.lookupAndCreateByPath("/").isNull());
    }

    @Test
    public void pathStartingWithSlashShouldNotConsiderIt() throws DuplicateTaxonomyIdException{
        index = new TaxonomyIndexImpl(taxonomyNodeRepository);
        String path = "/a";
        int expectedSize = index.size() + 1;

        index.lookupAndCreateByPath(path);
        Assert.assertEquals(expectedSize, index.size());
    }

    @Test
    public void consistentPathAlwaysReturnNode() throws DuplicateTaxonomyIdException{

        String path = "/sports/soccer";
        TaxonomyNode node = index.lookupAndCreateByPath(path);

        Assert.assertNotNull(node);
        Assert.assertEquals(TaxonomyNode.Type.CATEGORY, node.getType());
    }

    @Test
    public void pathsWithSameEntityInDifferentPositionShouldReferenceToDifferentNodes() throws DuplicateTaxonomyIdException{
        index = new TaxonomyIndexImpl(taxonomyNodeRepository);
        String path1 = "d/e/a";
        String path2 = "e/d/c/b/a";
        TaxonomyNode nodeA = index.lookupAndCreateByPath(path1);
        TaxonomyNode nodeB = index.lookupAndCreateByPath(path2);

        Assert.assertFalse(nodeA.equals(nodeB));
    }

    @Test
    public void addingNewNodeAfterAnArbitraryAdditionReturnsConsistentId() throws DuplicateTaxonomyIdException{
        index = new TaxonomyIndexImpl(taxonomyNodeRepository);
        int existingId = index.lookupByPath("TV/Film award").getId();
        String pathToAdd = "a/b/c/e";

        TaxonomyNode node = index.lookupAndCreateByPath(pathToAdd);
        Assert.assertTrue(node.getId() != existingId);
    }

    @Test
    public void addingFirstLevelNodeShouldHaveParentIdAsRoot() throws DuplicateTaxonomyIdException{
        index = new TaxonomyIndexImpl(taxonomyNodeRepository);
        String path = "/a";

        TaxonomyNode node = index.lookupAndCreateByPath(path);

        Assert.assertEquals(index.lookupByPath(ROOT_NAME).getId(), node.getParentId());
    }

    @Test
    public void addingNodePathPopulatesNodeParentId() throws DuplicateTaxonomyIdException{
        index = new TaxonomyIndexImpl(taxonomyNodeRepository);
        String parentPath = "/a/b";
        String childPath = "/a/b/c";

        TaxonomyNode parentNode = index.lookupAndCreateByPath(parentPath);
        TaxonomyNode childNode = index.lookupAndCreateByPath(childPath);

        Assert.assertEquals(childNode.getParentId(), parentNode.getId());
    }

    @Test
    public void givenBuiltTaxonomyNodeIndexShouldSaveit() throws DuplicateTaxonomyIdException{
        when(taxonomyNodeRepository.store(any(TaxonomyNode.class))).thenReturn(true);

        index = new TaxonomyIndexImpl(taxonomyNodeRepository);

        TaxonomyNode node = index.lookupAndCreateByPath("/sports/soccer");

        verify(taxonomyNodeRepository).store(node);

    }

    @Test
    public void buildTaxonomyFromRepository() throws Exception {

        when(taxonomyNodeRepository.getAllKeys()).thenReturn(nodeIds());
        when(taxonomyNodeRepository.lookup("9999")).thenReturn(new TaxonomyNode("Fake/Fake", 9999, 0));
        when(taxonomyNodeRepository.lookup("99999")).thenReturn(new TaxonomyNode("Fake/Fake", 99999, 0));

        int startingSize = index.size();

        index.loadFromRepository();

        verify(taxonomyNodeRepository).getAllKeys();
        verify(taxonomyNodeRepository, times(2)).lookup(any(String.class));

        Assert.assertTrue(index.size() > startingSize);
    }

    @SuppressWarnings("serial")
    private ArrayList<String> nodeIds() {
        return new ArrayList<String>() {{
            add("9999");
            add("99999");
        }};
    }
}
