package io.beancounter.profiler.rules.custom;

import io.beancounter.commons.model.Category;
import io.beancounter.commons.model.User;
import io.beancounter.commons.model.UserProfile;
import io.beancounter.commons.model.activity.*;
import io.beancounter.commons.model.activity.facebook.Like;
import io.beancounter.profiler.DefaultProfilerImpl;
import io.beancounter.profiler.ProfilerException;
import io.beancounter.profiler.rules.ObjectProfilingRule;
import io.beancounter.profiler.rules.ProfilingRuleException;
import io.beancounter.profiler.taxonomy.TaxonomyIndex;
import io.beancounter.profiler.taxonomy.TaxonomyIndexImpl;
import io.beancounter.profiles.FakeProfiles;
import io.beancounter.profiles.ProfileManager;
import io.beancounter.profiles.ProfileManagerException;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collection;
import java.util.Properties;
import java.util.UUID;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class FacebookProfilingRuleTest {

    private TaxonomyIndex index;

    private DefaultProfilerImpl profiler;

    private ProfileManager ps;

    private Properties properties;

    private static final int LIMIT = 10;

    private static final int ACTIVITIES_PER_INTEREST = 5;

    @BeforeMethod
    public void beforeTest() {
        index = new TaxonomyIndexImpl();
    }

    @BeforeTest
    public void setUp() throws ProfilerException {
        properties = new Properties();
        // tweets are more important than other
        properties.setProperty("verb.multiplier.TWEET", "1.1");
        // likes are nothing atm
        properties.setProperty("verb.multiplier.LIKE", "1");
        // profiles are made only of top 5 interests
        properties.setProperty("interest.limit", String.valueOf(LIMIT));
        // activities per interest limit
        properties.setProperty("interest.activities.limit", String.valueOf(ACTIVITIES_PER_INTEREST));
        ps = new FakeProfiles();
        profiler = new DefaultProfilerImpl(
                ps,
                null,
                null,
                new TaxonomyIndexImpl(),
                properties
                );
        profiler.registerRule(Tweet.class, TweetProfilingRule.class);
        profiler.registerRule(io.beancounter.commons.model.activity.Object.class, DevNullProfilingRule.class);
    }

    @Test
    public void processigLikeWithNoCategoriesReturnsEmptyList() throws MalformedURLException, ProfilingRuleException {

        FacebookLikeProfilingRule rule = new FacebookLikeProfilingRule(
                getSimpleLikeWithoutCategories(),
                null,
                null
                );

        rule.run(null);

        Collection<Category> actual = rule.getCategories();

        Assert.assertEquals(actual.size(), 0);
    }

    @Test
    public void processingLikeWithCategoryReturnsNotEmptyList() throws MalformedURLException, ProfilingRuleException{

        FacebookLikeProfilingRule rule = new FacebookLikeProfilingRule(
                getSimpleLikeWithCategory(),
                null,
                null,
                index
                );

        rule.run(null);

        Collection<Category> actual = rule.getCategories();

        Assert.assertNotEquals(actual.size(), 0);
    }

    @Test
    public void processingLikeWithCategoryReturnsNotEmptyListWithCategoryWithTaxonomyNodeId() throws MalformedURLException, ProfilingRuleException{

        FacebookLikeProfilingRule rule = new FacebookLikeProfilingRule(
                getSimpleLikeWithCategory(),
                null,
                null,
                index
                );

        rule.run(null);

        Collection<Category> actual = rule.getCategories();
        Assert.assertEquals(actual.iterator().next().getTaxonomyNodeId(), new Integer(2513));
    }

    @Test
    public void processingLikeWithMissingCategoryReturnsEmptyList() throws MalformedURLException, ProfilingRuleException{

        FacebookLikeProfilingRule rule = new FacebookLikeProfilingRule(
                (Like) getLikeActivityWithMissingCategory().getObject(),
                null,
                null,
                index
                );

        rule.run(null);

        Collection<Category> actual = rule.getCategories();

        Assert.assertEquals(actual.size(), 0);
    }

    @Test
    public void processingLikeWithMissingCategoryReturnsUserProfileWithEmptyCategoryList() throws MalformedURLException, ProfilingRuleException, ProfilerException{

        profiler.setTaxonomyIndex(index);

        Activity like = getLikeActivityWithMissingCategory();
        UserProfile newUserProfiled = profiler.profile(asResolvedActivity(like));

        Assert.assertEquals(newUserProfiled.getCategories().size(), 0);

    }

    @Test
    public void processingLikeWithMissingCategoryForExistingUserReturnsUserProfileWithSameCategoryList() throws MalformedURLException, ProfilingRuleException, ProfilerException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{

        profiler.setTaxonomyIndex(index);

        Activity likeActivityWithCategory = getFirstActivity();
        Activity likeActivityWithMissingCategory = getLikeActivityWithMissingCategory();

        FacebookLikeProfilingRule missingCategoryRule = new FacebookLikeProfilingRule(
                (Like) getLikeActivityWithMissingCategory().getObject(),
                null,
                null,
                index
                );
        missingCategoryRule.run(properties);

        UserProfile newUserProfiled = profiler.profile(asResolvedActivity(likeActivityWithCategory));

        Method updateMethod = profiler.getClass().getDeclaredMethod("update", UserProfile.class, ObjectProfilingRule.class, Activity.class, Double.class);

        updateMethod.setAccessible(true);

        UserProfile updatedUserProfile = (UserProfile) updateMethod.invoke(profiler, newUserProfiled, missingCategoryRule, likeActivityWithMissingCategory, 1.0);

        Assert.assertEquals(newUserProfiled.getCategories().size(), updatedUserProfile.getCategories().size());

    }

    @Test
    public void processingLikeWithCategoryReturnsNotEmptyListWithCategoryReturnsOnePoint() throws MalformedURLException, ProfilingRuleException, ProfilerException{

        profiler.setTaxonomyIndex(index);

        Activity activity = getFirstActivity();
        UserProfile up = profiler.profile(asResolvedActivity(activity));

        up.getCategories();
        Assert.assertEquals(up.getCategories().iterator().next().getWeight(), 1.0);
    }

    @Test
    public void processingDirectlyFromRuleLikeWithCategoryReturnsNotEmptyListWithCategoryReturnsOnePoint() throws MalformedURLException, ProfilingRuleException, ProfilerException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{

        profiler.setTaxonomyIndex(index);

        FacebookLikeProfilingRule rule = new FacebookLikeProfilingRule(
                getSimpleLikeWithCategory(),
                null,
                null,
                index
                );
        rule.run(properties);

        Method method = profiler.getClass().getDeclaredMethod("create", ObjectProfilingRule.class, UUID.class, Activity.class, Double.class);

        method.setAccessible(true);

        UserProfile newUserProfiled = (UserProfile) method.invoke(profiler, rule, UUID.randomUUID(), getFirstActivity(), 1.0);

        Assert.assertEquals(newUserProfiled.getCategories().iterator().next().getWeight(), 1.0);
    }

    @Test
    public void processingNewLikeAlreadyVisitedForReturningUserReturnsNotEmptyListWithCategoriesWithMultiplePoints() throws MalformedURLException, ProfilingRuleException, ProfilerException, ProfileManagerException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{

        profiler.setTaxonomyIndex(index);
        UUID profileUuid = UUID.randomUUID();
        Activity likeActivity = getFirstActivity();

        FacebookLikeProfilingRule rule = new FacebookLikeProfilingRule(
                getSimpleLikeWithCategory(),
                null,
                null,
                index
                );
        rule.run(properties);

        Method createMethod = profiler.getClass().getDeclaredMethod("create", ObjectProfilingRule.class, UUID.class, Activity.class, Double.class);
        Method updateMethod = profiler.getClass().getDeclaredMethod("update", UserProfile.class, ObjectProfilingRule.class, Activity.class, Double.class);

        createMethod.setAccessible(true);
        updateMethod.setAccessible(true);

        UserProfile oldProfile = (UserProfile) createMethod.invoke(profiler, rule, profileUuid, likeActivity, 1.0);

        UserProfile updatedProfile = (UserProfile) updateMethod.invoke(profiler, oldProfile, rule, likeActivity, 1.0);

        updatedProfile = (UserProfile) updateMethod.invoke(profiler, updatedProfile, rule, likeActivity, 1.0);

        Assert.assertEquals(updatedProfile.getCategories().iterator().next().getWeight(), 3.0);
    }

    @Test
    public void processingNewLikeAlreadyVisitedForReturningUserReturnsNotEmptyListWithMultipleCategoriesWithMultiplePoints() throws MalformedURLException, ProfilingRuleException, ProfilerException, ProfileManagerException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{

        profiler.setTaxonomyIndex(index);
        UUID profileUuid = UUID.randomUUID();
        Activity likeActivity = getFirstActivity();
        Activity secondLikeActivity = getSecondActivity();

        FacebookLikeProfilingRule rule = new FacebookLikeProfilingRule(
                getSimpleLikeWithCategory(),
                null,
                null,
                index
                );
        rule.run(properties);

        FacebookLikeProfilingRule rule2 = new FacebookLikeProfilingRule(
                getSecondLikeWithCategory(),
                null,
                null,
                index
                );
        rule.run(properties);

        Method createMethod = profiler.getClass().getDeclaredMethod("create", ObjectProfilingRule.class, UUID.class, Activity.class, Double.class);
        Method updateMethod = profiler.getClass().getDeclaredMethod("update", UserProfile.class, ObjectProfilingRule.class, Activity.class, Double.class);

        createMethod.setAccessible(true);
        updateMethod.setAccessible(true);

        UserProfile oldProfile = (UserProfile) createMethod.invoke(profiler, rule, profileUuid, likeActivity, 1.0);

        UserProfile updatedProfile = (UserProfile) updateMethod.invoke(profiler, oldProfile, rule, likeActivity, 1.0);

        updatedProfile = (UserProfile) updateMethod.invoke(profiler, updatedProfile, rule2, secondLikeActivity, 1.0);

        updatedProfile = (UserProfile) updateMethod.invoke(profiler, updatedProfile, rule2, secondLikeActivity, 1.0);


        Assert.assertNotEquals(updatedProfile.getCategories().size(), 0);
        Assert.assertEquals(updatedProfile.getCategories().iterator().next().getWeight(), 2.0);
        Assert.assertEquals(updatedProfile.getCategories().iterator().next().getWeight(), 2.0);
    }

    @Test
    public void processingNewLikeWithMissingCategoryForReturningUserReturnsPreviousCategoriesUntouched() throws MalformedURLException, ProfilingRuleException, ProfilerException, ProfileManagerException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{

        profiler.setTaxonomyIndex(index);
        UUID profileUuid = UUID.randomUUID();
        Activity likeActivity = getFirstActivity();

        FacebookLikeProfilingRule rule = new FacebookLikeProfilingRule(
                getSimpleLikeWithCategory(),
                null,
                null,
                index
                );
        rule.run(properties);

        FacebookLikeProfilingRule rule2 = new FacebookLikeProfilingRule(
                this.getSimpleLikeWithMissingCategory(),
                null,
                null,
                index
                );
        rule.run(properties);

        Method createMethod = profiler.getClass().getDeclaredMethod("create", ObjectProfilingRule.class, UUID.class, Activity.class, Double.class);
        Method updateMethod = profiler.getClass().getDeclaredMethod("update", UserProfile.class, ObjectProfilingRule.class, Activity.class, Double.class);

        createMethod.setAccessible(true);
        updateMethod.setAccessible(true);

        UserProfile oldProfile = (UserProfile) createMethod.invoke(profiler, rule, profileUuid, likeActivity, 1.0);

        UserProfile updatedProfile = (UserProfile) updateMethod.invoke(profiler, oldProfile, rule, likeActivity, 1.0);

        updatedProfile = (UserProfile) updateMethod.invoke(profiler, updatedProfile, rule2, getLikeActivityWithMissingCategory(), 1.0);

        updatedProfile = (UserProfile) updateMethod.invoke(profiler, updatedProfile, rule2, getLikeActivityWithMissingCategory(), 1.0);


        Assert.assertEquals(updatedProfile.getCategories().size(), 1);
        Assert.assertEquals(updatedProfile.getCategories().iterator().next().getWeight(), 2.0);
    }

    @Test
    public void processingNewLikeWithoutCategoryForReturningUserReturnsPreviousCategoriesUntouched() throws MalformedURLException, ProfilingRuleException, ProfilerException, ProfileManagerException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{

        profiler.setTaxonomyIndex(index);
        UUID profileUuid = UUID.randomUUID();
        Activity likeActivity = getFirstActivity();

        FacebookLikeProfilingRule rule = new FacebookLikeProfilingRule(
                getSimpleLikeWithCategory(),
                null,
                null,
                index
                );
        rule.run(properties);

        FacebookLikeProfilingRule rule2 = new FacebookLikeProfilingRule(
                getSimpleLikeWithoutCategories(),
                null,
                null,
                index
                );
        rule.run(properties);

        Method createMethod = profiler.getClass().getDeclaredMethod("create", ObjectProfilingRule.class, UUID.class, Activity.class, Double.class);
        Method updateMethod = profiler.getClass().getDeclaredMethod("update", UserProfile.class, ObjectProfilingRule.class, Activity.class, Double.class);

        createMethod.setAccessible(true);
        updateMethod.setAccessible(true);

        UserProfile oldProfile = (UserProfile) createMethod.invoke(profiler, rule, profileUuid, likeActivity, 1.0);

        UserProfile updatedProfile = (UserProfile) updateMethod.invoke(profiler, oldProfile, rule, likeActivity, 1.0);

        updatedProfile = (UserProfile) updateMethod.invoke(profiler, updatedProfile, rule2, getLikeActivityWithoutCategory(), 1.0);

        updatedProfile = (UserProfile) updateMethod.invoke(profiler, updatedProfile, rule2, getLikeActivityWithoutCategory(), 1.0);


        Assert.assertEquals(updatedProfile.getCategories().size(), 1);
        Assert.assertEquals(updatedProfile.getCategories().iterator().next().getWeight(), 2.0);
    }

    @Test
    public void processingNewLikeWithAlreadyVisitedCategoryForReturningUserReturnsListofSizeOne() throws MalformedURLException, ProfilingRuleException, ProfilerException, ProfileManagerException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{

        profiler.setTaxonomyIndex(index);
        UUID profileUuid = UUID.randomUUID();
        Activity likeActivity = getFirstActivity();

        FacebookLikeProfilingRule rule = new FacebookLikeProfilingRule(
                getSimpleLikeWithCategory(),
                null,
                null,
                index
                );
        rule.run(properties);

        Method createMethod = profiler.getClass().getDeclaredMethod("create", ObjectProfilingRule.class, UUID.class, Activity.class, Double.class);
        Method updateMethod = profiler.getClass().getDeclaredMethod("update", UserProfile.class, ObjectProfilingRule.class, Activity.class, Double.class);

        createMethod.setAccessible(true);
        updateMethod.setAccessible(true);

        UserProfile oldProfile = (UserProfile) createMethod.invoke(profiler, rule, profileUuid, likeActivity, 1.0);
        UserProfile updatedProfile = (UserProfile) updateMethod.invoke(profiler, oldProfile, rule, likeActivity, 1.0);

        Assert.assertEquals(updatedProfile.getCategories().size(), 1);
    }

    @Test
    public void processingNewLikeForReturningUserAlreadyPopulatedReturnsListofSizeMoreThanOne() throws MalformedURLException, ProfilingRuleException, ProfilerException, ProfileManagerException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{

        profiler.setTaxonomyIndex(index);
        UUID profileUuid = UUID.randomUUID();
        Activity likeActivity = getFirstActivity();

        FacebookLikeProfilingRule rule = new FacebookLikeProfilingRule(
                getSimpleLikeWithCategory(),
                null,
                null,
                index
                );
        rule.run(properties);

        Method createMethod = profiler.getClass().getDeclaredMethod("create", ObjectProfilingRule.class, UUID.class, Activity.class, Double.class);
        Method updateMethod = profiler.getClass().getDeclaredMethod("update", UserProfile.class, ObjectProfilingRule.class, Activity.class, Double.class);

        createMethod.setAccessible(true);
        updateMethod.setAccessible(true);

        UserProfile oldProfile = (UserProfile) createMethod.invoke(profiler, rule, profileUuid, likeActivity, 1.0);

        rule.run(properties);
        UserProfile updatedProfile = (UserProfile) updateMethod.invoke(profiler, oldProfile, rule, likeActivity, 1.0);

        rule = new FacebookLikeProfilingRule(
                (Like) getSecondActivity().getObject(),
                null,
                null,
                index
                );
        rule.run(properties);
        updatedProfile = (UserProfile) updateMethod.invoke(profiler, updatedProfile, rule, getSecondActivity(), 1.0);

        Assert.assertEquals(updatedProfile.getCategories().size(), 2);
    }

    /*
     * Utils
     */

    private Like getSimpleLikeWithoutCategories() throws MalformedURLException {
        Like o = new Like();
        o.setUrl(new URL("http://www.facebook.com/39332170515"));
        o.setName("Carbonara");
        return o;
    }

    private Like getSimpleLikeWithCategory() throws MalformedURLException {
        Like o = new Like();
        o.setCategories(Arrays.asList(new String[]{"Food/Grocery"}));
        o.setUrl(new URL("http://www.facebook.com/39332170515"));
        o.setName("Carbonara");
        return o;
    }

    private Like getSimpleLikeWithMissingCategory() throws MalformedURLException {
        Like o = new Like();
        o.setCategories(Arrays.asList(new String[]{"Food/Grocery/Missing"}));
        o.setUrl(new URL("http://www.facebook.com/39332170515"));
        o.setName("Carbonara");
        return o;
    }

    private Like getSecondLikeWithCategory() throws MalformedURLException {
        Like o = new Like();
        o.setCategories(Arrays.asList(new String[]{"Museum/Art Gallery"}));
        o.setUrl(new URL("http://www.facebook.com/39332170515"));
        o.setName("Carbonara");
        return o;
    }

    private Activity getLikeActivityWithMissingCategory() throws MalformedURLException {
        Activity activity = new Activity();
        activity.setVerb(Verb.LIKE);

        activity.setObject(getSimpleLikeWithMissingCategory());

        activity.setContext(new Context());
        return activity;
    }

    private Activity getLikeActivityWithoutCategory() throws MalformedURLException {
        Activity activity = new Activity();
        activity.setVerb(Verb.LIKE);

        activity.setObject(getSimpleLikeWithoutCategories());

        activity.setContext(new Context());
        return activity;
    }


    private Activity getFirstActivity() throws MalformedURLException {
        Activity activity = new Activity();
        activity.setVerb(Verb.LIKE);

        activity.setObject(getSimpleLikeWithCategory());

        activity.setContext(new Context());
        return activity;
    }

    private Activity getSecondActivity() throws MalformedURLException {
        Activity activity = new Activity();
        activity.setVerb(Verb.LIKE);

        activity.setObject(getSecondLikeWithCategory());

        activity.setContext(new Context());
        return activity;
    }

    private ResolvedActivity asResolvedActivity(Activity activity) {
        User user = new User();
        return new ResolvedActivity(user.getId(), activity, user);
    }
}
