package io.beancounter.storm.analyses;

import backtype.storm.Config;
import backtype.storm.StormSubmitter;
import backtype.storm.generated.AlreadyAliveException;
import backtype.storm.generated.InvalidTopologyException;
import backtype.storm.scheme.StringScheme;
import backtype.storm.spout.KestrelThriftSpout;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.tuple.Fields;
import storm.redis.JedisPoolConfigSerializable;
import storm.redis.RedisBolt;

/**
 * put class description here
 *
 * @author Davide Palmisano ( dpalmisano@gmail.com )
 */
public class DebateAnalysesTopology {

    private static final String KESTREL = "10.166.6.30";

    private static final String REDIS = "10.166.6.30";

    private static final int NTHREADS = 5;

    public static void main(String[] args) {
        TopologyBuilder builder = new TopologyBuilder();
        builder.setSpout(
                "tweets",
                new KestrelThriftSpout(KESTREL, 2229, "social-web-activities", new StringScheme()),
                NTHREADS
        );
        JedisPoolConfigSerializable config = new JedisPoolConfigSerializable();
        config.setMaxIdle(32);
        config.setMaxActive(32);

        // define analysis bolts
        builder.setBolt("mentions", new KeywordsCountBolt(config, REDIS, "#15minuti", "#pb2013", "#oppurevendola", "#csxiamo"), NTHREADS).shuffleGrouping("tweets");
        builder.setBolt("unique-users", new UniqueUsersCountBolt(config, REDIS), NTHREADS).shuffleGrouping("tweets");
        builder.setBolt("tweet-counter", new CounterBolt()).globalGrouping("tweets");
        builder.setBolt("tweets-rate", new TweetOMeterBolt(), 1).shuffleGrouping("tweets");
        builder.setBolt("usernames", new MentionCountBolt(config, REDIS), NTHREADS).shuffleGrouping("tweets");
        builder.setBolt("geo", new GeoTagFilter("IT"), NTHREADS + 5).shuffleGrouping("tweets");
        builder.setBolt("marker", new MapMarker(), NTHREADS).shuffleGrouping("geo");

        // define bolts pushing results to kestrel
        builder.setBolt("mentions-to-kestrel", new KestrelBolt(KESTREL, 2229, "mentions"), NTHREADS).shuffleGrouping("mentions");
        builder.setBolt("unique-to-kestrel", new KestrelBolt(KESTREL, 2229, "unique"), NTHREADS).shuffleGrouping("unique-users");
        builder.setBolt("counter-to-kestrel", new KestrelBolt(KESTREL, 2229, "tweet-count"), NTHREADS).shuffleGrouping("tweet-counter");
        builder.setBolt("rate-to-kestrel", new GenericJsonKestrelBolt(KESTREL, 2229, "tweets-rate"), NTHREADS).shuffleGrouping("tweets-rate");
        builder.setBolt("usernames-to-kestrel", new JsonKestrelBolt(KESTREL, 2229, "usernames"), NTHREADS).shuffleGrouping("usernames");
        builder.setBolt("geo-marker-to-kestrel", new GenericJsonKestrelBolt(KESTREL, 2229, "geomarker"), NTHREADS).shuffleGrouping("marker");

        // define bolts storing intermediate results
        builder.setBolt("mentions-storage", new RedisBolt(config, KESTREL, false), NTHREADS).shuffleGrouping("mentions");
        builder.setBolt("unique-storage", new RedisBolt(config, KESTREL, false), NTHREADS).shuffleGrouping("unique-users");
        builder.setBolt("usernames-storage", new RedisBolt(config, KESTREL, false), NTHREADS).shuffleGrouping("usernames");

        // Example use of the WordSplitter and KeywordCounter to avoid the use
        // of Redis for storing intermediate results.
        /*
        builder.setBolt("word-splitter", new WordSplitter(), 4)
                .shuffleGrouping("tweets");
        builder.setBolt("keyword-counter", new KeywordCounter("London", "Shoreditch", "BBC", "tube"), 4)
                .fieldsGrouping("word-splitter", new Fields("word"));
        */

        // Example use of the UsernameExtractor, PartialUniqueUsersCounter and
        // CounterBolt.
        /*
        builder.setBolt("username-extractor", new UsernameExtractor(), 4)
                .shuffleGrouping("tweets");
        builder.setBolt("partial-unique-users", new PartialUniqueUsersCounter(), 4)
                .fieldsGrouping("username-extractor", new Fields("username"));
        builder.setBolt("unique-users-counter", new CounterBolt())
                .globalGrouping("partial-unique-users");
        */

        Config conf = new Config();
        conf.setDebug(true);
        conf.setNumWorkers(2);

        try {
            StormSubmitter.submitTopology("analyses", conf, builder.createTopology());
        } catch (AlreadyAliveException e) {
            throw new RuntimeException("error while submitting topology", e);
        } catch (InvalidTopologyException e) {
            throw new RuntimeException("error while submitting topology", e);
        }

    }

}
