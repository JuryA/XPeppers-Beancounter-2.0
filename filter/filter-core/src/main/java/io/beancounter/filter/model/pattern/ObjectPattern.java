package io.beancounter.filter.model.pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.beancounter.filter.manager.JedisFilterManager;
import io.beancounter.filter.model.pattern.rai.CommentPattern;
import io.beancounter.filter.model.pattern.rai.TVEventPattern;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * put class description here
 *
 * @author Davide Palmisano ( dpalmisano@gmail.com )
 */
@JsonTypeInfo(use=JsonTypeInfo.Id.NAME, include= JsonTypeInfo.As.PROPERTY, property="type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = ObjectPattern.class, name = "io.beancounter.filter.model.pattern.ObjectPattern"),
        @JsonSubTypes.Type(value = TVEventPattern.class, name = "io.beancounter.filter.model.pattern.rai.TVEventPattern"),
        @JsonSubTypes.Type(value = CommentPattern.class, name = "io.beancounter.filter.model.pattern.rai.CommentPattern")

})
public class ObjectPattern implements Pattern<io.beancounter.commons.model.activity.Object> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ObjectPattern.class);

    public static final ObjectPattern ANY = new ObjectPattern(
            StringPattern.ANY,
            URLPattern.ANY
    );

    private StringPattern typePattern;

    private URLPattern url;

    public ObjectPattern() {
        this.typePattern = StringPattern.ANY;
        this.url = URLPattern.ANY;
    }

    public ObjectPattern(StringPattern typePattern, URLPattern url) {
        this.typePattern = typePattern;
        this.url = url;
    }

    public URLPattern getUrl() {
        return url;
    }

    public StringPattern getTypePattern() {
        return typePattern;
    }

    public void setTypePattern(StringPattern typePattern) {
        this.typePattern = typePattern;
    }

    public void setUrl(URLPattern url) {
        this.url = url;
    }

    @Override
    public boolean matches(io.beancounter.commons.model.activity.Object object) {
        LOGGER.debug("matching activity url: " + object.getUrl() + " with pattern url: " + this.url + 
                " and className " + object.getClass().getName() + " with type pattern: " + this.typePattern );
        return this.equals(ANY) || (this.url.matches(object.getUrl()) && this
                .typePattern.matches(object.getClass().getName()));
    }

    @Override
    public String toString() {
        return "ObjectPattern{" +
                "type=" + typePattern +
                ", url=" + url +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ObjectPattern that = (ObjectPattern) o;

        if (typePattern != null ? !typePattern.equals(that.typePattern) : that.typePattern != null)
            return false;
        if (url != null ? !url.equals(that.url) : that.url != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = typePattern != null ? typePattern.hashCode() : 0;
        result = 31 * result + (url != null ? url.hashCode() : 0);
        return result;
    }
}
