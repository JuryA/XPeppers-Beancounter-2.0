package io.beancounter.export.ga;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;

public class HttpClientFactory {
    public static HttpClient createHttpClient() {
        return new DefaultHttpClient();
    }
}
