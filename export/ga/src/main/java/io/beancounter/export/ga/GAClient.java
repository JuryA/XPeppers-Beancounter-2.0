package io.beancounter.export.ga;

import io.beancounter.commons.model.ExtendedUserProfile;

/**
 * Google Analytics compatible exporter internface.
 * @author alex@cloudware.it
 */
public interface GAClient {

    public static final String GA_EVENT_ACTION = "ea";
    public static final String GA_EVENT_CATEGORY = "ec";
    public static final String GA_EVENT_VALUE = "ev";
    public static final String GA_EVENT_LABEL = "el";
    public static final String GA_SOCIAL_ACTION = "sa";
    public static final String GA_SOCIAL_NETWORK = "sn";
    public static final String GA_SOCIAL_TARGET = "st";
    public static final String GA_PROTOCOL_VERSION = "v";
    public static final String GA_TRACKING_ID = "tid";
    public static final String GA_CLIENT_ID = "cid";
    public static final String GA_HIT_TYPE = "t";
    public static final String GA_NON_INTERACTIVE = "ni";
    public static final String GA_ANONYMIZE_IP = "aip";
    public static final String GA_DOCUMENT_TITLE = "dt";
    public static final String GA_CONTENT_DESC = "cd";
    public static final String GA_QUEUE_TIME = "qt";

    /**
     * Queue Time.
     * Maximum time delta (in millisec) between when the activity being reported
     * occurred and current time.
     * Activities older than 4 hours may be discarded by GA.
     */
    public static final long GA_TTL = 1000 * 60 * 60 * 4;

    /**
     * Send latest user profile updates to Google Analytics.
     * Must not throw an exception.
     *
     * @param extProfile Extended profile containing both {@code UserProfile}
     *                   and {@code ResolvedActivity}.
     */
    public void export(ExtendedUserProfile extProfile);
}
