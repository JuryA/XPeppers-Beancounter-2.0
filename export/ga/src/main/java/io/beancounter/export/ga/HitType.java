package io.beancounter.export.ga;

/**
 * Hit types as specified in the Measurement Protocol reference.
 * @see <a href="http://goo.gl/h2QCli">goo.gl/h2QCli</a>
 */
public enum HitType {
    PAGEVIEW("pageview"),
    APPVIEW("appview"),
    EVENT("event"),
    TRANSACTION("transaction"),
    ITEM("item"),
    SOCIAL("social"),
    EXCEPTION("exception"),
    TIMING("timing");

    private final String hit;

    private HitType(String hit) {
        this.hit = hit;
    }

    @Override
    public String toString() {
        return hit;
    }
}
