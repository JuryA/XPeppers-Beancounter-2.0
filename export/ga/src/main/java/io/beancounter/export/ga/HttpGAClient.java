package io.beancounter.export.ga;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;

import io.beancounter.commons.helper.Clock;
import io.beancounter.commons.model.Category;
import io.beancounter.commons.model.ExtendedUserProfile;
import io.beancounter.commons.model.User;
import io.beancounter.commons.model.UserProfile;
import io.beancounter.commons.model.activity.*;

import org.apache.http.Consts;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;

import org.apache.http.message.BasicNameValuePair;

import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Google Analytics client based on Measurement Protocol.
 * @see <a href="http://goo.gl/1cVT7F">goo.gl/1cVT7F</a>
 * @author alex@cloudware.it
 */
public class HttpGAClient implements GAClient {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(HttpGAClient.class);

    @Inject
    @Named("beancounter.app.version")
    String beancounterAppVersion;

    @Inject
    @Named("export.ga.endpoint")
    String endpoint;

    @Inject
    @Named("export.ga.tid")
    String trackingId;

    @Inject
    @Named("export.ga.cm.age")
    String CM_AGE;

    @Inject
    @Named("export.ga.cd.gender")
    String CD_GENDER;

    @Inject
    @Named("export.ga.cd.location")
    String CD_LOCATION;

    @Inject
    @Named("export.ga.cd.cats")
    String CD_CATS;


    private Clock clock;
    private Provider<HttpClient> httpClientProvider;

    @Inject
    public HttpGAClient(final Clock clock, final Provider<HttpClient> httpClientProvider) {
        this.clock = clock;
        this.httpClientProvider = httpClientProvider;
    }

    @Override
    public void export(ExtendedUserProfile extProfile) {
        LOGGER.debug("Export to GA:\n{}", extProfile);
        if (extProfile.getActivities() == null) {
            LOGGER.warn("Activities list is null");
            return;
        }

        UserProfile profile = extProfile.getProfile();
        String clientId = profile.getUserId().toString();

        for (ResolvedActivity resolvAct: extProfile.getActivities()) {
            Activity act = resolvAct.getActivity();
            long timeDelta = getTimeDelta(act);
            if (timeDelta > GA_TTL) {
                LOGGER.warn("Activity {} is too old.", act);
            }

            // Always use "event" GA hit type
            HitType hit = HitType.EVENT;

            List<NameValuePair> params = createRequestParams(
                    trackingId, clientId, hit, timeDelta);
            addEventData(params, act);
            addProfileData(params, profile, resolvAct.getUser());

            HttpPost req = createRequest(params);
            HttpResponse resp = null;
            HttpClient client = httpClientProvider.get();
            try {
                LOGGER.debug("{}\n{}", req, params);
                resp = client.execute(req);
                int code = resp.getStatusLine().getStatusCode();
                LOGGER.debug("GA collect replied with: " + code);
            } catch (IOException e) {
                final String msg = req.toString() + "\nPAYLOAD:\n" + params;
                LOGGER.error(msg, e);
            } finally {
                    // Make sure to release the connection.
                    try {
                        if (resp != null)
                            EntityUtils.consume(resp.getEntity());
                    } catch (IOException ignored) {
                        LOGGER.error("Consuming response entity ", ignored );
                    } finally {
                        LOGGER.debug("Calling ConnectionManager shutdown");
                        client.getConnectionManager().shutdown();
                    }
            }
        }
    }

    /**
     * Adds Event Tracking data to Measurement Proto request params
     * as described in the reference docs.
     *
     * @see <a href="http://goo.gl/u5N9Vb">goo.gl/u5N9Vb</a>
     */
    private void addEventData(List<NameValuePair> params, Activity act) {
        // required
        String category;
        String action = act.getVerb().toString();
        // optional
        String value = null;
        String label = null;

        io.beancounter.commons.model.activity.Object obj = act.getObject();

        switch (act.getVerb()) {
            case LIKE:
            case TWEET:
            case SHARE:
            case COMMENT:
            case FAVOURITE:
            case FAVORITED:
                category = act.getContext().getService();
                value = "1";
                if (obj != null && obj.getUrl() != null)
                    label = obj.getUrl().toString();
                break;
            case LOGIN_MOBILE:
            case LOGIN_WEB:
            case SIGNUP_MOBILE:
            case SIGNUP_WEB:
                category = "Authentication";
                label = act.getContext().getService();
                value = "1";
                break;
            case LOCATED:
            case CHECKIN:
                category = "Place";
                value = "1";
                break;
            case LISTEN:
            case SONG:
                category = "Audio";
                value = "1";
                break;
            case WATCHED:
                category = "Video";
                value = "1";
                break;
            default:
                if (obj != null) {
                    label = obj.getName();
                }
                category = "Event";
                value = "1";
        }

        if (label == null && obj != null) {
            label = obj.getName();
        }

        params.add(new BasicNameValuePair(GA_EVENT_CATEGORY, category));
        params.add(new BasicNameValuePair(GA_EVENT_ACTION, action));
        if (value != null && value.length() > 0)
            params.add(new BasicNameValuePair(GA_EVENT_VALUE, value));
        if (label != null && label.length() > 0) {
            params.add(new BasicNameValuePair(GA_EVENT_LABEL, label));
            params.add(new BasicNameValuePair(GA_CONTENT_DESC, label));
        }
    }

    /**
     * Adds user profile data to Measurement Proto request params using
     * custom dimesions and metrics.
     *
     * @see <a href="http://goo.gl/AxfmcC">goo.gl/AxfmcC</a>
     */
    private void addProfileData(List<NameValuePair> params,
                                UserProfile profile,
                                User user)
    {
        if (profile.getCategories() != null) {
            List<Category> cats = new ArrayList<Category>(profile.getCategories());
            Collections.sort(cats);
            String []catParams = CD_CATS.split(",");
            for (int i=0; i < cats.size() && i < catParams.length; i++) {
                Category cat = cats.get(i);
                params.add(new BasicNameValuePair(catParams[i], cat.getLabel()));
            }
        }

        String gender = user.getGender();
        if (gender != null)
            params.add(new BasicNameValuePair(CD_GENDER, gender));

        String location = user.getLocationName();
        if (location != null)
            params.add(new BasicNameValuePair(CD_LOCATION, location));

        Integer age = user.getAge();
        if (age == null)
            age = 0;
        params.add(new BasicNameValuePair(CM_AGE, age.toString()));
    }

    /**
     * Returns time delta (in millisec) between when the activity being reported
     * occurred and current time.
     */
    protected long getTimeDelta(Activity act) {
        Context ctx = act.getContext();
        if (ctx == null) {
            // Activities w/o context should not be reported.
            return GA_TTL + 1;
        }

        long diff = clock.utcnow().getMillis() - ctx.getDate().getMillis();

        LOGGER.debug("Activity {} timdelta: {} ms.", act.getId(), diff);
        return diff;
    }

    /**
     * Creates base params for GA hit request.
     *
     * @param tid Tracking ID, AKA Property ID (in GA admin console)
     * @param cid Client ID, uniquely identifies a single BC user
     * @param hit Valid GA hit type.
     * @param timeDelta Queue Time in millisec. See {@link #GA_TTL}
     *
     * @return Params to be used as Entity for an HTTP client request.
     */
    protected List<NameValuePair> createRequestParams(String tid,
                                                      String cid,
                                                      HitType hit,
                                                      long timeDelta)
    {
        List<NameValuePair> params = new ArrayList<NameValuePair>(4);

        params.add(new BasicNameValuePair(GA_PROTOCOL_VERSION, "1"));
        params.add(new BasicNameValuePair(GA_TRACKING_ID, tid));
        params.add(new BasicNameValuePair(GA_CLIENT_ID, cid));
        params.add(new BasicNameValuePair(GA_HIT_TYPE, hit.toString()));
        params.add(new BasicNameValuePair(GA_NON_INTERACTIVE, "1"));
        params.add(new BasicNameValuePair(GA_ANONYMIZE_IP, "1"));
        params.add(new BasicNameValuePair(GA_DOCUMENT_TITLE, getUserAgent()));
        params.add(new BasicNameValuePair(GA_QUEUE_TIME, Long.toString(timeDelta)));

        return params;
    }

    /**
     * Creates a request compatible with Measurement Protocol.
     *
     * @param params Payload to be sent to GA collect endpoint.
     */
    protected HttpPost createRequest(List<NameValuePair> params) {
        UrlEncodedFormEntity payload =
                new UrlEncodedFormEntity(params, Consts.UTF_8);
        HttpPost post = new HttpPost(endpoint);
        post.setEntity(payload);
        post.setHeader("User-Agent", getUserAgent());
        return post;
    }

    public String getUserAgent() {
        return "Beancounter/" + beancounterAppVersion;
    }
}
