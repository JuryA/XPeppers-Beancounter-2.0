package io.beancounter.export.ga;

import com.google.inject.Binder;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.google.inject.name.Names;

import com.sun.grizzly.http.embed.GrizzlyWebServer;
import com.sun.grizzly.http.servlet.ServletAdapter;
import io.beancounter.commons.helper.Clock;

import io.beancounter.commons.http.HttpClientProvider;
import io.beancounter.commons.model.Category;
import io.beancounter.commons.model.ExtendedUserProfile;
import io.beancounter.commons.model.User;
import io.beancounter.commons.model.UserProfile;
import io.beancounter.commons.model.activity.Activity;
import io.beancounter.commons.model.activity.Context;
import io.beancounter.commons.model.activity.ResolvedActivity;
import io.beancounter.commons.model.activity.Verb;

import org.apache.http.*;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.URL;
import java.util.*;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.testng.Assert.*;


public class HttpGAClientTest {

    private static Clock clock;
    private GAClient gaClient;

    private GrizzlyWebServer server;
    private RequestRecorderServlet recorder;
    private String collectEndpointURL;


    public static final String appVersion = "X";
    public static final String userAgent = "Beancounter/" + appVersion;
    public static final String collectEndpointPath = "/collect";

    @BeforeClass
    public void setupClass() throws Exception {
        int port = findFreePort();
        server = new GrizzlyWebServer(port);
        collectEndpointURL = "http://127.0.0.1:" + port + collectEndpointPath;

        recorder = new RequestRecorderServlet();
        ServletAdapter recAdapter = new ServletAdapter();
        recAdapter.setContextPath(collectEndpointPath);
        recAdapter.setServletInstance(recorder);

        server.addGrizzlyAdapter(recAdapter, new String[]{"/*"});
        server.start();
    }

    @AfterClass
    public void tearDownClass() {
        server.stop();
    }

    @BeforeMethod
    public void setUp() throws Exception {
        clock = spy(new Clock());

        HttpResponse resp = mock(HttpResponse.class);
        StatusLine status = mock(StatusLine.class);
        when(status.getStatusCode()).thenReturn(200);
        when(resp.getStatusLine()).thenReturn(status);

        HttpClient httpClient = mock(HttpClient.class);
        when(httpClient.execute(any(HttpPost.class))).thenReturn(resp);

        Injector injector = Guice.createInjector(new Module() {
            @Override
            public void configure(Binder binder) {
                Properties properties = new Properties();
                properties.put("beancounter.app.version", appVersion);
                properties.put("export.ga.endpoint", collectEndpointURL);
                properties.put("export.ga.tid", "UA-XXXX-Y");
                properties.put("export.ga.cm.age", "cm1");
                properties.put("export.ga.cd.gender", "cd1");
                properties.put("export.ga.cd.location", "cd2");
                properties.put("export.ga.cd.cats", "cd3,cd4,cd5");
                Names.bindProperties(binder, properties);

                binder.bind(Clock.class).toInstance(clock);
                binder.bind(HttpClient.class).toProvider(HttpClientProvider.class);
                binder.bind(GAClient.class).to(HttpGAClient.class);
            }
        });

        gaClient = injector.getInstance(GAClient.class);
        recorder.clearRequests();
    }

    @Test
    public void baseRequestData() throws Exception {
        DateTime now = DateTime.now(DateTimeZone.UTC);
        DateTime ctxTime = now.minusMinutes(5);
        ExtendedUserProfile extProfile = createDefaultExtProfile();
        extProfile.getActivities().get(0).getActivity().getContext().setDate(ctxTime);

        when(clock.now(any(DateTimeZone.class))).thenReturn(now);
        gaClient.export(extProfile);

        assertEquals(recorder.getRequests().size(), 1);
        RecordedRequest req = recorder.getLastRequest();
        verifyBaseRequestData(req, extProfile.getProfile().getUserId());

        long queueTime = Long.valueOf(req.params.get(GAClient.GA_QUEUE_TIME));
        assertEquals(queueTime, now.getMillis() - ctxTime.getMillis());
    }

    @Test
    public void userProfileData() throws Exception {
        Set<Category> cats = new HashSet<Category>(4);

        Category cat1 = new Category("Fishing", null);
        cat1.setWeight(0.3);
        cat1.setTaxonomyNodeId(1);
        cats.add(cat1);

        Category cat2 = new Category("Sport", null);
        cat2.setWeight(1.0);
        cat2.setTaxonomyNodeId(2);
        cats.add(cat2);

        Category cat3 = new Category("Hacking", null);
        cat3.setWeight(0.85);
        cat3.setTaxonomyNodeId(3);
        cats.add(cat3);

        Category cat4 = new Category("Snowboarding", null);
        cat4.setWeight(0.9);
        cat4.setTaxonomyNodeId(4);
        cats.add(cat4);

        ExtendedUserProfile extProfile = createDefaultExtProfile();
        extProfile.getProfile().setCategories(cats);

        DateTime birthday = DateTime.now().minusYears(25);
        String formattedBirthday = birthday.toString("MM/dd/yyyy");

        User user = extProfile.getActivities().get(0).getUser();
        user.addMetadata("facebook.user.locationname", "Trento, Italy");
        user.addMetadata("facebook.user.birthday", formattedBirthday);
        user.addMetadata("facebook.user.gender", "animal");

        gaClient.export(extProfile);

        assertEquals(recorder.getRequests().size(), 1);
        RecordedRequest req = recorder.getLastRequest();
        verifyBaseRequestData(req, user.getId());

        assertEquals(req.params.get("cm1"), "25");
        assertEquals(req.params.get("cd1"), "animal");
        assertEquals(req.params.get("cd2"), "Trento, Italy");
        assertEquals(req.params.get("cd3"), "Sport");
        assertEquals(req.params.get("cd4"), "Snowboarding");
        assertEquals(req.params.get("cd5"), "Hacking");
    }

    @Test
    public void userProfileWithOneCategory() throws Exception {
        // Basically, this tests the case where number of cats
        // is less than prop("export.ga.cd.cats").split(",").length

        Set<Category> cats = new HashSet<Category>();
        Category cat = new Category("Sport", null);
        cat.setWeight(1.0);
        cat.setTaxonomyNodeId(1);
        cats.add(cat);

        ExtendedUserProfile extProfile = createDefaultExtProfile();
        extProfile.getProfile().setCategories(cats);

        gaClient.export(extProfile);

        assertEquals(recorder.getRequests().size(), 1);
        RecordedRequest req = recorder.getLastRequest();
        verifyBaseRequestData(req, extProfile.getProfile().getUserId());

        assertEquals(req.params.get("cd3"), "Sport");
        assertNull(req.params.get("cd4"));
        assertNull(req.params.get("cd5"));
    }

    @Test
    public void userProfileWithNoCategories() throws Exception {
        // Edge case where categories property of user profile
        // is null or an empty set.
        List<Set<Category>> tests = new ArrayList<Set<Category>>(2);
        tests.add(new HashSet<Category>());
        tests.add(null);

        for (Set<Category> cats: tests) {
            ExtendedUserProfile extProfile = createDefaultExtProfile();
            extProfile.getProfile().setCategories(cats);

            gaClient.export(extProfile);

            assertEquals(recorder.getRequests().size(), 1);
            RecordedRequest req = recorder.getLastRequest();
            verifyBaseRequestData(req, extProfile.getProfile().getUserId());

            assertNull(req.params.get("cd3"));
            assertNull(req.params.get("cd4"));
            assertNull(req.params.get("cd5"));

            recorder.clearRequests();
        }
    }

    @Test
    public void socialActivity() throws Exception {
        final String url = "http://example.org/page";
        ExtendedUserProfile extProfile = createExtProfile(
                null, null, null,
                Verb.SHARE, url, "Page", "socservice");

        gaClient.export(extProfile);

        assertEquals(recorder.getRequests().size(), 1);
        RecordedRequest req = recorder.getLastRequest();
        verifyBaseRequestData(req, extProfile.getProfile().getUserId());

        assertEquals(req.params.get(GAClient.GA_HIT_TYPE), "event");
        assertEquals(req.params.get(GAClient.GA_EVENT_ACTION), "SHARE");
        assertEquals(req.params.get(GAClient.GA_EVENT_CATEGORY), "socservice");
        assertEquals(req.params.get(GAClient.GA_EVENT_VALUE), "1");
        assertEquals(req.params.get(GAClient.GA_EVENT_LABEL), url);
        assertEquals(req.params.get(GAClient.GA_CONTENT_DESC), url);
    }

    @Test
    public void eventActivity() throws Exception {
        ExtendedUserProfile extProfile = createExtProfile(
                null, null, null,
                Verb.DONATE, "http://example.org", "Test", "leevia");

        gaClient.export(extProfile);

        assertEquals(recorder.getRequests().size(), 1);
        RecordedRequest req = recorder.getLastRequest();
        verifyBaseRequestData(req, extProfile.getProfile().getUserId());

        assertEquals(req.params.get(GAClient.GA_HIT_TYPE), "event");
        assertEquals(req.params.get(GAClient.GA_EVENT_ACTION), "DONATE");
        assertEquals(req.params.get(GAClient.GA_EVENT_CATEGORY), "Event");
        assertEquals(req.params.get(GAClient.GA_EVENT_VALUE), "1");
        assertEquals(req.params.get(GAClient.GA_EVENT_LABEL), "Test");
    }

    @Test
    public void authActivity() throws Exception {
        List<Verb> verbs = Arrays.asList(Verb.LOGIN_WEB, Verb.LOGIN_MOBILE,
                                         Verb.SIGNUP_WEB, Verb.SIGNUP_MOBILE);
        for (Verb v: verbs) {
            ExtendedUserProfile extProfile = createExtProfile(
                    null, null, null,
                    v, "http://example.org", "Leevia", "facebook");

            gaClient.export(extProfile);

            assertEquals(recorder.getRequests().size(), 1);
            RecordedRequest req = recorder.getLastRequest();
            verifyBaseRequestData(req, extProfile.getProfile().getUserId());

            assertEquals(req.params.get(GAClient.GA_HIT_TYPE), "event");
            assertEquals(req.params.get(GAClient.GA_EVENT_ACTION), v.toString());
            assertEquals(req.params.get(GAClient.GA_EVENT_CATEGORY), "Authentication");
            assertEquals(req.params.get(GAClient.GA_EVENT_VALUE), "1");
            assertEquals(req.params.get(GAClient.GA_EVENT_LABEL), "facebook");

            recorder.clearRequests();
        }
    }

    private void verifyBaseRequestData(RecordedRequest req, UUID userId)
            throws Exception
    {
        assertNotNull(req);
        assertEquals(req.method, "POST");
        assertEquals(req.url, collectEndpointURL);
        assertEquals(req.contentType, "application/x-www-form-urlencoded");

        assertEquals(req.params.get(GAClient.GA_PROTOCOL_VERSION), "1");
        assertEquals(req.params.get(GAClient.GA_TRACKING_ID), "UA-XXXX-Y");
        assertEquals(req.params.get(GAClient.GA_CLIENT_ID), userId.toString());
        assertEquals(req.params.get(GAClient.GA_ANONYMIZE_IP), "1");
        assertEquals(req.params.get(GAClient.GA_NON_INTERACTIVE), "1");
        assertEquals(req.params.get(GAClient.GA_DOCUMENT_TITLE), userAgent);
        assertEquals(req.headers.get("user-agent"), userAgent);


        // Just to make sure "qt" param has a sane value and
        // it is indeed being specified.
        long timeDelta = Long.valueOf(req.params.get(GAClient.GA_QUEUE_TIME));
        assertTrue(timeDelta < GAClient.GA_TTL, "Time delta: " + timeDelta);
    }

    private ExtendedUserProfile createDefaultExtProfile() throws Exception {
        return createExtProfile(null, null, null, null, null, null, null);
    }

    private ExtendedUserProfile createExtProfile(DateTime timestamp,
                                                 User user,
                                                 Set<Category> cats,
                                                 Verb verb,
                                                 String url,
                                                 String name,
                                                 String service)
            throws Exception
    {
        if (timestamp == null)
            timestamp = clock.utcnow();

        if (user == null)
            user = new User("John", "Doe", "dude", "secret");

        if (cats == null) {
            Category cat = new Category("Sports", null);
            cat.setWeight(1.0);
            cats = new HashSet<Category>(Arrays.asList(cat));
        }

        if (verb == null)
            verb = Verb.LIKE;
        if (url == null)
            url = "http://facebook.com/pages/snowboarding";
        if (name == null)
            name = "Snowboarding";
        if (service == null)
            service = "facebook";

        io.beancounter.commons.model.activity.Object obj =
                new io.beancounter.commons.model.activity.Object();
        obj.setUrl(new URL(url));
        obj.setName(name);
        Context ctx = new Context(timestamp.minusMinutes(2));
        ctx.setUsername(user.getUsername());
        ctx.setService(service);

        Activity activity = new Activity(verb, obj, ctx);
        ResolvedActivity resolvAct = new ResolvedActivity(user.getId(), activity, user);

        UserProfile profile = new UserProfile(user.getId());
        profile.setUsername(user.getUsername());
        profile.setLastUpdated(timestamp);
        profile.setCategories(cats);

        ExtendedUserProfile extProfile = new ExtendedUserProfile(profile);
        extProfile.addActivity(resolvAct);

        return extProfile;
    }

    private int findFreePort() throws Exception {
        int retries = 0;

        while (retries++ < 3) {
            ServerSocket sock = null;
            try {
                sock = new ServerSocket(0);
                return sock.getLocalPort();
            } catch (IOException e) {
                // port is busy
            } finally {
                if (sock != null)
                    try { sock.close(); } catch (IOException ignored) {}
            }
        }

        throw new RuntimeException(
                "Unable to find free port");
    }
}
