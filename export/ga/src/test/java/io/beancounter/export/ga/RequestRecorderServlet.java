package io.beancounter.export.ga;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

/**
 * A dummy servlet that records in-flight requests.
 * Useful for testing HTTP clients.
 */
public class RequestRecorderServlet extends HttpServlet {

    private static final Logger log =
            LoggerFactory.getLogger(RequestRecorderServlet.class);

    List<RecordedRequest> requests = new ArrayList<RecordedRequest>();

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.debug(req.getMethod() + " " + req.getRequestURL().toString());

        RecordedRequest rec = new RecordedRequest(req);
        requests.add(rec);

        resp.setStatus(200);
    }

    public RecordedRequest getLastRequest() {
        RecordedRequest req = null;
        if (!requests.isEmpty())
            req = requests.get(0);
        return req;
    }

    public List<RecordedRequest> getRequests() {
        return requests;
    }

    public void clearRequests() {
        requests.clear();
    }
}
