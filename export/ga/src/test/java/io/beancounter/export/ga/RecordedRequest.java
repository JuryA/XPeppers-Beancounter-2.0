package io.beancounter.export.ga;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

public class RecordedRequest {
    public String method;
    public String url;
    public String contentType;
    public Map<String, String> params;
    public Map<String, String> headers;

    public RecordedRequest(String method, String url, String contentType) {
        this.method = method;
        this.url = url;
        this.params = new HashMap<String, String>();
        this.headers = new HashMap<String, String>();

        if (contentType != null) {
            String []parts = contentType.split("\\s+");
            if (parts[0].endsWith(";"))
                parts[0] = parts[0].substring(0, parts[0].length()-1);
            contentType = parts[0];
        }
        this.contentType = contentType;
    }

    @SuppressWarnings("unchecked")
    public RecordedRequest(HttpServletRequest req) {
        this(req.getMethod(),
             req.getRequestURL().toString(),
             req.getHeader("content-type"));

        Enumeration<String> params = req.getParameterNames();
        while (params.hasMoreElements()) {
            String name = params.nextElement();
            this.params.put(name, req.getParameter(name));
        }

        Enumeration<String> headers = req.getHeaderNames();
        while (headers.hasMoreElements()) {
            String name = headers.nextElement();
            this.headers.put(name, req.getHeader(name));
        }
    }

    @Override
    public String toString() {
        return "RecordedRequest{" +
                "method='" + method + '\'' +
                ", url='" + url + '\'' +
                ", params=" + params +
                ", headers=" + headers +
                '}';
    }
}
