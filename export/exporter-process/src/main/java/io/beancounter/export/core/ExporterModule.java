package io.beancounter.export.core;


import com.google.inject.Provides;
import com.google.inject.name.Names;
import io.beancounter.commons.helper.Clock;
import io.beancounter.commons.helper.PropertiesHelper;
import io.beancounter.commons.http.HttpClientProvider;
import io.beancounter.export.ga.GAClient;
import io.beancounter.export.ga.HttpGAClient;
import org.apache.camel.component.properties.PropertiesComponent;
import org.apache.camel.guice.CamelModuleWithMatchingRoutes;
import org.apache.camel.guice.jndi.JndiBind;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;

import java.util.Properties;

/**
 * Beancounter Data Export module.
 * @author alex@cloudware.it
 */
public class ExporterModule extends CamelModuleWithMatchingRoutes {

    @Override
    protected void configure() {
        super.configure();

        // Required by Guice injector for @Named annotations to work.
        Properties properties = PropertiesHelper.readFromClasspath(
                "/beancounter.properties");
        Names.bindProperties(binder(), properties);

        bind(Clock.class).toInstance(new Clock());
        bind(HttpClient.class).toProvider(HttpClientProvider.class);
        bind(GAClient.class).to(HttpGAClient.class);
        bind(ExporterRoute.class);
    }

    // Required by Camel.
    // Note: this is totally separate from Guice's @Named injections.
    @Provides
    @JndiBind("properties")
    PropertiesComponent propertiesComponent() {
        PropertiesComponent pc = new PropertiesComponent();
        pc.setLocation("classpath:beancounter.properties");
        return pc;
    }
}
