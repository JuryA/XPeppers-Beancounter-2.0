package io.beancounter.export.core;

import com.google.inject.Inject;

import io.beancounter.commons.model.ExtendedUserProfile;
import io.beancounter.export.ga.GAClient;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Routing for Beancounter Data export.
 * @author alex@cloudware.it
 */
public class ExporterRoute extends RouteBuilder {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(ExporterRoute.class);

    @Inject
    private GAClient gaClient;

    @Override
    public void configure() throws Exception {
        errorHandler(deadLetterChannel(errorEndpoint()));

        from(fromKestrel())
            .unmarshal().json(JsonLibrary.Jackson, ExtendedUserProfile.class)
            .process(new Processor() {
                @Override
                public void process(Exchange exchange) throws Exception {
                    ExtendedUserProfile extProfile =
                        exchange.getIn().getBody(ExtendedUserProfile.class);
                    if (extProfile == null) {
                        LOGGER.error("Passed ExtendedUserProfile is null!");
                        return;
                    }

                    gaClient.export(extProfile);
                }
            });
    }

    protected String fromKestrel() {
        final String opts = "?concurrentConsumers=10&waitTimeMs=500";
        return "kestrel://{{kestrel.queue.exporter.url}}" + opts;
    }

    protected String errorEndpoint() {
        final String opts = "?{{camel.log.options.error}}";
        return "log:" + getClass().getSimpleName() + opts;
    }
}
