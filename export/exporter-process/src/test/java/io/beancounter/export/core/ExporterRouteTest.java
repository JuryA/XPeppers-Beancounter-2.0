package io.beancounter.export.core;


import com.fasterxml.jackson.databind.ObjectMapper;

import com.google.inject.Binder;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;

import io.beancounter.commons.helper.jackson.ObjectMapperFactory;
import io.beancounter.commons.model.ExtendedUserProfile;
import io.beancounter.commons.model.User;
import io.beancounter.commons.model.UserProfile;
import io.beancounter.commons.model.VerbRandomizer;
import io.beancounter.commons.model.activity.Activity;
import io.beancounter.commons.model.activity.ResolvedActivity;
import io.beancounter.commons.model.auth.OAuthAuth;
import io.beancounter.commons.tests.TestsBuilder;
import io.beancounter.commons.tests.TestsException;

import io.beancounter.export.ga.GAClient;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.testng.CamelTestSupport;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;


public class ExporterRouteTest extends CamelTestSupport {

    public static final String IN_ENDPOINT = "direct:start";
    public static final String ERR_ENDPOINT = "mock:error";

    private Injector injector;
    private GAClient gaClient;

    private ObjectMapper mapper = ObjectMapperFactory.createMapper();


    @BeforeMethod
    public void setUp() throws Exception {
        gaClient = mock(GAClient.class);

        final ExporterRoute router = new ExporterRoute() {
            @Override
            public String fromKestrel() {
                return IN_ENDPOINT;
            }

            @Override
            public String errorEndpoint() {
                return ERR_ENDPOINT;
            }
        };

        injector = Guice.createInjector(new Module() {
            @Override
            public void configure(Binder binder) {
                binder.bind(GAClient.class).toInstance(gaClient);
                binder.bind(ExporterRoute.class).toInstance(router);
            }
        });

        super.setUp();
    }

    @Override
    protected RouteBuilder createRouteBuilder() throws Exception {
        return injector.getInstance(ExporterRoute.class);
    }

    @Test
    public void testErrorHandler() throws Exception {
        MockEndpoint error = getMockEndpoint(ERR_ENDPOINT);
        error.expectedMessageCount(3);

        template.sendBody(IN_ENDPOINT, "");
        template.sendBody(IN_ENDPOINT, "not even JSON");
        template.sendBody(IN_ENDPOINT, null);

        error.assertIsSatisfied();
        verifyZeroInteractions(gaClient);
    }

    @Test
    public void testDataReachExportClient() throws Exception {
        MockEndpoint error = getMockEndpoint(ERR_ENDPOINT);
        error.expectedMessageCount(0);

        ResolvedActivity resolvAct = createRandomResolvedActivity(null);
        UserProfile profile = new UserProfile(resolvAct.getUserId());
        ExtendedUserProfile extProfile = new ExtendedUserProfile(profile);
        extProfile.addActivity(resolvAct);

        template.sendBody(IN_ENDPOINT, asJSON(extProfile));

        error.assertIsSatisfied();
        verify(gaClient).export(extProfile);
    }

    private ResolvedActivity createRandomResolvedActivity(Activity act) {
        if (act == null) {
            act = createRandomActivity();
        }
        User user = new User("John", "Doe", "dude", "secret");
        user.addService("chumhum", new OAuthAuth("session", "secret"));

        return new ResolvedActivity(user.getId(), act, user);
    }

    private Activity createRandomActivity() {
        try {
            TestsBuilder testsBuilder = TestsBuilder.getInstance();
            testsBuilder.register(new VerbRandomizer("verb-randomizer"));
            return testsBuilder.build().build(Activity.class).getObject();
        } catch (TestsException e) {
            throw new RuntimeException(e);
        }
    }

    private String asJSON(Object obj) throws Exception {
        return mapper.writeValueAsString(obj);
    }
}
