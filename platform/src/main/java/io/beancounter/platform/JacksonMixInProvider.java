package io.beancounter.platform;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.fasterxml.jackson.databind.SerializationFeature;
import io.beancounter.commons.helper.jackson.ObjectMapperFactory;
import io.beancounter.commons.model.Category;
import io.beancounter.commons.model.Interest;
import io.beancounter.commons.model.mixin.*;
import io.beancounter.commons.model.User;
import io.beancounter.commons.model.auth.*;
import io.beancounter.platform.responses.ShortProfileResponse;

import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

/**
 * @author Enrico Candino ( enrico.candino@gmail.com )
 */
@Provider
public class JacksonMixInProvider implements ContextResolver<ObjectMapper> {

    @Override
    public ObjectMapper getContext(Class<?> klass) {
        final ObjectMapper mapper = ObjectMapperFactory.createMapper()
            .enable(SerializationFeature.INDENT_OUTPUT)
            .setSerializationInclusion(JsonInclude.Include.NON_NULL);

        mapper.addMixInAnnotations(User.class, PrivateUser.class);
        mapper.addMixInAnnotations(SimpleAuth.class, PrivateSimpleAuth.class);
        // Commentato per restituire a User anche i campi session e secret
        // da usare in app.
        //mapper.addMixInAnnotations(OAuthAuth.class, PrivateOAuth.class);

        // Customizations based on Platform response type (class)
        if (klass == ShortProfileResponse.class) {
            mapper.addMixInAnnotations(Interest.class, ShortInterest.class);
            mapper.addMixInAnnotations(Category.class, ShortCategory.class);
        }

        return mapper;
    }
}