package io.beancounter.platform;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.multibindings.MapBinder;
import com.google.inject.name.Names;
import com.google.inject.servlet.GuiceServletContextListener;
import com.sun.jersey.api.core.ClasspathResourceConfig;
import com.sun.jersey.api.core.ResourceConfig;
import com.sun.jersey.guice.JerseyServletModule;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;
import com.sun.jersey.spi.container.servlet.ServletContainer;
import io.beancounter.auth.*;
import io.beancounter.commons.helper.Clock;
import io.beancounter.commons.helper.IdGenerator;
import io.beancounter.commons.helper.es.ElasticSearchConfiguration;
import io.beancounter.commons.http.HttpClientProvider;
import io.beancounter.commons.model.AuthServiceConfig;
import io.beancounter.js.GoogleAnalyticsJsServlet;
import io.beancounter.platform.cors.CorsServletFilter;
import io.beancounter.platform.rai.MyRaiTVService;
import io.beancounter.profiles.ElasticSearchProfileManager;
import io.beancounter.profiles.JedisProfileManager;
import io.beancounter.profiles.UnifiedProfileManager;
import io.beancounter.usermanager.JedisUserManager;
import io.beancounter.usermanager.JedisUserTokenManager;
import io.beancounter.usermanager.UserTokenManager;
import io.beancounter.usermanager.grabber.ActivityGrabberManager;
import io.beancounter.usermanager.grabber.DefaultActivityGrabberManager;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import io.beancounter.activities.ActivityStore;
import io.beancounter.activities.ActivityStoreException;
import io.beancounter.activities.ElasticSearchActivityStoreFactory;
import io.beancounter.applications.ApplicationsManager;
import io.beancounter.applications.JedisApplicationsManagerImpl;
import io.beancounter.commons.helper.jedis.DefaultJedisPoolFactory;
import io.beancounter.commons.helper.PropertiesHelper;
import io.beancounter.commons.helper.jedis.JedisPoolFactory;
import io.beancounter.filter.manager.FilterManager;
import io.beancounter.filter.manager.JedisFilterManager;
import io.beancounter.profiles.ProfileManager;
import io.beancounter.queues.KestrelQueues;
import io.beancounter.queues.Queues;
import io.beancounter.resolver.JedisResolver;
import io.beancounter.resolver.Resolver;
import io.beancounter.usermanager.UserManager;
import org.apache.http.client.HttpClient;
import org.scribe.builder.ServiceBuilder;
import twitter4j.Twitter;

import javax.servlet.ServletContextEvent;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.MessageBodyWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Enrico Candino (enrico.candino@gmail.com)
 */
public class ProductionServiceConfig extends GuiceServletContextListener {

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        try {
            ElasticSearchActivityStoreFactory.getInstance().build().shutDown();
        } catch (ActivityStoreException e) {
            final String errMsg = "Error while closing clint to Elastic Search";
            throw new RuntimeException(errMsg, e);
        }
    }

    @Override
    protected Injector getInjector() {
        return Guice.createInjector(new JerseyServletModule() {
            @Override
            protected void configureServlets() {
                Properties redisProperties = PropertiesHelper.readFromClasspath("/redis.properties");
                Names.bindProperties(binder(), redisProperties);

                Properties properties = PropertiesHelper.readFromClasspath("/beancounter.properties");
                Names.bindProperties(binder(), properties);

                bind(HttpClient.class).toProvider(HttpClientProvider.class);
                bind(ServiceBuilder.class).toProvider(OAuthServiceBuilderProvider.class);
                bind(Twitter.class).toProvider(TwitterProvider.class);
                bind(FacebookFactory.class).to(FacebookProvider.class);

                bind(Clock.class).toInstance(new Clock());
                bind(IdGenerator.class).toInstance(new IdGenerator());
                bind(ExecutorService.class).toInstance(Executors.newFixedThreadPool(10));
                bind(JedisPoolFactory.class).to(DefaultJedisPoolFactory.class).asEagerSingleton();

                AuthServiceConfig twitterCfg = DefaultAuthServiceManager.buildService("twitter", properties);
                AuthServiceConfig facebookCfg = DefaultAuthServiceManager.buildService("facebook", properties);
                bind(AuthServiceConfig.class).annotatedWith(Names.named("service.twitter")).toInstance(twitterCfg);
                bind(AuthServiceConfig.class).annotatedWith(Names.named("service.facebook")).toInstance(facebookCfg);

                MapBinder<AuthServiceConfig, AuthHandler> authHandlers = MapBinder.newMapBinder(
                        binder(),
                        AuthServiceConfig.class,
                        AuthHandler.class);
                authHandlers.addBinding(twitterCfg).to(TwitterAuthHandler.class);
                authHandlers.addBinding(facebookCfg).to(FacebookAuthHandler.class);

                bind(Resolver.class).to(JedisResolver.class);
                bind(ApplicationsManager.class).to(JedisApplicationsManagerImpl.class);
                bind(UserTokenManager.class).to(JedisUserTokenManager.class);
                bind(RequestStateManager.class).to(JedisRequestStateManager.class);
                bind(UserManager.class).to(JedisUserManager.class).asEagerSingleton();
                //bind(ProfileManager.class).to(JedisProfileManager.class);
                bind(ProfileManager.class).to(UnifiedProfileManager.class);
                bind(ProfileManager.class).annotatedWith(Names.named("ElasticsearchProfile")).to(ElasticSearchProfileManager.class);
                bind(ProfileManager.class).annotatedWith(Names.named("JedisProfile")).to(JedisProfileManager.class);
                bind(ActivityStore.class).toInstance(getElasticSearch());
                Properties esProperties = PropertiesHelper.readFromClasspath("/es.properties");
                bind(ElasticSearchConfiguration.class).annotatedWith(Names.named("esConfiguration")).toInstance(ElasticSearchConfiguration.build(esProperties));
                bind(Queues.class).toInstance(new KestrelQueues(properties));
                bind(FilterManager.class).to(JedisFilterManager.class);
                bind(AuthServiceManager.class).to(DefaultAuthServiceManager.class);
                bind(ActivityGrabberManager.class).to(DefaultActivityGrabberManager.class).asEagerSingleton();

                bind(ApplicationService.class);
                bind(UserService.class);
                bind(ActivitiesService.class);
                bind(AliveService.class);
                bind(FilterService.class);
                bind(MyRaiTVService.class);

                bind(JacksonJaxbJsonProvider.class).asEagerSingleton();
                bind(JacksonMixInProvider.class).asEagerSingleton();
                bind(MessageBodyReader.class).to(JacksonJsonProvider.class);
                bind(MessageBodyWriter.class).to(JacksonJsonProvider.class);

                Map<String, String> filterInitParams = new HashMap<>();
                filterInitParams.put(
                        ServletContainer.RESOURCE_CONFIG_CLASS,
                        ClasspathResourceConfig.class.getName());

                Map<String, String> serveInitParams = new HashMap<>();
                serveInitParams.put(ResourceConfig.FEATURE_DISABLE_WADL, "true");

                // Route all REST requests through GuiceContainer
                // Enable CORS on /user resources
                filter("/rest/user/*").through(CorsServletFilter.class);
                filter("/rest/*").through(GuiceContainer.class, filterInitParams);
                serve("/rest/*").with(GuiceContainer.class, serveInitParams);

                // Dynamically generated assets
                serve("/js/dyn/bc.js").with(GoogleAnalyticsJsServlet.class);
            }

            private ActivityStore getElasticSearch() {
                return ElasticSearchActivityStoreFactory.getInstance().build();
            }
        });
    }


}