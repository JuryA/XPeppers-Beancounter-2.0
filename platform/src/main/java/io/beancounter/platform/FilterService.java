package io.beancounter.platform;

import com.google.inject.Inject;
import io.beancounter.platform.validation.Validations;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.beancounter.applications.ApplicationsManager;
import io.beancounter.commons.helper.jackson.ObjectMapperFactory;
import io.beancounter.filter.manager.FilterManager;
import io.beancounter.filter.manager.FilterManagerException;
import io.beancounter.filter.model.Filter;
import io.beancounter.filter.model.pattern.ActivityPattern;
import io.beancounter.platform.responses.FilterPlatformResponse;
import io.beancounter.platform.responses.StringsPlatformResponse;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.Collection;
import java.util.Set;

/**
 * @author Davide Palmisano ( dpalmisano@gmail.com )
 */
@Path("rest/filters")
@Produces(MediaType.APPLICATION_JSON)
public class FilterService extends JsonService {

    private ApplicationsManager applicationsManager;

    private FilterManager filterManager;

    @Inject
    public FilterService(ApplicationsManager applicationsManager, FilterManager filterManager) {
        this.applicationsManager = applicationsManager;
        this.filterManager = filterManager;
    }

    @POST
    @Path("/app/{appName}/register/{name}")
    public Response register(
            @PathParam("appName") String appName,
            @PathParam("name") String name,
            @FormParam("description") String description,
            @FormParam("pattern") String patternJson,
            @FormParam("queue") Set<String> queues,
            @QueryParam(API_KEY) String apiKey
    ) {
        try {
            Validations.checkNotEmpty(name, "Filter name must not be empty");
            Validations.checkNotEmpty(description, "Missing description parameter");
            Validations.checkNotEmpty(patternJson, "Missing filter pattern JSON");
            Validations.checkNotEmpty(queues, "You must specify at least one queue");
            Validations.validateApiKey(appName, apiKey, applicationsManager);
        } catch (Exception ex) {
            return error(ex.getMessage());
        }

        ActivityPattern pattern;
        try {
            pattern = parse(patternJson);
        } catch (IOException e) {
            return error(e, "Error: cannot parse your input json");
        }

        String actualName;
        try {
            actualName = filterManager.register(name, description, queues, pattern);
        } catch (FilterManagerException e) {
            return error(e, "Error while registering filter [" + name + "]");
        }

        return success("filter [" + name + "] successfully registered", actualName);
    }

    @GET
    @Path("/app/{appName}/{name}")
    public Response get(
            @PathParam("appName") String appName,
            @PathParam("name") String name,
            @QueryParam(API_KEY) String apiKey
    ) {
        Filter filter;
        try {
            Validations.checkNotEmpty(name, "Filter name must not be empty");
            Validations.validateApiKey(appName, apiKey, applicationsManager);
            filter = filterManager.get(name);
            Validations.checkNotNull(filter, "Filter [" + name + "] does not exist");
        } catch (Exception ex) {
            return error(ex.getMessage());
        }

        Response.ResponseBuilder rb = Response.ok();
        rb.entity(
                new FilterPlatformResponse(
                        FilterPlatformResponse.Status.OK,
                        "filter with name [" + name + "] found",
                        filter
                )
        );
        return rb.build();
    }

    @DELETE
    @Path("/app/{appName}/{name}")
    public Response delete(
            @PathParam("appName") String appName,
            @PathParam("name") String name,
            @QueryParam(API_KEY) String apikey
    ) {
        try {
            Validations.checkNotEmpty(name, "Filter name must not be empty");
            Validations.validateApiKey(appName, apikey, applicationsManager);
            filterManager.delete(name);
        } catch (Exception ex) {
            return error(ex.getMessage());
        }

        return success("filter with name [" + name + "] deleted", name);
    }

    @GET
    @Path("/app/{appName}/{name}/start")
    public Response start(
            @PathParam("appName") String appName,
            @PathParam("name") String name,
            @QueryParam(API_KEY) String apikey
    ) {
        try {
            Validations.checkNotEmpty(name, "Filter name must not be empty");
            Validations.validateApiKey(appName, apikey, applicationsManager);
        } catch (Exception ex) {
            return error(ex.getMessage());
        }

        try {
            filterManager.start(name);
        } catch (FilterManagerException e) {
            return error(e, "Error while starting filter with name [" + name + "]");
        }

        return success("filter with name [" + name + "] started", name);
    }

    @GET
    @Path("/app/{appName}/list/all")
    public Response filters(
            @PathParam("appName") String appName,
            @QueryParam(API_KEY) String apikey
    ) {
        try {
            Validations.validateApiKey(appName, apikey, applicationsManager);
        } catch (Exception ex) {
            return error(ex.getMessage());
        }

        Collection<String> filters;
        try {
            filters = filterManager.getRegisteredFilters();
        } catch (FilterManagerException e) {
            return error(e, "Error while getting registered filters");
        }

        Response.ResponseBuilder rb = Response.ok();
        rb.entity(
                new StringsPlatformResponse(
                        StringsPlatformResponse.Status.OK,
                        "[" + filters.size() + "] registered filters found",
                        filters
                    )
        );
        return rb.build();
    }

    @GET
    @Path("/app/{appName}/{name}/stop")
    public Response stop(
            @PathParam("appName") String appName,
            @PathParam("name") String name,
            @QueryParam(API_KEY) String apikey
    ) {
        try {
            Validations.checkNotEmpty(name, "Filter name must not be empty");
            Validations.validateApiKey(appName, apikey, applicationsManager);
        } catch (Exception ex) {
            return error(ex.getMessage());
        }

        try {
            filterManager.stop(name);
        } catch (FilterManagerException e) {
            return error(e, "Error while stopping filter with name [" + name +  "]");
        }

        return success("filter with name [" + name + "] stopped", name);
    }

    private ActivityPattern parse(String patternJson) throws IOException {
        ObjectMapper mapper = ObjectMapperFactory.createMapper();
        return mapper.readValue(patternJson, ActivityPattern.class);
    }
}