package io.beancounter.platform;

import com.google.inject.Inject;

import io.beancounter.activities.ActivityModifyException;
import io.beancounter.activities.ActivityNullException;
import io.beancounter.activities.ActivityStore;
import io.beancounter.activities.ActivityStoreException;
import io.beancounter.activities.InvalidOrderException;
import io.beancounter.activities.WildcardSearchException;
import io.beancounter.applications.ApplicationsManager;
import io.beancounter.commons.helper.jackson.ObjectMapperFactory;
import io.beancounter.commons.model.User;
import io.beancounter.commons.model.activity.Activity;
import io.beancounter.commons.model.activity.ResolvedActivity;
import io.beancounter.commons.model.activity.rai.Comment;
import io.beancounter.commons.model.notifies.Notify;
import io.beancounter.platform.responses.*;
import io.beancounter.platform.validation.*;
import io.beancounter.queues.Queues;
import io.beancounter.queues.QueuesException;
import io.beancounter.usermanager.UserManager;
import io.beancounter.usermanager.UserManagerException;
import io.beancounter.usermanager.UserTokenManager;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.joda.time.DateTime;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.UUID;

/**
 * This service implements all the <i>REST</i> APIs needed to manage user's
 * activities.
 * 
 * @author Davide Palmisano ( dpalmisano@gmail.com )
 */
@Path("rest/activities")
@Produces(MediaType.APPLICATION_JSON)
public class ActivitiesService extends JsonService {

    // TODO (mid) this should be configurable
    private static final int ACTIVITIES_LIMIT = 20;

    private Queues queues;

    private ActivityStore activities;

    private ApplicationsManager applicationsManager;

    private UserManager userManager;

    private UserTokenManager tokenManager;

    @Inject
    public ActivitiesService(final ApplicationsManager am,
            final UserManager um, final Queues queues,
            final ActivityStore activities, final UserTokenManager tokenManager) {
        applicationsManager = am;
        userManager = um;
        this.queues = queues;
        this.activities = activities;
        this.tokenManager = tokenManager;
    }
    
    @POST
    @Path("/add/{userId}")
    public Response addActivity(
            @PathParam("userId") final String userId,
            @FormParam(ACTIVITY) String jsonActivity,
            @QueryParam(USER_TOKEN) final String token) {
        User user;
        try {
            HashMap<String, Object> notEmpryParams = new HashMap<String, Object>() {{
                put("userId", userId);
                put("token", token);
            }};

            checkValidInput(notEmpryParams, null);
            user = userManager.getUser(userId);
        } catch (TokenValidationException ex) {
            return error(ex.getMessage(), 400);
        } catch (UserManagerException e) {
			return error(e.getMessage());
		} catch (TokenAuthenticationException e) {
			return error(e.getMessage(), 401);
		} catch (Exception e){
			return error(e.getMessage());
		}

        // deserialize the given activity
        Activity activity;
        try {
            activity = parse(jsonActivity);
        } catch (IOException e) {
            final String errMsg = "Error: cannot parse your input json";
            return error(e, errMsg, 400);
        }

        // if the activity has not been provided, then set it to now
        if (activity.getContext().getDate() == null) {
            activity.getContext().setDate(DateTime.now());
        }

        ResolvedActivity resolvedActivity = new ResolvedActivity(user.getId(),
                activity, user);
        String jsonResolvedActivity;
        try {
            jsonResolvedActivity = parseResolvedActivity(resolvedActivity);
        } catch (IOException e) {
            final String errMsg = "Error while writing an identified JSON for the resolved activity";
            return error(e, errMsg);
        } catch (Exception e){
        	return error(e.getMessage());
        }
        try {
            queues.push(jsonResolvedActivity); 
        } catch (QueuesException e) {
            final String errMsg = "Error while sending the resolved activity to the Queue";
            return error(e, errMsg);
        }
        LOGGER.info("beancounter user with username [" + userId + "] "
                + "and id [" + user.getId() + "] added this activity ["
                + activity + "] " + "with id [" + activity.getId() + "]");
        Response.ResponseBuilder rb = Response.ok();
        rb.entity(new UUIDPlatformResponse(UUIDPlatformResponse.Status.OK,
                "activity successfully registered", activity.getId()));
        return rb.build();
    }

    private String parseResolvedActivity(ResolvedActivity resolvedActivity)
            throws IOException {
        ObjectMapper mapper = ObjectMapperFactory.createMapper();
        return mapper.writeValueAsString(resolvedActivity);
    }

    private Activity parse(String jsonActivity) throws IOException {
        ObjectMapper mapper = ObjectMapperFactory.createMapper();
        return mapper.readValue(jsonActivity, Activity.class);
    }

    @PUT
    @Path("/app/{name}/{activityId}/visible/{isVisible}")
    public Response setVisibility(
            @PathParam("name") String appName,
            @PathParam(ACTIVITY_ID) final String activityId,
            @PathParam(IS_VISIBLE) final boolean isVisible,
            @QueryParam(API_KEY) final String apiKey) {

        UUID activityIdObj = null;
        String jsonNotifyMessage = null;
        try {
            Validations.validateApiKey(appName, apiKey, applicationsManager);

            // checkValidInputWithApikey(activityId, apiKey, isVisible);
            HashMap<String, Object> notEmpryParams = new HashMap<String, Object>() {{
                put("activityId", activityId);
            }};
            HashMap<String, Object> notNullParams = new HashMap<String, Object>() {{
                put("isVisible", isVisible);
            }};
            checkValidInput(notEmpryParams, notNullParams);

            activityIdObj = UUID.fromString(activityId);
            jsonNotifyMessage = changeActivityVisibility(activityIdObj,
                    jsonNotifyMessage, isVisible);
            return success("activity [" + activityId
                    + "] visibility has been modified to [" + isVisible + "]");
        } catch (ActivityStoreException e) {
            return error(e, "Error while getting activity [" + activityId
                    + "] from the storage");
        } catch (ActivityModifyException e) {
            return error(e,
                    "Error modifying the visibility of activity with id ["
                            + activityId + "]");
        } catch (ActivityNullException e) {
            return error("Activity [" + activityIdObj + "] not found");
        } catch (IOException e) {
            return error(e, "Error serializing the notification for ["
                    + activityId + "] to [" + isVisible + "]");
        } catch (QueuesException e) {
            return error(e, "Error pushing [" + jsonNotifyMessage
                    + "] down queue with name [social]");
        } catch (Exception ex) {
            return error(ex.getMessage());
        }
    }

    @PUT
    @Path("/{userId}/{activityId}/visible/{isVisible}")
    public Response setVisibilityFromUser(
            @PathParam("userId") final String userId,
            @PathParam("activityId") final String activityId,
            @PathParam("isVisible") final boolean isVisible,
            @QueryParam(USER_TOKEN) final String token) {
        UUID activityIdObj = null;
        String jsonNotifyMessage = null;
        try {
            HashMap<String, Object> notEmpryParams = new HashMap<String, Object>() {{
                put("activityId", activityId);
                put("userId", userId);
                put("token", token);
            }};
            HashMap<String, Object> notNullParams = new HashMap<String, Object>() {{
                put("isVisible", isVisible);
            }};

            checkValidInput(notEmpryParams, notNullParams);

            activityIdObj = UUID.fromString(activityId);
            jsonNotifyMessage = changeActivityVisibility(activityIdObj, jsonNotifyMessage, isVisible);
            return success("activity [" + activityId + "] visibility has been modified to [" + isVisible + "]");
        } catch (ActivityStoreException e) {
            return error(e, "Error while getting activity [" + activityId
                    + "] from the storage");
        } catch (ActivityModifyException e) {
            return error(e,
                    "Error modifying the visibility of activity with id ["
                            + activityId + "]");
        } catch (ActivityNullException e) {
            return error("Activity [" + activityIdObj + "] not found");
        } catch (IOException e) {
            return error(e, "Error serializing the notification for ["
                    + activityId + "] to [" + isVisible + "]");
        } catch (QueuesException e) {
            return error(e, "Error pushing [" + jsonNotifyMessage
                    + "] down queue with name [social]");
        } catch (Exception ex) {
            return error(ex.getMessage());
        }
    }

    private String changeActivityVisibility(UUID activityIdObj,
            String jsonNotifyMessage, boolean isVisible)
            throws ActivityStoreException, ActivityNullException,
            ActivityModifyException, IOException, QueuesException {
        ResolvedActivity resolvedActivity = activities
                .getActivityEvenIfHidden(activityIdObj);

        activities.setVisible(activityIdObj, isVisible);
        jsonNotifyMessage = getDeleteMessage(activityIdObj, isVisible,
                resolvedActivity);
        queues.push(jsonNotifyMessage, "social");
        return jsonNotifyMessage;
    }

    private String getDeleteMessage(UUID activityIdObj, boolean vObj,
            ResolvedActivity resolvedActivity) throws IOException {
        Notify notify = new Notify("setVisibility", activityIdObj.toString(),
                String.valueOf(vObj));
        Comment comment;
        try {
            comment = (Comment) resolvedActivity.getActivity().getObject();
            notify.addMetadata("onEvent", comment.getOnEvent());
        } catch (ClassCastException e) {
            // just avoid to set the onEvent
        }
        ObjectMapper objectMapper = ObjectMapperFactory.createMapper();
        return objectMapper.writeValueAsString(notify);
    }

    @GET
    @Path("/app/{name}/{activityId}")
    public Response getActivity(
            @PathParam("name") String appName,
            @PathParam("activityId") String activityId,
            @QueryParam(API_KEY) String apiKey) {
        try {
            Validations.checkNotEmpty(activityId,
                    "Missing activity id parameter");
            Validations.checkNotEmpty(apiKey, "Missing api key parameter");
            Validations.validateApiKey(appName, apiKey, applicationsManager);
        } catch (Exception ex) {
            return error(ex.getMessage());
        }

        ResolvedActivity activity;
        try {
            activity = activities.getActivity(UUID.fromString(activityId));
        } catch (ActivityStoreException e) {
            return error(e, "Error while getting activity [" + activityId + "]");
        }

        if (activity == null) {
            return success("no activity with id [" + activityId + "]", activity);
        } else {
            return success("activity with id [" + activityId + "] found",
                    activity);
        }
    }

    @GET
    @Path("/{userId}/{activityId}")
    public Response getUserActivity(
            @PathParam("userId") final String userId,
            @PathParam("activityId") final String activityId,
            @QueryParam(USER_TOKEN) final String token) {
        User user;
        try {
            HashMap<String, Object> notEmpryParams = new HashMap<String, Object>() {{
                put("activityId", activityId);
                put("userId", userId);
                put("token", token);
            }};

            checkValidInput(notEmpryParams, null);
            user = userManager.getUser(userId);
        } catch (Exception ex) {
            return error(ex.getMessage());
        }

        ResolvedActivity activity;
        try {
            activity = activities.getActivity(UUID.fromString(activityId));
        } catch (Exception e) {
            return error(e, "Error while getting activity [" + activityId + "]");
        }

        if (activity == null) {
            return success("no activity with id [" + activityId + "]", activity);
        }

        if (!user.getId().equals(activity.getUserId())) {
            return error("User [" + userId
                    + "] is not authorized to see activity [" + activityId
                    + "]");
        }

        return success("activity with id [" + activityId + "] found", activity);
    }

    @GET
    @Path("/search/me")
    public Response searchWithToken(
            @QueryParam(PATH) final String path,
            @QueryParam(VALUE) final String value,
            @QueryParam(PAGE) @DefaultValue("0") int page,
            @QueryParam(ORDER) @DefaultValue("desc") String order,
            @QueryParam("filter") List<String> filters,
            @QueryParam(USER_TOKEN) String token) {
        try {
            HashMap<String, Object> notEmpryParams = new HashMap<String, Object>() {{
                put("path", path);
                put("value", value);
            }};

            checkValidInput(notEmpryParams, null);
            Validations.check(page >= 0, "Page must be at least 0 (zero)");
            Validations.check(
                    tokenManager.checkTokenExists(UUID.fromString(token)),
                    "User token [" + token + "] is not valid");
        } catch (Exception ex) {
            return error(ex.getMessage());
        }

        return doSearch(path, value, page, order, filters);
    }

    @GET
    @Path("/app/{name}/search")
    public Response search(@QueryParam(PATH) final String path,
            @PathParam("name") String appName,
            @QueryParam(VALUE) final String value,
            @QueryParam(PAGE) @DefaultValue("0") int page,
            @QueryParam(ORDER) @DefaultValue("desc") String order,
            @QueryParam("filter") List<String> filters,
            @QueryParam(API_KEY) String apiKey) {
        try {
            HashMap<String, Object> notEmpryParams = new HashMap<String, Object>() {{
                put("path", path);
                put("value", value);
            }};

            checkValidInput(notEmpryParams, null);
            Validations.check(page >= 0, "Page must be at least 0 (zero)");
            Validations.checkNotEmpty(apiKey);
            Validations.validateApiKey(appName, apiKey, applicationsManager);
        } catch (Exception ex) {
            return error(ex.getMessage());
        }

        return doSearch(path, value, page, order, filters);
    }

    private Response doSearch(String path, String value, int page,
            String order, List<String> filters) {
        Collection<ResolvedActivity> activitiesRetrieved;
        try {
            activitiesRetrieved = activities.search(path, value, page,
                    ACTIVITIES_LIMIT, order, filters);
        } catch (ActivityStoreException ase) {
            return error(ase, "Error while getting page " + page
                    + " of activities where [" + path + "=" + value + "]");
        } catch (WildcardSearchException wse) {
            Response.ResponseBuilder rb = Response.serverError();
            rb.entity(new StringPlatformResponse(
                    StringPlatformResponse.Status.NOK, wse.getMessage()));
            return rb.build();
        } catch (InvalidOrderException ioe) {
            Response.ResponseBuilder rb = Response.serverError();
            rb.entity(new StringPlatformResponse(
                    StringPlatformResponse.Status.NOK, ioe.getMessage()));
            return rb.build();
        }

        return success((activitiesRetrieved.isEmpty()) ? "search for [" + path
                + "=" + value + "] found no " + (page != 0 ? "more " : "")
                + "activities." : "search for [" + path + "=" + value
                + "] found activities.", activitiesRetrieved);
    }

    @GET
    @Path("/all/{userId}")
    public Response getAllActivities(
            @PathParam("userId") final String userId,
            @QueryParam(PAGE) @DefaultValue("0") int page,
            @QueryParam(ORDER) @DefaultValue("desc") String order,
            @QueryParam(VERB) @DefaultValue("") String verb,
            @QueryParam(USER_TOKEN) final String token) {
        try {
            HashMap<String, Object> notEmpryParams = new HashMap<String, Object>() {{
                put("userId", userId);
                put("token", token);
            }};

            checkValidInput(notEmpryParams, null);

            User user = userManager.getUser(userId);
            Collection<ResolvedActivity> allActivities;
            if(verb.isEmpty())
                allActivities = activities.getByUserPaginated(user.getId(),
                        page, ACTIVITIES_LIMIT, order);
            else
                allActivities = activities.getByUserAndVerbPaginated(user.getId(),
                        verb, page, ACTIVITIES_LIMIT, order);

            return success((allActivities.isEmpty()) ? "user '" + userId
                    + "' has no " + (page != 0 ? "more " : "") + "activities."
                    : "user '" + userId + "' activities found.", allActivities);
        } catch (ActivityStoreException e) {
            return error(e, "Error while getting page " + page
                    + " of all the activities for user [" + userId + "]");
        } catch (InvalidOrderException ioe) {
            return error(ioe.getMessage());
        } catch (Exception ex) {
            return error(ex.getMessage());
        }
    }

    private static Response success(String message,
            Collection<ResolvedActivity> activities) {
        Response.ResponseBuilder rb = Response.ok();
        rb.entity(new ResolvedActivitiesPlatformResponse(
                PlatformResponse.Status.OK, message, activities));
        return rb.build();
    }

    private static Response success(String message, ResolvedActivity activity) {
        Response.ResponseBuilder rb = Response.ok();
        rb.entity(new ResolvedActivityPlatformResponse(
                PlatformResponse.Status.OK, message, activity));
        return rb.build();
    }

    private void checkValidInput(
            HashMap<String, Object> notEmptyParams,
            HashMap<String, Object> notNullParams
    ) throws UserManagerException, TokenAuthenticationException, TokenValidationException {
        if(notNullParams == null)
            notNullParams = new HashMap<String, Object>();

        if (notEmptyParams.containsKey("token")) {
            String userId = (String) notEmptyParams.get("userId");
            String token = (String) notEmptyParams.get("token");
            User user = userManager.getUser(userId);
            Validations.checkNotNull(user, "user with ID [" + userId + "] not found");
            Validations.validateUserToken(token, user, tokenManager);
        }

        for (Entry<String, Object> param : notEmptyParams.entrySet())
            Validations.checkNotEmpty((String) param.getValue());

        for (Entry<String, Object> param : notNullParams.entrySet())
            Validations.checkNotNull(param.getValue());
    }
}
