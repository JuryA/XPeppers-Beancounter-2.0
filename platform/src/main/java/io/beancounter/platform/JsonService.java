package io.beancounter.platform;

import io.beancounter.platform.responses.StringPlatformResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;

/**
 * put class description here
 *
 * @author Davide Palmisano ( dpalmisano@gmail.com )
 */
public abstract class JsonService extends PlatformService {

    protected static final Logger LOGGER = LoggerFactory.getLogger(JsonService.class);

    public static Response error(
            Exception e,
            String message
    ) {
    	return error(e, message, 500);
    }
    
    public static Response error(
            Exception e,
            String message,
            int code
    ) {
        LOGGER.error(code + " " + e.getMessage() + " " + message);
        Response.ResponseBuilder rb = Response.serverError();
        StringPlatformResponse response =         new StringPlatformResponse(
                StringPlatformResponse.Status.NOK,
                message,
                e.getMessage()
        );
        setResponseStatus(code, response);
        
        rb.entity(
        		response
        );
        return rb.build();
    }

    public static Response error(String message) {
        return error(message, 500);
    }

    public static Response error(String message, int code) {
        LOGGER.error(message);
        Response.ResponseBuilder rb = Response.status(code);
        StringPlatformResponse response = new StringPlatformResponse(StringPlatformResponse.Status.NOK, message);
        setResponseStatus(code, response);
        rb.entity(
                response
        );
        return rb.build();
    }

	private static void setResponseStatus(int code,
			StringPlatformResponse response) {
		switch(code){
        	case 400:
        		response.setStatus(StringPlatformResponse.Status.E_400);
                break;
        	case 401:
        		response.setStatus(StringPlatformResponse.Status.E_401);
        		break;
        	case 403:
        		response.setStatus(StringPlatformResponse.Status.E_403);
        		break;
            case 404:
                response.setStatus(StringPlatformResponse.Status.E_404);
                break;
        	default:
        		response.setStatus(StringPlatformResponse.Status.NOK);
        }
	}

    public static Response success(String message) {
        Response.ResponseBuilder rb = Response.ok();
        rb.entity(
                new StringPlatformResponse(
                        StringPlatformResponse.Status.OK,
                        message
                )
        );
        return rb.build();
    }

    public static Response success(String message, String object) {
        Response.ResponseBuilder rb = Response.ok();
        rb.entity(
                new StringPlatformResponse(
                        StringPlatformResponse.Status.OK,
                        message,
                        object
                )
        );
        return rb.build();
    }
}
