package io.beancounter.platform.cors;

import com.google.inject.*;
import com.google.inject.name.Named;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;


@Singleton
public class CorsServletFilter implements Filter {

    static final String STAR = "*";
    // Common headers
    static final String ORIGIN            = "origin";
    static final String ALLOW_ORIGIN      = "Access-Control-Allow-Origin";
    static final String EXPOSE_HEADERS    = "Access-Control-Expose-Headers";
    // Used only for preflight requests (OPTIONS)
    static final String ALLOW_CREDENTIALS = "Access-Control-Allow-Credentials";
    static final String ALLOW_METHODS     = "Access-Control-Allow-Methods";
    static final String ALLOW_HEADERS     = "Access-Control-Allow-Headers";
    static final String MAX_AGE           = "Access-Control-Max-Age";

    protected static final Logger logger =
            Logger.getLogger(CorsServletFilter.class);

    // Simple CORS request
    private final String[] origins;
    private final boolean starOrigin;
    private final String exposeHeaders;
    // CORS requests that require preflight
    private final String allowMethods;
    private final String allowHeaders;
    private final String maxAge;

    @Inject
    CorsServletFilter(@Named("cors.origin") String origin,
                      @Named("cors.expose.headers") String exposeHeaders,
                      @Named("cors.allow.methods") String allowMethods,
                      @Named("cors.allow.headers") String allowHeaders,
                      @Named("cors.preflight.maxage") int maxAge)
    {
        String[] origins = origin.split("\\s*,\\s*");
        Arrays.sort(origins);
        this.origins = origins;
        this.starOrigin = Arrays.binarySearch(origins, STAR) >= 0;
        this.exposeHeaders = isEmpty(exposeHeaders) ? null : exposeHeaders;
        this.allowMethods = allowMethods;
        this.allowHeaders = isEmpty(allowHeaders) ? null : allowHeaders;
        this.maxAge = maxAge > 0 ? Integer.toString(maxAge) : null;
    }

    @Override
    public void init(FilterConfig unused) throws ServletException {
        logger.info("CORS filter is enabled with these settings: \n" +
                "Origin: " + Arrays.asList(origins) + '\n' +
                "Exposed headers: " + exposeHeaders + '\n' +
                "Allowed methods: " + allowMethods + '\n' +
                "Allowed headers: " + allowHeaders + '\n' +
                "Preflight cache max age: " + maxAge);

        if (origins.length == 0)
            logger.error("Origin cannot be empty!");

        if (isEmpty(allowMethods))
            logger.error("Allowed Methods cannot be empty in Preflight!");
    }

    @Override
    public void doFilter(ServletRequest sreq,
                         ServletResponse sresp,
                         FilterChain chain
    ) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) sreq;
        HttpServletResponse resp = (HttpServletResponse) sresp;

        String reqOrigin = req.getHeader(ORIGIN);
        String allowOrigin = null;
        if (reqOrigin != null) {
            int idx = Arrays.binarySearch(origins, reqOrigin);
            allowOrigin = idx >= 0 ? origins[idx] : (starOrigin ? STAR : null);
        }

        if (!isEmpty(allowOrigin)) {
            // Simple CORS request
            resp.setHeader(ALLOW_ORIGIN, allowOrigin);
            if (!isEmpty(exposeHeaders))
                resp.setHeader(EXPOSE_HEADERS, exposeHeaders);

            // Preflight CORS request requires additional headers
            if (req.getMethod().equals("OPTIONS")) {
                if (!isEmpty(allowMethods))
                    resp.setHeader(ALLOW_METHODS, allowMethods);
                if (!isEmpty(allowHeaders))
                    resp.setHeader(ALLOW_HEADERS, allowHeaders);
                if (!isEmpty(maxAge))
                    resp.setHeader(MAX_AGE, maxAge);
            }
        }

        chain.doFilter(req, resp);
    }

    protected boolean isEmpty(String str) {
        return str == null || str.isEmpty();
    }

    @Override
    public void destroy() {
        logger.info("CORS filter is now disabled");
    }
}
