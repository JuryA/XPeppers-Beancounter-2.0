package io.beancounter.platform.responses;

import io.beancounter.commons.model.User;
import io.beancounter.platform.PlatformResponse;
import java.util.Collection;

public class UsersPlatformResponse extends PlatformResponse<Collection<User>> { 
    private Collection<User> users;

    public UsersPlatformResponse(){}

    public UsersPlatformResponse(Status status, String message) {
        super(status, message);
    }

    public UsersPlatformResponse(Status status, String message, Collection<User> users) {
        super(status, message);
        this.users = users;
    }

    public Collection<User> getObject() {
        return users;
    }

    public void setObject(Collection<User> users) {
        this.users = users;
    }
}
