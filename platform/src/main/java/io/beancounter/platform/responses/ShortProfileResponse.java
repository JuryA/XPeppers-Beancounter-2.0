package io.beancounter.platform.responses;

import io.beancounter.commons.model.ShortUserProfile;
import io.beancounter.platform.PlatformResponse;


public class ShortProfileResponse extends PlatformResponse<ShortUserProfile> {

    private ShortUserProfile shortProfile;

    public ShortProfileResponse() {
    }

    public ShortProfileResponse(Status status,
                                String message,
                                ShortUserProfile shortProfile)
    {
        super(status, message);
        this.shortProfile = shortProfile;
    }

    @Override
    public ShortUserProfile getObject() {
        return shortProfile;
    }

    public void setObject(ShortUserProfile shortProfile) {
        this.shortProfile = shortProfile;
    }
}
