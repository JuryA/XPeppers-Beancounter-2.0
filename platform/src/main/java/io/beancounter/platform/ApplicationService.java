package io.beancounter.platform;

import io.beancounter.applications.ApplicationsManager;
import io.beancounter.applications.ApplicationsManagerException;
import io.beancounter.commons.model.Application;

import java.util.UUID;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.inject.Inject;

/**
 * @author Davide Palmisano ( dpalmisano@gmail.com )
 */
@Path("rest/application")
@Produces(MediaType.APPLICATION_JSON)
public class ApplicationService extends JsonService {

    private ApplicationsManager applicationsManager;

    @Inject
    public ApplicationService(final ApplicationsManager am) {
        this.applicationsManager = am;
    }

    @POST
    @Path("/register")
    public Response register(
            @FormParam("name") String name,
            @FormParam("description") String description,
            @FormParam("email") String email
    ) {
        Application app;
        try {
            app = applicationsManager.registerApplication(name, description, email);
        } catch (ApplicationsManagerException e) {
            return error(e.getMessage());
        }

        return Response.ok(app).build();
    }

    @GET
    @Path("/{name}")
    public Response getApplication(
            @PathParam("name") String name,
            @QueryParam(API_KEY) String apiKey
    ) {
        UUID key = parseApiKey(apiKey);
        if (key == null)
            return error("Malformed API key", 400);

        Application app;
        try {
            app = applicationsManager.get(name);
        } catch (ApplicationsManagerException e) {
            return error(e.getMessage());
        }

        if (app == null)
            return error("Not Found", 404);

        if (!app.getApiKey().equals(key))
            return error("Invalid API key", 403);

        return Response.ok(app).build();
    }

    @PUT
    @Path("/{name}")
    public Response updateApplication(
            @PathParam("name") String name,
            Application newData,
            @QueryParam(API_KEY) String apiKey
    ) {
        UUID key = parseApiKey(apiKey);
        if (key == null)
            return error("Invalid API Key", 400);

        Application app;
        try {
            app = applicationsManager.get(name);
        } catch (ApplicationsManagerException e) {
            return error(e.getMessage());
        }

        if (app == null)
            return error("Not found", 404);

        if (!app.getApiKey().equals(key))
            return error("Invalid API Key", 403);

        app.update(newData);

        try {
            applicationsManager.update(app);
        } catch (ApplicationsManagerException e) {
            return error(e.getMessage());
        }
        return Response.ok(app).build();
    }

    @DELETE
    @Path("/{name}")
    public Response deregisterApplication(
            @PathParam("name") String name,
            @QueryParam(API_KEY) String apiKey
    ) {
        UUID key = parseApiKey(apiKey);
        if (key == null)
            return error("Invalid API Key", 400);

        Application app;
        try {
            app = applicationsManager.get(name);
        } catch (ApplicationsManagerException e) {
            return error(e.getMessage());
        }

        if (app == null)
            return error("App not found", 400);

        boolean ok;
        try {
            ok = applicationsManager.deregisterApplication(name);
        } catch (ApplicationsManagerException e) {
            return error(e.getMessage());
        }

        if (!ok) {
            return error("Error deregistering application");
        }

        return Response.ok().build();
    }

    protected UUID parseApiKey(String key) {
        if (key == null)
            return null;
        try {
            return UUID.fromString(key);
        } catch (IllegalArgumentException e) {
            LOGGER.debug(e.getMessage(), e);
            return null;
        }
    }
}
