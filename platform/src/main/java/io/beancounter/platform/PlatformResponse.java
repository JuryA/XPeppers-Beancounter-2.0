package io.beancounter.platform;

/**
 * This is the most generic response the REST platform is able to deliver.
 *
 * @author Enrico Candino ( enrico.candino@gmail.com )
 * @see {@link javax.xml.bind.annotation.XmlRootElement}
 */
public abstract class PlatformResponse<T> {

    public enum Status {
        OK,
        NOK,
        E_400,
        E_401,
        E_403,
        E_404
    }

    private Status status;

    private String message;

    public PlatformResponse() {}

    public PlatformResponse(Status status, String message) {
        this.status = status;
        this.message = message;
    }

    public PlatformResponse(String message) {
        this(Status.OK, message);
    }

    public Status getStatus() {
        return status;
    }
    
    public void setStatus(Status status){
    	this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public abstract T getObject();

}
