package io.beancounter.platform.validation;

public class TokenAuthenticationException extends Exception {

	public TokenAuthenticationException(String string) {
		super(string);
	}

}
