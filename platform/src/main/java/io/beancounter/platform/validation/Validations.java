package io.beancounter.platform.validation;

import io.beancounter.applications.ApplicationsManager;
import io.beancounter.applications.ApplicationsManagerException;
import io.beancounter.commons.model.User;
import io.beancounter.usermanager.UserTokenManager;

import java.util.Collection;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A utility class to perform various validations on parameters to REST API
 * calls.
 */
public class Validations {

    private static String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    
    private Validations() {}

    public static void checkNotNull(Object object) {
        if (object == null) {
            throw new NullPointerException();
        }
    }

    public static void checkNotNull(Object object, String errorMessage) {
        if (object == null) {
            throw new NullPointerException(String.valueOf(errorMessage));
        }
    }

    public static void checkNotEmpty(String parameter) {
        if (parameter == null || parameter.trim().isEmpty()) {
            throw new IllegalArgumentException();
        }
    }

    public static void checkNotEmpty(String parameter, String errorMessage) {
        if (parameter == null || parameter.trim().isEmpty()) {
            throw new IllegalArgumentException(String.valueOf(errorMessage));
        }
    }

    public static void checkNotEmpty(Collection parameter) {
        if (parameter == null || parameter.isEmpty()) {
            throw new IllegalArgumentException();
        }
    }

    public static void checkNotEmpty(Collection parameter, String errorMessage) {
        if (parameter == null || parameter.isEmpty()) {
            throw new IllegalArgumentException(String.valueOf(errorMessage));
        }
    }

    public static void check(boolean expression) {
        if (!expression) {
            throw new IllegalArgumentException();
        }
    }

    public static void check(boolean expression, String errorMessage) {
        if (!expression) {
            throw new IllegalArgumentException(String.valueOf(errorMessage));
        }
    }

    public static void validateUserToken(String userToken, User user, UserTokenManager tokenManager) throws TokenAuthenticationException, TokenValidationException {
        boolean tokenIsValid = true;

        try {
            UUID token = UUID.fromString(userToken);
            
            if(!token.equals(user.getUserToken()))
            	throw new TokenAuthenticationException("Error authenticating user token [" + userToken + "]");
            
            
            if (!tokenManager.checkTokenExists(token))
            	throw new TokenValidationException("User token [" + userToken + "] does not exist");
            	
        } catch (Exception ex) {
            throw new TokenValidationException("User token [" + userToken + "] is not valid");
        }
    }

    public static void validateApiKey(
            String appName,
            String apiKey,
            ApplicationsManager applicationsManager) {
        UUID applicationKey;
        try {
            applicationKey = UUID.fromString(apiKey);
        } catch (Exception e) {
            throw new IllegalArgumentException("Your apikey is not well formed");
        }

        boolean isAuth;
        try {
            isAuth = applicationsManager.isAuthorized(appName, applicationKey);
        } catch (ApplicationsManagerException e) {
            throw new IllegalArgumentException("Error while authorizing your application");
        }

        if (!isAuth) {
            throw new IllegalArgumentException("Application with key [" + apiKey + "] is not authorized");
        }
    }

    public static void validateEmail(String email) {
        if (email == null)
            throw new IllegalArgumentException("Email is undefined");
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        if (!matcher.matches())
            throw new IllegalArgumentException("Email [" + email + "] is not valid");
    }
}
