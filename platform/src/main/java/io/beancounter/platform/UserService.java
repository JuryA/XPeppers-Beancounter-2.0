package io.beancounter.platform;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.inject.Inject;
import com.google.inject.name.Named;

import io.beancounter.commons.helper.Clock;
import io.beancounter.commons.helper.IdGenerator;
import io.beancounter.commons.helper.UriUtils;
import io.beancounter.commons.helper.jackson.ObjectMapperFactory;
import io.beancounter.commons.model.ShortUserProfile;
import io.beancounter.commons.model.activity.*;
import io.beancounter.commons.model.activity.Object;
import io.beancounter.commons.model.auth.ServiceUser;
import io.beancounter.platform.validation.Validations;
import io.beancounter.profiles.ProfileManager;
import io.beancounter.profiles.ProfileManagerException;
import io.beancounter.queues.QueuesException;
import io.beancounter.usermanager.UserTokenManager;
import io.beancounter.activities.ActivityStore;
import io.beancounter.activities.ActivityStoreException;
import io.beancounter.activities.InvalidOrderException;
import io.beancounter.applications.ApplicationsManager;
import io.beancounter.applications.ApplicationsManagerException;
import io.beancounter.commons.model.Application;
import io.beancounter.commons.model.auth.OAuthToken;
import io.beancounter.commons.model.User;
import io.beancounter.commons.model.UserProfile;
import io.beancounter.commons.model.auth.OAuthAuth;
import io.beancounter.platform.responses.*;
import io.beancounter.queues.Queues;
import io.beancounter.commons.model.auth.AtomicSignUp;
import io.beancounter.usermanager.UserManager;
import io.beancounter.usermanager.UserManagerException;
import io.beancounter.usermanager.grabber.ActivityGrabberManager;
import io.beancounter.usermanager.grabber.Callback;
import io.beancounter.usermanager.grabber.FacebookGrabber;
import io.beancounter.usermanager.grabber.TwitterGrabber;
import io.beancounter.commons.model.auth.RequestState;
import io.beancounter.auth.RequestStateManager;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;

import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.*;
import java.nio.charset.Charset;
import java.util.*;

/**
 * @author Davide Palmisano ( dpalmisano@gmail.com )
 */
@Path("rest/user")
@Produces(MediaType.APPLICATION_JSON)
public class UserService extends JsonService {

    private Clock clock;

    private IdGenerator idGenerator;

    private ApplicationsManager applicationsManager;

    private UserManager userManager;

    private UserTokenManager tokenManager;

    private RequestStateManager stateManager;

    private ProfileManager profileManager;

    private Queues queues;

    private ActivityGrabberManager grabberManager;

    private ActivityStore activities;

    @Inject
    @Named("service.host")
    String host;

    /** {@link #host} converted into a URL. */
    URL hostURL;

    /** Cookie for tracking already signed in users. */
    @Inject
    @Named("cookie.token.name")
    private String tokenCookieName;

    /** Lifetime of the {@link #tokenCookieName} cookie, in seconds. */
    @Inject
    @Named("cookie.token.maxage")
    private long tokenCookieMaxage;

    /**
     * Cookie name used during the authorization phase,
     * i.e. signup via third party identity provider.
     */
    public static final String AUTH_COOKIE_NAME = "BCAUTH";

    /**
     * Lifetime of the {@link #AUTH_COOKIE_NAME} cookie, in seconds.
     */
    public static final int AUTH_COOKIE_MAX_AGE = 3600;

    /**
     * JSON Serializer used mainly to push data up the queues.
     * You can also use it to do any other kinds of JSON object
     * (de-)serializations.
     */
    private ObjectMapper mapper = ObjectMapperFactory.createMapper();


    @Inject
    public UserService(
            final Clock clock,
            final IdGenerator idGenerator,
            final ApplicationsManager am,
            final UserManager um,
            final UserTokenManager tokenManager,
            final RequestStateManager stateManager,
            final ProfileManager ps,
            final Queues queues,
            final ActivityGrabberManager grabberManager,
            final ActivityStore activities
    ) {
        this.clock = clock;
        this.idGenerator = idGenerator;
        this.applicationsManager = am;
        this.tokenManager = tokenManager;
        this.stateManager = stateManager;
        this.userManager = um;
        this.profileManager = ps;
        this.queues = queues;
        this.grabberManager = grabberManager;
        this.activities = activities;
    }


    // ------------ OAuth Authentication API ----------------------------------


    /**
     * Handles Sign-in or Sign-up for a customer using a service.
     * Both web and mobile modes are supported.
     *
     * @param customer A customer name, e.g. "xpeppers".
     * @param service Third party identity provider, e.g. "facebook".
     * @param contunueTo Customer's final redirect. If null, a mobile mode
     *                      is assumed and so in the callback phase the final
     *                      will be a JSON response instead of a redirect.
     * @param clientId Optional end-user tracking ID, e.g. from GA.
     * @return A temporary redirect to the {@code service} authorization page.
     */
    @GET
    @Path("/oauth2/{service}/{customer}")
    public Response handleSignUpWithService(
            @PathParam("service") String service,
            @PathParam("customer") String customer,
            @QueryParam("continue") String contunueTo,
            @QueryParam("cid") String clientId

    ) {
        LOGGER.info("Signing in with " + service);

        RequestState.Flow flow;
        URI continueToURI;

        if (contunueTo == null || contunueTo.length() == 0) {
            // Mobile version
            flow = RequestState.Flow.MOBILE;
            continueToURI = null;
            LOGGER.debug("Mobile mode");

        } else {
            // Web (desktop?) version
            flow = RequestState.Flow.WEB;
            try {
                continueToURI = new URI(contunueTo);
            } catch (URISyntaxException e) {
                return error(e, "'" + contunueTo + "' is not a valid URL");
            }
            if (continueToURI.getQuery() != null) {
                List<NameValuePair> query =
                        URLEncodedUtils.parse(contunueTo, Charset.forName("UTF-8"));
                for (NameValuePair nv: query) {
                    String name = nv.getName();
                    if (name.equals("username") || name.equals("token"))
                        return error(
                                "'username' and 'token' are reserved parameters");
                }
            }
        }

        // Store all params we'll need during callback phase in a request state.
        RequestState state = new RequestState(
                customer, clientId, continueToURI, flow);
        String stateId = stateManager.put(state);
        if (stateId == null) {
            return error("Could not store request state");
        }

        // Get auth token with a valid state ID.
        OAuthToken token;
        try {
            token = userManager.getOAuthTokenWithState(
                    stateId, service, customer);
        } catch (UserManagerException e) {
            stateManager.pop(stateId);
            return error(e, "Error while getting token from '" + service + "'");
        }

        NewCookie authCookie = createCookie(AUTH_COOKIE_NAME, stateId);
        URL redirect = token.getRedirectPage();
        try {
            LOGGER.info("Redirecting to " + redirect);
            Response.ResponseBuilder rb = Response.temporaryRedirect(
                    redirect.toURI());
            return addHttpOnlyCookie(authCookie, AUTH_COOKIE_MAX_AGE, rb).build();
        } catch (URISyntaxException e) {
            stateManager.pop(stateId);
            return error(e, "Malformed redirect URL");
        }
    }

    /**
     * Entry point for both OAuth 2.0 and 1.0a callbacks, web and mobile.
     *
     * Final (customer) callback URL, profile (web or mobile) and other info
     * can be retrieved using {@param stateId}.
     *
     * Handler path should match {@code service.<provider>.oauth2callback}
     * in a beancounter.properties file.
     *
     * @param appName A merchant name, e.g. "xpeppers". Should match the
     *                 value stored in the original request state.
     * @param stateId A state that corresponds to the original auth request.
     *                Always required. Snould match {@code bcauth}.
     * @param bcauth An auth cookie value, should match {@code stateId}.
     * @param code OAuth2.0 code used to exchange for access token. Preferred
     *             over (overwrites) {@code verifier}.
     * @param token OAuth 1.0a token
     * @param verifier OAuth 1.0a verifier
     * @param error Not null in case user declined authorization or other
     *              reasons normally originated on the service side.
     * @param denied Same as {@param error}, Twitter version.
     * @return Either redirect (for web profiles) or a JSON for mobile.
     */
    @GET
    @Path("/oauth2/callback/{service}/{appName}")
    public Response handleOAuth2CallbackWithState(
            @PathParam("service") String service,
            @PathParam("appName") String appName,
            @QueryParam("state") String stateId,
            @QueryParam("code") String code,
            @QueryParam("oauth_token") String token,
            @QueryParam("oauth_verifier") String verifier,
            @QueryParam("error") String error,
            @QueryParam("denied") String denied,
            @CookieParam(AUTH_COOKIE_NAME) String bcauth
    ) {
        LOGGER.info("Handle OAuth callback for: " + service);

        Application app = null;
        try {
            app = applicationsManager.get(appName);
        } catch (ApplicationsManagerException e) {
            LOGGER.error(e.getMessage(), e);
        }
        if (app == null)
            return error("Unrecognized application: " + appName, 400);

        // Normalize params: OAuth 2.0 vs 1.0a
        if (code != null)
            verifier = code;

        // Facebook uses 'error' param, while Twitter uses 'denied'
        if (error == null && denied != null)
            error = denied;

        // Check for identify provider errors
        if (error != null) {
            try {
                LOGGER.error(error);
                LOGGER.debug("Redirecting to: " + app.getCallbackFailure());
                return Response.temporaryRedirect(app.getCallbackFailure().toURI()).build();
            } catch (URISyntaxException e) {
                LOGGER.error(e.getMessage(), e);
                return error("Malformed failure callback URL: " + app.getCallbackFailure());
            }
        }

        // Validate authentication state
        if (stateId == null || !stateId.equals(bcauth))
            return error("Invalid state", 403);

        RequestState state = stateManager.pop(stateId);
        if (state == null) {
            LOGGER.error("State [" + stateId + "] does not exist");
            return error("Invalid state", 400);
        }

        if (!appName.equals(state.getCustomer())) {
            LOGGER.error("Invalid customer. Have " + appName + ", want " + state.getCustomer());
            return error("Invalid customer", 400);
        }

        // Process sign up or login
        AtomicSignUp signUp;
        try {
            signUp = userManager.storeUserFromOAuthWithState(service, token, verifier, state);
            LOGGER.debug("AtomicSignUp: " + signUp);
        } catch (UserManagerException e) {
            return error("Authorization error", 403);
        }

        pushLoginActivity(signUp, state);

        // TODO: do this in a background task.
        LOGGER.info("Grabbing initial activities for " + signUp.getUsername());
        try {
            grabInitialActivities(signUp, state);
        } catch (UserManagerException e) {
            final String errMsg = "Error while grabbing the first set of " +
                    "activities for user [" + signUp.getUsername() +
                    "] on service [" + signUp.getService() + "]";
            LOGGER.error(errMsg, e);
            //return error(e, errMsg);
        } catch (QueuesException e) {
            final String errMsg = "Error while pushing down the first set of " +
                    "activities for user [" + signUp.getUsername() +
                    "] on service [" + signUp.getService() + "]";
            LOGGER.error(errMsg, e);
            //return error(e, errMsg);
        } catch (Exception e) {
            final String errMsg = "Error while grabbing the first set of " +
                    "activities for user [" + signUp.getUsername() +
                    "] on service [" + signUp.getService() + "]";
            LOGGER.error(errMsg, e);
        }

        // Final response
        Response.ResponseBuilder rb;

        if (state.isMobile()) {

            // Mobile (JSON-based) response
            rb = Response.ok();
            rb.entity(new AtomicSignUpResponse(PlatformResponse.Status.OK, null, signUp));
            LOGGER.info("Successfully logged in " + signUp.getUsername());

        } else {

            // Web (redirect-based) response
            URI redirectURI;
            try {
                URL successURL = app.getCallbackSuccess();
                StringBuilder url = new StringBuilder(successURL.toString());
                url.append(successURL.getQuery() == null ? '?' : '&');
                url.append("username=").append(signUp.getUsername());
                url.append("&token=").append(signUp.getUserToken());
                redirectURI = new URI(url.toString());
            } catch (URISyntaxException e) {
                LOGGER.error(e.getMessage(), e);
                return error(e, "Malformed success callback URL: " + app.getCallbackSuccess());
            }

            LOGGER.info("Redirecting user to: " + redirectURI);

            rb = Response.temporaryRedirect(redirectURI);
            NewCookie tcookie = createCookie(tokenCookieName, signUp.getUserToken().toString());
            addHttpOnlyCookie(tcookie, tokenCookieMaxage, rb);
        }

        NewCookie removeAuthCookie = createCookie(AUTH_COOKIE_NAME, null);
        return addHttpOnlyCookie(removeAuthCookie, AUTH_COOKIE_MAX_AGE, rb).build();
    }


    // ----------------------- API based on user token ------------------------


    @GET
    @Path("/{userId}/me")
    public Response getUserWithUserToken(
            @PathParam("userId") String userId,
            @QueryParam(USER_TOKEN) String token
    ) {
        User user;
        try {
            Validations.checkNotEmpty(userId);
            user = userManager.getUser(userId);
            Validations.checkNotNull(user, "user with ID [" + userId + "] not found");
            Validations.validateUserToken(token, user, tokenManager);
        } catch (Exception ex) {
            return error(ex.getMessage());
        }

        Response.ResponseBuilder rb = Response.ok();
        rb.entity(
                new UserPlatformResponse(
                        UserPlatformResponse.Status.OK,
                        "user [" + userId + "] found",
                        user
                )
        );
        return rb.build();
    }

    @GET
    @Path("/{userId}/profile/me")
    public Response getProfile(
            @PathParam("userId") String userId,
            @QueryParam(USER_TOKEN) String token
    ) {
        User user;
        try {
            Validations.checkNotEmpty(userId, "Must specify a user ID");
            user = userManager.getUser(userId);
            Validations.checkNotNull(user, "user with ID [" + userId + "] not found");
            Validations.validateUserToken(token, user, tokenManager);
        } catch (Exception ex) {
            return error(ex.getMessage());
        }

        UserProfile up;
        try {
            up = profileManager.lookup(user.getId());
        } catch (ProfileManagerException e) {
            return error(e, "Error while retrieving profile for user [" + userId + "]");
        }

        if (up == null) {
            return error("Profile for user [" + userId + "] not found");
        }

        Response.ResponseBuilder rb = Response.ok();
        rb.entity(
                new UserProfilePlatformResponse(
                        UserProfilePlatformResponse.Status.OK,
                        "profile for user [" + userId + "] found",
                        up
                )
        );
        return rb.build();
    }

    /**
     * Responds with a short version of user profile and essential properties
     * of the profile owner (user).
     *
     * @param userToken Token generated during authentication phase.
     * @return JSON of {@link ShortProfileResponse}.
     */
    @GET
    @Path("/me")
    public Response getOwnShortProfile(
            @QueryParam(USER_TOKEN) String userToken
    ) {
        UUID token;
        try {
            token = UUID.fromString(userToken);
        } catch (IllegalArgumentException e) {
            return error("Invalid token", HttpStatus.SC_BAD_REQUEST);
        }

        ShortUserProfile shortProfile;
        try {
            UUID userId = tokenManager.getUserId(token);
            if (userId == null)
                return error("Invalid token", HttpStatus.SC_BAD_REQUEST);

            shortProfile = profileManager.lookupShortProfile(userId);
            if (shortProfile == null)
                return error("Not found", HttpStatus.SC_NOT_FOUND);

        } catch (ProfileManagerException e) {
            return error("Error while fetching profile");
        } catch (UserManagerException e) {
            return error("Token error");
        }

        return Response.ok().entity(new ShortProfileResponse(
                PlatformResponse.Status.OK, null, shortProfile
        )).build();
    }


    // ------------------ API based on application key ------------------------


    /**
     * Create a new user using custom username and a password.
     */
    @POST
    @Path("/app/{appName}/register")
    public Response signUp(
            @PathParam("appName") String appName,
            @FormParam("name") String name,
            @FormParam("surname") String surname,
            @FormParam("username") String username,
            @FormParam("password") String password,
            @QueryParam(API_KEY) String apiKey
    ) {
        User user;
        try {
            Validations.checkNotEmpty(name);
            Validations.checkNotEmpty(surname);
            Validations.checkNotEmpty(username);
            Validations.checkNotEmpty(password);
            user = userManager.getUser(username);
            Validations.check(user == null, "username [" + username + "] is already taken");
            Validations.validateApiKey(appName, apiKey, applicationsManager);
        } catch (Exception ex) {
            return error(ex.getMessage());
        }

        user = new User(name, surname, username, password);
        UUID userToken;
        try {
            userToken = tokenManager.createUserToken(user.getId());
        } catch (UserManagerException e) {
            final String errMsg = "Error while getting token for user [" + user + "]";
            return error(e, errMsg);
        }
        user.setUserToken(userToken);
        try {
            userManager.storeUser(user);
        } catch (UserManagerException e) {
            final String errMsg = "Error while storing user [" + user + "]";
            return error(e, errMsg);
        }

        Response.ResponseBuilder rb = Response.ok();
        rb.entity(new UUIDPlatformResponse(
                UUIDPlatformResponse.Status.OK,
                "user successfully registered",
                user.getId())
        );
        return rb.build();
    }

    @GET
    @Path("/app/{appName}/{userId}")
    public Response getUserWithApiKey(
            @PathParam("appName") String appName,
            @PathParam("userId") String userId,
            @QueryParam(API_KEY) String apiKey
    ) {
        User user;
        try {
            Validations.checkNotEmpty(userId);
            Validations.checkNotEmpty(apiKey, "Missing api key");
            Validations.validateApiKey(appName, apiKey, applicationsManager);
            user = userManager.getUser(userId);
            Validations.checkNotNull(user, "user with ID [" + userId + "] not found");
        } catch (Exception ex) {
            return error(ex.getMessage());
        }

        Response.ResponseBuilder rb = Response.ok();
        rb.entity(
                new UserPlatformResponse(
                        UserPlatformResponse.Status.OK,
                        "user [" + userId + "] found",
                        user
                )
        );
        return rb.build();
    }

    @DELETE
    @Path("/app/{appName}/{userId}")
    public Response deleteUser(
            @PathParam("appName") String appName,
            @PathParam("userId") String userId,
            @QueryParam(API_KEY) String apiKey
    ) {
        User user;
        try {
            Validations.checkNotEmpty(userId, "Must specify a user ID to delete");
            Validations.checkNotEmpty(apiKey, "Missing api key");
            Validations.validateApiKey(appName, apiKey, applicationsManager);
            user = userManager.getUser(userId);
            Validations.checkNotNull(user, "user with ID [" + userId + "] not found");
        } catch (Exception ex) {
            return error(ex.getMessage());
        }

        try {
            userManager.deleteUser(user);
        } catch (UserManagerException e) {
            return error(e, "Error while deleting user [" + userId + "]");
        }

        return success("user with ID [" + userId + "] deleted");
    }

    @GET
    @Path("/app/{appName}/all")
    public Response allUsersByCustomer(
            @PathParam("appName") String appName,
            @QueryParam(API_KEY) String apiKey
    ) {
        Collection<User> users;
        try {
            Validations.checkNotEmpty(apiKey, "Missing api key");
            Validations.validateApiKey(appName, apiKey, applicationsManager);
        } catch (Exception ex) {
            return error(ex.getMessage());
        }

        try {
            Application application = applicationsManager.get(appName);
            users = userManager.getUsersByCustomer(application.getName());
        } catch (UserManagerException e) {
            return error(e, "Error while deleting user ");
        } catch (ApplicationsManagerException e) {
            return error(e, "Error while deleting user ");
        }

        Response.ResponseBuilder rb = Response.ok();
        String message = (users.isEmpty()) ? "customer '" + apiKey + "' has no users."
                : "customer '" + apiKey + "' users found.";
        rb.entity(new UsersPlatformResponse(PlatformResponse.Status.OK, message, users));
        return rb.build();
    }

    @GET
    @Path("/app/{appName}/{userId}/{service}/check")
    public Response checkToken(
            @PathParam("appName") String appName,
            @PathParam("userId") String userId,
            @PathParam("service") String service,
            @QueryParam(API_KEY) String apiKey
    ) {
        User user;
        try {
            Validations.checkNotEmpty(userId, "Must specify a user ID");
            Validations.checkNotEmpty(service, "Must specify a valid service");
            Validations.checkNotEmpty(apiKey, "Missing api key");
            Validations.validateApiKey(appName, apiKey, applicationsManager);
            user = userManager.getUser(userId);
            Validations.checkNotNull(user, "user with ID [" + userId + "] not found");
        } catch (Exception ex) {
            return error(ex.getMessage());
        }

        OAuthAuth auth = (OAuthAuth) user.getAuth(service);
        if (auth == null) {
            return error("user with ID [" + userId + "] has not a token for service [" + service + "]");
        }
        if (auth.isExpired()) {
            return error("[" + service + "] token for [" + userId + "] has expired");
        }
        return success("[" + service + "] token for [" + userId + "] is valid");
    }

    @POST
    @Path("/app/{appName}/{userId}/authenticate")
    public Response authenticate(
            @PathParam("appName") String appName,
            @PathParam("userId") String userId,
            @FormParam("password") String password,
            @QueryParam(API_KEY) String apiKey
    ) {
        User user;
        try {
            Validations.checkNotEmpty(userId, "Must specify a user ID");
            Validations.checkNotEmpty(password, "Must specify a password");
            Validations.checkNotEmpty(apiKey, "Missing api key");
            Validations.validateApiKey(appName, apiKey, applicationsManager);
            user = userManager.getUser(userId);
            Validations.checkNotNull(user, "user with ID [" + userId + "] not found");
        } catch (Exception ex) {
            return error(ex.getMessage());
        }
        if (!user.getPassword().equals(password)) {
            return error("password for [" + userId + "] incorrect");
        }
        AtomicSignUp signUp = new AtomicSignUp(
                user.getId(),
                user.getUsername(),
                true,
                "beancounter",
                user.getUsername(),
                user.getUserToken()
        );
        Response.ResponseBuilder rb = Response.ok();
        rb.entity(
                new AtomicSignUpResponse(
                        PlatformResponse.Status.OK,
                        "user with user name [" + signUp.getUsername() + "] logged in with service [" + signUp.getService() + "]",
                        signUp
                )
        );
        return rb.build();
    }

    @DELETE
    @Path("app/{appName}/source/{userId}/{service}")
    public Response removeSource(
            @PathParam("appName") String appName,
            @PathParam("userId") String userId,
            @PathParam("service") String service,
            @QueryParam(API_KEY) String apiKey
    ) {
        User user;
        try {
            Validations.checkNotEmpty(userId, "Must specify a user ID");
            Validations.checkNotEmpty(service, "Must specify a valid service");
            Validations.checkNotEmpty(apiKey, "Missing api key");
            Validations.validateApiKey(appName, apiKey, applicationsManager);
            user = userManager.getUser(userId);
            Validations.checkNotNull(user, "User [" + userId + "] not found!");
        } catch (Exception ex) {
            return error(ex.getMessage());
        }

        try {
            userManager.deregisterService(service, user);
        } catch (UserManagerException e) {
            return error(e, "Error while retrieving user [" + userId + "]");
        }

        return success("service [" + service + "] removed from user [" + userId + "]");
    }

    @GET
    @Path("/app/{appName}/{userId}/profile")
    public Response getProfileWithApiKey(
            @PathParam("appName") String appName,
            @PathParam("userId") String userId,
            @QueryParam(API_KEY) String apiKey
    ) {
        User user;
        try {
            Validations.checkNotEmpty(userId, "Must specify a user ID");
            Validations.checkNotEmpty(apiKey, "Missing api key");
            Validations.validateApiKey(appName, apiKey, applicationsManager);
            user = userManager.getUser(userId);
            Validations.checkNotNull(user, "user with ID [" + userId + "] not found");
        } catch (Exception ex) {
            return error(ex.getMessage());
        }

        UserProfile up;
        try {
            up = profileManager.lookup(user.getId());
        } catch (ProfileManagerException e) {
            return error(e, "Error while retrieving profile for user [" + userId + "]");
        }

        if (up == null) {
            return error("Profile for user [" + userId + "] not found");
        }

        Response.ResponseBuilder rb = Response.ok();
        rb.entity(
                new UserProfilePlatformResponse(
                        UserProfilePlatformResponse.Status.OK,
                        "profile for user [" + userId + "] found",
                        up
                )
        );
        return rb.build();
    }

    /**
     * Converts {@code #host} into a URL object.
     * Otherwise, logs the error and returns null.
     */
    private URL getHostURL() {
        if (hostURL == null)
            try {
                hostURL = new URL(host);
            } catch (MalformedURLException e) {
                LOGGER.error("["+host+"] is not a valid URL. "+
                             "Check beancounter.properties");
                return null;
            }
        return hostURL;
    }

    /**
     * Creates a new OAuth state cookie.
     * @param value Cookie value
     */
    private NewCookie createCookie(String name, String value) {
        URL domainURL = getHostURL();
        if (domainURL == null)
            return null;

        return new NewCookie(name, value,
                "/",                    // path
                domainURL.getHost(),    // domain
                null,                   // comment
                -1,                     // max age, ignored
                // secure?
                domainURL.getProtocol().toLowerCase().equals("https"));
    }

    /**
     * Adds a Set-Cookie header with HttpOnly flag.
     *
     * @param cookie Any cookie. If value is null or empty, adds a proper
     *               expiration date so that browser will delete the cookie.
     * @param maxage Lifetime of the cookie or < 0 to remove the cookie.
     * @param rb A response builder
     *
     * @return Same as {@code rb} in the params.
     */
    private Response.ResponseBuilder addHttpOnlyCookie(
            NewCookie cookie,
            long maxage,
            Response.ResponseBuilder rb
    ) {
        String val = cookie.toString();
        long age = cookie.getValue() == null || cookie.getValue().isEmpty() ?
                0 : maxage;

        if (age > 0) {
            DateTime expiry = new DateTime(
                    System.currentTimeMillis() + age * 1000,
                    DateTimeZone.UTC);
            val += "; Expires=" + expiry.toString(
                    "EE, dd-MMM-yyyy HH:mm:ss 'GMT'", Locale.US);
        } else {
            val += "; Expires=Thu, 01-Jan-1970 00:00:01 GMT";
        }

        // We could use "max-age" cookie flag but some older IE only support
        // "expires". Unfortunately, NewCookie only provides "max-age" version
        // and does not provide "httponly" so, the cookie header has to be
        // constructed manually.
        rb.header("Set-Cookie", val + "; HttpOnly");

        return rb;
    }

    /**
     * Creates and pushes a custom activity up the queue to indicate that
     * a login or signup is just happend. This can be used by e.g. an exporter
     * to push data further into an analytics service.
     */
    private void pushLoginActivity(AtomicSignUp signUp, RequestState state) {
        String userId = signUp.getUserId().toString();
        User user;
        try {
            user = userManager.getUser(userId);
        } catch (UserManagerException e) {
            LOGGER.error("Error retrieving user from: " + signUp, e);
            return;
        }

        Verb v = signUp.isReturning() ?
                (state.isMobile() ? Verb.LOGIN_MOBILE : Verb.LOGIN_WEB) :
                (state.isMobile() ? Verb.SIGNUP_MOBILE : Verb.SIGNUP_WEB);

        Activity activity;
        ActivityBuilder builder = new DefaultActivityBuilder();
        try {
            builder.push();
            builder.setVerb(v);
            builder.setContext(clock.utcnow(), signUp.getService(), userId);
            // Set an empty dummy object so that profiler and other processes
            // won't bump into a null pointer exception.
            builder.setObject(
                    Object.class, null, null,
                    new HashMap<String, java.lang.Object>());
            activity = builder.pop();
        } catch (ActivityBuilderException e) {
            LOGGER.error("Error creating login activity from: " + signUp, e);
            return;
        }

        activity.setId(idGenerator.randomUUID(Activity.class));
        ResolvedActivity ra = new ResolvedActivity(signUp.getUserId(), activity, user);

        String raJson = null;
        try {
            raJson = mapper.writeValueAsString(ra);
            queues.push(raJson);
        } catch (JsonProcessingException e) {
            LOGGER.error("Error serializing RA: " + ra);
        } catch (QueuesException e) {
            LOGGER.error("Error pushing to queue: " + raJson, e);
        }
    }

    private void grabInitialActivities(AtomicSignUp signUp, RequestState state) throws UserManagerException, QueuesException {
        String service = signUp.getService();
        String userId = signUp.getUserId().toString();
        String customer = state.getCustomer();

        User user;
        try {
            user = userManager.getUser(userId);
        } catch (UserManagerException e) {
            throw new UserManagerException("Error while retrieving user: [" + userId + "]", e);
        }

        OAuthAuth auth = (OAuthAuth) user.getAuth(service);
        if (auth == null) {
            throw new UserManagerException("User [" + userId + "] has no auth for service [" + service + "]");
        }
        if (auth.isExpired()) {
            throw new UserManagerException("OAuth token for [" + service + "] on user [" + userId + "] has expired");
        }

        Map<Verb, ResolvedActivity> latestActivities = null;
        try {
            List<Verb> verbs = new ArrayList<Verb>();
            verbs.add(Verb.LIKE);
            verbs.add(Verb.SHARE);
            verbs.add(Verb.TWEET);
            latestActivities = activities.getLatestActivities(user.getId(), verbs);
            LOGGER.info("LastActivities: " + latestActivities + " for user: " + user.getId());
        } catch (ActivityStoreException e1) {
            throw new UserManagerException("Error retrieving activities for user: " + user.getId() + " err: " + e1.getMessage());
        } catch (InvalidOrderException e1) {
            throw new UserManagerException("Error retrieving activities for user: " + user.getId() + " err: " + e1.getMessage());
        }
        Callback<List<ResolvedActivity>> callback = new Callback<List<ResolvedActivity>>() {
            @Override
            public void complete(List<ResolvedActivity> result) {
                for (ResolvedActivity activity : result) {
                    String raJson;
                    try {
                        raJson = mapper.writeValueAsString(activity);
                    } catch (IOException e) {
                        // Just skip this one
                        LOGGER.warn("Error while serializing to JSON {}", activity);
                        continue;
                    }

                    try {
                        queues.push(raJson);
                    } catch (QueuesException e) {
                        LOGGER.error("Error while pushing down json " +
                                "resolved activity: [" + raJson + "] for user [" + activity.getUser().getUsername() + "] " +
                                "on service [" + activity.getActivity().getContext().getService() + "]", e);
                    }
                }
            }
        };

        Application application;
        try {
            application = applicationsManager.get(customer);
        } catch (ApplicationsManagerException e) {
            throw new UserManagerException("Error retrieving application for user: " + user.getId() +
                    " and customer: " + customer + " err: " + e.getMessage());
        }

        if ("facebook".equals(service)) {
            grabberManager.submit(FacebookGrabber.create(user, signUp.getIdentifier(), latestActivities), callback);
        } else if ("twitter".equals(service)) {
            grabberManager.submit(TwitterGrabber.create(user, signUp.getIdentifier(), application, latestActivities), callback);
        } else {
            throw new UserManagerException("Service [" + service + "] is not supported");
        }
    }
}