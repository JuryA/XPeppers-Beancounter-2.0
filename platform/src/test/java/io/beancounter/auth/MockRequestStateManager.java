package io.beancounter.auth;

import io.beancounter.commons.model.auth.RequestState;

/**
 * @deprecated
 */
@Deprecated
public class MockRequestStateManager implements RequestStateManager {

    @Override
    public RequestState pop(String stateId) {
        return null;
    }

    @Override
    public String put(RequestState state) {
        return null;
    }

    @Override
    public String put(RequestState state, int expire) {
        return null;
    }

    @Override
    public void put(String token, String secret, int expire) throws RequestStateException {

    }

    @Override
    public void put(String token, String secret) throws RequestStateException {

    }

    @Override
    public String popSecret(String token) {
        return null;
    }

    @Override
    public void notify(String channel, String serviceUserId) {

    }
}
