package io.beancounter.usermanager;

import io.beancounter.commons.model.AuthServiceConfig;
import io.beancounter.commons.model.auth.OAuthToken;
import io.beancounter.commons.model.User;
import io.beancounter.commons.model.activity.Activity;
import io.beancounter.commons.model.auth.*;
import io.beancounter.commons.tests.RandomBean;
import io.beancounter.commons.tests.Tests;
import io.beancounter.commons.tests.TestsBuilder;
import io.beancounter.commons.tests.TestsException;
import io.beancounter.commons.tests.randomisers.IntegerRandomiser;
import io.beancounter.commons.tests.randomisers.UUIDRandomiser;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

public class MockUserManager implements UserManager {

    private Tests tests = TestsBuilder.getInstance().build();

    private List<UUID> userUUIDs = new ArrayList<UUID>();

    public MockUserManager() {
        // instantiate users
        for (int i = 0; i <= (new IntegerRandomiser("anon", 20)).getRandom(); i++) {
            userUUIDs.add(new UUIDRandomiser("anon").getRandom());
        }
    }

    @Override
    public void storeUser(User user) throws UserManagerException {
    }

    @Override
    public synchronized User getUser(String username) throws UserManagerException {
        User user = null;
        if (!username.equals("missing-user")) {
            try {
                user = tests.build(User.class).getObject();
            } catch (TestsException e) {
                throw new UserManagerException("Error while building random user with username [" + username + "]");
            }
            user.setId("user-with-no-activities".equals(username)
                    ? UUID.fromString("0ad77722-1338-4c32-9209-5b952530959d")
                    : UUID.randomUUID());
            user.addService("fake-oauth-service", new OAuthAuth("fake-session", "fake-secret"));
            user.addService("fake-simple-service", new SimpleAuth("fake-session", "fake-username"));
            Collection<AuthServiceConfig> services = new ArrayList<AuthServiceConfig>();
            try {
                Collection<RandomBean<AuthServiceConfig>> randomBeansServices = tests.build(AuthServiceConfig.class, 3);
                for (RandomBean<AuthServiceConfig> bean : randomBeansServices) {
                    services.add(bean.getObject());
                    user.addService(
                            bean.getObject().getName(),
                            new SimpleAuth(
                                    bean.getObject().getDescription(),
                                    bean.getObject().getSecret()
                            )
                    );
                }
            } catch (TestsException e) {
                throw new UserManagerException("Error while building random services for user with username [" + username + "]");
            }
            user.setPassword("abc");
        }
        return user;
    }

    @Override
    public void deleteUser(User user) throws UserManagerException {}

    @Override
    public OAuthToken getOAuthToken(String serviceName, String username, URL url) throws UserManagerException {
        throw new UnsupportedOperationException("nah, NIY");
    }

    @Override
    public OAuthToken getOAuthToken(String service, String username)
            throws UserManagerException {
        try {
            return new OAuthToken(new URL("http://testurl.com/"));
        } catch (MalformedURLException e) {
            // it never happens
        }
        return null;
    }

    @Override
    public void registerService(String service, User user, String token)
            throws UserManagerException {}

    @Override
    public ServiceUser registerOAuthService(String service, User user, String token, String verifier)
            throws UserManagerException {
        return null;
    }

    @Override
    public void deregisterService(String service, User userObj)
            throws UserManagerException {
    }

    @Override
    public void voidOAuthToken(User user, String service) throws UserManagerException {
        throw new UnsupportedOperationException("nah, niy");
    }

    @Override
    public OAuthToken getOAuthToken(String service) throws UserManagerException {
        throw new UnsupportedOperationException("nah, niy");
    }
    
    @Override
    public OAuthToken getOAuthTokenWithCustomer(String service, String customer) throws UserManagerException {
        throw new UnsupportedOperationException("nah, niy");
    }

    @Override
    public OAuthToken getOAuthToken(String service, URL finalRedirectUrl) throws UserManagerException {
        throw new UnsupportedOperationException("nah, niy");
    }

    /**
     * Alternative version of {@link #getOAuthTokenWithCustomer} with additional
     * info about the original user request.
     *
     * @param stateId A state that refers to the original user request.
     * @param service Third party identity provider, e.g. "facebook'.
     * @param customer A merchant name, e.g. "xpeppers".
     * @return A token with the originally provided {@code stateId}.
     * @throws UserManagerException
     */
    @Override
    public OAuthToken getOAuthTokenWithState(String stateId,
                                             String service,
                                             String customer)
    throws UserManagerException {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    @Deprecated
    public List<Activity> grabUserActivities(User user, String identifier, String service, int limit) throws UserManagerException {
        throw new UnsupportedOperationException("nah, niy");
    }

    @Override
    public AtomicSignUp storeUserFromOAuthWithState(String service,
                                                    String token,
                                                    String verifier,
                                                    RequestState state)
            throws UserManagerException {
        throw new UnsupportedOperationException("nah, niy");
    }

    @Override
    public Collection<User> getUsersByCustomer(String customerName)
            throws UserManagerException {
        throw new UnsupportedOperationException("nah, niy");
    }

    @Override
    public OAuthToken getOAuthTokenWithCustomer(String service,
            URL finalRedirectURL, String customer) throws UserManagerException {
        throw new UnsupportedOperationException("nah, niy");
    }

    @Override
    public void setUserEmail(User user, String email) throws UserManagerException {
        throw new UnsupportedOperationException("nah, niy");
    }

    @Override
    public User updateUserWithOAuthCredentials(User reallyExistentUser,
            String service, ServiceUser srvUser)
            throws UserManagerException {
        throw new UnsupportedOperationException("nah, niy");
    }

}