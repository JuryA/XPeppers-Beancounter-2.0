package io.beancounter.platform;


import com.google.inject.servlet.GuiceFilter;
import com.sun.grizzly.http.servlet.ServletAdapter;
import io.beancounter.commons.model.Application;
import io.beancounter.tests.servlet.ServletBasedTestCase;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

public class ApiBaseTestCase extends ServletBasedTestCase {

    @Override
    protected void setupFrontendService() {
        ServletAdapter adapter = new ServletAdapter();
        adapter.addServletListener(TestServiceConfig.class.getName());
        adapter.setServletPath("/");
        adapter.addFilter(new GuiceFilter(), "filter", null);
        server.addGrizzlyAdapter(adapter, null);
    }

    protected Application registerTestApplication() throws IOException {
        HttpResponse resp = post("application/register",
                "name", "fake_application_name",
                "description", "This is a test registration!",
                "email", "fake_mail@test.com");
        String body = EntityUtils.toString(resp.getEntity());
        return fromJson(body, Application.class);
    }
}
