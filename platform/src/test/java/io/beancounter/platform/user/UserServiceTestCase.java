package io.beancounter.platform.user;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.name.Names;
import com.google.inject.servlet.GuiceFilter;
import com.google.inject.servlet.GuiceServletContextListener;
import com.sun.grizzly.http.servlet.ServletAdapter;
import com.sun.jersey.guice.JerseyServletModule;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;

import io.beancounter.activities.ActivityStore;
import io.beancounter.applications.ApplicationsManager;
import io.beancounter.applications.MockApplicationsManager;
import io.beancounter.commons.model.Application;
import io.beancounter.commons.helper.Clock;
import io.beancounter.commons.helper.IdGenerator;
import io.beancounter.commons.helper.jackson.ObjectMapperFactory;
import io.beancounter.commons.model.*;
import io.beancounter.commons.model.activity.Object;
import io.beancounter.commons.model.activity.Activity;
import io.beancounter.commons.model.activity.Context;
import io.beancounter.commons.model.activity.ResolvedActivity;
import io.beancounter.commons.model.activity.Verb;
import io.beancounter.commons.model.auth.*;
import io.beancounter.commons.tests.Tests;
import io.beancounter.commons.tests.TestsBuilder;
import io.beancounter.platform.*;
import io.beancounter.platform.cors.CorsServletFilter;
import io.beancounter.platform.responses.*;
import io.beancounter.profiles.ProfileManager;
import io.beancounter.profiles.ProfileManagerException;
import io.beancounter.queues.Queues;
import io.beancounter.usermanager.UserManager;
import io.beancounter.usermanager.UserManagerException;
import io.beancounter.usermanager.UserTokenManager;
import io.beancounter.usermanager.grabber.ActivityGrabberManager;
import io.beancounter.auth.RequestStateManager;

import org.apache.commons.httpclient.Cookie;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpState;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.DeleteMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import javax.servlet.ServletContextEvent;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.MessageBodyWriter;

import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.util.*;

import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

/**
 * Reference test case for {@link io.beancounter.platform.UserService}
 *
 * @author Enrico Candino ( enrico.candino@gmail.com )
 */
public class UserServiceTestCase extends ApiBaseTestCase {

    private Tests tests = TestsBuilder.getInstance().build();

    private Application application;
    private static String APPNAME = "xpeppers";
    private static String AUTH_SUCCESS_URL = "http://example.org/auth/success";
    private static String AUTH_FAILURE_URL = "http://example.org/auth/failure";
    private static String APIKEY;

    private static Clock clock;
    private static IdGenerator idGenerator;
    private static ApplicationsManager appManager;
    private static UserManager userManager;
    private static UserTokenManager tokenManager;
    private static RequestStateManager stateManager;
    private static Queues queues;
    private static ProfileManager profileManager;
    private static ActivityGrabberManager grabberManager;
    private static ActivityStore activities;

    private static ObjectMapper mapper = ObjectMapperFactory.createMapper();

    public static final String TOKEN_COOKIE_NAME = "BCTOKEN";
    public static final long TOKEN_COOKIE_MAXAGE = 3600;

    @Override
    protected void setupFrontendService() {
        ServletAdapter ga = new ServletAdapter();
        ga.addContextParameter("service.host", getServiceHost());
        ga.addServletListener(UserServiceTestConfig.class.getName());
        ga.addFilter(new GuiceFilter(), "filter", null);
        ga.setServletPath("/");
        server.addGrizzlyAdapter(ga, null);
    }

    @BeforeMethod
    public void setUp() throws Exception {
        reset(
            clock,
            idGenerator,
            userManager,
            tokenManager,
            stateManager,
            queues,
            profileManager,
            grabberManager,
            activities);

        application = new Application(APPNAME, "Test app", "beancounter@xpeppers.com");
        application.setCallbackSuccess(new URL(AUTH_SUCCESS_URL));
        application.setCallbackFailure(new URL(AUTH_FAILURE_URL));
        APIKEY = application.getApiKey().toString();
        when(appManager.get(APPNAME)).thenReturn(application);
        when(appManager.isAuthorized(APPNAME, application.getApiKey())).thenReturn(true);
    }

    @Test
    public void testSignUp() throws Exception {
        String baseQuery = "user/app/%s/register?apikey=%s";
        String name = "Fake_Name";
        String surname = "Fake_Surname";
        String username = "missing-user";
        String password = "abc";
        String query = String.format(baseQuery, APPNAME, APIKEY);

        when(userManager.getUser(username)).thenReturn(null);

        PostMethod postMethod = new PostMethod(base_uri + query);
        HttpClient client = new HttpClient();
        postMethod.addParameter("name", name);
        postMethod.addParameter("surname", surname);
        postMethod.addParameter("username", username);
        postMethod.addParameter("password", password);
        int result = client.executeMethod(postMethod);
        String responseBody = new String(postMethod.getResponseBody());
        logger.info("result code: " + result);
        logger.info("response body: " + responseBody);

        assertEquals(result, HttpStatus.SC_OK, "\"Unexpected result: [" + result + "]");
        assertNotEquals(responseBody, "");
        APIResponse actual = fromJson(responseBody, APIResponse.class);
        APIResponse expected = new APIResponse(
                actual.getObject(),
                "user successfully registered",
                "OK"
        );
        assertEquals(actual, expected);
        assertNotNull(actual.getObject());
        assertNotNull(UUID.fromString(actual.getObject()));

        User user = new User(name, surname, username, password);
        verify(userManager).storeUser(user);
    }

    @Test
    public void testSignUpRandom() throws Exception {
        User user = tests.build(User.class).getObject();

        String baseQuery = "user/app/%s/register?apikey=%s";
        String name = user.getName();
        String surname = user.getSurname();
        String username = "missing-user";
        String password = user.getPassword();
        String query = String.format(baseQuery, APPNAME, APIKEY);

        PostMethod postMethod = new PostMethod(base_uri + query);
        HttpClient client = new HttpClient();
        postMethod.addParameter("name", name);
        postMethod.addParameter("surname", surname);
        postMethod.addParameter("username", username);
        postMethod.addParameter("password", password);
        int result = client.executeMethod(postMethod);
        String responseBody = new String(postMethod.getResponseBody());
        logger.info("result code: " + result);
        logger.info("response body: " + responseBody);

        assertEquals(result, HttpStatus.SC_OK, "\"Unexpected result: [" + result + "]");
        assertNotEquals(responseBody, "");
        APIResponse actual = fromJson(responseBody, APIResponse.class);
        APIResponse expected = new APIResponse(
                actual.getObject(),
                "user successfully registered",
                "OK"
        );
        assertEquals(actual, expected);
        assertNotNull(actual.getObject());
        assertNotNull(UUID.fromString(actual.getObject()));
    }

    @Test
    public void signUpUserWithExistingUsernameShouldRespondWithError() throws Exception {
        String baseQuery = "user/app/%s/register?apikey=%s";
        String name = "Fake_Name";
        String surname = "Fake_Surname";
        String username = "test-user";
        String password = "abc";
        String query = String.format(baseQuery, APPNAME, APIKEY);

        User user = new User("Test", "User", username, "password");
        when(userManager.getUser(username)).thenReturn(user);

        PostMethod postMethod = new PostMethod(base_uri + query);
        HttpClient client = new HttpClient();
        postMethod.addParameter("name", name);
        postMethod.addParameter("surname", surname);
        postMethod.addParameter("username", username);
        postMethod.addParameter("password", password);
        int result = client.executeMethod(postMethod);
        String responseBody = new String(postMethod.getResponseBody());
        logger.info("result code: " + result);
        logger.info("response body: " + responseBody);

        assertEquals(result, HttpStatus.SC_INTERNAL_SERVER_ERROR, "\"Unexpected result: [" + result + "]");
        assertNotEquals(responseBody, "");
        APIResponse actual = fromJson(responseBody, APIResponse.class);
        APIResponse expected = new APIResponse(
                null,
                "username [test-user] is already taken",
                "NOK"
        );
        assertEquals(actual, expected);
    }

    @Test
    public void getUserWithValidUserToken() throws Exception {
        String baseQuery = "user/%s/me?token=%s";
        String username = "test-user";
        UUID userToken = UUID.randomUUID();
        User user = new User("Test", "User", username, "password");
        user.setUserToken(userToken);
        String query = String.format(
                baseQuery,
                username,
                userToken
        );

        when(userManager.getUser(username)).thenReturn(user);
        when(tokenManager.checkTokenExists(userToken)).thenReturn(true);

        GetMethod getMethod = new GetMethod(base_uri + query);
        HttpClient client = new HttpClient();

        int result = client.executeMethod(getMethod);
        String responseBody = new String(getMethod.getResponseBody());
        assertEquals(result, HttpStatus.SC_OK);
        assertFalse(responseBody.isEmpty());

        UserPlatformResponse response = fromJson(responseBody, UserPlatformResponse.class);
        assertEquals(response.getStatus(), UserPlatformResponse.Status.OK);
        assertEquals(response.getMessage(), "user [" + username + "] found");
        assertEquals(response.getObject().getId(), user.getId());
        assertEquals(response.getObject().getUsername(), user.getUsername());
        assertEquals(response.getObject().getName(), user.getName());
        assertNull(response.getObject().getUserToken(), "The token should not be returned.");
        assertNull(response.getObject().getPassword(), "The password should not be returned.");
    }

    @Test
    public void getMissingUserWithValidTokenShouldRespondWithError() throws Exception {
        String baseQuery = "user/%s/me?token=%s";
        String userId = "missing-user";
        String query = String.format(
                baseQuery,
                userId,
                UUID.randomUUID()
        );

        when(userManager.getUser(userId)).thenReturn(null);

        GetMethod getMethod = new GetMethod(base_uri + query);
        HttpClient client = new HttpClient();

        int result = client.executeMethod(getMethod);
        String responseBody = new String(getMethod.getResponseBody());
        assertEquals(result, HttpStatus.SC_INTERNAL_SERVER_ERROR);
        assertFalse(responseBody.isEmpty());

        APIResponse response = fromJson(responseBody, APIResponse.class);
        assertEquals(response.getStatus(), "NOK");
        assertEquals(response.getMessage(), "user with ID [" + userId + "] not found");
    }

    @Test
    public void getUserWithMalformedUserTokenShouldRespondWithError() throws Exception {
        String baseQuery = "user/%s/me?token=%s";
        String username = "test-user";
        String userToken = "malformed-123";
        User user = new User("Test", "User", username, "password");
        String query = String.format(
                baseQuery,
                username,
                userToken
        );

        when(userManager.getUser(username)).thenReturn(user);

        GetMethod getMethod = new GetMethod(base_uri + query);
        HttpClient client = new HttpClient();

        int result = client.executeMethod(getMethod);
        String responseBody = new String(getMethod.getResponseBody());
        assertEquals(result, HttpStatus.SC_INTERNAL_SERVER_ERROR);
        assertFalse(responseBody.isEmpty());

        APIResponse response = fromJson(responseBody, APIResponse.class);
        assertEquals(response.getStatus(), "NOK");
        assertEquals(response.getMessage(), "User token [" + userToken + "] is not valid");
    }

    @Test
    public void getUserWithWrongUserTokenShouldRespondWithError() throws Exception {
        String baseQuery = "user/%s/me?token=%s";
        String username = "test-user";
        UUID userToken = UUID.randomUUID();
        User user = new User("Test", "User", username, "password");
        user.setUserToken(UUID.randomUUID());
        String query = String.format(
                baseQuery,
                username,
                userToken
        );

        when(userManager.getUser(username)).thenReturn(user);

        GetMethod getMethod = new GetMethod(base_uri + query);
        HttpClient client = new HttpClient();

        int result = client.executeMethod(getMethod);
        String responseBody = new String(getMethod.getResponseBody());
        assertEquals(result, HttpStatus.SC_INTERNAL_SERVER_ERROR);
        assertFalse(responseBody.isEmpty());

        APIResponse response = fromJson(responseBody, APIResponse.class);
        assertEquals(response.getStatus(), "NOK");
        assertEquals(response.getMessage(), "User token [" + userToken + "] is not valid");
    }

    @Test
    public void getUserWithExpiredUserTokenShouldRespondWithError() throws Exception {
        String baseQuery = "user/%s/me?token=%s";
        String username = "test-user";
        UUID userToken = UUID.randomUUID();
        User user = new User("Test", "User", username, "password");
        user.setUserToken(userToken);
        String query = String.format(
                baseQuery,
                username,
                userToken
        );

        when(userManager.getUser(username)).thenReturn(user);
        when(tokenManager.checkTokenExists(userToken)).thenReturn(false);

        GetMethod getMethod = new GetMethod(base_uri + query);
        HttpClient client = new HttpClient();

        int result = client.executeMethod(getMethod);
        String responseBody = new String(getMethod.getResponseBody());
        assertEquals(result, HttpStatus.SC_INTERNAL_SERVER_ERROR);
        assertFalse(responseBody.isEmpty());

        APIResponse response = fromJson(responseBody, APIResponse.class);
        assertEquals(response.getStatus(), "NOK");
        assertEquals(response.getMessage(), "User token [" + userToken + "] is not valid");
    }

    @Test
    public void givenTokenManagerErrorOccursWhenGettingUserThenRespondWithError() throws Exception {
        String baseQuery = "user/%s/me?token=%s";
        String username = "test-user";
        UUID userToken = UUID.randomUUID();
        User user = new User("Test", "User", username, "password");
        user.setUserToken(userToken);
        String query = String.format(
                baseQuery,
                username,
                userToken
        );

        when(userManager.getUser(username)).thenReturn(user);
        when(tokenManager.checkTokenExists(userToken)).thenThrow(new UserManagerException("error"));

        GetMethod getMethod = new GetMethod(base_uri + query);
        HttpClient client = new HttpClient();

        int result = client.executeMethod(getMethod);
        String responseBody = new String(getMethod.getResponseBody());
        assertEquals(result, HttpStatus.SC_INTERNAL_SERVER_ERROR);
        assertFalse(responseBody.isEmpty());

        APIResponse response = fromJson(responseBody, APIResponse.class);
        assertEquals(response.getStatus(), "NOK");
        assertEquals(response.getMessage(), "User token [" + userToken + "] is not valid");
    }

    @Test
    public void getUserWithValidApiKey() throws Exception {
        String baseQuery = "user/app/%s/%s?apikey=%s";
        String username = "test-user";
        User user = new User("Test", "User", username, "password");
        String query = String.format(baseQuery, APPNAME, username, APIKEY);

        when(userManager.getUser(username)).thenReturn(user);

        GetMethod getMethod = new GetMethod(base_uri + query);
        HttpClient client = new HttpClient();

        int result = client.executeMethod(getMethod);
        String responseBody = new String(getMethod.getResponseBody());
        assertEquals(result, HttpStatus.SC_OK);
        assertFalse(responseBody.isEmpty());

        UserPlatformResponse response = fromJson(responseBody, UserPlatformResponse.class);
        assertEquals(response.getStatus(), UserPlatformResponse.Status.OK);
        assertEquals(response.getMessage(), "user [" + username + "] found");
        assertEquals(response.getObject().getId(), user.getId());
        assertEquals(response.getObject().getUsername(), user.getUsername());
        assertEquals(response.getObject().getName(), user.getName());
        assertNull(response.getObject().getPassword(), "The password should not be returned.");
    }

    @Test
    public void getMissingUserWithValidApiKeyShouldRespondWithError() throws Exception {
        String baseQuery = "user/app/%s/%s?apikey=%s";
        String userId = "missing-user";
        String query = String.format(baseQuery, APPNAME, userId, APIKEY);

        when(userManager.getUser(userId)).thenReturn(null);

        GetMethod getMethod = new GetMethod(base_uri + query);
        HttpClient client = new HttpClient();

        int result = client.executeMethod(getMethod);
        String responseBody = new String(getMethod.getResponseBody());
        assertEquals(result, HttpStatus.SC_INTERNAL_SERVER_ERROR);
        assertFalse(responseBody.isEmpty());

        APIResponse response = fromJson(responseBody, APIResponse.class);
        assertEquals(response.getStatus(), "NOK");
        assertEquals(response.getMessage(), "user with ID [" + userId + "] not found");
    }

    @Test
    public void getUserWithMalformedApiKeyShouldRespondWithError() throws Exception {
        String baseQuery = "user/app/%s/%s?apikey=%s";
        String username = "test-user";
        String apiKey = "malformed-123";
        String query = String.format(baseQuery, APPNAME, username, apiKey);

        GetMethod getMethod = new GetMethod(base_uri + query);
        HttpClient client = new HttpClient();

        int result = client.executeMethod(getMethod);
        String responseBody = new String(getMethod.getResponseBody());
        assertEquals(result, HttpStatus.SC_INTERNAL_SERVER_ERROR);
        assertFalse(responseBody.isEmpty());

        APIResponse response = fromJson(responseBody, APIResponse.class);
        assertEquals(response.getStatus(), "NOK");
        assertEquals(response.getMessage(), "Your apikey is not well formed");
    }

    @Test
    public void getUserWithMissingApiKeyShouldRespondWithError() throws Exception {
        String baseQuery = "user/app/%s/%s";
        String username = "test-user";
        String query = String.format(baseQuery, APPNAME, username);

        GetMethod getMethod = new GetMethod(base_uri + query);
        HttpClient client = new HttpClient();

        int result = client.executeMethod(getMethod);
        String responseBody = new String(getMethod.getResponseBody());
        assertEquals(result, HttpStatus.SC_INTERNAL_SERVER_ERROR);
        assertFalse(responseBody.isEmpty());

        APIResponse response = fromJson(responseBody, APIResponse.class);
        assertEquals(response.getStatus(), "NOK");
        assertEquals(response.getMessage(), "Missing api key");
    }

    @Test
    public void givenUserManagerErrorWhenGettingUserWithValidApiKeyThenRespondWithError() throws Exception {
        String baseQuery = "user/app/%s/%s?apikey=%s";
        String username = "test-user";
        String expectedMessage = "Error while retrieving user [" + username + "]";
        String query = String.format(baseQuery, APPNAME, username, APIKEY);

        when(userManager.getUser(username)).thenThrow(new UserManagerException(expectedMessage));

        GetMethod getMethod = new GetMethod(base_uri + query);
        HttpClient client = new HttpClient();

        int result = client.executeMethod(getMethod);
        String responseBody = new String(getMethod.getResponseBody());
        assertEquals(result, HttpStatus.SC_INTERNAL_SERVER_ERROR);
        assertFalse(responseBody.isEmpty());

        APIResponse response = fromJson(responseBody, APIResponse.class);
        assertEquals(response.getStatus(), "NOK");
        assertEquals(response.getMessage(), expectedMessage);
    }

    @Test
    public void testDeleteUser() throws Exception {
        UUID userId = UUID.randomUUID();
        String baseQuery = "user/app/%s/%s?apikey=%s";
        String query = String.format(baseQuery, APPNAME, userId, APIKEY);

        User user = new User("Test", "User", "test-user", "password");
        user.setId(userId);
        when(userManager.getUser(userId.toString())).thenReturn(user);

        DeleteMethod deleteMethod = new DeleteMethod(base_uri + query);
        HttpClient client = new HttpClient();
        int result = client.executeMethod(deleteMethod);
        String responseBody = new String(deleteMethod.getResponseBody());
        logger.info("result code: " + result);
        logger.info("response body: " + responseBody);
        assertNotEquals(responseBody, "");
        assertEquals(result, HttpStatus.SC_OK, "\"Unexpected result: [" + result + "]");
        APIResponse actual = fromJson(responseBody, APIResponse.class);
        APIResponse expected = new APIResponse(
                null,
                "user with ID [" + userId + "] deleted",
                "OK"
        );
        assertEquals(actual, expected);
    }

    @Test
    public void testAuthenticate() throws Exception {
        String baseQuery = "user/app/%s/%s/authenticate?apikey=%s";
        String username = "test-user";
        String password = "abc";
        String query = String.format(baseQuery, APPNAME, username, APIKEY);

        User user = new User("Test", "User", username, password);
        final UUID expected = UUID.randomUUID();
        user.setUserToken(expected);
        when(userManager.getUser(username)).thenReturn(user);

        PostMethod postMethod = new PostMethod(base_uri + query);
        HttpClient client = new HttpClient();
        postMethod.addParameter("password", password);
        int result = client.executeMethod(postMethod);
        String responseBody = new String(postMethod.getResponseBody());
        logger.info("result code: " + result);
        logger.info("response body: " + responseBody);
        assertNotEquals(responseBody, "");
        assertEquals(result, HttpStatus.SC_OK, "\"Unexpected result: [" + result + "]");
        AtomicSignUpResponse actual = fromJson(responseBody, AtomicSignUpResponse.class);
        AtomicSignUpResponse expectedResponse = new AtomicSignUpResponse(
                null,
                "user [" + username + "] authenticated",
                new AtomicSignUp(user.getId(), username, true, "beancounter", username, expected)
        );
        assertEquals(actual.getObject().getIdentifier(), expectedResponse.getObject().getIdentifier());
        assertEquals(actual.getObject().getUserId(), expectedResponse.getObject().getUserId());
        assertEquals(actual.getObject().getService(), expectedResponse.getObject().getService());
        assertEquals(actual.getObject().getUsername(), expectedResponse.getObject().getUsername());
        assertEquals(actual.getObject().getUserToken(), expectedResponse.getObject().getUserToken());
        assertEquals(actual.getObject().isReturning(), expectedResponse.getObject().isReturning());
    }

    @Test
    public void testRemoveSource() throws Exception {
        String baseQuery = "user/app/%s/source/%s/%s?apikey=%s";
        String username = "test-user";
        String service = "fake-service-1";
        String query = String.format(baseQuery,
                APPNAME, username, service, APIKEY);

        User user = new User("Test", "User", username, "password");
        when(userManager.getUser(username)).thenReturn(user);

        DeleteMethod deleteMethod = new DeleteMethod(base_uri + query);
        HttpClient client = new HttpClient();
        int result = client.executeMethod(deleteMethod);
        String responseBody = new String(deleteMethod.getResponseBody());
        logger.info("result code: " + result);
        logger.info("response body: " + responseBody);

        assertNotEquals(responseBody, "");
        assertEquals(result, HttpStatus.SC_OK, "\"Unexpected result: [" + result + "]");
        APIResponse actual = fromJson(responseBody, APIResponse.class);
        APIResponse expected = new APIResponse(
                null,
                "service [" + service + "] removed from user [" + username + "]",
                "OK"
        );
        assertEquals(actual, expected);

        verify(userManager).deregisterService(service, user);
    }

    @Test
    public void testRemoveSourceWithNotExistingUser() throws Exception {
        String baseQuery = "user/app/%s/source/%s/%s?apikey=%s";
        String username = "missing-user";
        String service = "fake-service-1";
        String query = String.format(baseQuery, APPNAME, username, service, APIKEY);

        when(userManager.getUser(username)).thenReturn(null);

        DeleteMethod deleteMethod = new DeleteMethod(base_uri + query);
        HttpClient client = new HttpClient();
        int result = client.executeMethod(deleteMethod);
        String responseBody = new String(deleteMethod.getResponseBody());
        logger.info("result code: " + result);
        logger.info("response body: " + responseBody);
        assertNotEquals(responseBody, "");
        assertEquals(result, HttpStatus.SC_INTERNAL_SERVER_ERROR, "\"Unexpected result: [" + result + "]");
        APIResponse actual = fromJson(responseBody, APIResponse.class);
        APIResponse expected = new APIResponse(
                null,
                "User [" + username + "] not found!",
                "NOK"
        );
        assertEquals(actual, expected);
    }

    @Test
    public void getProfileWithValidUserTokenShouldBeSuccessful() throws Exception {
        String baseQuery = "user/%s/profile/me?token=%s";
        String username = "test-user";
        UUID userToken = UUID.randomUUID();
        User user = new User("Test", "User", username, "password");
        user.setUserToken(userToken);
        String query = String.format(
                baseQuery,
                username,
                userToken
        );

        when(userManager.getUser(username)).thenReturn(user);
        when(tokenManager.checkTokenExists(userToken)).thenReturn(true);
        when(profileManager.lookup(user.getId())).thenReturn(new UserProfile(username));

        GetMethod getMethod = new GetMethod(base_uri + query);
        HttpClient client = new HttpClient();

        int result = client.executeMethod(getMethod);
        String responseBody = new String(getMethod.getResponseBody());
        assertEquals(result, HttpStatus.SC_OK);
        assertFalse(responseBody.isEmpty());

        UserProfilePlatformResponse response = fromJson(responseBody, UserProfilePlatformResponse.class);
        assertEquals(response.getStatus(), UserProfilePlatformResponse.Status.OK);
        assertEquals(response.getMessage(), "profile for user [" + username + "] found");

        UserProfile userProfile = response.getObject();
        assertNotNull(userProfile);
        assertEquals(userProfile.getUsername(), username);
        assertEquals(userProfile.getVisibility(), UserProfile.Visibility.PUBLIC);
    }

    @Test
    public void getProfileWithValidApiKeyShouldBeSuccessful() throws Exception {
        String baseQuery = "user/app/%s/%s/profile?apikey=%s";
        String username = "test-user";
        User user = new User("Test", "User", username, "password");
        //user.setUserToken(apiKey);
        String query = String.format(baseQuery, APPNAME, username, APIKEY);

        when(userManager.getUser(username)).thenReturn(user);
        when(profileManager.lookup(user.getId())).thenReturn(new UserProfile(username));

        GetMethod getMethod = new GetMethod(base_uri + query);
        HttpClient client = new HttpClient();

        int result = client.executeMethod(getMethod);
        String responseBody = new String(getMethod.getResponseBody());
        assertEquals(result, HttpStatus.SC_OK);
        assertFalse(responseBody.isEmpty());

        UserProfilePlatformResponse response = fromJson(responseBody, UserProfilePlatformResponse.class);
        assertEquals(response.getStatus(), UserProfilePlatformResponse.Status.OK);
        assertEquals(response.getMessage(), "profile for user [" + username + "] found");

        UserProfile userProfile = response.getObject();
        assertNotNull(userProfile);
        assertEquals(userProfile.getUsername(), username);
        assertEquals(userProfile.getVisibility(), UserProfile.Visibility.PUBLIC);
    }
    
    @Test
    public void getProfileForMissingUserShouldRespondWithError() throws Exception {
        String baseQuery = "user/%s/profile/me?token=%s";
        String userId = "missing-user";
        String query = String.format(
                baseQuery,
                userId,
                UUID.randomUUID()
        );

        when(userManager.getUser(userId)).thenReturn(null);

        GetMethod getMethod = new GetMethod(base_uri + query);
        HttpClient client = new HttpClient();

        int result = client.executeMethod(getMethod);
        String responseBody = new String(getMethod.getResponseBody());
        assertEquals(result, HttpStatus.SC_INTERNAL_SERVER_ERROR);
        assertFalse(responseBody.isEmpty());

        APIResponse response = fromJson(responseBody, APIResponse.class);
        assertEquals(response.getStatus(), "NOK");
        assertEquals(response.getMessage(), "user with ID [" + userId + "] not found");
    }

    @Test
    public void getProfileWithMalformedUserTokenShouldRespondWithError() throws Exception {
        String baseQuery = "user/%s/profile/me?token=%s";
        String username = "test-user";
        User user = new User("Test", "User", username, "password");
        String userToken = "malformed-token-123";
        String query = String.format(
                baseQuery,
                username,
                userToken
        );

        when(userManager.getUser(username)).thenReturn(user);

        GetMethod getMethod = new GetMethod(base_uri + query);
        HttpClient client = new HttpClient();

        int result = client.executeMethod(getMethod);
        String responseBody = new String(getMethod.getResponseBody());
        assertEquals(result, HttpStatus.SC_INTERNAL_SERVER_ERROR);
        assertFalse(responseBody.isEmpty());

        APIResponse response = fromJson(responseBody, APIResponse.class);
        assertEquals(response.getStatus(), "NOK");
        assertEquals(response.getMessage(), "User token [" + userToken + "] is not valid");
    }

    @Test
    public void getProfileWithWrongUserTokenShouldRespondWithError() throws Exception {
        String baseQuery = "user/%s/profile/me?token=%s";
        String username = "test-user";
        UUID userToken = UUID.randomUUID();
        User user = new User("Test", "User", username, "password");
        user.setUserToken(UUID.randomUUID());
        String query = String.format(
                baseQuery,
                username,
                userToken
        );

        when(userManager.getUser(username)).thenReturn(user);

        GetMethod getMethod = new GetMethod(base_uri + query);
        HttpClient client = new HttpClient();

        int result = client.executeMethod(getMethod);
        String responseBody = new String(getMethod.getResponseBody());
        assertEquals(result, HttpStatus.SC_INTERNAL_SERVER_ERROR);
        assertFalse(responseBody.isEmpty());

        APIResponse response = fromJson(responseBody, APIResponse.class);
        assertEquals(response.getStatus(), "NOK");
        assertEquals(response.getMessage(), "User token [" + userToken + "] is not valid");
    }

    @Test
    public void getProfileWithMissingUserTokenShouldRespondWithError() throws Exception {
        String baseQuery = "user/%s/profile/me?token=%s";
        String username = "test-user";
        UUID userToken = null;
        User user = new User("Test", "User", username, "password");
        user.setUserToken(UUID.randomUUID());
        String query = String.format(
                baseQuery,
                username,
                userToken
        );

        when(userManager.getUser(username)).thenReturn(user);

        GetMethod getMethod = new GetMethod(base_uri + query);
        HttpClient client = new HttpClient();

        int result = client.executeMethod(getMethod);
        String responseBody = new String(getMethod.getResponseBody());
        assertEquals(result, HttpStatus.SC_INTERNAL_SERVER_ERROR);
        assertFalse(responseBody.isEmpty());

        APIResponse response = fromJson(responseBody, APIResponse.class);
        assertEquals(response.getStatus(), "NOK");
        assertEquals(response.getMessage(), "User token [" + userToken + "] is not valid");
    }

    @Test
    public void getProfileWithExpiredUserTokenShouldRespondWithError() throws Exception {
        String baseQuery = "user/%s/profile/me?token=%s";
        String username = "test-user";
        UUID userToken = UUID.randomUUID();
        User user = new User("Test", "User", username, "password");
        user.setUserToken(userToken);
        String query = String.format(
                baseQuery,
                username,
                userToken
        );

        when(userManager.getUser(username)).thenReturn(user);
        when(tokenManager.checkTokenExists(userToken)).thenReturn(false);

        GetMethod getMethod = new GetMethod(base_uri + query);
        HttpClient client = new HttpClient();

        int result = client.executeMethod(getMethod);
        String responseBody = new String(getMethod.getResponseBody());
        assertEquals(result, HttpStatus.SC_INTERNAL_SERVER_ERROR);
        assertFalse(responseBody.isEmpty());

        APIResponse response = fromJson(responseBody, APIResponse.class);
        assertEquals(response.getStatus(), "NOK");
        assertEquals(response.getMessage(), "User token [" + userToken + "] is not valid");
    }

    @Test
    public void givenTokenManagerErrorWhenGettingProfileThenRespondWithError() throws Exception {
        String baseQuery = "user/%s/profile/me?token=%s";
        String username = "test-user";
        UUID userToken = UUID.randomUUID();
        User user = new User("Test", "User", username, "password");
        user.setUserToken(userToken);
        String query = String.format(
                baseQuery,
                username,
                userToken
        );

        when(userManager.getUser(username)).thenReturn(user);
        when(tokenManager.checkTokenExists(userToken)).thenThrow(new UserManagerException("error"));

        GetMethod getMethod = new GetMethod(base_uri + query);
        HttpClient client = new HttpClient();

        int result = client.executeMethod(getMethod);
        String responseBody = new String(getMethod.getResponseBody());
        assertEquals(result, HttpStatus.SC_INTERNAL_SERVER_ERROR);
        assertFalse(responseBody.isEmpty());

        APIResponse response = fromJson(responseBody, APIResponse.class);
        assertEquals(response.getStatus(), "NOK");
        assertEquals(response.getMessage(), "User token [" + userToken + "] is not valid");
    }

    @Test
    public void getProfileWhenNoProfileExistsShouldRespondWithError() throws Exception {
        String baseQuery = "user/%s/profile/me?token=%s";
        String username = "test-user";
        UUID userToken = UUID.randomUUID();
        User user = new User("Test", "User", username, "password");
        user.setUserToken(userToken);
        String query = String.format(
                baseQuery,
                username,
                userToken
        );

        when(userManager.getUser(username)).thenReturn(user);
        when(tokenManager.checkTokenExists(userToken)).thenReturn(true);
        when(profileManager.lookup(user.getId())).thenReturn(null);

        GetMethod getMethod = new GetMethod(base_uri + query);
        HttpClient client = new HttpClient();

        int result = client.executeMethod(getMethod);
        String responseBody = new String(getMethod.getResponseBody());
        assertEquals(result, HttpStatus.SC_INTERNAL_SERVER_ERROR);
        assertFalse(responseBody.isEmpty());

        APIResponse response = fromJson(responseBody, APIResponse.class);
        assertEquals(response.getStatus(), "NOK");
        assertEquals(response.getMessage(), "Profile for user [" + username + "] not found");
    }

    @Test
    public void givenProfilesErrorWhenGettingProfileThenRespondWithError() throws Exception {
        String baseQuery = "user/%s/profile/me?token=%s";
        String username = "test-user";
        UUID userToken = UUID.randomUUID();
        User user = new User("Test", "User", username, "password");
        user.setUserToken(userToken);
        String query = String.format(
                baseQuery,
                username,
                userToken
        );

        when(userManager.getUser(username)).thenReturn(user);
        when(tokenManager.checkTokenExists(userToken)).thenReturn(true);
        when(profileManager.lookup(user.getId())).thenThrow(new ProfileManagerException("error", new Exception()));

        GetMethod getMethod = new GetMethod(base_uri + query);
        HttpClient client = new HttpClient();

        int result = client.executeMethod(getMethod);
        String responseBody = new String(getMethod.getResponseBody());
        assertEquals(result, HttpStatus.SC_INTERNAL_SERVER_ERROR);
        assertFalse(responseBody.isEmpty());

        APIResponse response = fromJson(responseBody, APIResponse.class);
        assertEquals(response.getStatus(), "NOK");
        assertEquals(response.getMessage(), "Error while retrieving profile for user [" + username + "]");
    }

    @Test
    public void getShortUserProfile() throws Exception {
        UUID userId = idGenerator.randomUUID(User.class);
        UUID userToken = idGenerator.randomUUID(Auth.class);
        ShortUserProfile shortProfile = new ShortUserProfile(userId);
        shortProfile.setGender("female");
        shortProfile.setLocation("Trento, Italy");
        shortProfile.setAge(100);
        shortProfile.setCustomer("xpeppers");
        shortProfile.setVisibility(UserProfile.Visibility.PROTECTED);
        shortProfile.setUpdated(clock.utcnow());
        shortProfile.addTopics(Arrays.asList(
            new Category(new URI("/cat1"), "Cat1", 0.5, 1),
            new Interest(new URI("/int1"), "In1", 0.7, true)
        ));

        when(tokenManager.getUserId(userToken)).thenReturn(userId);
        when(profileManager.lookupShortProfile(userId)).thenReturn(shortProfile);

        String path = String.format("user/me?token=%s", userToken);
        GetMethod getMethod = new GetMethod(base_uri + path);
        HttpClient client = new HttpClient();

        int result = client.executeMethod(getMethod);
        String responseBody = new String(getMethod.getResponseBody());
        assertEquals(result, HttpStatus.SC_OK);
        assertFalse(responseBody.isEmpty());

        ShortProfileResponse response = fromJson(
                responseBody, ShortProfileResponse.class);
        assertEquals(response.getStatus(), PlatformResponse.Status.OK);

        ShortUserProfile respProfile = response.getObject();
        assertNotNull(respProfile);
        assertEquals(respProfile.getUserId(), shortProfile.getUserId());
        assertEquals(respProfile.getGender(), shortProfile.getGender());
        assertEquals(respProfile.getLocation(), shortProfile.getLocation());
        assertEquals(respProfile.getAge(), shortProfile.getAge());
        assertEquals(respProfile.getCustomer(), shortProfile.getCustomer());
        assertEquals(respProfile.getVisibility(), shortProfile.getVisibility());
        assertEquals(respProfile.getUpdated(), shortProfile.getUpdated());

        for (Topic t: respProfile.getTopics()) {
            assertTrue(respProfile.getTopics().contains(t), t.toString());
        }
        assertEquals(respProfile.getTopics().size(), 2);
    }

    @Test
    public void getShortUserProfileCors() throws Exception {
        UUID userId = idGenerator.randomUUID(User.class);
        UUID userToken = idGenerator.randomUUID(Auth.class);
        ShortUserProfile shortProfile = new ShortUserProfile();

        when(tokenManager.getUserId(userToken)).thenReturn(userId);
        when(profileManager.lookupShortProfile(userId)).thenReturn(shortProfile);

        String path = String.format("user/me?token=%s", userToken);
        HttpResponse resp = makeCorsRequest(HttpGet.class, path);
        verifyHeaderValue(resp, "Access-Control-Allow-Origin", "*");

        resp = makeCorsPreflightRequest(path);
        verifyHeaderValue(resp, "Access-Control-Allow-Origin", "*");
    }

    @Test
    public void getShortUserProfileServerError() throws Exception {
        UUID userId = idGenerator.randomUUID(User.class);
        UUID userToken = idGenerator.randomUUID(Auth.class);
        String path = String.format("user/me?token=%s", userToken);

        when(tokenManager.getUserId(any(UUID.class))).thenReturn(userId);
        when(profileManager.lookupShortProfile(any(UUID.class))).
                thenThrow(new ProfileManagerException("Not found"));

        GetMethod getMethod = new GetMethod(base_uri + path);
        HttpClient client = new HttpClient();
        int result = client.executeMethod(getMethod);
        assertEquals(result, HttpStatus.SC_INTERNAL_SERVER_ERROR);
    }

    @Test
    public void getShortUserProfileNotFound() throws Exception {
        UUID userId = idGenerator.randomUUID(User.class);
        UUID userToken = idGenerator.randomUUID(Auth.class);
        String path = String.format("user/me?token=%s", userToken);

        when(tokenManager.getUserId(any(UUID.class))).thenReturn(userId);
        when(profileManager.lookupShortProfile(any(UUID.class))).thenReturn(null);

        GetMethod getMethod = new GetMethod(base_uri + path);
        HttpClient client = new HttpClient();
        int result = client.executeMethod(getMethod);
        assertEquals(result, HttpStatus.SC_NOT_FOUND);
    }

    @Test
    public void getShortUserProfileWithInvalidToken() throws Exception {
        GetMethod getMethod = new GetMethod(base_uri + "user/me?token=invalid");
        HttpClient client = new HttpClient();

        int result = client.executeMethod(getMethod);
        assertEquals(result, 400);
    }

    @Test
    public void getShortUserProfileWithNonexitentToken() throws Exception {
        UUID userToken = idGenerator.randomUUID(Auth.class);
        String path = String.format("user/me?token=%s", userToken);

        when(tokenManager.getUserId(any(UUID.class))).thenReturn(null);

        GetMethod getMethod = new GetMethod(base_uri + path);
        HttpClient client = new HttpClient();
        int result = client.executeMethod(getMethod);
        assertEquals(result, HttpStatus.SC_BAD_REQUEST);
    }

    @Test
    public void getShortUserProfileTokenServerError() throws Exception {
        UUID userToken = idGenerator.randomUUID(Auth.class);
        String path = String.format("user/me?token=%s", userToken);

        when(tokenManager.getUserId(any(UUID.class))).
                thenThrow(new UserManagerException("An error"));

        GetMethod getMethod = new GetMethod(base_uri + path);
        HttpClient client = new HttpClient();
        int result = client.executeMethod(getMethod);
        assertEquals(result, HttpStatus.SC_INTERNAL_SERVER_ERROR);
    }

    @Test
    public void signUpViaOAuth2FromWeb() throws Exception {
        String service = "chumhum";
        String customer = "xpeppers";
        URI continueTo = new URI("http://chumhum.com/success");
        URL authorizePage = new URL("http://chumhum.com/authorize?some=params");

        String stateId = "some-uuid-12345";

        when(stateManager.put(any(RequestState.class))).thenReturn(stateId);
        when(userManager.getOAuthTokenWithState(
            anyString(), anyString(), anyString())).
            thenReturn(new OAuthToken(authorizePage, stateId));

        String path = String.format(
            "user/oauth2/%s/%s?continue=%s",
            service,
            customer,
            continueTo.toString());
        GetMethod req = new GetMethod(base_uri + path);
        req.setFollowRedirects(false);

        HttpClient client = new HttpClient();
        int respCode = client.executeMethod(req);

        verify(stateManager).put(new RequestState(
            customer, null, continueTo, RequestState.Flow.WEB));
        verify(userManager).getOAuthTokenWithState(stateId, service, customer);

        assertEquals(respCode, HttpStatus.SC_TEMPORARY_REDIRECT);
        assertEquals(req.getResponseHeader("Location").getValue(),
                     authorizePage.toString());

        verifyCookie(
                UserService.AUTH_COOKIE_NAME,
                stateId,
                UserService.AUTH_COOKIE_MAX_AGE,
                client.getState().getCookies());
    }

    @Test
    public void signUpViaOAuth2FromMobile() throws Exception {
        String service = "chumhum";
        String customer = "xpeppers";
        URL authorizePage = new URL("http://chumhum.com/authorize?some=params");

        String stateId = "some-uuid-12345";

        when(stateManager.put(any(RequestState.class))).thenReturn(stateId);
        when(userManager.getOAuthTokenWithState(
            anyString(), anyString(), anyString())).
            thenReturn(new OAuthToken(authorizePage, stateId));

        String path = String.format("user/oauth2/%s/%s", service, customer);
        GetMethod req = new GetMethod(base_uri + path);
        req.setFollowRedirects(false);

        HttpClient client = new HttpClient();
        int respCode = client.executeMethod(req);

        verify(stateManager).put(new RequestState(
            customer, null, null, RequestState.Flow.MOBILE));
        verify(userManager).getOAuthTokenWithState(stateId, service, customer);

        assertEquals(respCode, HttpStatus.SC_TEMPORARY_REDIRECT);
        assertEquals(req.getResponseHeader("Location").getValue(),
            authorizePage.toString());

        verifyCookie(
                UserService.AUTH_COOKIE_NAME,
                stateId,
                UserService.AUTH_COOKIE_MAX_AGE,
                client.getState().getCookies());
    }

    @Test
    public void handleOAuth2CallbackFromWeb() throws Exception {
        DateTime now = new DateTime();
        UUID aid = UUID.randomUUID();

        String service = "facebook";
        String customer = "xpeppers";
        URI continueTo = new URI("http://chumhum.com/success?k=v");
        String stateId = "state-12345";
        String code = "cc12345";

        User user = new User("John", "Doe", "dude", "secret");
        user.addService("facebook", new OAuthAuth("token-123", "secret-321"));

        RequestState authState = new RequestState(
            customer,
            null,
            continueTo,
            RequestState.Flow.WEB);

        AtomicSignUp signUp = new AtomicSignUp(
            user.getId(),
            user.getId().toString(),
            false,
            service,
            "fb-123",
            UUID.randomUUID());

        when(clock.now(any(DateTimeZone.class))).thenReturn(now);
        when(idGenerator.randomUUID(Activity.class.getName())).thenReturn(aid);
        when(stateManager.pop(anyString())).thenReturn(authState);
        when(userManager.storeUserFromOAuthWithState(
                anyString(), anyString(), anyString(), any(RequestState.class))).
                thenReturn(signUp);
        when(userManager.getUser(user.getId().toString())).thenReturn(user);

        String path = String.format(
            "user/oauth2/callback/%s/%s?state=%s&code=%s",
            service,
            customer,
            stateId,
            code);

        String domain = base_uri.getHost() + ':' + base_uri.getPort();
        Cookie xsrf = new Cookie(domain, UserService.AUTH_COOKIE_NAME, stateId);
        xsrf.setPath("/");
        HttpState httpState = new HttpState();
        httpState.addCookie(xsrf);

        HttpClient client = new HttpClient();
        client.setState(httpState);

        GetMethod req = new GetMethod(base_uri + path);
        req.setFollowRedirects(false);
        int respCode = client.executeMethod(req);

        String location = AUTH_SUCCESS_URL +
            "?username=" + user.getId().toString() +
            "&token=" + signUp.getUserToken();

        verify(stateManager).pop(stateId);
        verify(userManager).storeUserFromOAuthWithState(
            service,
            null,
            code,
            authState);

        assertEquals(respCode, HttpStatus.SC_TEMPORARY_REDIRECT);
        assertEquals(req.getResponseHeader("Location").getValue(), location);

        Cookie[] cookies = client.getState().getCookies();

        verifyCookie(
                UserService.AUTH_COOKIE_NAME,
                null,
                UserService.AUTH_COOKIE_MAX_AGE,
                cookies);

        verifyCookie(
                TOKEN_COOKIE_NAME,
                signUp.getUserToken().toString(),
                TOKEN_COOKIE_MAXAGE,
                cookies);

        verifyLoginActivity(now, aid, user, Verb.SIGNUP_WEB, service);
    }

    @Test
    public void handleOAuth2CallbackWebError() throws Exception {
        String path = "user/oauth2/callback/facebook/" + APPNAME + "?state=123&error=Cancelled";
        GetMethod req = new GetMethod(base_uri + path);
        req.setFollowRedirects(false);

        HttpClient client = new HttpClient();
        int respCode = client.executeMethod(req);

        assertEquals(respCode, HttpStatus.SC_TEMPORARY_REDIRECT);
        assertEquals(req.getResponseHeader("Location").getValue(), AUTH_FAILURE_URL);
    }

    @Test
    public void handleOAuth2CallbackFromMobile() throws Exception {
        DateTime now = new DateTime();
        UUID aid = UUID.randomUUID();

        String service = "twitter";
        String customer = "xpeppers";
        String stateId = "state-12345";
        String token = "tok12345";
        String verifier = "ver12345";
        String twitterUserId = "twitterer-12345";

        User user = new User("John", "Doe", "dude", "secret");
        user.addService("twitter", new OAuthAuth("token-123", "secret-321"));

        RequestState authState = new RequestState(
            customer,
            null,
            null,
            RequestState.Flow.MOBILE);

        AtomicSignUp signUp = new AtomicSignUp(
            user.getId(),
            user.getUsername(),
            false,
            service,
            twitterUserId,
            UUID.randomUUID());

        when(clock.now(any(DateTimeZone.class))).thenReturn(now);
        when(idGenerator.randomUUID(Activity.class.getName())).thenReturn(aid);
        when(stateManager.pop(anyString())).thenReturn(authState);
        when(userManager.storeUserFromOAuthWithState(
            anyString(), anyString(), anyString(), any(RequestState.class))).
            thenReturn(signUp);
        when(userManager.getUser(user.getId().toString())).thenReturn(user);

        String path = String.format("user/oauth2/callback" +
            "/%s/%s?state=%s&oauth_token=%s&oauth_verifier=%s",
            service,
            customer,
            stateId,
            token,
            verifier);

        String domain = base_uri.getHost() + ':' + base_uri.getPort();
        Cookie xsrf = new Cookie(domain, UserService.AUTH_COOKIE_NAME, stateId);
        xsrf.setPath("/");
        HttpState httpState = new HttpState();
        httpState.addCookie(xsrf);

        HttpClient client = new HttpClient();
        client.setState(httpState);

        GetMethod req = new GetMethod(base_uri + path);
        req.setFollowRedirects(false);
        int respCode = client.executeMethod(req);

        verify(stateManager).pop(stateId);
        verify(userManager).storeUserFromOAuthWithState(
            service,
            token,
            verifier,
            authState);

        assertEquals(respCode, HttpStatus.SC_OK);

        AtomicSignUpResponse resp = fromJson(
            req.getResponseBodyAsString(),
            AtomicSignUpResponse.class);
        assertEquals(resp.getStatus(), AtomicSignUpResponse.Status.OK);
        assertEquals(resp.getObject().getService(), service);
        assertEquals(resp.getObject().getIdentifier(), twitterUserId);
        assertEquals(resp.getObject().getUserId(), user.getId());
        assertEquals(resp.getObject().getUsername(), user.getUsername());
        assertEquals(resp.getObject().getUserToken(), signUp.getUserToken());

        verifyLoginActivity(now, aid, user, Verb.SIGNUP_MOBILE, service);
        verifyCookie(
                UserService.AUTH_COOKIE_NAME,
                null,
                UserService.AUTH_COOKIE_MAX_AGE,
                client.getState().getCookies());
    }

    /**
     * Verifies either presence or absence of XSRF auth cookie.
     *
     * @param name Cookie name.
     * @param value If non-null asserts equality, otherwise asserts that the
     *              cookie is not actually present: http client will remove
     *              any cookies with a passed expiration date.
     * @param maxage Expected cookie life time.
     * @param cookies All cookies from a HTTP client response.
     */
    private void verifyCookie(String name, String value, long maxage, Cookie[] cookies) {
        Cookie cookie = null;
        for (Cookie c: cookies) {
            if (c.getName().equals(name)) {
                cookie = c;
                break;
            }
        }

        if (value != null && cookie == null)
            fail(name + " cookie not found in " + Arrays.asList(cookies));

        if (value == null && cookie != null)
            fail("Didn't expect to find " + name + " cookie in: " + cookie.toString());

        else if (cookie == null)
            return;

        assertEquals(cookie.getValue(), value);
        assertEquals(cookie.getPath(), "/");
        assertEquals(cookie.getDomain(), base_uri.getHost());

        assertNotNull(cookie.getExpiryDate());
        long expiryDiff = System.currentTimeMillis()
                + maxage * 1000
                - cookie.getExpiryDate().getTime();
        assertTrue(expiryDiff > 0 && expiryDiff < 10 * 1000,
                "diff too large: " + expiryDiff);
    }

    /**
     * Verifies that a login activity has been pushed up the queue.
     * Makes sense only during OAuth callback handler testing.
     */
    private void verifyLoginActivity(
            DateTime now,
            UUID activityId,
            User user,
            Verb verb,
            String service
    ) throws Exception {
        Context ctx = new Context(now);
        ctx.setService(service);
        ctx.setUsername(user.getId().toString());
        Object obj = new Object();

        Activity a = new Activity(verb, obj, ctx);
        a.setId(activityId);

        ResolvedActivity ra = new ResolvedActivity(user.getId(), a, user);
        String payload = mapper.writeValueAsString(ra);
        verify(queues).push(payload);
}
    
    public static class UserServiceTestConfig extends GuiceServletContextListener {

        private String serviceHost;

        @Override
        public void contextInitialized(ServletContextEvent servletContextEvent) {
            serviceHost = servletContextEvent.getServletContext().getInitParameter("service.host");
            super.contextInitialized(servletContextEvent);
        }

        @Override
        protected Injector getInjector() {
            return Guice.createInjector(new JerseyServletModule() {
                @Override
                protected void configureServlets() {
                    clock = spy(new Clock());
                    idGenerator = spy(new IdGenerator());
                    appManager = mock(ApplicationsManager.class);
                    userManager = mock(UserManager.class);
                    tokenManager = mock(UserTokenManager.class);
                    stateManager = mock(RequestStateManager.class);
                    queues = mock(Queues.class);
                    profileManager = mock(ProfileManager.class);
                    grabberManager = mock(ActivityGrabberManager.class);
                    activities = mock(ActivityStore.class);

                    Map<String, String> props = new HashMap<String, String>();
                    props.put("oauth.fail.redirect", "http://api.beancounter.io/");
                    // Has to be the same as where Grizzly web server
                    // is bound to:
                    props.put("service.host", serviceHost);
                    // Token cookie
                    props.put("cookie.token.name", TOKEN_COOKIE_NAME);
                    props.put("cookie.token.maxage", Long.toString(TOKEN_COOKIE_MAXAGE));
                    // CORS support
                    props.put("cors.origin", "*");
                    props.put("cors.expose.headers", "");
                    props.put("cors.allow.methods", "GET");
                    props.put("cors.allow.headers", "");
                    props.put("cors.preflight.maxage", "3600");
                    Names.bindProperties(binder(), props);

                    bind(Clock.class).toInstance(clock);
                    bind(IdGenerator.class).toInstance(idGenerator);
                    bind(ApplicationsManager.class).toInstance(appManager);
                    bind(UserTokenManager.class).toInstance(tokenManager);
                    bind(RequestStateManager.class).toInstance(stateManager);
                    bind(UserManager.class).toInstance(userManager);
                    bind(ProfileManager.class).toInstance(profileManager);
                    bind(Queues.class).toInstance(queues);
                    bind(ActivityGrabberManager.class).toInstance(grabberManager);
                    bind(ActivityStore.class).toInstance(activities);


                    // add REST services
                    bind(ApplicationService.class);
                    bind(UserService.class);

                    // add bindings for Jackson
                    bind(JacksonJaxbJsonProvider.class).asEagerSingleton();
                    bind(JacksonMixInProvider.class).asEagerSingleton();
                    bind(MessageBodyReader.class).to(JacksonJsonProvider.class);
                    bind(MessageBodyWriter.class).to(JacksonJsonProvider.class);

                    // Route all requests through GuiceContainer
                    filter("/*").through(CorsServletFilter.class);
                    filter("/*").through(GuiceContainer.class);
                    serve("/*").with(GuiceContainer.class);
                }
            });
        }
    }
}