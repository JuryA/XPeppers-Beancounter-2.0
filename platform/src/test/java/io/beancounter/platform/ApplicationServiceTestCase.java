package io.beancounter.platform;

import io.beancounter.commons.model.Application;
import io.beancounter.platform.ApiBaseTestCase;

import io.beancounter.platform.ApplicationService;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.*;

import java.io.IOException;
import java.util.UUID;

import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 * Reference test class for {@link io.beancounter.platform.ApplicationService}.
 *
 * @author Enrico Candino ( enrico.candino@gmail.com )
 * @author Davide Palmisano ( dpalmisano@gmail.com )
 */
public class ApplicationServiceTestCase extends ApiBaseTestCase {

    @Test
    public void testRegisterAndDeregister() throws IOException {
        final String name = "fake_application_name";
        final String description = "This is a test registration!";
        final String email = "fake_mail@test.com";

        String baseQuery = "application/register";
        PostMethod postMethod = new PostMethod(base_uri + baseQuery);
        postMethod.addParameter("name", name);
        postMethod.addParameter("description", description);
        postMethod.addParameter("email", email);

        HttpClient client = new HttpClient();
        int result = client.executeMethod(postMethod);
        String responseBody = new String(postMethod.getResponseBody());

        assertEquals(result, HttpStatus.SC_OK);

        Application app = fromJson(responseBody, Application.class);
        assertNotNull(app);
        assertEquals(app.getName(), name);
        assertEquals(app.getDescription(), description);
        assertEquals(app.getEmail(), email);
        assertNotNull(app.getApiKey());

        baseQuery = "application/" + name + "?apikey=" + app.getApiKey().toString();
        DeleteMethod deleteMethod = new DeleteMethod(base_uri + baseQuery);
        result = client.executeMethod(deleteMethod);
        assertEquals(result, HttpStatus.SC_OK);
    }

    @Test
    public void testDeregisterNotExistingApplication() throws IOException {
        final UUID applicationKey = UUID.randomUUID();
        String baseQuery = "application/xxxx?apikey=" + applicationKey;

        HttpClient client = new HttpClient();
        DeleteMethod deleteMethod = new DeleteMethod(base_uri + baseQuery);
        int result = client.executeMethod(deleteMethod);
        assertEquals(result, HttpStatus.SC_INTERNAL_SERVER_ERROR);
    }

    @Test
    public void testGetApplication() throws Exception {
        Application app = registerTestApplication();

        String url = base_uri + "application/" + app.getName() + "?apikey=" + app.getApiKey();
        GetMethod req = new GetMethod(url);

        int status = new HttpClient().executeMethod(req);
        assertEquals(status, 200);
        Application actualApp = fromJson(req.getResponseBodyAsString(), Application.class);
        assertEquals(actualApp, app);
    }

    @Test
    public void testUpdateApplication() throws Exception {
        Application app = registerTestApplication();
        String fbAppId = "fb-app-123456";

        String payload = "{\"facebookAppId\": \"" + fbAppId + "\"}";
        String url = base_uri + "application/" + app.getName() + "?apikey=" + app.getApiKey();
        PutMethod req = new PutMethod(url);
        req.setRequestEntity(new StringRequestEntity(payload, "application/json", "UTF-8"));

        int status = new HttpClient().executeMethod(req);
        assertEquals(status, 200);

        Application actualApp = fromJson(req.getResponseBodyAsString(), Application.class);
        assertEquals(actualApp.getFacebookAppId(), fbAppId);
        assertEquals(actualApp.getName(), app.getName());
        assertEquals(actualApp.getApiKey(), app.getApiKey());
    }

    @Test
    public void testParseApiKey() {
        UUID key = UUID.randomUUID();
        ApplicationService appService = new ApplicationService(null);

        UUID result = appService.parseApiKey(key.toString());
        assertEquals(result, key);
        assertTrue(result.equals(key));
    }
}