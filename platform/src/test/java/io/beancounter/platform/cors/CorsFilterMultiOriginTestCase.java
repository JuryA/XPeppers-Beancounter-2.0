package io.beancounter.platform.cors;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.name.Names;
import com.google.inject.servlet.GuiceServletContextListener;
import com.sun.jersey.api.core.ResourceConfig;
import com.sun.jersey.guice.JerseyServletModule;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;

import com.google.inject.servlet.GuiceFilter;
import com.sun.grizzly.http.servlet.ServletAdapter;

import io.beancounter.platform.ApiBaseTestCase;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.*;
import org.apache.http.util.EntityUtils;
import org.testng.annotations.Test;

import javax.ws.rs.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import static org.testng.Assert.*;


/**
 * Tests for {@link CorsServletFilter} with preflight enabled.
 */
public class CorsFilterMultiOriginTestCase extends ApiBaseTestCase {

    @Override
    protected void setupFrontendService() {
        ServletAdapter adapter = new ServletAdapter();
        adapter.addServletListener(CorsWithPreflight.class.getName());
        adapter.setServletPath("/");
        adapter.addFilter(new GuiceFilter(), "filter", null);
        server.addGrizzlyAdapter(adapter, null);
    }


    @Path("/rest")
    public static class DummyService {
        @HEAD @Path("/cors/head")
        public String head() { return "cors/head"; }

        @GET @Path("/cors/get")
        public String get() { return "cors/get"; }

        @POST @Path("/cors/post")
        public String post() { return "cors/post"; }

        @PUT @Path("/cors/put")
        public String put() { return "cors/put"; }

        @DELETE @Path("/cors/delete")
        public String delete() { return "cors/delete"; }

        @PUT @Path("/put-no-cors")
        public String putNoCors() { return "put-no-cors"; }
    }

    @Test
    public void getWithPreflight() throws Exception {
        Map<String, Class<? extends HttpRequestBase>> tests =
                new HashMap<String, Class<? extends HttpRequestBase>>();
        tests.put("cors/head", HttpHead.class);
        tests.put("cors/get", HttpGet.class);
        tests.put("cors/post", HttpPost.class);
        tests.put("cors/put", HttpPut.class);
        tests.put("cors/delete", HttpDelete.class);

        for (Map.Entry<String, Class<? extends HttpRequestBase>> entry: tests.entrySet()) {
            final String path = entry.getKey();

            HttpResponse resp = makeCorsRequest(entry.getValue(), CorsWithPreflight.ORIGIN1, path);
            assertEquals(resp.getStatusLine().getStatusCode(), HttpStatus.SC_OK);
            if (entry.getValue() != HttpHead.class)
                assertEquals(EntityUtils.toString(resp.getEntity()), path);

            verifyHeaderValue(resp, CorsServletFilter.ALLOW_ORIGIN, CorsWithPreflight.ORIGIN1);
            verifyHeaderValue(resp, CorsServletFilter.EXPOSE_HEADERS, CorsWithPreflight.EXPOSE_HEADERS);
            verifyNullHeader(resp,
                    CorsServletFilter.ALLOW_METHODS,
                    CorsServletFilter.ALLOW_HEADERS,
                    CorsServletFilter.ALLOW_CREDENTIALS,
                    CorsServletFilter.MAX_AGE);

            resp = makeCorsPreflightRequest(CorsWithPreflight.ORIGIN1, path);
            assertEquals(resp.getStatusLine().getStatusCode(), HttpStatus.SC_NO_CONTENT);

            verifyHeaderValue(resp, CorsServletFilter.ALLOW_ORIGIN, CorsWithPreflight.ORIGIN1);
            verifyHeaderValue(resp, CorsServletFilter.EXPOSE_HEADERS, CorsWithPreflight.EXPOSE_HEADERS);
            verifyHeaderValue(resp, CorsServletFilter.ALLOW_HEADERS, CorsWithPreflight.ALLOW_HEADERS);
            verifyHeaderValue(resp, CorsServletFilter.ALLOW_METHODS, CorsWithPreflight.ALLOW_METHODS);
            verifyHeaderValue(resp, CorsServletFilter.MAX_AGE, CorsWithPreflight.MAX_AGE);
            verifyNullHeader(resp, CorsServletFilter.ALLOW_CREDENTIALS);
        }
    }

    @Test
    public void correctOrigin() throws Exception {
        HttpResponse resp = makeCorsRequest(HttpGet.class, CorsWithPreflight.ORIGIN2, "cors/get");
        verifyHeaderValue(resp, CorsServletFilter.ALLOW_ORIGIN, CorsWithPreflight.ORIGIN2);

        resp = makeCorsPreflightRequest(CorsWithPreflight.ORIGIN2, "cors/get");
        verifyHeaderValue(resp, CorsServletFilter.ALLOW_ORIGIN, CorsWithPreflight.ORIGIN2);
    }

    @Test
    public void invalidOrigin() throws Exception {
        HttpResponse resp = makeCorsRequest(HttpGet.class, "http://invalid.test", "cors/get");
        verifyNoCors(resp);

        resp = makeCorsPreflightRequest("http://invalid.test", "cors/get");
        verifyNoCors(resp);
    }

    // TODO(alex): enable when I figure out why filter works only for catch-all
    // URL pattern in this specific test case.
    @Test(enabled = false)
    public void noCors() throws Exception {
        final String path = "put-no-cors";
        HttpResponse resp = makeCorsRequest(HttpPut.class, path);

        assertEquals(resp.getStatusLine().getStatusCode(), HttpStatus.SC_OK);
        assertEquals(EntityUtils.toString(resp.getEntity()), path);
        verifyNoCors(resp);

        resp = makeCorsPreflightRequest(path);
        assertEquals(resp.getStatusLine().getStatusCode(), HttpStatus.SC_NO_CONTENT);
        verifyNoCors(resp);
    }


    public static class CorsWithPreflight extends GuiceServletContextListener {

        static final String ORIGIN1 = "http://origin1.cors";
        static final String ORIGIN2 = "http://origin2.cors";
        static final String EXPOSE_HEADERS = "X-Trace";
        static final String ALLOW_HEADERS = "Authorization,X-Requested-With";
        static final String ALLOW_METHODS = "GET";
        static final String MAX_AGE = "86400";

        @Override
        protected Injector getInjector() {
            return Guice.createInjector(new JerseyServletModule() {
                @Override
                protected void configureServlets() {
                    Map<String, String> props = new HashMap<String, String>();
                    props.put(ResourceConfig.FEATURE_DISABLE_WADL, "true");

                    Properties properties = new Properties();
                    properties.put("cors.origin", ORIGIN2 + ", " + ORIGIN1);
                    properties.put("cors.expose.headers", EXPOSE_HEADERS);
                    properties.put("cors.allow.methods", ALLOW_METHODS);
                    properties.put("cors.allow.headers", ALLOW_HEADERS);
                    properties.put("cors.preflight.maxage", MAX_AGE);
                    Names.bindProperties(binder(), properties);

                    bind(CorsFilterMultiOriginTestCase.DummyService.class);

                    filter("/*").through(CorsServletFilter.class);
                    filter("/*").through(GuiceContainer.class);
                    serve("/*").with(GuiceContainer.class, props);
                }
            });
        }
    }

}
