package io.beancounter.platform.cors;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.name.Names;
import com.google.inject.servlet.GuiceFilter;
import com.google.inject.servlet.GuiceServletContextListener;
import com.sun.grizzly.http.servlet.ServletAdapter;
import com.sun.jersey.api.core.ResourceConfig;
import com.sun.jersey.guice.JerseyServletModule;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;
import io.beancounter.platform.ApiBaseTestCase;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.testng.annotations.Test;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Tests for {@link CorsServletFilter} with star origin.
 */
public class CorsFilterStarOriginTestCase extends ApiBaseTestCase {

    @Override
    protected void setupFrontendService() {
        ServletAdapter adapter = new ServletAdapter();
        adapter.addServletListener(CorsWithStarOrigin.class.getName());
        adapter.setServletPath("/");
        adapter.addFilter(new GuiceFilter(), "filter", null);
        server.addGrizzlyAdapter(adapter, null);
    }

    @Path("/rest")
    public static class DummyService {
        @GET
        @Path("/cors/get")
        public String get() { return "cors/get"; }
    }

    @Test
    public void getWithAnyOrigin() throws Exception {
        final String path = "cors/get";
        HttpResponse resp = makeCorsRequest(HttpGet.class, path);

        verifyHeaderValue(resp, CorsServletFilter.ALLOW_ORIGIN, "*");
        verifyNullHeader(resp,
                CorsServletFilter.EXPOSE_HEADERS,
                CorsServletFilter.ALLOW_METHODS,
                CorsServletFilter.ALLOW_HEADERS,
                CorsServletFilter.ALLOW_CREDENTIALS,
                CorsServletFilter.MAX_AGE);
    }

    @Test
    public void preflight() throws Exception {
        final String path = "cors/get";
        HttpResponse resp = makeCorsPreflightRequest(path);

        verifyHeaderValue(resp, CorsServletFilter.ALLOW_ORIGIN, "*");
        verifyHeaderValue(resp, CorsServletFilter.ALLOW_METHODS, CorsWithStarOrigin.ALLOW_METHODS);
        verifyNullHeader(resp,
                CorsServletFilter.EXPOSE_HEADERS,
                CorsServletFilter.ALLOW_HEADERS,
                CorsServletFilter.ALLOW_CREDENTIALS,
                CorsServletFilter.MAX_AGE);
    }


    public static class CorsWithStarOrigin extends GuiceServletContextListener {

        static final String EXPOSE_HEADERS = "";
        static final String ALLOW_HEADERS = "";
        static final String ALLOW_METHODS = "GET,POST,PUT";
        static final String MAX_AGE = "0";

        @Override
        protected Injector getInjector() {
            return Guice.createInjector(new JerseyServletModule() {
                @Override
                protected void configureServlets() {
                    Map<String, String> props = new HashMap<String, String>();
                    props.put(ResourceConfig.FEATURE_DISABLE_WADL, "true");

                    Properties properties = new Properties();
                    properties.put("cors.origin", "*");
                    properties.put("cors.expose.headers", EXPOSE_HEADERS);
                    properties.put("cors.allow.methods", ALLOW_METHODS);
                    properties.put("cors.allow.headers", ALLOW_HEADERS);
                    properties.put("cors.preflight.maxage", MAX_AGE);
                    Names.bindProperties(binder(), properties);

                    bind(CorsFilterStarOriginTestCase.DummyService.class);

                    filter("/*").through(CorsServletFilter.class);
                    filter("/*").through(GuiceContainer.class);
                    serve("/*").with(GuiceContainer.class, props);
                }
            });
        }
    }
}
