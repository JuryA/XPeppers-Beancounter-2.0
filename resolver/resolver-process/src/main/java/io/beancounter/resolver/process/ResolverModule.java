package io.beancounter.resolver.process;

import io.beancounter.applications.ApplicationsManager;
import io.beancounter.applications.JedisApplicationsManagerImpl;
import io.beancounter.auth.*;
import io.beancounter.commons.helper.PropertiesHelper;
import io.beancounter.commons.helper.jedis.DefaultJedisPoolFactory;
import io.beancounter.commons.helper.jedis.JedisPoolFactory;
import io.beancounter.commons.model.AuthServiceConfig;
import io.beancounter.resolver.JedisResolver;
import io.beancounter.resolver.Resolver;
import io.beancounter.usermanager.JedisUserManager;
import io.beancounter.usermanager.JedisUserTokenManager;
import io.beancounter.usermanager.UserManager;
import io.beancounter.usermanager.UserTokenManager;
import io.beancounter.auth.DefaultAuthServiceManager;

import java.util.Properties;

import org.apache.camel.component.properties.PropertiesComponent;
import org.apache.camel.guice.CamelModuleWithMatchingRoutes;
import org.apache.camel.guice.jndi.JndiBind;

import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.multibindings.MapBinder;
import com.google.inject.name.Names;
import twitter4j.Twitter;

/**
 * @author Enrico Candino ( enrico.candino@gmail.com )
 */
public class ResolverModule extends CamelModuleWithMatchingRoutes {

    @Override
    protected void configure() {
        super.configure();
        Properties redisProperties = PropertiesHelper.readFromClasspath("/redis.properties");
        Names.bindProperties(binder(), redisProperties);
        bindInstance("redisProperties", redisProperties);
        bind(JedisPoolFactory.class).to(DefaultJedisPoolFactory.class).asEagerSingleton();
        bind(TwitterFactoryWrapper.class).in(Singleton.class);
        bind(Twitter.class).toProvider(TwitterProvider.class);
        bind(FacebookFactory.class).to(FacebookProvider.class);

        Properties properties = PropertiesHelper.readFromClasspath("/beancounter.properties");

        AuthServiceConfig twitterService = DefaultAuthServiceManager.buildService("twitter", properties);
        AuthServiceConfig facebookService = DefaultAuthServiceManager.buildService("facebook", properties);
        bind(AuthServiceConfig.class)
                .annotatedWith(Names.named("service.twitter"))
                .toInstance(twitterService);
        bind(AuthServiceConfig.class)
                .annotatedWith(Names.named("service.facebook"))
                .toInstance(facebookService);

        MapBinder<AuthServiceConfig, AuthHandler> authHandlerBinder
                = MapBinder.newMapBinder(binder(), AuthServiceConfig.class, AuthHandler.class);
        authHandlerBinder.addBinding(twitterService).to(TwitterAuthHandler.class);
        authHandlerBinder.addBinding(facebookService).to(FacebookAuthHandler.class);

        bind(UserTokenManager.class).to(JedisUserTokenManager.class).asEagerSingleton();
        bind(UserManager.class).to(JedisUserManager.class).asEagerSingleton();
        bind(Resolver.class).to(JedisResolver.class);
        bind(ApplicationsManager.class).to(JedisApplicationsManagerImpl.class);
        bind(AuthServiceManager.class).to(DefaultAuthServiceManager.class);
        bind(RequestStateManager.class).to(JedisRequestStateManager.class);

        bind(ResolverRoute.class);
    }

    @Provides
    @JndiBind("properties")
    PropertiesComponent propertiesComponent() {
        PropertiesComponent pc = new PropertiesComponent();
        pc.setLocation("classpath:beancounter.properties");
        return pc;
    }


}