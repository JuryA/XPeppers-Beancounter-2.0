package io.beancounter.resolver;

import com.google.inject.name.Named;
import io.beancounter.commons.helper.jedis.JedisPoolFactory;
import io.beancounter.commons.model.User;
import io.beancounter.commons.model.activity.Activity;

import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.exceptions.JedisConnectionException;

import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * <i>Redis</i>-based implementation of {@link Resolver}.
 *
 * @author Enrico Candino ( enrico.candino@gmail.com )
 */
@Singleton
public class JedisResolver implements Resolver {

    private static final Logger LOGGER = LoggerFactory.getLogger(JedisResolver.class);

    private JedisPool pool;

    @Inject
    @Named("redis.db.resolver") private int resolverDb;

    @Inject
    public JedisResolver(JedisPoolFactory factory) {
        pool = factory.build();
    }

    @Override
    public UUID resolve(Activity activity) throws ResolverException {
        String service = activity.getContext().getService();
        String serviceUserId = activity.getContext().getUsername();
        String identifier = createIdentifier(activity.getApplicationName(), serviceUserId, service);
        Jedis jedis = getJedisResource();
        String userId;
        boolean isConnectionIssue = false;
        try {
            userId = jedis.hget(identifier, "uuid");
        } catch (JedisConnectionException e) {
            isConnectionIssue = true;
            final String errMsg = "Jedis Connection error while getting userId for [" + identifier
                    + "] for service [" + service + "]";
            LOGGER.error(errMsg, e);
            throw new ResolverException(errMsg, e);
        } catch (Exception e) {
            final String errMsg = "Error while getting userId for [" + identifier + "] for service [" + service + "]";
            LOGGER.error(errMsg, e);
            throw new ResolverException(errMsg, e);
        } finally {
            if(isConnectionIssue) {
                pool.returnBrokenResource(jedis);
            } else {
                pool.returnResource(jedis);
            }
        }
        if (userId == null) {
            final String errmsg = "User [" + identifier + "] not found for [" + service + "]";
            LOGGER.error(errmsg);
            throw new ResolverException(errmsg);
        }
        try {
            return UUID.fromString(userId);
        } catch (IllegalArgumentException e) {
            final String errmsg = "Illformed beancounter userId [" + userId +
                    "] for userIdentifier [" + identifier + "] in service [" + service + "]";
            LOGGER.error(errmsg, e);
            throw new ResolverException(errmsg, e);
        }
    }

    @Override
    public UUID resolveId(String identifier, String service) throws ResolverException {
        Jedis jedis = getJedisResource();
        String userId;
        boolean isConnectionIssue = false;
        try {
            userId = jedis.hget(identifier, "uuid");
        } catch (JedisConnectionException e) {
            isConnectionIssue = true;
            final String errMsg = "Jedis Connection error while getting userId for [" + identifier + "] for service ["
                    + service + "]";
            LOGGER.error(errMsg, e);
            throw new ResolverException(errMsg, e);
        } catch (Exception e) {
            final String errMsg = "Error while getting userId for [" + identifier + "] for service [" + service + "]";
            LOGGER.error(errMsg, e);
            throw new ResolverException(errMsg, e);
        } finally {
            if(isConnectionIssue) {
                pool.returnBrokenResource(jedis);
            } else {
                pool.returnResource(jedis);
            }
        }
        if (userId == null) {
            final String errmsg = "User [" + identifier + "] not found for [" + service + "]";
            LOGGER.error(errmsg);
            throw new ResolverMappingNotFoundException(errmsg);
        }
        try {
            return UUID.fromString(userId);
        } catch (IllegalArgumentException e) {
            final String errmsg = "Illformed beancounter userId [" + userId +
                    "] for userIdentifier [" + identifier + "] in service [" + service + "]";
            LOGGER.error(errmsg, e);
            throw new ResolverException(errmsg, e);
        }
    }

    @Override
    public String resolveUserId(String applicationName, String service, String serviceUserId) throws ResolverException {
        Jedis jedis = getJedisResource();
        String username;
        String identifier = createIdentifier(applicationName, serviceUserId, service);
        return resolveId(identifier, service).toString();
//        boolean isConnectionIssue = false;
//        try {
//        	LOGGER.debug("Getting: " + identifier + " field: username with service: " + service);
//            username = jedis.hget(identifier, "username");
//        } catch (JedisConnectionException e) {
//            isConnectionIssue = true;
//            final String errMsg = "Jedis Connectino error while getting username for [" + identifier + "] for service ["
//                    + service + "]";
//            LOGGER.error(errMsg, e);
//            throw new ResolverException(errMsg, e);
//        } catch (Exception e) {
//            final String errMsg = "Error while getting username for [" + identifier + "] for service [" + service + "]";
//            LOGGER.error(errMsg, e);
//            throw new ResolverException(errMsg, e);
//        } finally {
//            if(isConnectionIssue) {
//                pool.returnBrokenResource(jedis);
//            } else {
//                pool.returnResource(jedis);
//            }
//        }
//        if (username == null) {
//            final String errmsg = "User [" + identifier + "] not found for [" + service + "]";
//            LOGGER.error(errmsg);
//            throw new ResolverMappingNotFoundException(errmsg);
//        }
//        LOGGER.debug("Username found: " + username);
//        return username;
    }

    @Override
    public void store(String serviceUserId, String service, User user) throws ResolverException {
        if (user.getUsername() == null) {
            throw new IllegalArgumentException("username parameter cannot be null");
        }
        if (service == null) {
            throw new IllegalArgumentException("service parameter cannot be null");
        }

        String identifier = createIdentifier(user.getCustomer(), serviceUserId, service);

        Jedis jedis = getJedisResource();
        boolean isConnectionIssue = false;
        try {
            jedis.hset(identifier, "uuid", user.getId().toString());
            jedis.hset(identifier, "username", user.getUsername());
        } catch (JedisConnectionException e) {
            isConnectionIssue = true;
            final String errMsg = "Jedis Connection error while storing " + identifier;
            LOGGER.error(errMsg, e);
            throw new ResolverException(errMsg, e);
        } catch (Exception e) {
            final String errMsg = "Error while storing user " + identifier;
            LOGGER.error(errMsg, e);
            throw new ResolverException(errMsg, e);
        } finally {
            if(isConnectionIssue) {
                pool.returnBrokenResource(jedis);
            } else {
                pool.returnResource(jedis);
            }
        }
    }

    private String createIdentifier(String applicationName, String serviceUserId, String service) {
        return applicationName + ":" + service + ":" + serviceUserId;
    }

    /**
     * @deprecated Remove in the nearest future
     */
    @Deprecated
    @Override
    public List<String> getUserIdsFor(String serviceName, int start, int stop) throws ResolverException {
        Jedis jedis = getJedisResource();
        List<String> userIds;
        boolean isConnectionIssue = false;
        try {
            userIds = jedis.lrange(serviceName, start, stop);
        } catch (JedisConnectionException e) {
            isConnectionIssue = true;
            final String errMsg = "Jedis Connection error while retrieving userIds [from " + start + " to " + stop
                    + "] for service [" + serviceName + "]";
            LOGGER.error(errMsg, e);
            throw new ResolverException(errMsg, e);
        } catch (Exception e) {
            final String errMsg = "Error while retrieving userIds [from " + start + " to " + stop + "] for service ["
                    + serviceName + "]";
            LOGGER.error(errMsg, e);
            throw new ResolverException(errMsg, e);
        } finally {
            if(isConnectionIssue) {
                pool.returnBrokenResource(jedis);
            } else {
                pool.returnResource(jedis);
            }
        }
        return userIds;
    }

    private Jedis getJedisResource() throws ResolverException {
        Jedis jedis;
        try {
            jedis = pool.getResource();
        } catch (Exception e) {
            final String errMsg = "Error while getting a Jedis resource";
            LOGGER.error(errMsg, e);
            throw new ResolverException(errMsg, e);
        }
        boolean isConnectionIssue = false;
        try {
            jedis.select(resolverDb);
        } catch (JedisConnectionException e) {
            isConnectionIssue = true;
            final String errMsg = "Jedis Connection error while selecting database [" + resolverDb + "]";
            LOGGER.error(errMsg, e);
            throw new ResolverException(errMsg, e);
        } catch (Exception e) {
            pool.returnResource(jedis);
            final String errMsg = "Error while selecting database [" + resolverDb + "]";
            LOGGER.error(errMsg, e);
            throw new ResolverException(errMsg, e);
        } finally {
            if(isConnectionIssue) {
                pool.returnBrokenResource(jedis);
            }
        }
        return jedis;
    }

}