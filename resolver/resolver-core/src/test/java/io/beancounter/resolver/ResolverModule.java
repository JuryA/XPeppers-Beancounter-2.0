package io.beancounter.resolver;

import io.beancounter.commons.helper.PropertiesHelper;
import io.beancounter.commons.helper.jedis.DefaultJedisPoolFactory;
import io.beancounter.commons.helper.jedis.JedisPoolFactory;

import java.util.Properties;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;

/**
 * <i>Guice</i> module for {@link Resolver}.
 *
 * @author Davide Palmisano ( dpalmisano@gmail.com )
 */
public class ResolverModule extends AbstractModule {

    @Override
    protected void configure() {
        Properties redisProperties = PropertiesHelper.readFromClasspath("/redis.properties");
        Names.bindProperties(binder(), redisProperties);

        bind(JedisPoolFactory.class).to(DefaultJedisPoolFactory.class).asEagerSingleton();
        bind(Resolver.class).to(JedisResolver.class);
    }
}
