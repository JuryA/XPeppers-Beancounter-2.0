package io.beancounter.js;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.name.Names;
import com.google.inject.servlet.GuiceFilter;
import com.google.inject.servlet.GuiceServletContextListener;
import com.google.inject.servlet.ServletModule;
import com.sun.grizzly.http.servlet.ServletAdapter;
import io.beancounter.commons.model.Category;
import io.beancounter.commons.model.Interest;
import io.beancounter.commons.model.ShortUserProfile;
import io.beancounter.profiles.ProfileManager;
import io.beancounter.profiles.ProfileManagerException;
import io.beancounter.tests.servlet.ServletBasedTestCase;
import io.beancounter.usermanager.UserManagerException;
import io.beancounter.usermanager.UserTokenManager;
import org.apache.http.HttpResponse;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;
import org.mozilla.javascript.*;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.net.URI;
import java.util.*;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

/**
 * Tests for {@link GoogleAnalyticsJsServlet} service.
 */
public class GoogleAnalyticsJsServletTestCase extends ServletBasedTestCase {

    public static final String TOKEN_COOKIE_NAME = "BCTOKEN";

    private static UserTokenManager tokenManager;
    private static ProfileManager profileManager;

    @Override
    protected void setupFrontendService() {
        ServletAdapter ga = new ServletAdapter();
        ga.addContextParameter("service.host", getServiceHost());
        ga.addServletListener(ServiceConfig.class.getName());
        ga.addFilter(new GuiceFilter(), "filter", null);
        ga.setServletPath("/");
        server.addGrizzlyAdapter(ga, null);
    }

    @BeforeMethod
    public void resetMocks() {
        // Have to reset mocks because of their static nature
        reset(tokenManager, profileManager);
    }

    @Test
    public void contentType() throws IOException {
        HttpResponse resp = get("/");
        String ctype = resp.getEntity().getContentType().getValue();
        assertTrue(ctype.startsWith("application/javascript"), ctype);
    }

    @Test
    public void defaultDataLayer() throws IOException {
        HttpResponse resp = get(resolveUrl("/"));
        String js = EntityUtils.toString(resp.getEntity());

        ScriptableObject window = evaluateScript(js);
        NativeArray ary = verifyDataLayer("dataLayer", window);
        assertEquals(ary.getLength(), 1);

        Scriptable obj = (Scriptable) ary.get(0, ary);
        assertTrue(obj instanceof NativeObject, obj.getClassName());
        assertEquals(obj.get("bc.ready", obj), "true");
    }

    @Test
    public void customDataLayerVarName() throws IOException {
        final String varName = "data";
        HttpResponse resp = get(resolveUrl("/") + "?layerName=" + varName);
        String js = EntityUtils.toString(resp.getEntity());

        ScriptableObject window = evaluateScript(js);
        NativeArray ary = verifyDataLayer(varName, window);
        assertEquals(ary.getLength(), 1);

        Scriptable obj = (Scriptable) ary.get(0, ary);
        assertTrue(obj instanceof NativeObject, obj.getClassName());
        assertEquals(obj.get("bc.ready", obj), "true");
    }

    @Test
    public void withProfileData() throws Exception {
        UUID userId = UUID.randomUUID();
        UUID token = UUID.randomUUID();
        ShortUserProfile profile = new ShortUserProfile(userId);
        profile.setGender("female");
        profile.setLocation("Trento, Italy");
        profile.setAge(30);
        profile.addTopics(Arrays.asList(
                new Category(new URI("/cat0.1"), "Cat0.1", 0.1, 1),
                new Category(new URI("/cat0.5"), "Cat0.5", 0.5, 2),
                new Interest(new URI("/int0.9"), "Int0.9", 0.9, true),
                new Interest(new URI("/int0.2"), "Int0.2", 0.2, true)
        ));

        when(tokenManager.getUserId(token)).thenReturn(userId);
        when(profileManager.lookupShortProfile(userId)).thenReturn(profile);

        HttpGet req = new HttpGet(getServiceHost());
        req.addHeader("Cookie", TOKEN_COOKIE_NAME + '=' + token.toString());
        HttpResponse resp = makeRequest(req);
        String js = EntityUtils.toString(resp.getEntity());

        ScriptableObject window = evaluateScript(js);
        NativeArray ary = verifyDataLayer("dataLayer", window);

        Scriptable obj = (Scriptable) ary.get(0, ary);
        assertEquals(obj.get("bc.ready", obj), "true");
        assertEquals(obj.get("bc.profile.id", obj), userId.toString());
        assertEquals(obj.get("bc.profile.gender", obj), profile.getGender());
        assertEquals(obj.get("bc.profile.location", obj), profile.getLocation());
        assertEquals(obj.get("bc.profile.age", obj), profile.getAge());
        assertEquals(obj.get("bc.profile.topic0.label", obj), "Int0.9");
        assertEquals(obj.get("bc.profile.topic0.weight", obj), 0.9);
        assertEquals(obj.get("bc.profile.topic1.label", obj), "Cat0.5");
        assertEquals(obj.get("bc.profile.topic1.weight", obj), 0.5);
        assertEquals(obj.get("bc.profile.topic2.label", obj), "Int0.2");
        assertEquals(obj.get("bc.profile.topic2.weight", obj), 0.2);
    }

    @Test
    public void invalidToken() throws Exception {
        UUID userToken = UUID.randomUUID();
        when(tokenManager.getUserId(any(UUID.class))).thenReturn(null);

        HttpGet req = new HttpGet(getServiceHost());
        req.addHeader("Cookie", TOKEN_COOKIE_NAME + '=' + userToken);
        HttpResponse resp = makeRequest(req);
        String js = EntityUtils.toString(resp.getEntity());

        ScriptableObject window = evaluateScript(js);
        verifyDataLayer("dataLayer", window);

        // make sure mocks were setup correctly
        verify(tokenManager).getUserId(userToken);
    }

    @Test
    public void tokenManagerError() throws Exception {
        UUID userToken = UUID.randomUUID();
        Throwable error = new UserManagerException("fake token manager error");
        when(tokenManager.getUserId(any(UUID.class))).thenThrow(error);

        HttpGet req = new HttpGet(getServiceHost());
        req.addHeader("Cookie", TOKEN_COOKIE_NAME + '=' + userToken);
        HttpResponse resp = makeRequest(req);
        String js = EntityUtils.toString(resp.getEntity());

        ScriptableObject window = evaluateScript(js);
        verifyDataLayer("dataLayer", window);

        // make sure mocks were setup correctly
        verify(tokenManager).getUserId(userToken);
    }

    @Test
    public void profileManagerError() throws Exception {
        UUID userId =UUID.randomUUID();
        UUID userToken = UUID.randomUUID();
        Throwable error = new ProfileManagerException("fake profile manager error");

        when(tokenManager.getUserId(any(UUID.class))).thenReturn(userId);
        when(profileManager.lookupShortProfile(any(UUID.class))).thenThrow(error);

        HttpGet req = new HttpGet(getServiceHost());
        req.addHeader("Cookie", TOKEN_COOKIE_NAME + '=' + userToken);
        HttpResponse resp = makeRequest(req);
        String js = EntityUtils.toString(resp.getEntity());

        ScriptableObject window = evaluateScript(js);
        verifyDataLayer("dataLayer", window);

        // make sure mocks were setup correctly
        verify(tokenManager).getUserId(userToken);
        verify(profileManager).lookupShortProfile(userId);
    }


    /**
     * Asserts that a {@code varName} dataLayer var is present on provided
     * {@code window} object, and contains at least 1 element.
     * @param varName dataLayer variable name, typically {@code "dataLayer"}.
     * @param window Global window object
     * @return dataLayer array
     */
    private NativeArray verifyDataLayer(String varName, ScriptableObject window) {
        Scriptable dataLayer = (Scriptable) window.get(varName, window);
        assertTrue(dataLayer instanceof NativeArray, dataLayer.getClassName());

        NativeArray ary = (NativeArray) dataLayer;
        assertTrue(ary.getLength() > 0, "dataLayer array Should not be empty");

        return ary;
    }

    /**
     * Evaluates Javascript and returns {@code window} object from the global
     * scope.
     * @param js Script to evaluate
     * @return window From global scope
     */
    private ScriptableObject evaluateScript(String js) {
        Context ctx = Context.enter();
        Scriptable scope;
        ScriptableObject window = new NativeObject();
        try {
            ctx.setLanguageVersion(Context.VERSION_1_7);
            scope = ctx.initStandardObjects();
            scope.put("window", scope, window);
            ctx.evaluateString(scope, js, "bc.js", 1, null);
        } finally {
            Context.exit();
        }
        return window;
    }

    public static class ServiceConfig extends GuiceServletContextListener {
        @Override
        protected Injector getInjector() {
            return Guice.createInjector(new ServletModule() {
                @Override
                protected void configureServlets() {
                    tokenManager = mock(UserTokenManager.class);
                    profileManager = mock(ProfileManager.class);

                    Map<String, String> props = new HashMap<String, String>();
                    props.put("cookie.token.name", TOKEN_COOKIE_NAME);
                    Names.bindProperties(binder(), props);

                    bind(UserTokenManager.class).toInstance(tokenManager);
                    bind(ProfileManager.class).toInstance(profileManager);

                    serve("/*").with(GoogleAnalyticsJsServlet.class);
                }
            });
        }
    }
}