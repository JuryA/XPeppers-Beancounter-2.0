package io.beancounter.js;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import io.beancounter.commons.helper.jackson.ObjectMapperFactory;
import io.beancounter.commons.model.ShortUserProfile;
import io.beancounter.commons.model.Topic;
import io.beancounter.profiles.ProfileManager;
import io.beancounter.profiles.ProfileManagerException;
import io.beancounter.usermanager.UserManagerException;
import io.beancounter.usermanager.UserTokenManager;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

/**
 * Dynamic JS service generates a piece of javascript for web tracking.
 *
 * @author alex@cloudware.it
 */
@Singleton
public class GoogleAnalyticsJsServlet extends HttpServlet {

    @Inject
    @Named("cookie.token.name")
    private String tokenCookieName;

    @Inject
    private UserTokenManager tokenManager;

    @Inject
    private ProfileManager profileManager;

    /** JSON serializer */
    protected final ObjectMapper mapper = ObjectMapperFactory.createMapper();


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        resp.setContentType("application/javascript");
        resp.setCharacterEncoding("UTF-8");

        String layerName = req.getParameter("layerName");
        if (layerName == null || layerName.isEmpty())
            layerName = "dataLayer";

        UUID userToken = getUserToken(req.getCookies());
        ShortUserProfile shortProfile = getShortProfileFromToken(userToken);
        Map<String, Object> dataLayer = createDataLayer(shortProfile);
        String jsonData = mapper.writeValueAsString(dataLayer);

        String template = getTemplate("/js/bc.template.js");
        String js = String.format(template, layerName, jsonData);

        resp.getWriter().write(wrapJs(js));
    }

    protected UUID getUserToken(Cookie[] cookies) {
        if (cookies == null || cookies.length == 0)
            return null;

        Cookie cookie = null;
        for (Cookie c: cookies) {
            if (c.getName().equals(tokenCookieName)) {
                cookie = c;
                break;
            }
        }

        try {
            return cookie == null ? null : UUID.fromString(cookie.getValue());
        } catch (IllegalArgumentException e) {
            return null;
        }
    }

    protected ShortUserProfile getShortProfileFromToken(UUID token) {
        if (token == null)
            return null;

        UUID userId;
        try {
            userId = tokenManager.getUserId(token);
        } catch (UserManagerException e) {
            return null;
        }

        if (userId == null)
            return null;

        try {
            return profileManager.lookupShortProfile(userId);
        } catch (ProfileManagerException e) {
            return null;
        }
    }

    protected Map<String, Object> createDataLayer(ShortUserProfile profile) {
        Map<String, Object> data = new HashMap<>();
        data.put("bc.ready", "true");

        if (profile != null) {
            data.put("bc.profile.id", profile.getUserId().toString());
            data.put("bc.profile.gender", profile.getGender());
            data.put("bc.profile.location", profile.getLocation());
            data.put("bc.profile.age", profile.getAge());
            List<Topic> topics = new ArrayList<>(profile.getTopics().size());
            topics.addAll(profile.getTopics());
            Collections.sort(topics);
            for (int i=0; i < 3 && i < topics.size(); i++) {
                Topic t = topics.get(i);
                data.put("bc.profile.topic" + i + ".label", t.getLabel());
                data.put("bc.profile.topic" + i + ".weight", t.getWeight());
            }
        }

        return data;
    }

    protected String getTemplate(String path) throws IOException {
        StringBuilder content = new StringBuilder();
        InputStream in = getClass().getResourceAsStream(path);

        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        for (int c = reader.read(); c != -1; c = reader.read())
            content.append((char)c);

        return content.toString();
    }

    protected String wrapJs(String js) {
        return "(function(window){\n" + js + "\n})(window);";
    }
}
