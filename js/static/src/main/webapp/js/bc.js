/** @const */
var PARAM_TOKEN = 'token';
/** @const */
var STORE_TOKEN_KEY = '__bc_token';

/**
 * Parse "search" part of the location and store token in storage, if any.
 * @param {Storage} storage Typically localStorage.
 * @param {Location} location Window.location object.
 * @return {?string} Token from either provided location or storage, if any.
 */
function storeTokenFromLocation(storage, location) {
  if (!storage || !location)
    return;

  /** @type {!string} */
  var query = location.search.substring(1);
  /** @type {!Array.<string>} */
  var parts = query.split('&');
  /** @type {string} */
  var token = null;
  for (var i=0; i < parts.length; i++) {
    var pair = parts[i].split('=');
    if (pair[0] === PARAM_TOKEN) {
      token = decodeURIComponent(pair[1]);
    }
  }

  if (token) {
    // store token from window.location
    storage.setItem(STORE_TOKEN_KEY, token);
  } else {
    // get latest token from storage, if any
    token = storage.getItem(STORE_TOKEN_KEY);
  }

  return token;
}
