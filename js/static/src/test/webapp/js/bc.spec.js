var expect = chai.expect;

describe('Token', function(){

  var storage = {};

  before(function(){
    storage.items = {};
    storage.getItem = function(key) { return storage.items[key] };
    storage.setItem = function(key, val) { storage.items[key] = val };
  });

  it('should store and return token', function(){
    var token = 'abcdefg12345';
    var location = {search: '?token=' + token};

    var result = storeTokenFromLocation(storage, location);
    expect(storage.items[STORE_TOKEN_KEY]).to.equal(token);
    expect(result).to.equal(token);
  });

  it('should not overwrite existing token if not provided', function(){
    var token = 'existing';
    var location = {search: ''};
    storage.items[STORE_TOKEN_KEY] = token;

    var result = storeTokenFromLocation(storage, location);
    expect(storage.items[STORE_TOKEN_KEY]).to.equal(token);
    expect(result).to.equal(token);
  });

  it('should not error if storage is unavalable', function(){
    var location = {search: '?token=123'};

    var result = storeTokenFromLocation(undefined, location);
    expect(result).to.be.undefined;
  });
});
