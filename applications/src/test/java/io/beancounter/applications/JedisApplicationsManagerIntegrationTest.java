package io.beancounter.applications;

import io.beancounter.commons.model.Application;
import io.beancounter.commons.helper.jedis.JedisPoolFactory;

import java.net.MalformedURLException;
import java.util.UUID;

import junit.framework.Assert;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import redis.clients.jedis.JedisPool;

import com.google.inject.Guice;
import com.google.inject.Injector;

/**
 * Reference test case for {@link JedisApplicationsManagerImpl}.
 *
 * @author Davide Palmisano ( dpalmisano@gmail.com )
 */
public class JedisApplicationsManagerIntegrationTest {

    private ApplicationsManager applicationsManager;

    @BeforeClass
    public void setUp() {
        Injector injector = Guice.createInjector(new ApplicationsModule());
        applicationsManager = injector.getInstance(ApplicationsManager.class);
        JedisPool pool = injector.getInstance(JedisPoolFactory.class).build();
        pool.getResource().flushAll();
    }

    @Test
    public void testRegisterAndDeregisterApplication()
            throws MalformedURLException, ApplicationsManagerException {
        final String name = "test-app";
        final String description = "a test app";
        final String email = "t@test.com";
        Application app = applicationsManager.registerApplication(
                name,
                description,
                email);
        UUID key = app.getApiKey();
        Application actual = applicationsManager.get(name);
        Assert.assertNotNull(actual);
        Assert.assertEquals(actual.getApiKey(), key);
        Assert.assertEquals(actual.getName(), name);
        Assert.assertEquals(actual.getDescription(), description);

        Assert.assertTrue(applicationsManager.isAuthorized(name, key));

        applicationsManager.deregisterApplication(name);

        actual = applicationsManager.get(name);
        Assert.assertNull(actual);
    }
}