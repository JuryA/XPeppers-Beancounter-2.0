package io.beancounter.applications;

import io.beancounter.commons.model.Application;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.UUID;

/**
 * Mockup class for {@link ApplicationsManager}.
 *
 * @author Enrico Candino (enrico.candino@gmail.com)
 * @author Davide Palmisano (dpalmisano@gmail.com)
 */
public class MockApplicationsManager implements ApplicationsManager {

    private Set<Application> applications = new HashSet<Application>();

    @Override
    public Application registerApplication(String name, String description, String email)
            throws ApplicationsManagerException
    {
        Application application = new Application(name, description, email);
        applications.add(application);
        return application;
    }

    @Override
    public boolean deregisterApplication(String name)
            throws ApplicationsManagerException
    {
        int size = applications.size();
        int originalSize = size;
        Iterator it = applications.iterator();
        while(it.hasNext()) {
            Application app = (Application) it.next();
            if(app.getName().equals(name)) {
               it.remove();
                size--;
            }
        }
        return !(originalSize == size);
    }

    @Override
    public Application get(String name) throws ApplicationsManagerException {
        for(Application application : applications) {
            if(application.getName().equals(name)) {
                return application;
            }
        }
        throw new ApplicationsManagerException("application with name [" + name + "] not found");
    }

    @Override
    public void update(Application newApp) throws ApplicationsManagerException {
        Application app = get(newApp.getName());
        app.update(newApp);
    }

    @Override
    public boolean isAuthorized(String appName, UUID key)
            throws ApplicationsManagerException
    {
        Application application = get(appName);
        return application.getApiKey().equals(key);
    }
}