package io.beancounter.applications;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.beancounter.commons.model.Application;
import io.beancounter.commons.helper.jackson.ObjectMapperFactory;
import io.beancounter.commons.helper.jedis.JedisPoolFactory;

import java.io.IOException;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.exceptions.JedisConnectionException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * put class description here
 *
 * @author Davide Palmisano ( dpalmisano@gmail.com )
 */
public class JedisApplicationsManagerImpl implements ApplicationsManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(JedisApplicationsManagerImpl.class);
    private static final String CHANNEL_NAME = "appchanges";

    @Inject
    @Named("redis.db.applications") int database;

    private JedisPool pool;

    private ObjectMapper mapper;

    @Inject
    public JedisApplicationsManagerImpl(JedisPoolFactory factory) {
        pool = factory.build();
        mapper = ObjectMapperFactory.createMapper();
    }

    @Override
    public Application registerApplication(
            String name,
            String description,
            String email
    ) throws ApplicationsManagerException {
        Application application = new Application(name, description, email);
        String applicationJson;
        try {
            applicationJson = mapper.writeValueAsString(application);
        } catch (IOException e) {
            final String errMsg = "Error while getting json for app [" + name + "]";
            LOGGER.error(errMsg, e);
            throw new ApplicationsManagerException(
                    errMsg,
                    e
            );
        }
        Jedis jedis = getJedisResource();
        boolean isConnectionIssue = false;
        Long result;
        try {
            result = jedis.setnx(name, applicationJson);
        } catch (JedisConnectionException e) {
            isConnectionIssue = true;
            final String errMsg = "Jedis Connection error while storing application [" + applicationJson +
                    "] with name [" + name + "]";
            LOGGER.error(errMsg, e);
            throw new ApplicationsManagerException(errMsg, e);
        } catch (Exception e) {
            final String errMsg = "Error while storing application [" + applicationJson +
                    "] with name [" + name + "]";
            LOGGER.error(errMsg, e);
            throw new ApplicationsManagerException(errMsg, e);
        } finally {
            if(isConnectionIssue) {
                pool.returnBrokenResource(jedis);
            } else {
                pool.returnResource(jedis);
            }
        }

        if (result == 0)
            throw new ApplicationsManagerException("App [" + name + "] already exists");

        return application;
    }

    @Override
    public boolean deregisterApplication(String name) throws ApplicationsManagerException {
        Jedis jedis = getJedisResource();
        boolean isConnectionIssue = false;
        long result;
        try {
            result = jedis.del(name);
        } catch (JedisConnectionException e) {
            isConnectionIssue = true;
            final String errMsg = "Jedis Connection error while deleting application with name [" + name + "]";
            LOGGER.error(errMsg, e);
            throw new ApplicationsManagerException(errMsg, e);
        } catch (Exception e) {
            final String errMsg = "Error while deleting application with name [" + name + "]";
            LOGGER.error(errMsg, e);
            throw new ApplicationsManagerException(errMsg, e);
        } finally {
            if(isConnectionIssue) {
                pool.returnBrokenResource(jedis);
            } else {
                pool.returnResource(jedis);
            }
        }
        return !(result == 0);
    }

    @Override
    public Application get(String name) throws ApplicationsManagerException {
        Jedis jedis = getJedisResource();
        String applicationJson;
        boolean isConnectionIssue = false;
        try {
            applicationJson = jedis.get(name);
        } catch (JedisConnectionException e) {
            isConnectionIssue = true;
            final String errMsg = "Jedis Connection error while retrieving application with name [" + name + "]";
            LOGGER.error(errMsg, e);
            throw new ApplicationsManagerException(errMsg, e);
        } catch (Exception e) {
            final String errMsg = "Error while retrieving application with name [" + name + "]";
            LOGGER.error(errMsg, e);
            throw new ApplicationsManagerException(errMsg, e);
        } finally {
            if(isConnectionIssue) {
                pool.returnBrokenResource(jedis);
            } else {
                pool.returnResource(jedis);
            }
        }

        if (applicationJson == null) {
            return null;
        }

        try {
            return mapper.readValue(applicationJson, Application.class);
        } catch (IOException e) {
            final String errMsg = "Error while getting json for app with name [" + name + "]";
            LOGGER.error(errMsg, e);
            throw new ApplicationsManagerException(errMsg, e);
        }
    }

    @Override
    public void update(Application app) throws ApplicationsManagerException {
        String appName = app.getName();
        String appJson;
        try {
            appJson = mapper.writeValueAsString(app);
        } catch (JsonProcessingException e) {
            LOGGER.error("Serialization error", e);
            throw new ApplicationsManagerException(e.getMessage());
        }

        Jedis jedis = getJedisResource();
        boolean isConnectionIssue = false;
        try {
            jedis.set(appName, appJson);
            jedis.publish(CHANNEL_NAME, appName);
        } catch (JedisConnectionException e) {
            isConnectionIssue = true;
            final String errMsg = "Jedis Connection error while updating application with name [" + appName + "]";
            LOGGER.error(errMsg, e);
            throw new ApplicationsManagerException(errMsg, e);
        } catch (Exception e) {
            final String errMsg = "Error while updating application with name [" + appName + "]";
            LOGGER.error(errMsg, e);
            throw new ApplicationsManagerException(errMsg, e);
        } finally {
            if(isConnectionIssue) {
                pool.returnBrokenResource(jedis);
            } else {
                pool.returnResource(jedis);
            }
        }
    }

    @Override
    public boolean isAuthorized(String name, UUID key)
            throws ApplicationsManagerException {
        Application app = get(name);
        return app != null && app.getApiKey().equals(key);
    }

    private Jedis getJedisResource() throws ApplicationsManagerException {
        Jedis jedis;
        try {
            jedis = pool.getResource();
        } catch (Exception e) {
            final String errMsg = "Error while getting a Jedis resource";
            LOGGER.error(errMsg, e);
            throw new ApplicationsManagerException(errMsg, e);
        }
        boolean isConnectionIssue = false;
        try {
            jedis.select(database);
        } catch (JedisConnectionException e) {
            isConnectionIssue = true;
            final String errMsg = "Jedis Connection error while selecting database [" + database + "]";
            LOGGER.error(errMsg, e);
            throw new ApplicationsManagerException(errMsg, e);
        } catch (Exception e) {
            pool.returnResource(jedis);
            final String errMsg = "Error while selecting database [" + database + "]";
            LOGGER.error(errMsg, e);
            throw new ApplicationsManagerException(errMsg, e);
        } finally {
            if(isConnectionIssue) {
                pool.returnBrokenResource(jedis);
            }
        }
        return jedis;
    }

}
