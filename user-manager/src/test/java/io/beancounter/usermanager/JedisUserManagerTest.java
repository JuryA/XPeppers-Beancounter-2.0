package io.beancounter.usermanager;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

import io.beancounter.auth.AuthServiceManagerException;
import io.beancounter.commons.helper.jackson.ObjectMapperFactory;
import io.beancounter.commons.helper.jedis.JedisPoolFactory;
import io.beancounter.commons.model.AuthServiceConfig;
import io.beancounter.commons.model.auth.OAuthToken;
import io.beancounter.commons.model.User;
import io.beancounter.auth.AuthHandler;
import io.beancounter.auth.AuthHandlerException;
import io.beancounter.commons.model.auth.AtomicSignUp;
import io.beancounter.commons.model.auth.ServiceUser;
import io.beancounter.resolver.Resolver;
import io.beancounter.resolver.ResolverMappingNotFoundException;
import io.beancounter.commons.model.auth.RequestState;
import io.beancounter.auth.AuthServiceManager;

import java.net.URL;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.exceptions.JedisConnectionException;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JedisUserManagerTest {

    private UserManager userManager;
    private Jedis jedis;
    private JedisPool jedisPool;
    private Resolver resolver;
    private AuthServiceManager authManager;
    private UserTokenManager tokenManager;
    private ObjectMapper mapper;

    @BeforeMethod
    public void setUp() throws Exception {
        jedis = mock(Jedis.class);
        jedisPool = mock(JedisPool.class);
        JedisPoolFactory jedisPoolFactory = mock(JedisPoolFactory.class);
        when(jedisPoolFactory.build()).thenReturn(jedisPool);
        when(jedisPool.getResource()).thenReturn(jedis);

        resolver = mock(Resolver.class);
        authManager = mock(AuthServiceManager.class);
        tokenManager = mock(UserTokenManager.class);

        userManager = new JedisUserManager(jedisPoolFactory, resolver, authManager, tokenManager);
        mapper = ObjectMapperFactory.createMapper();
    }

    // Test: [delete]

    @Test
    public void deletingUserShouldAlsoDeleteTheirUserToken() throws Exception {
        String username = "username";
        UUID userToken = UUID.randomUUID();
        User user = new User("Test", "User", username, "password");
        user.setUserToken(userToken);

        userManager.deleteUser(user);

        verify(jedis).del(username);
        verify(tokenManager).deleteUserToken(userToken);
        verify(jedisPool, times(1)).returnResource(jedis);
    }

    @Test
    public void deletingUserWithNoUserTokenShouldJustDeleteTheUser() throws Exception {
        String username = "username";
        User user = new User("Test", "User", username, "password");

        userManager.deleteUser(user);

        verify(jedis).del(username);
        verify(jedisPool, times(1)).returnResource(jedis);
    }
    
    @Test
    public void getUsersByCustomerShouldRetriveAListOfCustomer() throws UserManagerException{
        String customerName = "anyCustomer";
        String defaultJSONUser = "{}";
        Set<String> usernamesFromJedis = new HashSet<String>(){{
            add("123123");
            add("456456");
        }};
        when(jedis.smembers("customer:anyCustomer")).thenReturn(usernamesFromJedis);
        when(jedis.get("123123")).thenReturn(defaultJSONUser);
        when(jedis.get("456456")).thenReturn(defaultJSONUser);

        Collection<User> userCollection = userManager.getUsersByCustomer(customerName);
        assertEquals(userCollection.size(), 2);
    }

    @Test(expectedExceptions = UserManagerException.class)
    public void givenErrorOccursWhenDeletingUserTokenDuringUserDeletionThenThrowException() throws Exception {
        String username = "username";
        UUID userToken = UUID.randomUUID();
        User user = new User("Test", "User", username, "password");
        user.setUserToken(userToken);

        when(tokenManager.deleteUserToken(userToken)).thenThrow(new UserManagerException("error"));

        userManager.deleteUser(user);
    }

    @Test
    public void givenJedisResourceErrorOccursWhenDeletingUserThenThrowException() throws Exception {
        User user = new User("Test", "User", "username", "password");

        when(jedisPool.getResource())
                .thenThrow(new JedisConnectionException("Could not get a resource from the pool"));

        try {
            userManager.deleteUser(user);
        } catch (UserManagerException ume) {
            assertEquals(ume.getMessage(), "Error while getting a Jedis resource");
        }

        verify(jedisPool, never()).returnResource(jedis);
        verify(jedisPool, never()).returnBrokenResource(jedis);
    }

    @Test
    public void givenJedisConnectionProblemWhenSelectingDatabaseToDeleteUserThenThrowException() throws Exception {
        User user = new User("Test", "User", "username", "password");

        when(jedis.select(anyInt())).thenThrow(new JedisConnectionException("error"));

        try {
            userManager.deleteUser(user);
        } catch (UserManagerException ume) {
            assertTrue(ume.getMessage().startsWith("Jedis Connection error while selecting database"));
        }

        verify(jedisPool).returnBrokenResource(jedis);
    }

    @Test
    public void givenSomeOtherProblemWhenSelectingDatabaseToDeleteUserThenThrowException() throws Exception {
        User user = new User("Test", "User", "username", "password");

        when(jedis.select(anyInt())).thenThrow(new RuntimeException("error"));

        try {
            userManager.deleteUser(user);
        } catch (UserManagerException ume) {
            assertTrue(ume.getMessage().startsWith("Error while selecting database"));
        }

        verify(jedisPool).returnResource(jedis);
    }

    @Test
    public void givenJedisConnectionProblemWhenDeletingUserThenThrowException() throws Exception {
        String username = "username";
        User user = new User("Test", "User", username, "password");

        when(jedis.del(username)).thenThrow(new JedisConnectionException("error"));

        try {
            userManager.deleteUser(user);
        } catch (UserManagerException ume) {
            assertEquals(ume.getMessage(), "Jedis Connection error while deleting user [" + username + "]");
        }

        verify(jedisPool).returnBrokenResource(jedis);
    }

    @Test
    public void givenSomeOtherProblemWhenDeletingUserThenThrowException() throws Exception {
        String username = "username";
        User user = new User("Test", "User", username, "password");

        when(jedis.del(username)).thenThrow(new RuntimeException("error"));

        try {
            userManager.deleteUser(user);
        } catch (UserManagerException ume) {
            assertEquals(ume.getMessage(), "Error while deleting user [" + username + "]");
        }

        verify(jedisPool).returnResource(jedis);
    }

    // Test: [oauth]

    @Test(expectedExceptions = UserManagerException.class)
    public void checkingIfAnUnsupportedServiceIsSupportedShouldThrowAnException() throws Exception {
        String serviceName = "not-supported";
        when(authManager.getService(serviceName)).thenReturn(null);

        ((JedisUserManager) userManager).checkServiceIsSupported(serviceName);
    }

    @Test(expectedExceptions = UserManagerException.class)
    public void encounteringAnErrorWhileCheckingIfAServiceIsSupportedShouldThrowAnException() throws Exception {
        String serviceName = "some-service";
        when(authManager.getService(serviceName))
                .thenThrow(new AuthServiceManagerException("error"));

        ((JedisUserManager) userManager).checkServiceIsSupported(serviceName);
    }

    @Test
    public void checkingIfASupportedServiceIsSupportedShouldCompleteWithoutError() throws Exception {
        String serviceName = "supported-service";
        when(authManager.getService(serviceName)).thenReturn(new AuthServiceConfig(serviceName));

        ((JedisUserManager) userManager).checkServiceIsSupported(serviceName);
    }

    @Test(expectedExceptions = UserManagerException.class)
    public void gettingAuthHandlerForUnsupportedServiceShouldThrowAnException() throws Exception {
        String serviceName = "not-supported";
        when(authManager.getService(serviceName)).thenReturn(null);

        ((JedisUserManager) userManager).getAuthHandlerForService(serviceName);
    }

    @Test(expectedExceptions = UserManagerException.class)
    public void encounteringAnErrorWhileCheckingIfAServiceIsSupportedWhenGettingAnAuthHandlerShouldThrowAnException() throws Exception {
        String serviceName = "some-service";
        when(authManager.getService(serviceName))
                .thenThrow(new AuthServiceManagerException("error"));

        ((JedisUserManager) userManager).getAuthHandlerForService(serviceName);
    }

    @Test(expectedExceptions = UserManagerException.class)
    public void whenAnAuthHandlerCannotBeRetrievedThenThrowAnException() throws Exception {
        String serviceName = "supported-service";
        when(authManager.getService(serviceName)).thenReturn(new AuthServiceConfig(serviceName));
        when(authManager.getHandler(serviceName)).thenThrow(new AuthServiceManagerException("error"));

        ((JedisUserManager) userManager).getAuthHandlerForService(serviceName);
    }

    @Test
    public void gettingAuthHandlerForSupportedServiceShouldReturnTheCorrectAuthHandler() throws Exception {
        String serviceName = "supported-service";
        AuthHandler authHandler = mock(AuthHandler.class);

        when(authManager.getService(serviceName)).thenReturn(new AuthServiceConfig(serviceName));
        when(authManager.getHandler(serviceName)).thenReturn(authHandler);
        when(authHandler.getService()).thenReturn(serviceName);

        AuthHandler handler = ((JedisUserManager) userManager).getAuthHandlerForService(serviceName);
        assertEquals(handler.getService(), serviceName);
    }

    @Test(expectedExceptions = UserManagerException.class)
    public void whenAnAnonymousOAuthTokenCannotBeRetrievedThenThrowAnException() throws Exception {
        String serviceName = "supported-service";
        AuthHandler authHandler = mock(AuthHandler.class);

        when(authManager.getService(serviceName)).thenReturn(new AuthServiceConfig(serviceName));
        when(authManager.getHandler(serviceName)).thenReturn(authHandler);
        when(authHandler.getTokenWithCustomer(null)).thenThrow(new AuthHandlerException("error"));

        userManager.getOAuthToken(serviceName);
    }

    @Test
    public void anAnonymousOAuthTokenShouldBeRetrievedForASupportedService() throws Exception {
        String serviceName = "supported-service";
        String authRedirectUrl = "http://my.service.com/oauth/token-12345";
        AuthHandler authHandler = mock(AuthHandler.class);

        when(authManager.getService(serviceName)).thenReturn(new AuthServiceConfig(serviceName));
        when(authManager.getHandler(serviceName)).thenReturn(authHandler);
        when(authHandler.getTokenWithCustomer(null)).thenReturn(new OAuthToken(new URL(authRedirectUrl)));

        OAuthToken token = userManager.getOAuthToken(serviceName);
        assertEquals(token.getRedirectPage().toString(), authRedirectUrl);
    }

    @Test(expectedExceptions = UserManagerException.class)
    public void gettingAnonymousOAuthTokenWithCustomFinalRedirectOnSystemThatDoesNotSupportUTF8ThrowsException() throws Exception {
        String serviceName = "supported-service";
        URL finalRedirectUrl = new URL("http://final.redirect.com");
        AuthHandler authHandler = mock(AuthHandler.class);

        when(authManager.getService(serviceName)).thenReturn(new AuthServiceConfig(serviceName));
        when(authManager.getHandler(serviceName)).thenReturn(authHandler);
        when(authHandler.getTokenWithCustomer(finalRedirectUrl, null)).thenThrow(new AuthHandlerException("No UTF-8"));

        userManager.getOAuthToken(serviceName, finalRedirectUrl);
    }

    @Test
    public void anAnonymousOAuthTokenShouldBeRetrievedWhenAValidFinalRedirectUrlIsSpecified() throws Exception {
        String serviceName = "supported-service";
        String authRedirectUrl = "http://my.service.com/oauth/token-12345";
        URL finalRedirectUrl = new URL("http://final.redirect.com");
        AuthHandler authHandler = mock(AuthHandler.class);

        when(authManager.getService(serviceName)).thenReturn(new AuthServiceConfig(serviceName));
        when(authManager.getHandler(serviceName)).thenReturn(authHandler);
        when(authHandler.getTokenWithCustomer(finalRedirectUrl, null)).thenReturn(new OAuthToken(new URL(authRedirectUrl)));

        OAuthToken token = userManager.getOAuthToken(serviceName, finalRedirectUrl);
        assertEquals(token.getRedirectPage().toString(), authRedirectUrl);
    }

    @Test
    public void gettingOAuthTokenForUserShouldBeSuccessful() throws Exception {
        String serviceName = "supported-service";
        String username = "test-user";
        String authRedirectUrl = "http://my.service.com/oauth/token-12345";
        AuthHandler authHandler = mock(AuthHandler.class);

        when(authManager.getService(serviceName)).thenReturn(new AuthServiceConfig(serviceName));
        when(authManager.getHandler(serviceName)).thenReturn(authHandler);
        when(authHandler.getToken(username)).thenReturn(new OAuthToken(new URL(authRedirectUrl)));

        OAuthToken token = userManager.getOAuthToken(serviceName, username);
        assertEquals(token.getRedirectPage().toString(), authRedirectUrl);
    }

    @Test
    public void gettingOAuthTokenForUserWithCustomCallbackShouldBeSuccessful() throws Exception {
        String serviceName = "supported-service";
        String username = "test-user";
        URL callback = new URL("http://example.com/callback/" + username);
        String authRedirectUrl = "http://my.service.com/oauth/token-12345";
        AuthHandler authHandler = mock(AuthHandler.class);

        when(authManager.getService(serviceName)).thenReturn(new AuthServiceConfig(serviceName));
        when(authManager.getHandler(serviceName)).thenReturn(authHandler);
        when(authHandler.getToken(username, callback)).thenReturn(new OAuthToken(new URL(authRedirectUrl)));

        OAuthToken token = userManager.getOAuthToken(serviceName, username, callback);
        assertEquals(token.getRedirectPage().toString(), authRedirectUrl);
    }

    @Test
    public void getOAuthTokenWithState() throws Exception {
        URL redirect = new URL("http://test/redirect");
        String sid = "state-12345";
        String cust = "xpeppers";
        String srv = "supported-service";
        AuthServiceConfig service = new AuthServiceConfig(srv);
        AuthHandler authHandler = mock(AuthHandler.class);

        when(authHandler.getTokenWithState(anyString(), anyString())).
            thenReturn(new OAuthToken(redirect, sid));
        when(authManager.getService(srv)).thenReturn(service);
        when(authManager.getHandler(srv)).thenReturn(authHandler);

        OAuthToken token = userManager.getOAuthTokenWithState(sid, srv, cust);
        assertNotNull(token);
        assertEquals(token.getStateId(), sid);
        assertEquals(token.getRedirectPage(), redirect);
        verify(authHandler).getTokenWithState(sid, cust);
    }

    @Test(expectedExceptions = UserManagerException.class)
    public void getOAuthTokenWithStateShouldThrowUserManagerException()
        throws Exception
    {
        String sid = "state-12345";
        String cust = "xpeppers";
        String srv = "supported-service";
        AuthServiceConfig service = new AuthServiceConfig(srv);
        AuthHandler authHandler = mock(AuthHandler.class);

        when(authHandler.getTokenWithState(sid, cust)).
            thenThrow(new AuthHandlerException("fake error"));
        when(authManager.getService(srv)).thenReturn(service);
        when(authManager.getHandler(srv)).thenReturn(authHandler);

        userManager.getOAuthTokenWithState(sid, srv, cust);
    }

    @Test(expectedExceptions = UserManagerException.class)
    public void getOAuthTokenWithStateShouldCheckForSupportedService()
        throws Exception
    {
        when(authManager.getService("supported")).
            thenReturn(new AuthServiceConfig("supported"));

        userManager.getOAuthTokenWithState("12345", "unsupported", "xpeppers");
    }

    @Test
    public void storeNewUserFromOAuthWithState() throws Exception {
        String verifier = "code-12345";
        String srvName = "srv";
        String srvUserId = "srv-user-12345";
        String username = "dude";
        UUID userToken = UUID.randomUUID();

        AuthServiceConfig srv = new AuthServiceConfig(srvName);
        srv.setOAuth2Callback("http://example.org/callback/{customer}");

        User user = new User("John", "Doe", username, "secret");
        ServiceUser srvUser = new ServiceUser(srvUserId, user);

        RequestState state = new RequestState(
                "chumhum", null, null, RequestState.Flow.MOBILE);

        AuthHandler handler = mock(AuthHandler.class);
        when(authManager.getService(anyString())).thenReturn(srv);
        when(authManager.getHandler(anyString())).thenReturn(handler);
        when(handler.getService()).thenReturn(srvName);
        when(handler.auth2WithCustomer(anyString(), anyString(), anyString())).
                thenReturn(srvUser);
        when(resolver.resolveUserId(anyString(), anyString(), anyString()))
                .thenThrow(new ResolverMappingNotFoundException("not found"));
        when(tokenManager.createUserToken(any(UUID.class))).thenReturn(userToken);

        AtomicSignUp signUp = userManager.storeUserFromOAuthWithState(
                srvName, null, verifier, state);

        assertNotNull(signUp);
        assertEquals(signUp.getUsername(), username);
        assertEquals(signUp.getUserId(), user.getId());
        assertEquals(signUp.getService(), srvName);
        assertEquals(signUp.getIdentifier(), srvUserId);
        assertEquals(signUp.getUserToken(), userToken);
        assertFalse(signUp.isReturning());

        verify(authManager).getService(srvName);
        verify(authManager).getHandler(srvName);
        verify(handler).auth2WithCustomer(null, verifier, state.getCustomer());
        verify(resolver).resolveUserId("chumhum", srvName, srvUserId);
        verify(tokenManager).createUserToken(user.getId());
    }

    @Test
    public void registeringTwitterOAuthServiceToUserWithNoUserTokenShouldBeSuccessful() throws Exception {
        String serviceName = "twiter";
        String username = "username";
        String serviceUserId = "17473832";
        String token = "oauth_token";
        String verifier = "oauth_verifier";
        UUID userToken = UUID.randomUUID();

        AuthHandler authHandler = mock(AuthHandler.class);
        User user = new User("Test", "User", username, "password");
        ServiceUser srvUser = new ServiceUser(serviceUserId, user);

        when(authManager.getService(serviceName)).thenReturn(new AuthServiceConfig(serviceName));
        when(authManager.getHandler(serviceName)).thenReturn(authHandler);
        when(authHandler.auth(user, token, verifier)).thenReturn(srvUser);
        when(tokenManager.createUserToken(user.getId())).thenReturn(userToken);

        userManager.registerOAuthService(serviceName, user, token, verifier);
        user.setUserToken(userToken);

        verify(authHandler).auth(user, token, verifier);
        verify(tokenManager, times(2)).createUserToken(user.getId());
        verify(resolver).store(serviceUserId, serviceName, user);
        verify(jedis, times(2)).select(0);
        verify(jedis, times(2)).set(user.getId().toString(), mapper.writeValueAsString(user));
        verify(jedisPool, times(2)).getResource();
        verify(jedisPool, times(2)).returnResource(jedis);
    }

    @Test
    public void registeringTwitterOAuthServiceToUserWithExistingUserTokenShouldBeSuccessful() throws Exception {
        String serviceName = "twiter";
        String username = "username";
        String serviceUserId = "17473832";
        String token = "oauth_token";
        String verifier = "oauth_verifier";
        UUID oldUserToken = UUID.randomUUID();
        UUID newUserToken = UUID.randomUUID();

        AuthHandler authHandler = mock(AuthHandler.class);
        User user = new User("Test", "User", username, "password");
        user.setUserToken(oldUserToken);
        ServiceUser srvUser = new ServiceUser(serviceUserId, user);

        when(authManager.getService(serviceName)).thenReturn(new AuthServiceConfig(serviceName));
        when(authManager.getHandler(serviceName)).thenReturn(authHandler);
        when(authHandler.auth(user, token, verifier)).thenReturn(srvUser);
        when(tokenManager.createUserToken(user.getId())).thenReturn(newUserToken);

        userManager.registerOAuthService(serviceName, user, token, verifier);
        user.setUserToken(newUserToken);

        verify(authHandler).auth(user, token, verifier);
        verify(tokenManager).deleteUserToken(oldUserToken);
        verify(tokenManager, times(2)).createUserToken(user.getId());
        verify(resolver).store(serviceUserId, serviceName, user);
        verify(jedis, times(2)).select(0);
        verify(jedis, times(2)).set(user.getId().toString(), mapper.writeValueAsString(user));
        verify(jedisPool, times(2)).getResource();
        verify(jedisPool, times(2)).returnResource(jedis);
    }

    @Test
    public void registeringFacebookOAuthServiceToUserWithNoUserTokenShouldBeSuccessful() throws Exception {
        String serviceName = "facebook";
        String username = "username";
        String serviceUserId = "17473832";
        String token = null;
        String code = "oauth2_code";
        UUID userToken = UUID.randomUUID();

        AuthHandler authHandler = mock(AuthHandler.class);
        User user = new User("Test", "User", username, "password");
        ServiceUser srvUser = new ServiceUser(serviceUserId, user);

        when(authManager.getService(serviceName)).thenReturn(new AuthServiceConfig(serviceName));
        when(authManager.getHandler(serviceName)).thenReturn(authHandler);
        when(authHandler.auth(user, token, code)).thenReturn(srvUser);
        when(tokenManager.createUserToken(user.getId())).thenReturn(userToken);

        userManager.registerOAuthService(serviceName, user, token, code);
        user.setUserToken(userToken);

        verify(authHandler).auth(user, token, code);
        verify(tokenManager, times(2)).createUserToken(user.getId());
        verify(resolver).store(serviceUserId, serviceName, user);
        verify(jedis, times(2)).select(0);
        verify(jedis, times(2)).set(user.getId().toString(), mapper.writeValueAsString(user));
        verify(jedisPool, times(2)).getResource();
        verify(jedisPool, times(2)).returnResource(jedis);
    }

    @Test
    public void registeringFacebookOAuthServiceToUserWithExistingUserTokenShouldBeSuccessful() throws Exception {
        String serviceName = "facebook";
        String username = "username";
        String serviceUserId = "17473832";
        String token = null;
        String code = "oauth2_code";
        UUID oldUserToken = UUID.randomUUID();
        UUID newUserToken = UUID.randomUUID();

        AuthHandler authHandler = mock(AuthHandler.class);
        User user = new User("Test", "User", username, "password");
        user.setUserToken(oldUserToken);
        ServiceUser srvUser = new ServiceUser(serviceUserId, user);

        when(authManager.getService(serviceName)).thenReturn(new AuthServiceConfig(serviceName));
        when(authManager.getHandler(serviceName)).thenReturn(authHandler);
        when(authHandler.auth(user, token, code)).thenReturn(srvUser);
        when(tokenManager.createUserToken(user.getId())).thenReturn(newUserToken);

        userManager.registerOAuthService(serviceName, user, token, code);
        user.setUserToken(newUserToken);

        verify(authHandler).auth(user, token, code);
        verify(tokenManager).deleteUserToken(oldUserToken);
        verify(tokenManager, times(2)).createUserToken(user.getId());
        verify(resolver).store(serviceUserId, serviceName, user);
        verify(jedis, times(2)).select(0);
        verify(jedis, times(2)).set(user.getId().toString(), mapper.writeValueAsString(user));
        verify(jedisPool, times(2)).getResource();
        verify(jedisPool, times(2)).returnResource(jedis);
    }
}
