package io.beancounter.usermanager;

import io.beancounter.commons.helper.PropertiesHelper;
import io.beancounter.commons.helper.jedis.DefaultJedisPoolFactory;
import io.beancounter.commons.helper.jedis.JedisPoolFactory;
import io.beancounter.commons.model.AuthServiceConfig;
import io.beancounter.auth.AuthHandler;
import io.beancounter.resolver.JedisResolver;
import io.beancounter.resolver.Resolver;
import io.beancounter.auth.DefaultAuthServiceManager;
import io.beancounter.auth.AuthServiceManager;
import io.beancounter.auth.FacebookAuthHandler;
import io.beancounter.auth.TwitterAuthHandler;
import io.beancounter.auth.TwitterFactoryWrapper;

import java.util.Properties;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.google.inject.multibindings.MapBinder;
import com.google.inject.name.Names;
import twitter4j.TwitterFactory;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

/**
 * put class description here
 *
 * @author Davide Palmisano ( dpalmisano@gmail.com )
 */
public class UserManagerModule extends AbstractModule {

    @Override
    protected void configure() {
        Properties properties = PropertiesHelper.readFromClasspath("/beancounter.properties");
        Properties redisProperties = PropertiesHelper.readFromClasspath("/redis.properties");
        Names.bindProperties(binder(), redisProperties);

        AuthServiceConfig twitterService = DefaultAuthServiceManager.buildService("twitter", properties);
        AuthServiceConfig facebookService = DefaultAuthServiceManager.buildService("facebook", properties);
        bind(AuthServiceConfig.class)
                .annotatedWith(Names.named("service.twitter"))
                .toInstance(twitterService);
        bind(AuthServiceConfig.class)
                .annotatedWith(Names.named("service.facebook"))
                .toInstance(facebookService);

        bind(JedisPoolFactory.class).to(DefaultJedisPoolFactory.class).in(Singleton.class);

        Configuration conf = new ConfigurationBuilder().setUseSSL(true).build();
        TwitterFactory factory = new TwitterFactory(conf);
        bind(TwitterFactoryWrapper.class).toInstance(new TwitterFactoryWrapper(factory));

        bind(Resolver.class).to(JedisResolver.class);
        bind(UserTokenManager.class).to(JedisUserTokenManager.class);
        bind(UserManager.class).to(JedisUserManager.class);

        MapBinder<AuthServiceConfig, AuthHandler> authHandlerBinder
                = MapBinder.newMapBinder(binder(), AuthServiceConfig.class, AuthHandler.class);
        authHandlerBinder.addBinding(twitterService).to(TwitterAuthHandler.class);
        authHandlerBinder.addBinding(facebookService).to(FacebookAuthHandler.class);

        bind(AuthServiceManager.class).to(DefaultAuthServiceManager.class);
    }
}
