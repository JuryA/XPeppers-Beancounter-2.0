package io.beancounter.usermanager.services.auth;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.beancounter.auth.JedisRequestStateManager;
import io.beancounter.commons.model.auth.RequestState;
import io.beancounter.auth.RequestStateManager;
import io.beancounter.commons.helper.jackson.ObjectMapperFactory;
import io.beancounter.commons.helper.jedis.JedisPoolFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import redis.clients.jedis.*;
import redis.clients.jedis.exceptions.JedisConnectionException;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;

import static org.mockito.Mockito.*;
import static org.testng.Assert.*;


/**
 * Tests for {@link io.beancounter.auth.JedisRequestStateManager}
 *
 * @author alex@cloudware.it
 */
public class JedisRequestStateManagerTest {

    private ObjectMapper mapper;

    private Jedis jedis;
    private JedisPool pool;

    private RequestStateManager stateMgr;


    @BeforeMethod
    public void setUp() {
        mapper = ObjectMapperFactory.createMapper();

        jedis = mock(Jedis.class);
        pool = mock(JedisPool.class);
        JedisPoolFactory jedisPoolFactory = mock(JedisPoolFactory.class);

        when(jedisPoolFactory.build()).thenReturn(pool);
        when(pool.getResource()).thenReturn(jedis);

        stateMgr = new JedisRequestStateManager(jedisPoolFactory);
    }

    @Test
    public void put() throws Exception {
        RequestState state = new RequestState(
            "chumhum", null, null, RequestState.Flow.MOBILE);
        String json = mapper.writeValueAsString(state);

        when(jedis.setnx(anyString(), anyString())).thenReturn(1L);

        String stateId = stateMgr.put(state);

        assertNotNull(stateId);
        assertTrue(stateId.length() > 0);

        verify(jedis).setnx(stateId, json);
        verify(jedis).expire(stateId, RequestStateManager.DEFAULT_EXPIRE);
        verify(pool).returnResource(jedis);
    }

    @Test
    public void putWithRetries() throws Exception {
        RequestState state = new RequestState(
                "chumhum", null, null, RequestState.Flow.MOBILE);
        String json = mapper.writeValueAsString(state);

        // 1st retry will succeed
        when(jedis.setnx(anyString(), anyString())).thenReturn(0L, 1L);

        String stateId = stateMgr.put(state);

        assertNotNull(stateId);
        assertTrue(stateId.length() > 0);

        verify(jedis).select(anyInt());
        verify(jedis, times(2)).setnx(anyString(), eq(json));
        verify(jedis).expire(stateId, RequestStateManager.DEFAULT_EXPIRE);
        verifyNoMoreInteractions(jedis);
        verify(pool).returnResource(jedis);
    }

    @Test
    public void putTooManyRetries() throws Exception {
        RequestState state = new RequestState(
                "chumhum", null, null, RequestState.Flow.MOBILE);

        // should never succeed
        when(jedis.setnx(anyString(), anyString())).
                thenReturn(0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L);

        String stateId = stateMgr.put(state);
        assertNull(stateId);
        verify(pool).returnResource(jedis);
    }

//    @Test
//    @Ignore("Mockito is not able to mock jedis.multi() after upgrading jedis.jar to version 2.4.1")
//    public void pop() throws Exception {
//        String sid = "state-12345";
//
//        RequestState state = new RequestState(
//            "chumhum", null, new URI("/redirect"), RequestState.Flow.WEB);
//        final String json = mapper.writeValueAsString(state);
//
//        Response<String> resp = new Response<String>(new Builder<String>() {
//            @Override
//            public String build(Object data) {
//                return json;
//            }
//        });
//        resp.set(json);
//
//        Transaction txn = mock(Transaction.class);
//        when(jedis.multi()).thenReturn(txn);
//        when(txn.get(anyString())).thenReturn(resp);
//
//        assertEquals(stateMgr.pop(sid), state);
//
//        verify(jedis).multi();
//        verify(txn).get(sid);
//        verify(txn).del(sid);
//        verify(txn).exec();
//        verifyNoMoreInteractions(txn);
//        verify(pool).returnResource(jedis);
//    }

    @Test
    public void getJedisConnectionExceptionPop() {
        when(jedis.select(anyInt())).
            thenThrow(new JedisConnectionException("fake issue"));

        assertNull(stateMgr.pop("any"));
        verify(pool).returnBrokenResource(jedis);
    }

    @Test
    public void getJedisConnectionExceptionPut() {
        when(jedis.select(anyInt())).
                thenThrow(new JedisConnectionException("fake issue"));

        assertNull(stateMgr.put(new RequestState()));
        verify(pool).returnBrokenResource(jedis);
    }

    @Test
    public void getJedisAnyErrorPop() {
        when(jedis.select(anyInt())).
            thenThrow(new RuntimeException("generic error"));

        assertNull(stateMgr.pop("any"));
        verify(pool).returnResource(jedis);
    }

    @Test
    public void getJedisAnyErrorPut() {
        when(jedis.select(anyInt())).
                thenThrow(new RuntimeException("generic error"));

        assertNull(stateMgr.pop("any"));
        verify(pool).returnResource(jedis);
    }
}
