package io.beancounter.usermanager.grabber;

import io.beancounter.commons.http.HttpClientProvider;
import io.beancounter.commons.model.User;
import io.beancounter.commons.model.activity.Activity;
import io.beancounter.commons.model.activity.Context;
import io.beancounter.commons.model.activity.Object;
import io.beancounter.commons.model.activity.ResolvedActivity;
import io.beancounter.commons.model.activity.Verb;
import io.beancounter.commons.model.auth.Auth;
import io.beancounter.listener.facebook.core.FacebookUtils;
import io.beancounter.listener.facebook.core.converter.custom.Converter;
import io.beancounter.listener.facebook.core.converter.custom.ConverterException;
import io.beancounter.listener.facebook.core.converter.custom.FacebookLikeConverter;
import io.beancounter.listener.facebook.core.converter.custom.FacebookShareConverter;
import io.beancounter.listener.facebook.core.model.FacebookData;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableMap;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.types.FacebookType;
import com.restfb.types.Post;

/**
 * @author Alex Cowell
 */
public final class FacebookGrabber implements ActivityGrabber {

    private static final Logger LOG = LoggerFactory.getLogger(FacebookGrabber.class);

    public static final String SHARES = "shares";
    public static final String LIKES = "likes";

    private final User user;
    private final String serviceUserId;
    private final FacebookClient client;
    private final ImmutableMap<String, Integer> limits;
    private final Map<Verb, ResolvedActivity> latestActivities;

    public static FacebookGrabber create(User user, String serviceUserId, Map<Verb, ResolvedActivity> latestActivities) {
        Auth facebookAuth = user.getAuth("facebook");
        if (facebookAuth == null) {
            throw new IllegalArgumentException("User [" + user.getUsername() + "] does not have Facebook authentication");
        }

        // TODO: Load custom settings from bc.prop
        ImmutableMap<String, Integer> limits = ImmutableMap.of(
                SHARES, 10,
                LIKES, 5
        );
        DefaultFacebookClient facebookClient = new DefaultFacebookClient(facebookAuth.getSession());
        return new FacebookGrabber(user, serviceUserId, facebookClient, limits, latestActivities);
    }

    FacebookGrabber(User user, String serviceUserId, FacebookClient client, ImmutableMap<String, Integer> limits, Map<Verb, ResolvedActivity> latestActivities) {
        this.user = user;
        this.serviceUserId = serviceUserId;
        this.client = client;
        this.limits = ImmutableMap.copyOf(limits);
        this.latestActivities = latestActivities;
    }

    @Override
    public List<ResolvedActivity> grab() {
        List<ResolvedActivity> activities = new ArrayList<ResolvedActivity>();
        
        grabAndAddActivities(
                Post.class,
                Verb.SHARE,
                new FacebookShareConverter(),
                client,
                activities,
                "feed",
                SHARES,
                getLastActivityDateByVerb(Verb.SHARE));

        grabAndAddActivities(
                FacebookData.class,
                Verb.LIKE,
                new FacebookLikeConverter(new HttpClientProvider()),
                client,
                activities,
                "likes",
                LIKES,
                getLastActivityDateByVerb(Verb.LIKE));

        return activities;
    }

	private DateTime getLastActivityDateByVerb(Verb verb) {
		if (latestActivities == null)
			return null;
		DateTime lastShare = null;
        ResolvedActivity activity = latestActivities.get(verb);
        if (activity != null) {
        	lastShare = activity.getActivity().getContext().getDate();
        }
        return lastShare;
	}

    private boolean hasValidLimit(String facebookType) {
        return limits.containsKey(facebookType) && limits.get(facebookType) > 0;
    }

    private <T extends FacebookType, M extends Object> void grabAndAddActivities(
            Class<T> type,
            Verb verb,
            Converter<T, M> converter,
            FacebookClient client,
            List<ResolvedActivity> activities,
            String field,
            String fieldType,
            DateTime lastActivityDate
    ) {
        if (!hasValidLimit(fieldType)) return;

        Collection<T> posts;
        try {
            posts = FacebookUtils.fetch(type, client, field, limits.get(fieldType), lastActivityDate);
        } catch (Exception ex) {
            // TODO (med): What is the desired behaviour here?
            LOG.error("Error grabbing activities from Facebook for user [{}]", serviceUserId, ex);
            return;
        }
       
        for (T post : posts) {        	
            Activity activity;
            try {
                Object object = converter.convert(post, true);
                Context context = converter.getContext(post, serviceUserId);
                activity = new Activity(verb, object, context);
                LOG.debug("Verb: " + verb + " lastActivityDate: " + lastActivityDate + " createdActivity: " + activity);
            } catch (ConverterException cex) {
                LOG.warn("Could not convert Facebook {} from user [{}]", verb, serviceUserId);
                continue;
            }
            if (lastActivityDate == null || activity.getContext().getDate().isAfter(lastActivityDate.getMillis())){
            	LOG.info("Grabbing new: " + verb);
            	activities.add(new ResolvedActivity(user.getId(), activity, user));
            }
        }
    }
}
