package io.beancounter.usermanager;

import io.beancounter.auth.AuthServiceManagerException;
import io.beancounter.commons.helper.jackson.ObjectMapperFactory;
import io.beancounter.commons.helper.jedis.JedisPoolFactory;
import io.beancounter.commons.model.auth.OAuthToken;
import io.beancounter.commons.model.User;
import io.beancounter.commons.model.activity.Activity;
import io.beancounter.auth.AuthHandler;
import io.beancounter.auth.AuthHandlerException;
import io.beancounter.commons.model.auth.AtomicSignUp;
import io.beancounter.commons.model.auth.ServiceUser;
import io.beancounter.commons.model.auth.OAuthAuth;
import io.beancounter.resolver.Resolver;
import io.beancounter.resolver.ResolverException;
import io.beancounter.resolver.ResolverMappingNotFoundException;
import io.beancounter.commons.model.auth.RequestState;
import io.beancounter.auth.AuthServiceManager;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.exceptions.JedisConnectionException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * <i>REDIS</i>-based implementation of {@link UserManager}.
 *
 * @author Davide Palmisano ( dpalmisano@gmail.com )
 */
public class JedisUserManager implements UserManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(JedisUserManager.class);

    private JedisPool pool;

    private ObjectMapper mapper;

    private Resolver resolver;

    private AuthServiceManager sam;

    private UserTokenManager tokenManager;

    @Inject
    @Named("redis.db.users")
    private int database;

    @Inject
    public JedisUserManager(
            JedisPoolFactory factory,
            Resolver resolver,
            AuthServiceManager sam,
            UserTokenManager tokenManager) {
        pool = factory.build();
        mapper = ObjectMapperFactory.createMapper();
        this.resolver = resolver;
        this.sam = sam;
        this.tokenManager = tokenManager;
    }

    @Override
    public synchronized void storeUser(User user) throws UserManagerException {
        String userJson;
        try {
            userJson = mapper.writeValueAsString(user);
        } catch (IOException e) {
            final String errMsg = "Error while getting json for user [" + user.getId() + "]";
            LOGGER.error(errMsg, e);
            throw new UserManagerException(
                    errMsg,
                    e
            );
        }
        Jedis jedis = getJedisResource();
        boolean isConnectionIssue = false;
        try {
            jedis.set(user.getId().toString(), userJson);
            addUserToCustomerSet(user, jedis);
        } catch (JedisConnectionException e) {
            isConnectionIssue = true;
            final String errMsg = "Jedis Connection error while storing user [" + user.getId() + "]";
            LOGGER.error(errMsg, e);
            throw new UserManagerException(errMsg, e);
        } catch (Exception e) {
            final String errMsg = "Error while storing user [" + user.getId() + "]";
            LOGGER.error(errMsg, e);
            throw new UserManagerException(errMsg, e);
        } finally {
            if (isConnectionIssue) {
                pool.returnBrokenResource(jedis);
            } else {
                pool.returnResource(jedis);
            }
        }
    }

    private void addUserToCustomerSet(User user, Jedis jedis) {
        if(user.getCustomer() != null)
            jedis.sadd("customer:" + user.getCustomer(), user.getId().toString());
    }

    @Override
    public User getUser(String userId) throws UserManagerException {
        Jedis jedis = getJedisResource();
        String userJson;
        boolean isConnectionIssue = false;
        try {
            userJson = jedis.get(userId);
        } catch (JedisConnectionException e) {
            isConnectionIssue = true;
            final String errMsg = "Jedis Connection error while retrieving user [" + userId + "]";
            LOGGER.error(errMsg, e);
            throw new UserManagerException(errMsg, e);
        } catch (Exception e) {
            final String errMsg = "Error while retrieving user [" + userId + "]";
            LOGGER.error(errMsg, e);
            throw new UserManagerException(errMsg, e);
        } finally {
            if (isConnectionIssue) {
                pool.returnBrokenResource(jedis);
            } else {
                pool.returnResource(jedis);
            }
        }
        if (userJson == null) {
            return null;
        }
        try {
            return mapper.readValue(userJson, User.class);
        } catch (IOException e) {
            final String errMsg = "Error while getting json for user with username [" + userId + "]";
            LOGGER.error(errMsg, e);
            throw new UserManagerException(
                    errMsg,
                    e
            );
        }
    }

    @Override
    public Collection<User> getUsersByCustomer(String customerName) throws UserManagerException {
        Jedis jedis = getJedisResource();
        Set<String> usernames = new HashSet<String>();
        Collection<User> users = new ArrayList<User>();
        boolean isConnectionIssue = false;
        try {
            usernames = jedis.smembers("customer:" + customerName);
            for(String username : usernames)
                users.add(getUser(username));
        } catch (JedisConnectionException e) {
            isConnectionIssue = true;
            final String errMsg = "Jedis Connection error while retrieving user";
            LOGGER.error(errMsg, e);
            throw new UserManagerException(errMsg, e);
        } catch (Exception e) {
            final String errMsg = "Error while retrieving user";
            LOGGER.error(errMsg, e);
            throw new UserManagerException(errMsg, e);
        } finally {
            if (isConnectionIssue) {
                pool.returnBrokenResource(jedis);
            } else {
                pool.returnResource(jedis);
            }
        }
        return users;
    }

    @Override
    public synchronized void deleteUser(User user) throws UserManagerException {
        tokenManager.deleteUserToken(user.getUserToken());

        String username = user.getUsername();
        Jedis jedis = getJedisResource();
        boolean isConnectionIssue = false;
        try {
            jedis.del(username);
        } catch (JedisConnectionException e) {
            isConnectionIssue = true;
            final String errMsg = "Jedis Connection error while deleting user [" + username + "]";
            LOGGER.error(errMsg, e);
            throw new UserManagerException(errMsg, e);
        } catch (Exception e) {
            final String errMsg = "Error while deleting user [" + username + "]";
            LOGGER.error(errMsg, e);
            throw new UserManagerException(errMsg, e);
        } finally {
            if (isConnectionIssue) {
                pool.returnBrokenResource(jedis);
            } else {
                pool.returnResource(jedis);
            }
        }
    }

    @Override
    public OAuthToken getOAuthToken(
            String serviceName,
            String username
    ) throws UserManagerException {
        AuthHandler authHandler = getAuthHandlerForService(serviceName);

        try {
            return authHandler.getToken(username);
        } catch (AuthHandlerException e) {
            final String errMsg = "Error while getting OAuth token for service '" + serviceName + "'";
            LOGGER.error(errMsg, e);
            throw new UserManagerException(errMsg, e);
        }
    }

    @Override
    public OAuthToken getOAuthToken(String serviceName) throws UserManagerException {
        return getOAuthTokenWithCustomer(serviceName, null);
    }

    @Override
    public OAuthToken getOAuthTokenWithCustomer(String serviceName, String customer) throws UserManagerException {
        AuthHandler authHandler = getAuthHandlerForService(serviceName);
        try {
            return authHandler.getTokenWithCustomer(customer);
        } catch (AuthHandlerException e) {
            final String errMsg = "Error while getting OAuth token for service '" + serviceName + "'";
            LOGGER.error(errMsg, e);
            throw new UserManagerException(errMsg, e);
        }
    }

    @Override
    public OAuthToken getOAuthToken(String serviceName, URL finalRedirectUrl)
            throws UserManagerException {
        return getOAuthTokenWithCustomer(serviceName, finalRedirectUrl, null);
    }

    @Override
    public OAuthToken getOAuthTokenWithCustomer(String service, URL finalRedirectUrl, String customer) throws UserManagerException {
        AuthHandler authHandler = getAuthHandlerForService(service);
        try {
            return authHandler.getTokenWithCustomer(finalRedirectUrl, customer);
        } catch (AuthHandlerException e) {
            final String errMsg = "Error while getting OAuth token for service '" + service + "'";
            LOGGER.error(errMsg, e);
            throw new UserManagerException(errMsg, e);
        }
    }

    @Override
    public OAuthToken getOAuthToken(String serviceName, String username, URL callback)
            throws UserManagerException {
        AuthHandler authHandler = getAuthHandlerForService(serviceName);

        try {
            return authHandler.getToken(username, callback);
        } catch (AuthHandlerException e) {
            final String errMsg = "Error while getting OAuth token for service '" + serviceName + "'";
            LOGGER.error(errMsg, e);
            throw new UserManagerException(errMsg, e);
        }
    }

    /**
     * Alternative version of {@link #getOAuthTokenWithCustomer} with additional
     * info about the original user request.
     *
     * @param stateId A state that refers to the original user request.
     * @param service Third party identity provider, e.g. "facebook'.
     * @param customer A merchant name, e.g. "xpeppers".
     * @return A token with the originally provided {@code stateId}.
     * @throws UserManagerException
     */
    @Override
    public OAuthToken getOAuthTokenWithState(String stateId,
                                             String service,
                                             String customer)
    throws UserManagerException {
        AuthHandler authHandler = getAuthHandlerForService(service);
        try {
            return authHandler.getTokenWithState(stateId, customer);
        } catch (AuthHandlerException e) {
            throw new UserManagerException(
                "Error while getting OAuth token for service '" + service + "'",
                e);
        }
    }

    /**
     * Counterpart of {@link #getOAuthTokenWithState}:
     *
     * verifies user credentials and creates a new user or fetches the
     * existing one.
     *
     * Supports both OAuth 1.0a and OAuth 2.0.
     *
     * @param service Third party service name, e.g. "facebook".
     * @param token OAuth 1.0a token param. Should be null for OAuth 2.0.
     * @param verifier Either verifier (1.0a) or code (2.0) param.
     * @param state The original auth request state.
     *
     * @return Already persisted user info on success.
     * @throws UserManagerException Mostly when third party errors off.
     */
    @Override
    public AtomicSignUp storeUserFromOAuthWithState(String service,
                                                    String token,
                                                    String verifier,
                                                    RequestState state)
            throws UserManagerException {
        LOGGER.debug(String.format("Finalizing auth with %s, " +
                "token: %s, " +
                "verifier: %s, " +
                "state: %s", service, token, verifier, state));

        AuthHandler handler = getAuthHandlerForService(service);
        ServiceUser srvUser;

        String applicationName = state.getCustomer();
        try {
            srvUser = handler.auth2WithCustomer(
                    token,
                    verifier,
                    applicationName);
        } catch (AuthHandlerException e) {
            final String msg = "Error authenticating with " + service;
            LOGGER.error(msg, e);
            throw new UserManagerException(msg, e);
        }

        LOGGER.debug("AtomicSignUp ServiceUser: " + srvUser);

        return signUpServiceUser(handler, srvUser, applicationName);
    }

    @Override
    @Deprecated
    public List<Activity> grabUserActivities(
            User user,
            String identifier,
            String serviceName,
            int limit
    ) throws UserManagerException {
        AuthHandler authHandler = getAuthHandlerForService(serviceName);

        OAuthAuth auth = (OAuthAuth) user.getAuth(serviceName);
        if (auth == null) {
            final String errMsg = "it seems there is no auth for service [" + serviceName + "] on user [" + user.getUsername() + "]";
            LOGGER.error(errMsg);
            throw new UserManagerException(errMsg);
        }
        if (auth.isExpired()) {
            final String errMsg = "OAuth token for [" + serviceName + "] on user [" + user.getUsername() + "] is expired";
            LOGGER.error(errMsg);
            throw new UserManagerException(errMsg);
        }
        try {
            return authHandler.grabActivities(user.getCustomer(), auth, identifier, limit);
        } catch (AuthHandlerException e) {
            final String errMsg = "OAuth token for [" + serviceName + "] on user [" + user.getUsername() + "] is expired";
            LOGGER.error(errMsg);
            throw new UserManagerException(errMsg);
        }
    }

    @Override
    public synchronized void registerService(
            String serviceName,
            User user,
            String token
    ) throws UserManagerException {
        checkServiceIsSupported(serviceName);

        ServiceUser serviceUser;
        try {
            serviceUser = sam.getHandler(serviceName).auth(
                    user,
                    token,
                    null
            );
        } catch (AuthHandlerException e) {
            final String errMsg = "Error while getting auth manager for service '" + serviceName + "'";
            LOGGER.error(errMsg, e);
            throw new UserManagerException(errMsg, e);
        } catch (AuthServiceManagerException e) {
            final String errMsg = "Error while authenticating user '" + user.getUsername() + "' to service '" + serviceName + "'";
            LOGGER.error(errMsg, e);
            throw new UserManagerException(errMsg, e);
        }
        storeUser(serviceUser.getUser());
    }

    @Override
    public synchronized ServiceUser registerOAuthService(
            String serviceName,
            User user,
            String token,
            String verifier
    ) throws UserManagerException {
        AuthHandler authHandler = getAuthHandlerForService(serviceName);

        // now that the user grant the permission, we should ask for its username
        ServiceUser srvUser;
        try {
            srvUser = authHandler.auth(
                    user,
                    token,
                    verifier
            );
        } catch (AuthHandlerException e) {
            final String errMsg = "Error while getting auth manager for service '" + serviceName + "'";
            LOGGER.error(errMsg, e);
            throw new UserManagerException(errMsg, e);
        }

        user = srvUser.getUser();
        try {
            resolver.store(srvUser.getId(), serviceName, user);
        } catch (ResolverException e) {
            final String errMsg =
                    "Error while storing username for user [" + user.getId() +
                    "] on service [" + serviceName + "]";
            LOGGER.error(errMsg, e);
            throw new UserManagerException(errMsg, e);
        }

        tokenManager.deleteUserToken(user.getUserToken());
        UUID userToken = tokenManager.createUserToken(user.getId());
        user.setUserToken(userToken);
        updateUserWithOAuthCredentials(user, serviceName, srvUser);
        storeUser(user);

        return new ServiceUser(srvUser.getId(), user);
    }

    @Override
    public synchronized void deregisterService(
            String service,
            User user) throws UserManagerException {
        user.removeService(service);
        storeUser(user);
    }

    @Override
    public synchronized void voidOAuthToken(User user, String service) throws UserManagerException {
        OAuthAuth auth = (OAuthAuth) user.getAuth(service);
        if (auth == null) {
            throw new UserManagerException("it seems there is no auth for service [" + service + "] on user [" + user.getUsername() + "]");
        }
        auth.setExpired(true);
        user.removeService(service);
        user.addService(service, auth);
        this.storeUser(user);
    }

    void checkServiceIsSupported(String serviceName) throws UserManagerException {
        try {
            if (sam.getService(serviceName) == null) {
                final String errMsg = "AuthService '" + serviceName + "' is not supported";
                LOGGER.error(errMsg);
                throw new UserManagerException(errMsg);
            }
        } catch (AuthServiceManagerException e) {
            final String errMsg = "Error while getting service '" + serviceName + "'";
            LOGGER.error(errMsg, e);
            throw new UserManagerException(errMsg, e);
        }
    }

    AuthHandler getAuthHandlerForService(String serviceName) throws UserManagerException {
        checkServiceIsSupported(serviceName);
        AuthHandler authHandler;
        try {
            authHandler = sam.getHandler(serviceName);
        } catch (AuthServiceManagerException e) {
            final String errMsg = "Error while getting AuthHandler for service [" + serviceName + "]";
            LOGGER.error(errMsg, e);
            throw new UserManagerException(errMsg, e);
        }
        return authHandler;
    }

    private AtomicSignUp signUpServiceUser(AuthHandler handler, ServiceUser srvUser, String applicationName)
            throws UserManagerException
    {
        String candidateUsername;
        String service = handler.getService();
        try {
            // Check if the user already exists
            candidateUsername = resolver.resolveUserId(
                    applicationName,
                    service,
                    srvUser.getId()
            );
            LOGGER.info("candidate username: " + candidateUsername +
                        " for service user ID: " + srvUser.getId());
        } catch (ResolverMappingNotFoundException e) {
            return nonExistentUser(handler, srvUser);
        } catch (ResolverException e) {
            final String errMsg =
                    "Error while asking mapping for user [" + srvUser.getUser().getUsername() +
                    "] with identifier [" + srvUser.getId() +
                    "] on service [" + service + "]";
            LOGGER.error(errMsg, e);
            throw new UserManagerException(errMsg, e);
        }

        // check if the user is really in the usermanager db
        User user = getUser(candidateUsername);
        if (user == null) {
            // this code is executed to be sure that if in the resolver we have
            // a mapping but there is no a user object in the usermanager db (due to some corrupted data)
            // then the sign is guaranteed to be completed
            return nonExistentUser(handler, srvUser);
        }
        user = updateUserWithOAuthCredentials(user, service, srvUser);
        LOGGER.debug("Already existing user: " + user);
        UUID userToken = user.getUserToken();
        return new AtomicSignUp(
                user.getId(),
                user.getUsername(),
                true,
                service,
                srvUser.getId(),
                userToken);
    }

    private AtomicSignUp nonExistentUser(AuthHandler handler, ServiceUser srvUser)
            throws UserManagerException {
        // ok, this is the first access from this user so:
        // 1. Create a new user token for them (no need to check if there
        //    is an existing one because this is a new user).
        // 2. Add a record to the resolver
        String service = handler.getService();
        User user = srvUser.getUser();
        LOGGER.debug("Create new User with username: " + user.getUsername());
        UUID userToken = tokenManager.createUserToken(user.getId());
        user.setUserToken(userToken);
        mapUserToServiceInResolver(service, srvUser);
        storeUser(user);
        handler.notifySignup(user.getCustomer());
        return new AtomicSignUp(
                user.getId(),
                user.getUsername(),
                false,
                service,
                srvUser.getId(),
                userToken
        );
    }

    private void mapUserToServiceInResolver(
            String service,
            ServiceUser srvUser
    ) throws UserManagerException {
        try {
            resolver.store(srvUser.getId(), service, srvUser.getUser());
        } catch (ResolverException e) {
            final String errMsg =
                    "Error while storing mapping for service user [" +
                    srvUser.getUser().getUsername() +
                    "] with identifier [" + srvUser.getId() +
                    "] on service [" + service + "]";
            LOGGER.error(errMsg, e);
            throw new UserManagerException(errMsg, e);
        }
    }

    public User updateUserWithOAuthCredentials(
            User existingUser,
            String service,
            ServiceUser srvUser
    ) throws UserManagerException {
        User userFromSrv = srvUser.getUser();
        existingUser.setMetadata(userFromSrv.getMetadata());
        LOGGER.info("Updated metadata for existing user [" + existingUser.getMetadata() + "]");
        existingUser.setCustomer(userFromSrv.getCustomer());
        existingUser.addService(service, userFromSrv.getAuth(service));

        LOGGER.debug("Delete existing UserToken: " + existingUser.getUserToken());
        tokenManager.deleteUserToken(existingUser.getUserToken());

        UUID userToken = tokenManager.createUserToken(existingUser.getId());
        existingUser.setUserToken(userToken);
        LOGGER.debug("Update User with userToken: " + userToken);
        storeUser(existingUser);
        return existingUser;
    }

    private Jedis getJedisResource() throws UserManagerException {
        Jedis jedis;
        try {
            jedis = pool.getResource();
        } catch (Exception e) {
            final String errMsg = "Error while getting a Jedis resource";
            LOGGER.error(errMsg, e);
            throw new UserManagerException(errMsg, e);
        }
        boolean isConnectionIssue = false;
        try {
            jedis.select(database);
        } catch (JedisConnectionException e) {
            isConnectionIssue = true;
            final String errMsg = "Jedis Connection error while selecting database [" + database + "]";
            LOGGER.error(errMsg, e);
            throw new UserManagerException(errMsg, e);
        } catch (Exception e) {
            pool.returnResource(jedis);
            final String errMsg = "Error while selecting database [" + database + "]";
            LOGGER.error(errMsg, e);
            throw new UserManagerException(errMsg, e);
        } finally {
            if (isConnectionIssue) {
                pool.returnBrokenResource(jedis);
            }
        }
        return jedis;
    }

    @Override
    public void setUserEmail(User user, String email) throws UserManagerException {
        user.setEmail(email);
        storeUser(user);
    }

}
